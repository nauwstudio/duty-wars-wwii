package com.nauwstudio.dutywars_ww2.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.nauwstudio.dutywars_ww2.AndroidLauncher;
import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.WebUtils;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Robot;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "database0.db";

    public static final String PARAM_KEY = "key";
    public static final String PARAM_VALUE = "value";
    public static final String PARAM_TABLE = "param";
    public static final String PARAM_TABLE_CREATE =
            "CREATE TABLE " + PARAM_TABLE + " (" +
                    PARAM_KEY + " TEXT, " +
                    PARAM_VALUE + " TEXT); ";
    public static final String PARAM_TABLE_DROP = "DROP TABLE IF EXISTS " + PARAM_TABLE + ";";

    public static final String MAP_ID = "id";
    public static final String MAP_NAME = "name";
    public static final String MAP_FILE = "file";
    public static final String MAP_TYPE = "type";
    public static final String MAP_LOCK = "lock";
    public static final String MAP_MISSION_ID = "mission_id";
    public static final String MAP_TABLE = "map";
    public static final String MAP_TABLE_CREATE =
            "CREATE TABLE " + MAP_TABLE + " (" +
                    MAP_ID + " INTEGER PRIMARY KEY, " +
                    MAP_NAME + " TEXT, " +
                    MAP_FILE + " TEXT, " +
                    MAP_TYPE + " INTEGER, " +
                    MAP_LOCK + " INTEGER NOT NULL CHECK (" + MAP_LOCK + " IN (0,1)), " +
                    MAP_MISSION_ID + " INTEGER); ";
    public static final String MAP_TABLE_DROP = "DROP TABLE IF EXISTS " + MAP_TABLE + ";";

    public static final String MISSION_ID = "id";
    public static final String MISSION_NAME = "name";
    public static final String MISSION_MAP_ID = "map_id";
    public static final String MISSION_DESC = "desc";
    public static final String MISSION_LOCATION = "location";
    public static final String MISSION_ARMIES = "armies";
    public static final String MISSION_SCORE = "score";
    public static final String MISSION_POSX = "x";
    public static final String MISSION_POSY = "y";
    public static final String MISSION_LOCK = "lock";
    public static final String MISSION_TABLE = "mission";
    public static final String MISSION_TABLE_CREATE =
            "CREATE TABLE " + MISSION_TABLE + " (" +
                    MISSION_ID + " INTEGER PRIMARY KEY, " +
                    MISSION_NAME + " TEXT, " +
                    MISSION_MAP_ID + " INTEGER, " +
                    MISSION_DESC + " TEXT, " +
                    MISSION_LOCATION + " TEXT, " +
                    MISSION_ARMIES + " TEXT, " +
                    MISSION_SCORE + " INTEGER, " +
                    MISSION_POSX + " INTEGER, " +
                    MISSION_POSY + " INTEGER, " +
                    MISSION_LOCK + " INTEGER NOT NULL CHECK (" + MISSION_LOCK + " IN (0,1))," +
                    "FOREIGN KEY (" + MISSION_MAP_ID + ") REFERENCES " + MAP_TABLE + "(" + MAP_ID + "));";
    public static final String MISSION_TABLE_DROP = "DROP TABLE IF EXISTS " + MISSION_TABLE + ";";

    public static final String SAVE_ID = "id";
    public static final String SAVE_TYPE = "type";
    public static final String SAVE_MAP_ID = "map_id";
    public static final String SAVE_TURN = "turn";
    public static final String SAVE_TABLE = "save";
    public static final String SAVE_TABLE_CREATE =
            "CREATE TABLE " + SAVE_TABLE + " (" +
                    SAVE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    SAVE_TYPE + " INTEGER, " +
                    SAVE_MAP_ID + " INTEGER, " +
                    SAVE_TURN + " INTEGER);";
    public static final String SAVE_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_TABLE + ";";

    public static final String SAVE_PLAYER_SAVE_ID = "save_id";
    public static final String SAVE_PLAYER_POSITION = "position";
    public static final String SAVE_PLAYER_TYPE = "type";
    public static final String SAVE_PLAYER_ARMY = "army";
    public static final String SAVE_PLAYER_TEAM = "team";
    public static final String SAVE_PLAYER_MONEY = "money";
    public static final String SAVE_PLAYER_ALIVE = "alive";
    public static final String SAVE_PLAYER_TABLE = "save_player";
    public static final String SAVE_PLAYER_TABLE_CREATE =
            "CREATE TABLE " + SAVE_PLAYER_TABLE + " (" +
                    SAVE_PLAYER_SAVE_ID + " INTEGER, " +
                    SAVE_PLAYER_POSITION + " INTEGER, " +
                    SAVE_PLAYER_TYPE + " INTEGER, " +
                    SAVE_PLAYER_ARMY + " INTEGER, " +
                    SAVE_PLAYER_TEAM + " INTEGER, " +
                    SAVE_PLAYER_MONEY + " INTEGER, " +
                    SAVE_PLAYER_ALIVE + " INTEGER);";
    public static final String SAVE_PLAYER_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_TABLE + ";";

    public static final String SAVE_BUILDING_SAVE_ID = "save_id";
    public static final String SAVE_BUILDING_TYPE = "type";
    public static final String SAVE_BUILDING_SUBTYPE = "subtype";
    public static final String SAVE_BUILDING_ROW = "row";
    public static final String SAVE_BUILDING_COL = "col";
    public static final String SAVE_BUILDING_OWNER = "owner";
    public static final String SAVE_BUILDING_TABLE = "save_building";
    public static final String SAVE_BUILDING_TABLE_CREATE =
            "CREATE TABLE " + SAVE_BUILDING_TABLE + " (" +
                    SAVE_BUILDING_SAVE_ID + " INTEGER, " +
                    SAVE_BUILDING_TYPE + " INTEGER, " +
                    SAVE_BUILDING_SUBTYPE + " INTEGER, " +
                    SAVE_BUILDING_ROW + " INTEGER, " +
                    SAVE_BUILDING_COL + " INTEGER, " +
                    SAVE_BUILDING_OWNER + " INTEGER);";
    public static final String SAVE_BUILDING_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_TABLE + ";";

    public static final String SAVE_UNIT_SAVE_ID = "save_id";
    public static final String SAVE_UNIT_TYPE = "type";
    public static final String SAVE_UNIT_ROW = "row";
    public static final String SAVE_UNIT_COL = "col";
    public static final String SAVE_UNIT_OWNER = "owner";
    public static final String SAVE_UNIT_LIFE = "life";
    public static final String SAVE_UNIT_AMMO = "ammo";
    public static final String SAVE_UNIT_GAS = "gas";
    public static final String SAVE_UNIT_ORIENTATION = "orientation";
    public static final String SAVE_UNIT_TABLE = "save_unit";
    public static final String SAVE_UNIT_TABLE_CREATE =
            "CREATE TABLE " + SAVE_UNIT_TABLE + " (" +
                    SAVE_UNIT_SAVE_ID + " INTEGER, " +
                    SAVE_UNIT_TYPE + " INTEGER, " +
                    SAVE_UNIT_ROW + " INTEGER, " +
                    SAVE_UNIT_COL + " INTEGER, " +
                    SAVE_UNIT_OWNER + " INTEGER, " +
                    SAVE_UNIT_LIFE + " INTEGER, " +
                    SAVE_UNIT_AMMO + " INTEGER, " +
                    SAVE_UNIT_GAS + " INTEGER, " +
                    SAVE_UNIT_ORIENTATION + " INTEGER);";
    public static final String SAVE_UNIT_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_UNIT_TABLE + ";";

    private SQLiteDatabase database;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void open() {
        this.database = this.getWritableDatabase();
    }

    public void close() {
        if (this.database != null) {
            this.database.close();
        }
    }

    public void addParam(SQLiteDatabase db, String key, String value) {
        try {
            ContentValues values = new ContentValues();
            values.put(PARAM_KEY, key);
            values.put(PARAM_VALUE, value);
            db.insert(PARAM_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getParam(String key) {
        String result = "0";
        Cursor c = this.database.query(PARAM_TABLE, new String[]{PARAM_VALUE}, PARAM_KEY + " = ? ", new String[]{key + ""}, null, null, null);
        if (c.moveToNext()) {
            result = c.getString(0);
        }
        c.close();
        return result;
    }

    public void updateParam(String key, String value) {
        ContentValues values = new ContentValues();
        values.put(PARAM_VALUE, value);
        this.database.update(PARAM_TABLE, values, PARAM_KEY + " = ?", new String[]{key + ""});
    }

    public void addMap(SQLiteDatabase db, Map map) {
        try {
            this.database.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(MAP_ID, map.getId());
            values.put(MAP_NAME, map.getName());
            values.put(MAP_FILE, map.getFile());
            values.put(MAP_TYPE, map.getType());
            if (map.isLocked()) {
                values.put(MAP_LOCK, 1);
            } else {
                values.put(MAP_LOCK, 0);
            }
            values.put(MAP_MISSION_ID, map.getLinkedMissionId());
            db.insert(MAP_TABLE, null, values);
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int createMap(String name) {
        try {
            this.database.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(MAP_NAME, name);
            values.put(MAP_FILE, name + ".xml");
            values.put(MAP_TYPE, Map.CUSTOM);
            values.put(MAP_LOCK, 0);
            values.put(MAP_MISSION_ID, 0);
            int mapId = (int) this.database.insert(MAP_TABLE, null, values);
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
            return mapId;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void deleteMap(int mapID) {
        this.database.delete(MAP_TABLE, MAP_ID + "=?", new String[]{mapID + ""});
    }

    public Map getMap(int map_id) {
        Map map = null;
        Cursor c = this.database.query(MAP_TABLE, new String[]{MAP_ID, MAP_NAME, MAP_FILE, MAP_TYPE, MAP_LOCK, MAP_MISSION_ID}, MAP_ID + " = ? ", new String[]{map_id + ""}, null, null, null);
        if (c.moveToNext()) {
            map = new Map(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3), c.getInt(4) == 1, c.getInt(5));
        }
        c.close();
        return map;
    }

    public boolean checkMapName(String map_name) {
        boolean result = true;
        Cursor c = this.database.query(MAP_TABLE, new String[]{MAP_NAME}, null, null, null, null, null);
        while (c.moveToNext()) {
            if (c.getString(0).equalsIgnoreCase(map_name)) {
                result = false;
                break;
            }
        }
        c.close();
        return result;
    }

    public ArrayList<Map> getMapsByType(int map_type) {
        ArrayList<Map> mapList = new ArrayList<Map>();
        Cursor c = this.database.query(MAP_TABLE, new String[]{MAP_ID, MAP_NAME, MAP_FILE, MAP_LOCK, MAP_MISSION_ID}, MAP_TYPE + " = ?", new String[]{map_type + ""}, null, null, null);
        while (c.moveToNext()) {
            mapList.add(new Map(c.getInt(0), c.getString(1), c.getString(2), map_type, c.getInt(3) == 1, c.getInt(4)));
        }
        c.close();
        return mapList;
    }

    public void unlockMap(Activity main, int map_id) {
        ContentValues values = new ContentValues();
        values.put(MAP_LOCK, 0);
        this.database.update(MAP_TABLE, values, MAP_ID + " = ?", new String[]{map_id + ""});
    }

    public void addMission(SQLiteDatabase db, Mission mission) {
        try {
            this.database.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(MISSION_ID, mission.getId());
            values.put(MISSION_NAME, mission.getName());
            values.put(MISSION_MAP_ID, mission.getMapId());
            values.put(MISSION_DESC, mission.getDesc());
            values.put(MISSION_LOCATION, mission.getLocation());
            values.put(MISSION_ARMIES, Mission.armiesToString(mission.getArmies()));
            values.put(MISSION_SCORE, mission.getScore());
            values.put(MISSION_POSX, mission.getPosition()[0]);
            values.put(MISSION_POSY, mission.getPosition()[1]);
            if (mission.isLocked()) {
                values.put(MISSION_LOCK, 1);
            } else {
                values.put(MISSION_LOCK, 0);
            }
            db.insert(MISSION_TABLE, null, values);
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Mission> getMissions(Activity main) {
        try {
            ArrayList<Mission> missionList = new ArrayList<Mission>();
            Cursor c = this.database.query(MISSION_TABLE, new String[]{MISSION_ID, MISSION_NAME, MISSION_MAP_ID, MISSION_DESC, MISSION_LOCATION, MISSION_ARMIES, MISSION_SCORE, MISSION_POSX, MISSION_POSY}, MISSION_LOCK + " = ?", new String[]{0 + ""}, null, null, null);
            System.out.println("AHA : " + c.getCount());
            while (c.moveToNext()) {
                System.out.println("CCC : " + c.getCount());
                missionList.add(new Mission(
                        c.getInt(0),
                        c.getString(1),
                        c.getInt(2),
                        c.getString(3),
                        c.getString(4),
                        Mission.stringToArmies(c.getString(5)),
                        c.getInt(6),
                        new int[]{c.getInt(7), c.getInt(8)},
                        false));
            }
            c.close();
            System.out.println("BBB : " + missionList.size());
            return missionList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Mission>();
        }
    }

    public void updateMissionScore(Activity main, int mission_id, int score) {
        int current_score = 0;
        Cursor c = this.database.query(MISSION_TABLE, new String[]{MISSION_SCORE}, MISSION_ID + " = ? ", new String[]{mission_id + ""}, null, null, null);
        if (c.moveToNext()) {
            current_score = c.getInt(0);
        }
        c.close();
        if (current_score == 0 || current_score > score) {
            ContentValues values = new ContentValues();
            values.put(MISSION_SCORE, score);
            this.database.update(MISSION_TABLE, values, MISSION_ID + " = ?", new String[]{mission_id + ""});
        }
    }

    public void unlockMission(int mission_id) {
        ContentValues values = new ContentValues();
        values.put(MISSION_LOCK, 0);
        this.database.update(MISSION_TABLE, values, MISSION_ID + " = ?", new String[]{mission_id + ""});
    }

    public int save(Activity main, Save save) {
        try {
            this.database.beginTransaction();
            int saveId = save.getId();
            // CREATION OF NEW SAVE
            if (saveId == 0) {
                ContentValues values = new ContentValues();
                values.put(SAVE_TYPE, save.getMode());
                values.put(SAVE_MAP_ID, save.getMap().getId());
                values.put(SAVE_TURN, save.getTurn());
                saveId = (int) this.database.insert(SAVE_TABLE, null, values);
            }
            // UPDATE OF EXISTANT SAVE
            else {
                // Delete old datas
                this.database.delete(SAVE_BUILDING_TABLE, SAVE_BUILDING_SAVE_ID + "=?", new String[]{save.getId() + ""});
                this.database.delete(SAVE_UNIT_TABLE, SAVE_UNIT_SAVE_ID + "=?", new String[]{save.getId() + ""});
                this.database.delete(SAVE_PLAYER_TABLE, SAVE_PLAYER_SAVE_ID + "=?", new String[]{save.getId() + ""});
                // Update save turn
                ContentValues values = new ContentValues();
                values.put(SAVE_TURN, save.getTurn());
                this.database.update(SAVE_TABLE, values, SAVE_ID + " = ?", new String[]{save.getId() + ""});
            }
            // PLAYERS
            // Players
            for (Player player : save.getPlayers()) {
                String alive = "";
                if (player.isAlive()) {
                    alive = "1";
                } else {
                    alive = "0";
                }
                ContentValues values_player = new ContentValues();
                values_player.put(SAVE_PLAYER_SAVE_ID, saveId);
                values_player.put(SAVE_PLAYER_POSITION, player.getOrder());
                values_player.put(SAVE_PLAYER_TYPE, player.getType());
                values_player.put(SAVE_PLAYER_ARMY, player.getArmy().getType());
                values_player.put(SAVE_PLAYER_TEAM, player.getTeam());
                values_player.put(SAVE_PLAYER_MONEY, player.getMoney());
                values_player.put(SAVE_PLAYER_ALIVE, alive);
                this.database.insert(SAVE_PLAYER_TABLE, null, values_player);
            }
            // Buildings
            for (Building building : save.getBuildings()) {
                ContentValues values_building = new ContentValues();
                values_building.put(SAVE_BUILDING_SAVE_ID, saveId);
                values_building.put(SAVE_BUILDING_OWNER, building.getOwner().getOrder());
                values_building.put(SAVE_BUILDING_TYPE, building.getType());
                values_building.put(SAVE_BUILDING_SUBTYPE, building.getSubtype());
                values_building.put(SAVE_BUILDING_ROW, building.getRow());
                values_building.put(SAVE_BUILDING_COL, building.getCol());
                this.database.insert(SAVE_BUILDING_TABLE, null, values_building);
            }
            // Units
            for (Unit unit : save.getUnits()) {
                ContentValues values_unit = new ContentValues();
                values_unit.put(SAVE_UNIT_SAVE_ID, saveId);
                values_unit.put(SAVE_UNIT_OWNER, unit.getOwner().getOrder());
                values_unit.put(SAVE_UNIT_TYPE, unit.getType());
                values_unit.put(SAVE_UNIT_ROW, unit.getRow());
                values_unit.put(SAVE_UNIT_COL, unit.getCol());
                values_unit.put(SAVE_UNIT_LIFE, unit.getLife());
                values_unit.put(SAVE_UNIT_AMMO, unit.getAmmo());
                values_unit.put(SAVE_UNIT_GAS, unit.getGas());
                values_unit.put(SAVE_UNIT_ORIENTATION, unit.getOrientationValue());
                this.database.insert(SAVE_UNIT_TABLE, null, values_unit);
            }
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
            return saveId;
        } catch (Exception e) {
            this.database.endTransaction();
            AndroidLauncher.showToast(main, "Save failed : " + e.toString());
            return 0;
        }
    }

    public ArrayList<Save> getSavesSummary(DWController controller, int mode) {
        ArrayList<Save> saves = new ArrayList<Save>();
        Cursor c = this.database.rawQuery("SELECT a.*, b." + MAP_NAME + " FROM " + SAVE_TABLE + " AS a JOIN " + MAP_TABLE + " AS b ON a." + SAVE_MAP_ID + "=b." + MAP_ID + " WHERE a." + SAVE_TYPE + "=?", new String[]{"" + mode});
        while (c.moveToNext()) {
                int save_id = c.getInt(0);
                int save_mode = c.getInt(1);
                int save_map_id = c.getInt(2);
                int save_turn = c.getInt(3);
                ArrayList<Player> players = new ArrayList<Player>();
                // ADD GAIA PLAYER
                players.add(0, new Player(controller.getPlayers().get(Army.GAI).getArmy(), 0, 0, Player.GAIA_ORDER, Player.ALIVE));
                Cursor c2 = this.database.rawQuery("SELECT * FROM " + SAVE_PLAYER_TABLE + " WHERE " + SAVE_PLAYER_SAVE_ID + "=?", new String[]{"" + save_id});
                while (c2.moveToNext()) {
                    if (c2.getInt(2) == Player.HUMAN) {
                        players.add(new Player(controller.getPlayers().get(c2.getInt(3)).getArmy(), c2.getInt(4), c2.getInt(5), c2.getInt(1), c2.getInt(6)));
                    } else {
                        players.add(new Robot(controller.getPlayers().get(c2.getInt(3)).getArmy(), c2.getInt(4), c2.getInt(5), c2.getInt(1), c2.getInt(6)));

                    }
                }
                Save save = new Save(save_id, save_mode, getMap(save_map_id), players, save_turn);
                // Get Buildings
                Cursor c3 = this.database.rawQuery("SELECT * FROM " + SAVE_BUILDING_TABLE + " WHERE " + SAVE_BUILDING_SAVE_ID + "=?", new String[]{"" + save.getId()});
                while (c3.moveToNext()) {
                    save.addBuilding(Building.createBuilding(c3.getInt(3), c3.getInt(4), save.getPlayerFromOrder(c3.getInt(5)), c3.getInt(1), c3.getInt(2)));
                }
                c3.close();
                saves.add(save);
                c2.close();
        }
        c.close();
        return saves;
    }

    /*
    * Send mail AsyncTask
    */
    public class SendMailAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            WebUtils.sendMail(params[0],params[1]);
            return null;
        }

    }

    public void getSaveComplete(Activity main, Save save) {
        // Get Units
        Cursor c4 = this.database.rawQuery("SELECT * FROM " + SAVE_UNIT_TABLE + " WHERE " + SAVE_UNIT_SAVE_ID + "=?", new String[]{"" + save.getId()});
        while (c4.moveToNext()) {
            Unit unit = Unit.createUnit(c4.getInt(2), c4.getInt(3), save.getPlayerFromOrder(c4.getInt(4)), c4.getInt(1), Unit.valueToOrientation(c4.getInt(8)));
            unit.setLife(c4.getInt(5));
            unit.setAmmo(c4.getInt(6));
            unit.setGas(c4.getInt(7));
            save.addUnit(unit);
        }
        c4.close();
    }

    public void deleteSave(int saveID) {
        try {
            this.database.beginTransaction();
            this.database.delete(SAVE_TABLE, SAVE_ID + "=?", new String[]{saveID + ""});
            this.database.delete(SAVE_BUILDING_TABLE, SAVE_BUILDING_SAVE_ID + "=?", new String[]{saveID + ""});
            this.database.delete(SAVE_UNIT_TABLE, SAVE_UNIT_SAVE_ID + "=?", new String[]{saveID + ""});
            this.database.delete(SAVE_PLAYER_TABLE, SAVE_PLAYER_SAVE_ID + "=?", new String[]{saveID + ""});
            this.database.setTransactionSuccessful();
            this.database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            this.database.endTransaction();
        }
    }

    public boolean checkMapSaves(int mapId) {
        boolean result = true;
        Cursor c = this.database.query(SAVE_TABLE, new String[]{SAVE_ID}, SAVE_MAP_ID + " = ?", new String[]{mapId + ""}, null, null, null, null);
        while (c.moveToNext()) {
            result = false;
        }
        c.close();
        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("CREATE DB");
        db.execSQL(PARAM_TABLE_CREATE);
        initParams(db);
        db.execSQL(MAP_TABLE_CREATE);
        initMaps(db);
        db.execSQL(MISSION_TABLE_CREATE);
        initMissions(db);
        db.execSQL(SAVE_TABLE_CREATE);
        db.execSQL(SAVE_PLAYER_TABLE_CREATE);
        db.execSQL(SAVE_BUILDING_TABLE_CREATE);
        db.execSQL(SAVE_UNIT_TABLE_CREATE);
    }

    private void initParams(SQLiteDatabase db) {
        this.addParam(db, Util.PARAM_AUDIO_MUSIC, "1");
        this.addParam(db, Util.PARAM_AUDIO_SOUND, "1");
        this.addParam(db, Util.PARAM_SPEED_HUMAN, "1");
        this.addParam(db, Util.PARAM_SPEED_ROBOT, "1");
    }

    private void initMaps(SQLiteDatabase db) {
        // CAMPAIGN
        System.out.println("initMaps...START");
        // CAMPAIGN MAPS : 1 - 25
        this.addMap(db, new Map(Map.BRITAIN_ID, Map.BRITAIN, Map.BRITAIN_FILE, Map.CAMPAIGN, true, Mission.BRITAIN_ID));
        this.addMap(db, new Map(Map.TOBRUK_ID, Map.TOBRUK, Map.TOBRUK_FILE, Map.CAMPAIGN, true, Mission.TOBRUK_ID));
        this.addMap(db, new Map(Map.PEARL_HARBOR_ID, Map.PEARL_HARBOR, Map.PEARL_HARBOR_FILE, Map.CAMPAIGN, true, Mission.PEARL_HARBOR_ID));
        this.addMap(db, new Map(Map.HONG_KONG_ID, Map.HONG_KONG, Map.HONG_KONG_FILE, Map.CAMPAIGN, true, Mission.HONG_KONG_ID));
        this.addMap(db, new Map(Map.MIDWAY_ID, Map.MIDWAY, Map.MIDWAY_FILE, Map.CAMPAIGN, true, Mission.MIDWAY_ID));
        this.addMap(db, new Map(Map.STALINGRAD_ID, Map.STALINGRAD, Map.STALINGRAD_FILE, Map.CAMPAIGN, true, Mission.STALINGARD_ID));
        this.addMap(db, new Map(Map.GUADALCANAL_ID, Map.GUADALCANAL, Map.GUADALCANAL_FILE, Map.CAMPAIGN, true, Mission.GUADALCANAL_ID));
        this.addMap(db, new Map(Map.EL_ALAMEIN_ID, Map.EL_ALAMEIN, Map.EL_ALAMEIN_FILE, Map.CAMPAIGN, true, Mission.EL_ALAMEIN_ID));
        this.addMap(db, new Map(Map.CAPRI_ID, Map.CAPRI, Map.CAPRI_FILE, Map.CAMPAIGN, true, Mission.CAPRI_ID));
        this.addMap(db, new Map(Map.KURSK_ID, Map.KURSK, Map.KURSK_FILE, Map.CAMPAIGN, true, Mission.KURSK_ID));
        this.addMap(db, new Map(Map.HUSKY_ID, Map.HUSKY, Map.HUSKY_FILE, Map.CAMPAIGN, true, Mission.HUSKY_ID));
        this.addMap(db, new Map(Map.KHARKOV_ID, Map.KHARKOV, Map.KHARKOV_FILE, Map.CAMPAIGN, true, Mission.KHARKOV_ID));
        this.addMap(db, new Map(Map.LENINGRAD_ID, Map.LENINGRAD, Map.LENINGRAD_FILE, Map.CAMPAIGN, true, Mission.LENINGRAD_ID));
        this.addMap(db, new Map(Map.CRIMEA_ID, Map.CRIMEA, Map.CRIMEA_FILE, Map.CAMPAIGN, true, Mission.CRIMEA_ID));
        this.addMap(db, new Map(Map.ROMA_ID, Map.ROMA, Map.ROMA_FILE, Map.CAMPAIGN, true, Mission.ROMA_ID));
        this.addMap(db, new Map(Map.OMAHA_BEACH_ID, Map.OMAHA_BEACH, Map.OMAHA_BEACH_FILE, Map.CAMPAIGN, true, Mission.OMAHA_BEACH_ID));
        this.addMap(db, new Map(Map.PARIS_ID, Map.PARIS, Map.PARIS_FILE, Map.CAMPAIGN, true, Mission.PARIS_ID));
        this.addMap(db, new Map(Map.MARKET_GARDEN_ID, Map.MARKET_GARDEN, Map.MARKET_GARDEN_FILE, Map.CAMPAIGN, true, Mission.MARKET_GARDEN_ID));
        this.addMap(db, new Map(Map.BULGE_ID, Map.BULGE, Map.BULGE_FILE, Map.CAMPAIGN, true, Mission.BULGE_ID));
        this.addMap(db, new Map(Map.LUZON_ID, Map.LUZON, Map.LUZON_FILE, Map.CAMPAIGN, true, Mission.LUZON_ID));
        this.addMap(db, new Map(Map.IWO_JIMA_ID, Map.IWO_JIMA, Map.IWO_JIMA_FILE, Map.CAMPAIGN, true, Mission.IWO_JIMA_ID));
        this.addMap(db, new Map(Map.VIENNA_ID, Map.VIENNA, Map.VIENNA_FILE, Map.CAMPAIGN, true, Mission.VIENNA_ID));
        this.addMap(db, new Map(Map.OKINAWA_ID, Map.OKINAWA, Map.OKINAWA_FILE, Map.CAMPAIGN, true, Mission.OKINAWA_ID));
        this.addMap(db, new Map(Map.BERLIN_ID, Map.BERLIN, Map.BERLIN_FILE, Map.CAMPAIGN, true, Mission.BERLIN_ID));
        this.addMap(db, new Map(Map.SAKHALIN_ID, Map.SAKHALIN, Map.SAKHALIN_FILE, Map.CAMPAIGN, true, Mission.SAKHALIN_ID));
        // 2P MAPS : 26-30
        this.addMap(db, new Map(Map.DUAL_ID, Map.DUAL, Map.DUAL_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.INFINITE_ID, Map.INFINITE, Map.INFINITE_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.PLUS_ID, Map.PLUS, Map.PLUS_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.AUSTRALIA_ID, Map.AUSTRALIA, Map.AUSTRALIA_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.UNKNOWN_ID, Map.UNKNOWN, Map.UNKNOWN_FILE, Map.CLASSIC_2P, false, 0));
        // 3P MAPS : 31-35
        this.addMap(db, new Map(Map.TRIANGLE_ID, Map.TRIANGLE, Map.TRIANGLE_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.POSEIDON_ID, Map.POSEIDON, Map.POSEIDON_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.AFRICA_ID, Map.AFRICA, Map.AFRICA_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.MAZE_ID, Map.MAZE, Map.MAZE_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.CARIBBEAN_ID, Map.CARIBBEAN, Map.CARIBBEAN_FILE, Map.CLASSIC_3P, false, 0));
        // 4P MAPS : 36-40
        this.addMap(db, new Map(Map.SQUARE_ID, Map.SQUARE, Map.SQUARE_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.SPIRAL_ID, Map.SPIRAL, Map.SPIRAL_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.RIVERS_ID, Map.RIVERS, Map.RIVERS_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.EIGHT_ID, Map.EIGHT, Map.EIGHT_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.AMERICA_ID, Map.AMERICA, Map.AMERICA_FILE, Map.CLASSIC_4P, false, 0));
        // 5P MAPS : 41-45
        this.addMap(db, new Map(Map.PENTAGON_ID, Map.PENTAGON, Map.PENTAGON_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.EUROPE_ID, Map.EUROPE, Map.EUROPE_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.STAR_ID, Map.STAR, Map.STAR_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.ISLANDS_ID, Map.ISLANDS, Map.ISLANDS_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.WORLD_ID, Map.WORLD, Map.WORLD_FILE, Map.CLASSIC_5P, false, 0));
        System.out.println("initMaps...END");
    }

    private void initMissions(SQLiteDatabase db) {
        System.out.println("initMissions...START");
        this.addMission(db, new Mission(Mission.BRITAIN_ID, Mission.BRITAIN_NAME, Map.BRITAIN_ID, Mission.BRITAIN_DESCRIPTION, Mission.BRITAIN_LOCATION, Mission.BRITAIN_ARMIES, 0, Mission.BRITAIN_POSITION, false));
        this.addMission(db, new Mission(Mission.TOBRUK_ID, Mission.TOBRUK_NAME, Map.TOBRUK_ID, Mission.TOBRUK_DESCRIPTION, Mission.TOBRUK_LOCATION, Mission.TOBRUK_ARMIES, 0, Mission.TOBRUK_POSITION, true));
        this.addMission(db, new Mission(Mission.PEARL_HARBOR_ID, Mission.PEARL_HARBOR_NAME, Map.PEARL_HARBOR_ID, Mission.PEARL_HARBOR_DESCRIPTION, Mission.PEARL_HARBOR_LOCATION, Mission.PEARL_HARBOR_ARMIES, 0, Mission.PEARL_HARBOR_POSITION, true));
        this.addMission(db, new Mission(Mission.HONG_KONG_ID, Mission.HONG_KONG_NAME, Map.HONG_KONG_ID, Mission.HONG_KONG_DESCRIPTION, Mission.HONG_KONG_LOCATION, Mission.HONG_KONG_ARMIES, 0, Mission.HONG_KONG_POSITION, true));
        this.addMission(db, new Mission(Mission.MIDWAY_ID, Mission.MIDWAY_NAME, Map.MIDWAY_ID, Mission.MIDWAY_DESCRIPTION, Mission.MIDWAY_LOCATION, Mission.MIDWAY_ARMIES, 0, Mission.MIDWAY_POSITION, true));
        this.addMission(db, new Mission(Mission.STALINGARD_ID, Mission.STALINGRAD_NAME, Map.STALINGRAD_ID, Mission.STALINGRAD_DESCRIPTION, Mission.STALINGRAD_LOCATION, Mission.STALINGRAD_ARMIES, 0, Mission.STALINGRAD_POSITION, true));
        this.addMission(db, new Mission(Mission.GUADALCANAL_ID, Mission.GUADALCANAL_NAME, Map.GUADALCANAL_ID, Mission.GUADALCANAL_DESCRIPTION, Mission.GUADALCANAL_LOCATION, Mission.GUADALCANAL_ARMIES, 0, Mission.GUADALCANAL_POSITION, true));
        this.addMission(db, new Mission(Mission.EL_ALAMEIN_ID, Mission.EL_ALAMEIN_NAME, Map.EL_ALAMEIN_ID, Mission.EL_ALAMEIN_DESCRIPTION, Mission.EL_ALAMEIN_LOCATION, Mission.EL_ALAMEIN_ARMIES, 0, Mission.EL_ALAMEIN_POSITION, true));
        this.addMission(db, new Mission(Mission.CAPRI_ID, Mission.CAPRI_NAME, Map.CAPRI_ID, Mission.CAPRI_DESCRIPTION, Mission.CAPRI_LOCATION, Mission.CAPRI_ARMIES, 0, Mission.CAPRI_POSITION, true));
        this.addMission(db, new Mission(Mission.KURSK_ID, Mission.KURSK_NAME, Map.KURSK_ID, Mission.KURSK_DESCRIPTION, Mission.KURSK_LOCATION, Mission.KURSK_ARMIES, 0, Mission.KURSK_POSITION, true));
        this.addMission(db, new Mission(Mission.HUSKY_ID, Mission.HUSKY_NAME, Map.HUSKY_ID, Mission.HUSKY_DESCRIPTION, Mission.HUSKY_LOCATION, Mission.HUSKY_ARMIES, 0, Mission.HUSKY_POSITION, true));
        this.addMission(db, new Mission(Mission.KHARKOV_ID, Mission.KHARKOV_NAME, Map.KHARKOV_ID, Mission.KHARKOV_DESCRIPTION, Mission.KHARKOV_LOCATION, Mission.KHARKOV_ARMIES, 0, Mission.KHARKOV_POSITION, true));
        this.addMission(db, new Mission(Mission.LENINGRAD_ID, Mission.LENINGRAD_NAME, Map.LENINGRAD_ID, Mission.LENINGRAD_DESCRIPTION, Mission.LENINGRAD_LOCATION, Mission.LENINGRAD_ARMIES, 0, Mission.LENINGRAD_POSITION, true));
        this.addMission(db, new Mission(Mission.CRIMEA_ID, Mission.CRIMEA_NAME, Map.CRIMEA_ID, Mission.CRIMEA_DESCRIPTION, Mission.CRIMEA_LOCATION, Mission.CRIMEA_ARMIES, 0, Mission.CRIMEA_POSITION, true));
        this.addMission(db, new Mission(Mission.ROMA_ID, Mission.ROMA_NAME, Map.ROMA_ID, Mission.ROMA_DESCRIPTION, Mission.ROMA_LOCATION, Mission.ROMA_ARMIES, 0, Mission.ROMA_POSITION, true));
        this.addMission(db, new Mission(Mission.OMAHA_BEACH_ID, Mission.OMAHA_BEACH_NAME, Map.OMAHA_BEACH_ID, Mission.OMAHA_BEACH_DESCRIPTION, Mission.OMAHA_BEACH_LOCATION, Mission.OMAHA_BEACH_ARMIES, 0, Mission.OMAHA_BEACH_POSITION, true));
        this.addMission(db, new Mission(Mission.PARIS_ID, Mission.PARIS_NAME, Map.PARIS_ID, Mission.PARIS_DESCRIPTION, Mission.PARIS_LOCATION, Mission.PARIS_ARMIES, 0, Mission.PARIS_POSITION, true));
        this.addMission(db, new Mission(Mission.MARKET_GARDEN_ID, Mission.MARKET_GARDEN_NAME, Map.MARKET_GARDEN_ID, Mission.MARKET_GARDEN_DESCRIPTION, Mission.MARKET_GARDEN_LOCATION, Mission.MARKET_GARDEN_ARMIES, 0, Mission.MARKET_GARDEN_POSITION, true));
        this.addMission(db, new Mission(Mission.BULGE_ID, Mission.BULGE_NAME, Map.BULGE_ID, Mission.BULGE_DESCRIPTION, Mission.BULGE_LOCATION, Mission.BULGE_ARMIES, 0, Mission.BULGE_POSITION, true));
        this.addMission(db, new Mission(Mission.LUZON_ID, Mission.LUZON_NAME, Map.LUZON_ID, Mission.LUZON_DESCRIPTION, Mission.LUZON_LOCATION, Mission.LUZON_ARMIES, 0, Mission.LUZON_POSITION, true));
        this.addMission(db, new Mission(Mission.IWO_JIMA_ID, Mission.IWO_JIMA_NAME, Map.IWO_JIMA_ID, Mission.IWO_JIMA_DESCRIPTION, Mission.IWO_JIMA_LOCATION, Mission.IWO_JIMA_ARMIES, 0, Mission.IWO_JIMA_POSITION, true));
        this.addMission(db, new Mission(Mission.VIENNA_ID, Mission.VIENNA_NAME, Map.VIENNA_ID, Mission.VIENNA_DESCRIPTION, Mission.VIENNA_LOCATION, Mission.VIENNA_ARMIES, 0, Mission.VIENNA_POSITION, true));
        this.addMission(db, new Mission(Mission.OKINAWA_ID, Mission.OKINAWA_NAME, Map.OKINAWA_ID, Mission.OKINAWA_DESCRIPTION, Mission.OKINAWA_LOCATION, Mission.OKINAWA_ARMIES, 0, Mission.OKINAWA_POSITION, true));
        this.addMission(db, new Mission(Mission.BERLIN_ID, Mission.BERLIN_NAME, Map.BERLIN_ID, Mission.BERLIN_DESCRIPTION, Mission.BERLIN_LOCATION, Mission.BERLIN_ARMIES, 0, Mission.BERLIN_POSITION, true));
        this.addMission(db, new Mission(Mission.SAKHALIN_ID, Mission.SAKHALIN_NAME, Map.SAKHALIN_ID, Mission.SAKHALIN_DESCRIPTION, Mission.SAKHALIN_LOCATION, Mission.SAKHALIN_ARMIES, 0, Mission.SAKHALIN_POSITION, true));
        System.out.println("initMissions...END");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}