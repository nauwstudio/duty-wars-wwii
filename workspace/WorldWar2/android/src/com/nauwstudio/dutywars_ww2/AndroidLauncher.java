package com.nauwstudio.dutywars_ww2;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.StringBuilder;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.database.DatabaseHelper;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.interfaces.AdsInterface;
import com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface;
import com.nauwstudio.dutywars_ww2.interfaces.FileInterface;
import com.nauwstudio.dutywars_ww2.interfaces.GooglePlayInterface;
import com.nauwstudio.dutywars_ww2.interfaces.WebInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AndroidLauncher extends AndroidApplication implements DatabaseInterface, GooglePlayInterface, WebInterface, AdsInterface, FileInterface {

    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_GET_FILE = 8001;
    private static final int RC_GET_PERMISSION_STORAGE = 6001;

    private DatabaseHelper database;

    private com.google.android.gms.ads.InterstitialAd launcherAd;

    // Client used to sign in with Google APIs
    private GoogleSignInClient mGoogleSignInClient;

    private DWController controller;

    private boolean launch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		launchGame();
    }


    private void launchGame() {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        this.database = new DatabaseHelper(this.getContext());
        this.controller = new DWController(this, this, this, this, this);
        initialize(this.controller, config);
        // PLAY GAMES API
        mGoogleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN).build());
        // ADS API
        createLauncherAd();
        loadLauncherAd();
    }

    @Override
    public Map getMap(int id) {
        database.open();
        Map result = database.getMap(id);
        database.close();
        return result;
    }

    @Override
    public ArrayList<Map> getMapsByType(int type) {
        database.open();
        ArrayList<Map> maps = database.getMapsByType(type);
        database.close();
        return maps;
    }

    @Override
    public ArrayList<Mission> getMissions() {
        database.open();
        ArrayList<Mission> result = database.getMissions(this);
        database.close();
        return result;
    }

    @Override
    public void updateScore(int missionID, int score) {
        database.open();
        database.updateMissionScore(this, missionID, score);
        database.close();
    }

    @Override
    public void unlockMap(int mapID) {
        database.open();
        database.unlockMap(this, mapID);
        database.close();
    }

    @Override
    public void unlockMission(int missionID) {
        database.open();
        database.unlockMission(missionID);
        database.close();
    }

    @Override
    public int saveGame(Save save) {
        database.open();
        int saveID = database.save(this, save);
        database.close();
        return saveID;
    }

    @Override
    public ArrayList<Save> getSavesSummary(DWController controller, int mode) {
        database.open();
        ArrayList<Save> saves = database.getSavesSummary(controller, mode);
        database.close();
        return saves;
    }

    @Override
    public void getSaveComplete(Save save) {
        database.open();
        database.getSaveComplete(this, save);
        database.close();
    }

    @Override
    public void deleteSave(int saveID) {
        database.open();
        database.deleteSave(saveID);
        database.close();
    }

    @Override
    public String getParam(String key) {
        database.open();
        String value = database.getParam(key);
        database.close();
        return value;
    }

    @Override
    public void updateParam(String key, String value) {
        database.open();
        database.updateParam(key, value);
        database.close();
    }

    @Override
    public boolean checkMapName(String name) {
        database.open();
        boolean result = database.checkMapName(name);
        database.close();
        return result;
    }

    @Override
    public Map createMap(String name) {
        database.open();
        int mapID = database.createMap(name);
        System.out.println("MAPID : " + mapID);
        Map map = database.getMap(mapID);
        System.out.println("MAP :" + map);
        database.close();
        return map;
    }

    @Override
    public void deleteMap(int mapID) {
        database.open();
        database.deleteMap(mapID);
        database.close();
    }

    @Override
    public boolean checkMapSaves(int mapId) {
        database.open();
        boolean result = database.checkMapSaves(mapId);
        database.close();
        return result;
    }

    /*
    * ADS
    */
    private void createLauncherAd() {
        launcherAd = new InterstitialAd(this);
        launcherAd.setAdUnitId(getResources().getString(R.string.ads_launcher));
        launcherAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                launcherAd.show();
            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }
        });
    }

    private void loadLauncherAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        this.launcherAd.loadAd(adRequest);
    }

    @Override
    public void showLauncherAd() {
        try {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    if (launcherAd.isLoaded()) {
                        launcherAd.show();
                    } else {
                        AdRequest adRequest = new AdRequest.Builder().build();
                        launcherAd.loadAd(adRequest);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
     * Google Play Game
     */
    public boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    @Override
    public void logIn() {
        startSignInIntent();
    }

    @Override
    public void logOut() {
        signOut();
    }

    private void signInSilently() {
        mGoogleSignInClient.silentSignIn().addOnCompleteListener(this, new OnCompleteListener<GoogleSignInAccount>() {
            @Override
            public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                if (task.isSuccessful()) {
                    onConnected(task.getResult());
                } else {
                    if (launch) {
                        launch = false;
                        logIn();
                    } else {
                        onDisconnected();
                    }
                }
            }
        });
    }

    private void startSignInIntent() {
        startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Since the state of the signed in user can change when the activity is not active
        // it is recommended to try and sign in silently from when the app resumes.
        signInSilently();
    }

    public void signOut() {
        if (!isSignedIn()) {
            return;
        }
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        Log.d("Test", "signOut(): " + (successful ? "success" : "failed"));
                        onDisconnected();
                    }
                });
    }

    @Override
    public void submitScore(int missionId, int score) {
        if (isSignedIn()) {
            Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .submitScore(getLeaderboardId(missionId), score);
        }
    }

    @Override
    public void getAllLeaderboards() {
        if (isSignedIn()) {
            Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .getAllLeaderboardsIntent()
                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                        @Override
                        public void onSuccess(Intent intent) {
                            startActivityForResult(intent, RC_UNUSED);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            handleException(e, getString(R.string.leaderboards_exception));
                        }
                    });
        }
    }

    @Override
    public void getLeaderboard(int missionId) {
        if (isSignedIn()) {
            Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .getLeaderboardIntent(getLeaderboardId(missionId))
                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                        @Override
                        public void onSuccess(Intent intent) {
                            startActivityForResult(intent, RC_UNUSED);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            handleException(e, getString(R.string.leaderboards_exception));
                        }
                    });
        }
    }

    private String getLeaderboardId(int missionId) {
        switch (missionId) {
            case Mission.BRITAIN_ID:
                return getResources().getString(R.string.leaderboard_battle_of_britain);
            case Mission.TOBRUK_ID:
                return getResources().getString(R.string.leaderboard_siege_of_tobruk);
            case Mission.PEARL_HARBOR_ID:
                return getResources().getString(R.string.leaderboard_pearl_harbor);
            case Mission.HONG_KONG_ID:
                return getResources().getString(R.string.leaderboard_hong_kong);
            case Mission.MIDWAY_ID:
                return getResources().getString(R.string.leaderboard_midway);
            case Mission.STALINGARD_ID:
                return getResources().getString(R.string.leaderboard_stalingrad);
            case Mission.GUADALCANAL_ID:
                return getResources().getString(R.string.leaderboard_guadalcanal);
            case Mission.EL_ALAMEIN_ID:
                return getResources().getString(R.string.leaderboard_el_alamein);
            case Mission.CAPRI_ID:
                return getResources().getString(R.string.leaderboard_operation_capri);
            case Mission.KURSK_ID:
                return getResources().getString(R.string.leaderboard_kursk);
            case Mission.HUSKY_ID:
                return getResources().getString(R.string.leaderboard_husky_operation);
            case Mission.KHARKOV_ID:
                return getResources().getString(R.string.leaderboard_belgorodkharkov);
            case Mission.LENINGRAD_ID:
                return getResources().getString(R.string.leaderboard_siege_of_leningrad);
            case Mission.CRIMEA_ID:
                return getResources().getString(R.string.leaderboard_crimea);
            case Mission.ROMA_ID:
                return getResources().getString(R.string.leaderboard_battle_for_rome);
            case Mission.OMAHA_BEACH_ID:
                return getResources().getString(R.string.leaderboard_omaha_beach);
            case Mission.PARIS_ID:
                return getResources().getString(R.string.leaderboard_paris);
            case Mission.MARKET_GARDEN_ID:
                return getResources().getString(R.string.leaderboard_market_garden);
            case Mission.BULGE_ID:
                return getResources().getString(R.string.leaderboard_battle_of_the_bulge);
            case Mission.LUZON_ID:
                return getResources().getString(R.string.leaderboard_battle_of_luzon);
            case Mission.IWO_JIMA_ID:
                return getResources().getString(R.string.leaderboard_iwo_jima);
            case Mission.VIENNA_ID:
                return getResources().getString(R.string.leaderboard_vienna_offensive);
            case Mission.OKINAWA_ID:
                return getResources().getString(R.string.leaderboard_okinawa);
            case Mission.BERLIN_ID:
                return getResources().getString(R.string.leaderboard_berlin);
            case Mission.SAKHALIN_ID:
                return getResources().getString(R.string.leaderboard_sakhalin);
            default:
                return "";
        }
    }

    private void handleException(Exception e, String details) {
        int status = 0;

        if (e instanceof ApiException) {
            ApiException apiException = (ApiException) e;
            status = apiException.getStatusCode();
        }

        String message = getString(R.string.status_exception_error, details, status, e);

        AlertDialog show = new AlertDialog.Builder(AndroidLauncher.this)
                .setMessage(message)
                .setNeutralButton(android.R.string.ok, null)
                .show();
    }

    private void onConnected(GoogleSignInAccount googleSignInAccount) {
    }

    private void onDisconnected() {

    }


    @Override
    public boolean checkPermission() {
        // PERMISSIONS
        int writePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void getPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_GET_PERMISSION_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RC_GET_PERMISSION_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showToast(this, "This game can now create custom maps.");
                } else {
                    showToast(this, "This game need access to files to create custom maps.");
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                onConnected(account);
            } catch (ApiException apiException) {
                String message = apiException.getMessage();
                if (message == null || message.isEmpty()) {
                    message = getString(R.string.signin_other_error);
                }
                onDisconnected();

                new AlertDialog.Builder(this)
                        .setMessage(message)
                        .setNeutralButton(android.R.string.ok, null)
                        .show();
            }
        } else if (requestCode == RC_GET_FILE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uri = intent.getData();
                    String fileName = uri.getPath().substring(uri.getPath().lastIndexOf(File.separator) + 1);
                    downloadFile(fileName, uri);
                    this.controller.importMap(fileName);
                } catch (Exception e) {
                    showMessage("getIntent : " + e.toString());
                }
            }
        }
    }

    private void downloadFile(String fileName, Uri uri) throws Exception {
        // Read file
        InputStream is = getContentResolver().openInputStream(uri);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        // Create temp file
        FileHandle tempFile = Gdx.files.local(MapFile.TEMP + fileName);
        tempFile.writeString(sb.toString(), false);
        is.close();
        br.close();
    }

    @Override
    public void facebook() {
        openWebsite("www.facebook.com/dutywarswwii/");
    }

    @Override
    public void googlePlay() {
        openWebsite("https://play.google.com/store/apps/details?id=com.nauwstudio.dutywars_ww2");
    }

    @Override
    public void web() {
        openWebsite("www.nauwstudio.be/dutywars");
    }

    @Override
    public void help() {
        openWebsite("www.nauwstudio.be/dutywars");
    }

    private void openWebsite(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public String getExportFolder() {
        return "DutyWars_WWII" + File.separator;
    }

    @Override
    public void importFile() {
        Intent intent = new Intent().setType("text/xml").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a file"), RC_GET_FILE);
    }

    @Override
    public void showMessage(String message) {
        showToast(this, message);
    }


    public static void showToast(final Activity main, final String message) {
        main.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(main, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}
