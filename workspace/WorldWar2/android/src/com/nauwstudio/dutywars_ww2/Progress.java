package com.nauwstudio.dutywars_ww2;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Progress {

    private static final String TAG = "CampaignProgress";
    private static final String SERIAL_VERSION = "1.1";
    private static final String VERSION = "version";
    private static final String MISSIONS = "missions";
    private static final String MAPS = "maps";
    private static final String ID = "id";
    private static final String LOCK = "lock";
    private static final String SCORE = "score";


    Map<Integer, Integer> maps = new HashMap<Integer, Integer>();
    Map<Integer, int[]> missions = new HashMap<Integer, int[]>();

    public Progress(String json) {
        loadFromJson(json);
    }

    public boolean isEmpty() {
        return this.maps.isEmpty() || this.missions.isEmpty();
    }

    public int getMissionLock(int missionId) {
        return missions.get(missionId)[0];
    }

    public int getMissionScore(int missionId) {
        return missions.get(missionId)[1];
    }

    public void setMission(int missionId, int lock, int score) {
        missions.put(missionId, new int[]{lock, score});
    }

    public int getMapLock(int mapId) {
        return maps.get(mapId);
    }

    public void setMap(int mapId, int lock) {
        maps.put(mapId, lock);
    }

    public void loadFromJson(String json) {
        missions.clear();
        maps.clear();
        if (json == null || json.trim().equals("")) {
            return;
        }
        try {
            JSONObject json_obj = new JSONObject(json);
            String format = json_obj.getString(VERSION);
            if (!format.equals(SERIAL_VERSION)) {
                throw new RuntimeException("Unexpected loot format " + format);
            }
            JSONArray json_missions = json_obj.getJSONArray(MISSIONS);
            for (int i = 0; i < json_missions.length(); i++) {
                JSONObject json_mission = json_missions.getJSONObject(i);
                missions.put(json_mission.getInt(ID), new int[]{json_mission.getInt(LOCK),json_mission.getInt(SCORE)});
            }
            JSONArray json_maps = json_obj.getJSONArray(MAPS);
            for (int i = 0; i < json_maps.length(); i++) {
                JSONObject json_map = json_maps.getJSONObject(i);
                maps.put(json_map.getInt(ID), json_map.getInt(LOCK));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            Log.e(TAG, "Save data has a syntax error: " + json, ex);

            // Initializing with empty stars if the game file is corrupt.
            // NOTE: In your game, you want to try recovering from the snapshot payload.
            missions.clear();
            maps.clear();
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Save data has an invalid number in it: " + json, ex);
        }
    }

    /**
     * Serializes this Progress to a JSON string.
     */
    @Override
    public String toString() {
        try {
            // MISSIONS
            JSONArray json_missions = new JSONArray();
            for (Integer missionId : this.missions.keySet()) {
                JSONObject mission = new JSONObject();
                mission.put(ID, missionId);
                mission.put(LOCK, this.missions.get(missionId)[0]);
                mission.put(SCORE, this.missions.get(missionId)[1]);
                json_missions.put(mission);
            }
            // MAPS
            JSONArray json_maps = new JSONArray();
            for (Integer mapId : this.maps.keySet()) {
                JSONObject map = new JSONObject();
                map.put(ID, mapId);
                map.put(LOCK, this.maps.get(mapId));
                json_maps.put(map);
            }
            JSONObject obj = new JSONObject();
            obj.put(VERSION, SERIAL_VERSION);
            obj.put(MISSIONS, missions);
            obj.put(MAPS, maps);
            return obj.toString();
        } catch (JSONException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error converting save data to JSON.", ex);
        }
    }

}