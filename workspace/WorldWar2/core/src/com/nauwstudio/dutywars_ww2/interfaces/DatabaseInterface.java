package com.nauwstudio.dutywars_ww2.interfaces;

import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.campaign.Mission;

import java.util.ArrayList;

import com.nauwstudio.dutywars_ww2.game.Map;

public interface DatabaseInterface {
	public Map getMap(int id);
	public ArrayList<Mission> getMissions();
	public void updateScore(int missionID, int score);
	public void unlockMap(int mapID);
	public void unlockMission(int missionID);
	public ArrayList<Map> getMapsByType(int type);
	// SAVE
	public int saveGame(Save save);
	public ArrayList<Save> getSavesSummary(DWController controller, int mode);
	public void getSaveComplete(Save save);
	public void deleteSave(int saveID);
	// PARAM
	public String getParam(String key);
	public void updateParam(String key, String value);
	// MAP EDITOR
    public boolean checkMapName(String name);
	public Map createMap(String name);
	public void deleteMap(int mapId);
	public boolean checkMapSaves(int mapId);
}
