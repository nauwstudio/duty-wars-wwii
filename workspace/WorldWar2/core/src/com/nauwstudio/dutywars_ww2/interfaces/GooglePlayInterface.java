package com.nauwstudio.dutywars_ww2.interfaces;

public interface GooglePlayInterface {
	public boolean isSignedIn();
	public void logIn();
	public void logOut();
	public void submitScore(int missionId, int score);
	public void getAllLeaderboards();
	public void getLeaderboard(int missionId);
}
