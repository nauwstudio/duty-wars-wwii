package com.nauwstudio.dutywars_ww2.interfaces;

import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.game.Map;

import java.util.ArrayList;

public interface WebInterface {
	public void facebook();
	public void googlePlay();
	public void web();
	public void help();
}
