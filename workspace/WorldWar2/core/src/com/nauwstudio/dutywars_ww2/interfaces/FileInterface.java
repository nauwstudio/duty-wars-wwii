package com.nauwstudio.dutywars_ww2.interfaces;

public interface FileInterface {
	public boolean checkPermission();
	public void getPermission();
	public String getExportFolder();
	public void importFile();
    public void showMessage(String message);
}
