package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Tile;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by nauwpie on 27/02/2018.
 */

public class MapFile {

    public static final String FOLDER = "maps" + File.separator;
    public static final String TEMP = FOLDER + "temp" + File.separator;

    public static final String XML_MAP = "map";
    public static final String XML_SIZE = "size";
    public static final String XML_PLAYERS = "players";
    public static final String XML_CAPITALS = "capitals";
    public static final String XML_CITIES = "cities";
    public static final String XML_FACTORIES = "factories";
    public static final String XML_AIRPORTS = "airports";
    public static final String XML_SHIPYARDS = "shipyards";
    public static final String XML_TILE_LAYER = "tileLayer";
    public static final String XML_TILE = "tile";
    public static final String XML_BUILDING_LAYER = "buildingLayer";
    public static final String XML_BUILDING = "building";
    public static final String XML_UNIT_LAYER = "unitLayer";
    public static final String XML_UNIT = "unit";
    public static final String XML_PASSENGER_LAYER = "passengerLayer";
    public static final String XML_PASSENGER = "passenger";
    public static final String XML_ROW = "row";
    public static final String XML_COL = "col";
    public static final String XML_TYPE = "type";
    public static final String XML_SUBTYPE = "subtype";
    public static final String XML_SEASON = "season";
    public static final String XML_PLAYER = "player";
    public static final String XML_ORIENTATION = "orientation";

    private int size;
    private int players;
    private int capitals;
    private int cities;
    private int factories;
    private int airports;
    private int shipyards;

    ArrayList<XmlReader.Element> tileLayer;
    ArrayList<XmlReader.Element> buildingLayer;
    ArrayList<XmlReader.Element> unitLayer;
    ArrayList<XmlReader.Element> passengerLayer;

    public MapFile(int type, String fileName) throws ParseException, IOException {
        // Getting file
        FileHandle file;
        if (type == Map.CUSTOM) {
            file = Gdx.files.local(fileName);
        } else {
            file = Gdx.files.internal(fileName);
        }
        XmlReader xml = new XmlReader();
        XmlReader.Element root = xml.parse(file);
        // Properties
        this.size = Integer.parseInt(root.getAttribute(XML_SIZE));
        this.players = Integer.parseInt(root.getAttribute(XML_PLAYERS));
        this.capitals = Integer.parseInt(root.getAttribute(XML_CAPITALS));
        this.cities = Integer.parseInt(root.getAttribute(XML_CITIES));
        this.factories = Integer.parseInt(root.getAttribute(XML_FACTORIES));
        this.airports = Integer.parseInt(root.getAttribute(XML_AIRPORTS));
        this.shipyards = Integer.parseInt(root.getAttribute(XML_SHIPYARDS));
        // Content
        XmlReader.Element tiles = root.getChild(0);
        tileLayer = new ArrayList<XmlReader.Element>();
        for (int i = 0; i < tiles.getChildCount(); i++) {
            tileLayer.add(tiles.getChild(i));
        }
        XmlReader.Element buildings = root.getChild(1);
        buildingLayer = new ArrayList<XmlReader.Element>();
        for (int i = 0; i < buildings.getChildCount(); i++) {
            buildingLayer.add(buildings.getChild(i));
        }
        XmlReader.Element units = root.getChild(2);
        unitLayer = new ArrayList<XmlReader.Element>();
        for (int i = 0; i < units.getChildCount(); i++) {
            unitLayer.add(units.getChild(i));
        }
        XmlReader.Element passengers = root.getChild(3);
        passengerLayer = new ArrayList<XmlReader.Element>();
        for (int i = 0; i < passengers.getChildCount(); i++) {
            passengerLayer.add(passengers.getChild(i));
        }
    }

    public int getSize() {
        return this.size;
    }

    public ArrayList<XmlReader.Element> getTileLayer() {
        return this.tileLayer;
    }

    public ArrayList<XmlReader.Element> getBuildingLayer() {
        return this.buildingLayer;
    }

    public ArrayList<XmlReader.Element> getUnitLayer() {
        return this.unitLayer;
    }

    public ArrayList<XmlReader.Element> getPassengerLayer() {
        return this.passengerLayer;
    }

    public void dispose() {
        this.tileLayer = null;
        this.buildingLayer = null;
        this.unitLayer = null;
        this.passengerLayer = null;
    }

    public static void create(String name, int size, int season, int type, int playersAmount) {
        StringWriter writer = new StringWriter();
        XmlWriter xml = new XmlWriter(writer);
        try {
            ArrayList<Integer> tempPlayers = new ArrayList<Integer>();
            tempPlayers.add(Army.GAI);
            int[] buildings = new int[]{0, 0, 0, 0, 0};
            xml.element(XML_MAP);
            xml.attribute(XML_SIZE, size).attribute(XML_PLAYERS, playersAmount).attribute(XML_CAPITALS, buildings[0]).attribute(XML_CITIES, buildings[1]).attribute(XML_FACTORIES, buildings[2]).attribute(XML_AIRPORTS, buildings[3]).attribute(XML_SHIPYARDS, buildings[4]);
            // GET CONTENT
            xml.element(XML_TILE_LAYER);
            for (int row = 0; row < size; row++) {
                for (int col = 0; col < size; col++) {
                    xml.element(XML_TILE).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_SEASON, season).attribute(XML_TYPE, type).attribute(XML_SUBTYPE, 0);
                    xml.pop();
                }
            }
            xml.pop();
            xml.element(XML_BUILDING_LAYER);
            xml.pop();
            xml.element(XML_UNIT_LAYER);
            xml.pop();
            xml.element(XML_PASSENGER_LAYER);
            xml.pop();
            xml.pop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String newFile = FOLDER + name + ".xml";
        FileHandle file = Gdx.files.local(newFile);
        file.writeString(writer.toString(), false);
        System.out.println("FILE CREATED : " + file.path());
    }

    public static void delete(String fileName) {
        String fileToDelete = FOLDER + fileName;
        FileHandle file = Gdx.files.local(fileToDelete);
        file.delete();
    }

    public static void deleteTemp(String fileName) {
        String fileToDelete = TEMP + fileName;
        FileHandle file = Gdx.files.local(fileToDelete);
        file.delete();
    }

    public static void tmxToXml(String tmxFile) {
        TiledMap tiledMap = new TmxMapLoader().load(tmxFile);
        TiledMapTileLayer tileLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
        TiledMapTileLayer buildingLayer = (TiledMapTileLayer) tiledMap.getLayers().get(1);
        TiledMapTileLayer unitLayer = (TiledMapTileLayer) tiledMap.getLayers().get(2);
        TiledMapTileLayer passengerLayer = (TiledMapTileLayer) tiledMap.getLayers().get(3);
        StringWriter writer = new StringWriter();
        XmlWriter xml = new XmlWriter(writer);
        try {
            int size = tileLayer.getWidth();
            int players = 0;
            int[] buildings = new int[]{0, 0, 0, 0, 0};
            xml.element(XML_MAP);
            // GET PROPERTIES
            for (int row = 0; row < tileLayer.getWidth(); row++) {
                for (int col = 0; col < tileLayer.getWidth(); col++) {
                    TiledMapTileLayer.Cell c = buildingLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    if (c != null) {
                        int player = Integer.parseInt("" + c.getTile().getProperties().get("player"));
                        int type = Integer.parseInt("" + c.getTile().getProperties().get("type"));
                        // Update properties
                        buildings[type - 1]++;
                        if (player > players) {
                            players = player;
                        }
                    }
                    TiledMapTileLayer.Cell c2 = unitLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    if (c2 != null) {
                        int player = Integer.parseInt("" + c2.getTile().getProperties().get("player"));
                        // Update properties
                        if (player > players) {
                            players = player;
                        }
                    }
                }
            }
            xml.attribute(XML_SIZE, size).attribute(XML_PLAYERS, players).attribute(XML_CAPITALS, buildings[0]).attribute(XML_CITIES, buildings[1]).attribute(XML_FACTORIES, buildings[2]).attribute(XML_AIRPORTS, buildings[3]).attribute(XML_SHIPYARDS, buildings[4]);
            // GET CONTENT
            xml.element(XML_TILE_LAYER);
            for (int row = 0; row < tileLayer.getWidth(); row++) {
                for (int col = 0; col < tileLayer.getWidth(); col++) {
                    TiledMapTileLayer.Cell c = tileLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    int season = Integer.parseInt("" + c.getTile().getProperties().get("season"));
                    int type = Integer.parseInt("" + c.getTile().getProperties().get("type"));
                    int subtype = Integer.parseInt("" + c.getTile().getProperties().get("subtype"));
                    xml.element(XML_TILE).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_SEASON, season).attribute(XML_TYPE, type).attribute(XML_SUBTYPE, subtype);
                    xml.pop();
                }
            }
            xml.pop();
            xml.element(XML_BUILDING_LAYER);
            for (int row = 0; row < tileLayer.getWidth(); row++) {
                for (int col = 0; col < tileLayer.getWidth(); col++) {
                    TiledMapTileLayer.Cell c = buildingLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    if (c != null) {
                        int player = Integer.parseInt("" + c.getTile().getProperties().get("player"));
                        int type = Integer.parseInt("" + c.getTile().getProperties().get("type"));
                        int subtype = Integer.parseInt("" + c.getTile().getProperties().get("subtype"));
                        xml.element(XML_BUILDING).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, player).attribute(XML_TYPE, type).attribute(XML_SUBTYPE, subtype);
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.element(XML_UNIT_LAYER);
            for (int row = 0; row < tileLayer.getWidth(); row++) {
                for (int col = 0; col < tileLayer.getWidth(); col++) {
                    TiledMapTileLayer.Cell c = unitLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    if (c != null) {
                        int player = Integer.parseInt("" + c.getTile().getProperties().get("player"));
                        int type = Integer.parseInt("" + c.getTile().getProperties().get("type"));
                        int orientation = Integer.parseInt("" + c.getTile().getProperties().get("orientation"));
                        xml.element(XML_UNIT).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, player).attribute(XML_TYPE, type).attribute(XML_ORIENTATION, orientation);
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.element(XML_PASSENGER_LAYER);
            for (int row = 0; row < tileLayer.getWidth(); row++) {
                for (int col = 0; col < tileLayer.getWidth(); col++) {
                    TiledMapTileLayer.Cell c = passengerLayer.getCell(tileLayer.getWidth() - col - 1, row);
                    if (c != null) {
                        int player = Integer.parseInt("" + c.getTile().getProperties().get("player"));
                        int type = Integer.parseInt("" + c.getTile().getProperties().get("type"));
                        int orientation = Integer.parseInt("" + c.getTile().getProperties().get("orientation"));
                        xml.element(XML_PASSENGER).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, player).attribute(XML_TYPE, type).attribute(XML_ORIENTATION, orientation);
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.pop();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String newFile = FOLDER + tmxFile.split("\\.")[0] + ".xml";
        FileHandle file = Gdx.files.internal(newFile);
        file.writeString(writer.toString(), false);

        tiledMap.dispose();
    }

    public static void mapToXml(Map map, Unit[][] unitLayer, Unit[][] passengerLayer) {
        StringWriter writer = new StringWriter();
        XmlWriter xml = new XmlWriter(writer);
        try {
            int size = map.getSize();
            int[] buildings = new int[]{0, 0, 0, 0, 0};
            xml.element(XML_MAP);
            // GET BUILDINGS PROPERTIES
            for (int row = 0; row < map.getSize(); row++) {
                for (int col = 0; col < map.getSize(); col++) {
                    Building b = map.getBuilding(row, col);
                    if (b != null) {
                        // Update properties
                        buildings[b.getType() - 1]++;
                    }
                }
            }
            xml.attribute(XML_SIZE, size).attribute(XML_PLAYERS, map.getPlayersAmount()).attribute(XML_CAPITALS, buildings[0]).attribute(XML_CITIES, buildings[1]).attribute(XML_FACTORIES, buildings[2]).attribute(XML_AIRPORTS, buildings[3]).attribute(XML_SHIPYARDS, buildings[4]);
            // GET CONTENT
            xml.element(XML_TILE_LAYER);
            for (int row = 0; row < map.getSize(); row++) {
                for (int col = 0; col < map.getSize(); col++) {
                    Tile t = map.getTile(row, col);
                    xml.element(XML_TILE).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_SEASON, t.getSeason().getType()).attribute(XML_TYPE, t.getType()).attribute(XML_SUBTYPE, t.getSubtype());
                    xml.pop();
                }
            }
            xml.pop();
            xml.element(XML_BUILDING_LAYER);
            for (int row = 0; row < map.getSize(); row++) {
                for (int col = 0; col < map.getSize(); col++) {
                    Building b = map.getBuilding(row, col);
                    if (b != null) {
                        xml.element(XML_BUILDING).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, b.getOwner().getArmy().getType()).attribute(XML_TYPE, b.getType()).attribute(XML_SUBTYPE, b.getSubtype());
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.element(XML_UNIT_LAYER);
            for (int row = 0; row < map.getSize(); row++) {
                for (int col = 0; col < map.getSize(); col++) {
                    Unit u = unitLayer[row][col];
                    if (u != null) {
                        xml.element(XML_UNIT).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, u.getOwner().getArmy().getType()).attribute(XML_TYPE, u.getType()).attribute(XML_ORIENTATION, u.getOrientationValue());
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.element(XML_PASSENGER_LAYER);
            for (int row = 0; row < map.getSize(); row++) {
                for (int col = 0; col < map.getSize(); col++) {
                    Unit p = passengerLayer[row][col];
                    if (p != null) {
                        xml.element(XML_PASSENGER).attribute(XML_ROW, row).attribute(XML_COL, col).attribute(XML_PLAYER, p.getOwner().getArmy().getType()).attribute(XML_TYPE, p.getType()).attribute(XML_ORIENTATION, p.getOrientationValue());
                        xml.pop();
                    }
                }
            }
            xml.pop();
            xml.pop();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String newFile = FOLDER + map.getFile();
        FileHandle file = Gdx.files.local(newFile);
        file.writeString(writer.toString(), false);
    }

    public static void exportMap(String folder, String fileName) {
        FileHandle fileToExport = Gdx.files.local(MapFile.TEMP + fileName);
        FileHandle newFile = Gdx.files.external(folder + fileName);
        fileToExport.copyTo(newFile);
    }

    public static void importMap(String fileName) {
        FileHandle fileToImport = Gdx.files.local(MapFile.TEMP + fileName);
        FileHandle newFile = Gdx.files.local(FOLDER + fileName);
        fileToImport.copyTo(newFile);
        deleteTemp(fileName);
    }

    public static boolean checkMapExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".")).equals(".xml");
    }

    public static boolean checkMapFormat(DWController controller, String mapFile) {
        try {
            // Getting file
            FileHandle file = Gdx.files.local(MapFile.TEMP + mapFile);
            XmlReader xml = new XmlReader();
            XmlReader.Element root = xml.parse(file);
            // Properties
            int size = Integer.parseInt(root.getAttribute(XML_SIZE));
            int players = Integer.parseInt(root.getAttribute(XML_PLAYERS));
            int capitals = Integer.parseInt(root.getAttribute(XML_CAPITALS));
            int cities = Integer.parseInt(root.getAttribute(XML_CITIES));
            int factories = Integer.parseInt(root.getAttribute(XML_FACTORIES));
            int airports = Integer.parseInt(root.getAttribute(XML_AIRPORTS));
            int shipyards = Integer.parseInt(root.getAttribute(XML_SHIPYARDS));
            // Content
            XmlReader.Element tiles = root.getChild(0);
            ArrayList<XmlReader.Element> tileLayer = new ArrayList<XmlReader.Element>();
            for (int i = 0; i < tiles.getChildCount(); i++) {
                tileLayer.add(tiles.getChild(i));
            }
            XmlReader.Element buildings = root.getChild(1);
            ArrayList<XmlReader.Element> buildingLayer = new ArrayList<XmlReader.Element>();
            for (int i = 0; i < buildings.getChildCount(); i++) {
                buildingLayer.add(buildings.getChild(i));
            }
            XmlReader.Element units = root.getChild(2);
            ArrayList<XmlReader.Element> unitLayer = new ArrayList<XmlReader.Element>();
            for (int i = 0; i < units.getChildCount(); i++) {
                unitLayer.add(units.getChild(i));
            }
            XmlReader.Element passengers = root.getChild(3);
            ArrayList<XmlReader.Element> passengerLayer = new ArrayList<XmlReader.Element>();
            for (int i = 0; i < passengers.getChildCount(); i++) {
                passengerLayer.add(passengers.getChild(i));
            }
            return true;
        } catch (Exception e) {
            controller.showMessage("Check format : " + e.toString());
            e.printStackTrace();
            return false;
        }
    }

    public int getPlayers() {
        return players;
    }

    public int getCapitals() {
        return capitals;
    }

    public int getCities() {
        return cities;
    }

    public int getFactories() {
        return factories;
    }

    public int getAirports() {
        return airports;
    }

    public int getShipyards() {
        return shipyards;
    }

}
