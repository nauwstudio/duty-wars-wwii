package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;

public class LayerVisibilityButton extends ButtonCircle {

    private LayerButton layerButton;

    public LayerVisibilityButton(Vector2 position, float width, TextureRegion texture, TextureRegion texture_pressed, LayerButton unitButton) {
        super(position, width, BUTTON_VISIBILITY, texture, texture_pressed, null);
        this.layerButton = unitButton;
    }

    public void render(SpriteBatch batch, MainAssets assets) {
        super.render(batch);
    }

    public LayerButton getLayerButton() {
        return this.layerButton;
    }

    @Override
    public boolean isUnitInfoButton() {
        return true;
    }
}
