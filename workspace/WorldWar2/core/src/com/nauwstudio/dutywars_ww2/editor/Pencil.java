package com.nauwstudio.dutywars_ww2.editor;

import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.game.Tile;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;

/**
 * Created by nauwpie on 24/07/2018.
 */

public class Pencil {

    private enum State {
        DRAW, CHOOSE, ERASE
    }

    private State state;
    private Layer tilelayer;
    private Layer buildingLayer;
    private Layer unitLayer;
    private Layer passengerLayer;
    private Layer currentLayer;
    private Tile currentTile;
    private Building currentBuilding;
    private Unit currentUnit;
    private Unit currentPassenger;
    private Unit.Orientation currentOrientation;

    public Pencil(MainAssets mainAssets, Tile defaultTile, Building defaultBuilding, Unit defaultUnit, Unit defaultPassenger) {
        this.tilelayer = new Layer(Layer.LayerType.TILE, mainAssets);
        this.buildingLayer = new Layer(Layer.LayerType.BUILDING, mainAssets);
        this.unitLayer = new Layer(Layer.LayerType.UNIT, mainAssets);
        this.passengerLayer = new Layer(Layer.LayerType.PASSENGER, mainAssets);
        draw();
        toTileLayer();
        setCurrentTile(defaultTile);
        System.out.println("DEFAULT TILE : " + defaultTile);
        setCurrentBuilding(defaultBuilding);
        System.out.println("DEFAULT BUILDING : " + defaultBuilding);
        setCurrentUnit(defaultUnit);
        this.currentOrientation = Unit.Orientation.SOUTH;
        System.out.println("DEFAULT UNIT : " + defaultUnit);
        setCurrentPassenger(defaultPassenger);
        System.out.println("DEFAULT PASSENGER : " + defaultPassenger);
    }

    public State getState() {
        return this.state;
    }

    public void draw() {
        this.state = State.DRAW;
    }

    public void choose() {
        this.state = State.CHOOSE;
    }

    public void erase() {
        this.state = State.ERASE;
    }

    public Layer getCurrentLayer() {
        return this.currentLayer;
    }

    public Layer getTileLayer() {
        return this.tilelayer;
    }

    public Layer getBuildingLayer() {
        return this.buildingLayer;
    }

    public Layer getUnitLayer() {
        return this.unitLayer;
    }

    public Layer getPassengerLayer() {
        return this.passengerLayer;
    }

    public void toTileLayer() {
        this.currentLayer = this.tilelayer;
    }

    public void toBuildingLayer() {
        this.currentLayer = this.buildingLayer;
    }

    public void toUnitLayer() {
        this.currentLayer = this.unitLayer;
    }

    public void toPassengerLayer() {
        this.currentLayer = this.passengerLayer;
    }

    public Tile getCurrentTile() {
        return currentTile;
    }

    public void setCurrentTile(Tile currentTile) {
        this.currentTile = currentTile;
    }

    public Building getCurrentBuilding() {
        return currentBuilding;
    }

    public void setCurrentBuilding(Building currentBuilding) {
        this.currentBuilding = currentBuilding;
    }

    public Unit getCurrentUnit() {
        return currentUnit;
    }

    public void setCurrentUnit(Unit currentUnit) {
        this.currentUnit = currentUnit;
    }

    public Unit getCurrentPassenger() {
        return currentPassenger;
    }

    public void setCurrentPassenger(Unit currentPassenger) {
        this.currentPassenger = currentPassenger;
    }

    public Unit.Orientation getCurrentOrientation() {
        return this.currentOrientation;
    }

    public void updateOrientation() {
        switch (this.currentOrientation) {
            case SOUTH:
                this.currentOrientation = Unit.Orientation.WEST;
                break;
            case WEST:
                this.currentOrientation = Unit.Orientation.NORTH;
                break;
            case NORTH:
                this.currentOrientation = Unit.Orientation.EAST;
                break;
            case EAST:
                this.currentOrientation = Unit.Orientation.SOUTH;
                break;
        }
        System.out.println(this.currentOrientation);
    }

    public boolean isChoose() {
        return this.state == State.CHOOSE;
    }

    public boolean isDraw() {
        return this.state == State.DRAW;
    }

    public boolean isErase() {
        return this.state == State.ERASE;
    }

}
