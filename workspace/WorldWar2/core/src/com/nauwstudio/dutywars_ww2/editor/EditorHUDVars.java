package com.nauwstudio.dutywars_ww2.editor;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class EditorHUDVars {

    private Editor editor;

    // FIXED BUTTONS
    float back_x, back_y;
    float save_x, save_y;
    float resize_button_x, resize_button_y;
    float layer_y, layer_tile_x, layer_building_x, layer_unit_x, layer_passenger_x;
    float state_y, state_draw_x, state_choose_x, state_erase_x;
    // DIALOG POSITION
    float choose_x, choose_y, choose_width, choose_height;
    float choose_close_x, choose_close_y;
    float square_title_x, square_title_y, square_title_width, square_title_height;
    // EDIT SQUARE
    float choose_buttons_x, choose_buttons_y, choose_buttons_width, choose_buttons_height;
    float choose_toggle_x, choose_toggle_y, choose_toggle_width, choose_toggle_height;
    float orientation_button_x, orientation_button_y;
    // EXIT DIALOG
    float exit_x, exit_y, exit_width, exit_height;
    float exit_close_x, exit_close_y;
    float exit_text_x, exit_text_y, exit_text_width, exit_text_height;
    float exit_confirm_x, exit_confirm_y;
    // CREATE MAP
    float resize_x, resize_y, resize_width, resize_height;
    float resize_cancel_x, resize_cancel_y;
    float resize_ok_x, resize_ok_y;
    float resize_title_x, resize_title_y, resize_title_width, resize_title_height;
    float size_title_x, size_title_y, size_title_width, size_title_height;
    float size_x, size_y, size_width, size_height;
    float offset_title_x, offset_title_y, offset_title_width, offset_title_height;
    float offset_x, offset_y, offset_width, offset_height;
	float player_title_x, player_title_y, player_title_width, player_title_height;
    float player_x, player_y, player_width, player_height;
    float tile_title_x, tile_title_y, tile_title_width, tile_title_height;
    float tile_x, tile_y, tile_width, tile_height;
    float map_x, map_y, map_width, map_height;

    public EditorHUDVars(Editor editor) {
        this.editor = editor;
        initHUDVariables();
    }

    public void initHUDVariables() {
        // BACK BUTTON POSITION
        back_x = Util.getCircleButtonSpace()/4f;
        back_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace()/4f;
        // SAVE MAP BUTTON POSITION
        save_x = Util.getScreenWidth() - Util.getCircleButtonSize() - Util.getCircleButtonSpace()/4f ;
        save_y = back_y;
        // resize MAP BUTTON POSITION
        resize_button_x = save_x ;
        resize_button_y = back_y - Util.getCircleButtonSize() - Util.getCircleButtonSpace()/4f;
        // PENCIL LAYER BUTTONS
        layer_y = Util.getCircleButtonSpace() / 4f;
        layer_building_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSpace() / 2f - Util.getCircleButtonSize();
        layer_unit_x = Util.getScreenWidth() / 2f + Util.getCircleButtonSpace() / 2f;
        layer_tile_x = layer_building_x - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
        layer_passenger_x = layer_unit_x + Util.getCircleButtonSpace() + Util.getCircleButtonSize();
        // PENCIL STATE BUTTONS
        state_y = layer_y + Util.getCircleButtonSize() + Util.getCircleButtonSpace()/2f;
        state_choose_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f;
        state_draw_x = state_choose_x - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
        state_erase_x = state_choose_x + Util.getCircleButtonSpace() + Util.getCircleButtonSize();

        float button_ratio = this.editor.getMainAssets().button_confirm.getRegionWidth() / (float) this.editor.getMainAssets().button_confirm.getRegionHeight();
        initChooseHUDVars();
        initExitHUDVars(button_ratio);
        initResizeVars(button_ratio);
    }

    private void initChooseHUDVars() {
        this.choose_width = Util.getScreenWidth() - Util.getCircleButtonSpace();
        this.choose_x = Util.getScreenWidth() / 2f - this.choose_width / 2f;
        this.choose_height = Util.getScreenHeight() - Util.getCircleButtonSpace();
        this.choose_y = Util.getScreenHeight() / 2f - this.choose_height / 2f;
        this.choose_close_x = this.choose_x + this.choose_width - 0.7f * Util.getCircleButtonSize();
        this.choose_close_y = this.choose_y + this.choose_height - 0.7f * Util.getCircleButtonSize();
        if (Util.isVertical()) {
            // TITLE
            this.square_title_width = this.choose_width;
            this.square_title_x = this.choose_x;
            this.square_title_height = this.choose_height / 8f;
            this.square_title_y = this.choose_y + this.choose_height - this.square_title_height;
            // CHOOSE
            this.choose_buttons_x = this.choose_x;
            this.choose_buttons_width = this.choose_width;
            this.choose_buttons_height = this.square_title_height;
            this.choose_buttons_y = this.square_title_y - this.choose_buttons_height;
            this.choose_toggle_x = this.choose_x;
            this.choose_toggle_width = this.choose_width;
            this.choose_toggle_height = this.choose_buttons_y - this.choose_y - Util.getCircleButtonSpace();
            this.choose_toggle_y = this.choose_y;
            // ORIENTATION
            this.orientation_button_y = this.choose_y + Util.getCircleButtonSpace()/4f;
            this.orientation_button_x = this.choose_x + this.choose_width/2f - Util.getMediumCircleButtonSize()/2f;
        } else {
            // TITLE
            this.square_title_width = this.choose_width;
            this.square_title_x = this.choose_x;
            this.square_title_height = this.choose_height / 6f;
            this.square_title_y = this.choose_y + this.choose_height - this.square_title_height;
            // CHOOSE
            this.choose_buttons_x = this.choose_x + Util.getCircleButtonSpace();
            this.choose_buttons_width = 2*this.choose_width/3f - Util.getCircleButtonSpace()*2;
            this.choose_buttons_height = this.square_title_height;
            this.choose_buttons_y = this.square_title_y - this.choose_buttons_height;
            this.choose_toggle_x = this.choose_x + Util.getCircleButtonSpace();
            this.choose_toggle_width = this.choose_width- Util.getCircleButtonSpace()*2;
            this.choose_toggle_height = this.choose_buttons_y - this.choose_y;
            this.choose_toggle_y = this.choose_y;
            this.orientation_button_y = this.choose_buttons_y;
            this.orientation_button_x = this.choose_buttons_x + this.choose_buttons_width + (this.choose_width- this.choose_buttons_width)/2f - Util.getMediumCircleButtonSize()/2f;
        }
    }

    private void initExitHUDVars(float button_ratio) {
        this.exit_width = Math.min(Util.getScreenHeight(), Util.getScreenWidth()) - Util.getCircleButtonSpace() / 2f;
        this.exit_height = Util.getCircleButtonSize()*2.5f;
        this.exit_x = Util.getScreenWidth() / 2f - this.exit_width / 2f;
        this.exit_y = Util.getScreenHeight() / 2f - this.exit_height / 2f;
        // Unlock close
        this.exit_close_x = this.exit_x + this.exit_width - 0.7f * Util.getCircleButtonSize();
        this.exit_close_y = this.exit_y + this.exit_height - 0.7f * Util.getCircleButtonSize();
        // Exit confirm
        this.exit_confirm_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        this.exit_confirm_y = exit_y + Util.getCircleButtonSpace()/4f;
        // Exit text
        this.exit_text_width = this.exit_width;
        this.exit_text_height = this.exit_close_y - this.exit_confirm_y + Util.getCircleButtonSpace()/4f + Util.getRectangleButtonHeight();
        this.exit_text_x = this.exit_x;
        this.exit_text_y = this.exit_y + Util.getCircleButtonSpace()/2f;
    }

    private void initResizeVars(float button_ratio) {
        this.resize_width = Util.getScreenWidth() - Util.getCircleButtonSpace();
        this.resize_x = Util.getScreenWidth() / 2f - this.resize_width / 2f;
        this.resize_height = Util.getScreenHeight() - Util.getCircleButtonSpace();
        this.resize_y = Util.getScreenHeight() / 2f - this.resize_height / 2f;
        this.resize_cancel_x = this.resize_x + this.resize_width - 0.7f * Util.getCircleButtonSize();
        this.resize_cancel_y = this.resize_y + this.resize_height - 0.7f * Util.getCircleButtonSize();
        this.resize_ok_x = this.resize_x + this.resize_width / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        this.resize_ok_y = this.resize_y + Util.getCircleButtonSpace() / 4f;
        if (Util.isVertical()) {
            this.resize_title_width = this.resize_width;
            this.resize_title_x = resize_x;
            this.resize_title_height = this.resize_height / 9f;
            this.resize_title_y = this.resize_y + this.resize_height - this.resize_title_height;
			this.player_title_x = this.resize_x + Util.getCircleButtonSpace()/4f;
            this.player_title_width = this.resize_width/2f - Util.getCircleButtonSpace()/2f;
            this.player_title_height = (this.resize_height - this.resize_title_height - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace()/4f)/11f;
            this.player_title_y = this.resize_title_y - this.player_title_height;
            this.player_x = this.player_title_x;
            this.player_width = this.player_title_width;
            this.player_height = this.player_title_height;
            this.player_y = this.player_title_y - this.player_height;
            this.size_title_x = this.resize_x + this.resize_width/2f + Util.getCircleButtonSpace()/4f;
            this.size_title_width = this.resize_width/2f - Util.getCircleButtonSpace()/2f;
            this.size_title_height = this.player_title_height;
            this.size_title_y = this.player_title_y;
            this.size_x = this.size_title_x;
            this.size_width = this.size_title_width;
            this.size_height = this.player_title_height;
            this.size_y = this.player_y;
            this.offset_title_x = this.resize_x + Util.getCircleButtonSpace()/4f;
            this.offset_title_width = this.resize_width - Util.getCircleButtonSpace()/2f;
            this.offset_title_height = this.player_title_height;
            this.offset_title_y = this.size_y - this.offset_title_height;
            this.offset_x = this.offset_title_x;
            this.offset_width = this.resize_width - Util.getCircleButtonSpace()/2f;
            this.offset_height = this.player_title_height;
            this.offset_y = this.offset_title_y - this.offset_height;
            this.tile_title_x = this.player_title_x;
            this.tile_title_width = this.resize_width - Util.getCircleButtonSpace()/2f;
            this.tile_title_height = this.player_title_height;
            this.tile_title_y = this.offset_y - this.tile_title_height;
            this.tile_x = this.resize_x + Util.getCircleButtonSpace();
            this.tile_width = this.resize_width - Util.getCircleButtonSpace()*2;
            this.tile_height = this.player_title_height;
            this.tile_y = this.tile_title_y - this.tile_height + Util.getCircleButtonSpace()/2f;
            this.map_x = this.resize_x + Util.getCircleButtonSpace()*1.5f;
            this.map_width = this.resize_width - Util.getCircleButtonSpace()*3;
            this.map_height = 5*this.player_title_height;
            this.map_y = this.tile_y - this.map_height;
        } else {
            this.resize_title_width = this.resize_width;
            this.resize_title_x = resize_x;
            this.resize_title_height = this.resize_height / 6f;
            this.resize_title_y = this.resize_y + this.resize_height - this.resize_title_height;
			this.player_title_x = this.resize_x + Util.getCircleButtonSpace()/4f;;
            this.player_title_width = this.resize_width/2f - Util.getCircleButtonSpace()/2f;
            this.player_title_height = (this.resize_height - this.resize_title_height - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace())/6f;
            this.player_title_y = this.resize_title_y - this.player_title_height;
            this.player_x = this.player_title_x + Util.getCircleButtonSpace()/4f;
            this.player_width = this.resize_width/2f - Util.getCircleButtonSpace();
            this.player_height = this.player_title_height;
            this.player_y = this.player_title_y - this.player_height;
            this.size_title_x = this.resize_x + Util.getCircleButtonSpace()/4f;
            this.size_title_width = this.resize_width/2f - Util.getCircleButtonSpace()/2f;
            this.size_title_height = this.player_title_height;
            this.size_title_y = this.player_y - this.size_title_height;
            this.size_x = this.size_title_x + Util.getCircleButtonSpace()/4f;
            this.size_width = this.resize_width/2f - Util.getCircleButtonSpace();
            this.size_height = this.player_title_height;
            this.size_y = this.size_title_y - this.size_height;
            this.offset_title_x = this.resize_x + Util.getCircleButtonSpace()/4f;;
            this.offset_title_width = this.size_title_width;
            this.offset_title_height = this.player_title_height;
            this.offset_title_y = this.size_y - this.offset_title_height ;
            this.offset_x = this.offset_title_x;
            this.offset_width = this.resize_width/2f - Util.getCircleButtonSpace()/2f;
            this.offset_height = this.player_title_height;
            this.offset_y = this.offset_title_y - this.offset_height;
            //
            this.tile_title_x = this.size_title_x + this.resize_width/2 + Util.getCircleButtonSpace()/4f;
            this.tile_title_width = this.size_title_width;
            this.tile_title_height = this.player_title_height;
            this.tile_title_y = this.resize_title_y - this.tile_title_height;
            this.tile_x = this.resize_x + resize_width/2f + Util.getCircleButtonSpace();
            this.tile_width = this.resize_width/2f - Util.getCircleButtonSpace()*2;
            this.tile_height = this.player_title_height;
            this.tile_y = this.tile_title_y - this.tile_height + Util.getCircleButtonSpace()/2f;
            this.map_x = this.resize_x + resize_width/2f + Util.getCircleButtonSpace()*2;
            this.map_width = this.resize_width/2f - Util.getCircleButtonSpace()*4;
            this.map_height = 4*this.player_title_height;
            this.map_y = this.tile_y - this.map_height - Util.getCircleButtonSpace()/2f;
        }
    }

}
