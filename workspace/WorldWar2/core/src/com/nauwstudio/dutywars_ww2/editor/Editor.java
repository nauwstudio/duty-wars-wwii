package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.EditorScreen;
import com.nauwstudio.dutywars_ww2.GameAssets;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.MapFile;
import com.nauwstudio.dutywars_ww2.ToggleButton;
import com.nauwstudio.dutywars_ww2.ToggleGroup;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.GameObject;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Season;
import com.nauwstudio.dutywars_ww2.game.Square;
import com.nauwstudio.dutywars_ww2.game.Tile;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.buildings.Capital;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class Editor {

    public enum EditorState {
        PENCIL, EXIT, RESIZE
    }

    public static final int TOGGLE_COLUMNS_VERTICAL = 4;
    public static final int TOGGLE_COLUMNS_HORIZONTAL = 7;

    private EditorScreen screen;

    private ArrayList<ButtonCircle> fixedButtons;
    private ArrayList<Button> buttons;
    private ArrayList<ToggleGroup> toggleGroups;

    private MainAssets mainAssets;
    private EditorHUDVars hudVars;

    private Map map;
    private ArrayList<Player> players;
    private Square[][] squareLayer;
    private com.nauwstudio.dutywars_ww2.game.units.Unit[][] unitLayer;
    private com.nauwstudio.dutywars_ww2.game.units.Unit[][] passengerLayer;

    private Pencil pencil;

    private boolean saved;
    private EditorState state;
    private int resizeSize;
    private int resizeOffsetX;
    private int resizeOffsetY;
    private int resizePlayers;
    private Map resizedMap;

    public Editor(EditorScreen screen, MainAssets assets, Map map, ArrayList<Player> players, int playersAmount) {
        this.screen = screen;
        this.mainAssets = assets;
        this.map = map;
        System.out.println("map name : " + map.getName());
        this.players = players;
		this.map.setPlayersAmount(playersAmount);
        this.hudVars = new EditorHUDVars(this);
        Tile defaultTile = new Tile(-1, -1, this.screen.getController().getSeasons().get(0), Tile.PLAIN, 0);
        Building defaultBuilding = Building.createBuilding(-1, -1, players.get(1), Building.CAPITAL, Capital.CLASSIC);
        Unit defaultUnit = Unit.createUnit(-1, -1, players.get(1), Unit.INFANTRY, Unit.Orientation.SOUTH);
        Unit defaultPassenger = Unit.createUnit(-1, -1, players.get(1), Unit.INFANTRY, Unit.Orientation.SOUTH);
        this.pencil = new Pencil(this.mainAssets, defaultTile, defaultBuilding, defaultUnit, defaultPassenger);
        initLayers();
        createButtons();
        this.saved = true;
        this.state = EditorState.PENCIL;
    }

    public void initLayers() {
        this.squareLayer = new Square[this.map.getSize()][this.map.getSize()];
        for (int row = 0; row < this.map.getSize(); row++) {
            for (int col = 0; col < this.map.getSize(); col++) {
                this.squareLayer[row][col] = new Square(row, col);
            }
        }
        try {
            MapFile mapFile = new MapFile(this.map.getType(), MapFile.FOLDER + this.map.getFile());
            initUnitLayer(mapFile.getUnitLayer(), mapFile.getPassengerLayer());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initUnitLayer(ArrayList<XmlReader.Element> unit_layer, ArrayList<XmlReader.Element> passenger_layer) {
        this.unitLayer = new Unit[this.map.getSize()][this.map.getSize()];
        for (XmlReader.Element e : unit_layer) {
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            Unit.Orientation orientation = Unit.valueToOrientation(Integer.parseInt("" + e.getAttribute(MapFile.XML_ORIENTATION)));
            this.unitLayer[row][col] = Unit.createUnit(row, col, this.players.get(player), type, orientation);
        }
        this.passengerLayer = new Unit[this.map.getSize()][this.map.getSize()];
        for (XmlReader.Element e : passenger_layer) {
            System.out.println(e);
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            Unit.Orientation orientation = Unit.valueToOrientation(Integer.parseInt("" + e.getAttribute(MapFile.XML_ORIENTATION)));
            this.passengerLayer[row][col] = Unit.createUnit(row, col, this.players.get(player), type, orientation);
        }
    }


    public void createButtons() {
        clearButtons();
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.fixedButtons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_back, mainAssets.button_back_pressed, mainAssets.button_back));
        // SAVE BUTTON
        Vector2 save_pos = new Vector2(hudVars.save_x, hudVars.save_y);
        this.fixedButtons.add(new ButtonCircle(save_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_SAVE, mainAssets.button_map_save, mainAssets.button_map_save_pressed, mainAssets.button_map_save_disabled));
        // PARAMS BUTTON
        Vector2 params_pos = new Vector2(hudVars.resize_button_x, hudVars.resize_button_y);
        this.fixedButtons.add(new ButtonCircle(params_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_PARAMS, mainAssets.button_map_params, mainAssets.button_map_params_pressed, mainAssets.button_map_params_disabled));

        createPencilButtons();
    }

    public void createPencilButtons() {
        // PENCIL STATE BUTTONS
        Vector2 state_draw_pos = new Vector2(hudVars.state_draw_x, hudVars.state_y);
        Vector2 state_choose_pos = new Vector2(hudVars.state_choose_x, hudVars.state_y);
        Vector2 state_erase_pos = new Vector2(hudVars.state_erase_x, hudVars.state_y);
        ButtonCircle drawButton = new ButtonCircle(state_draw_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_DRAW, mainAssets.button_map_pencil, mainAssets.button_map_pencil_pressed, mainAssets.button_map_pencil_disabled);
        ButtonCircle eraseButton = new ButtonCircle(state_erase_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ERASE, mainAssets.button_map_eraser, mainAssets.button_map_eraser_pressed, mainAssets.button_map_eraser_disabled);
        if (this.pencil.isErase()) {
            eraseButton.disable();
        } else {
            drawButton.disable();
            this.fixedButtons.add(new ButtonCircle(state_choose_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_CHOOSE, mainAssets.button_empty, mainAssets.button_empty_pressed, mainAssets.button_empty_disabled));
        }
        this.fixedButtons.add(drawButton);
        if (this.pencil.getCurrentLayer().getType() != Layer.LayerType.TILE) {
            this.fixedButtons.add(eraseButton);
        }
        // PENCIL LAYER BUTTONS
        Vector2 layer_tile_pos = new Vector2(hudVars.layer_tile_x, hudVars.layer_y);
        Vector2 layer_building_pos = new Vector2(hudVars.layer_building_x, hudVars.layer_y);
        Vector2 layer_unit_pos = new Vector2(hudVars.layer_unit_x, hudVars.layer_y);
        Vector2 layer_passenger_pos = new Vector2(hudVars.layer_passenger_x, hudVars.layer_y);
        ButtonCircle tileButton = new LayerButton(layer_tile_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_LAYER, this.pencil.getTileLayer(), mainAssets.button_classic, mainAssets.button_classic_pressed, mainAssets.button_classic_disabled, this.mainAssets);
        ButtonCircle buildingButton = new LayerButton(layer_building_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_LAYER, this.pencil.getBuildingLayer(), mainAssets.button_building, mainAssets.button_building_pressed, mainAssets.button_building_disabled, this.mainAssets);
        ButtonCircle unitButton = new LayerButton(layer_unit_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_LAYER, this.pencil.getUnitLayer(), mainAssets.button_unit, mainAssets.button_unit_pressed, mainAssets.button_unit_disabled, this.mainAssets);
        ButtonCircle passengerButton = new LayerButton(layer_passenger_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_LAYER, this.pencil.getPassengerLayer(), mainAssets.button_passenger, mainAssets.button_passenger_pressed, mainAssets.button_passenger_disabled, this.mainAssets);
        switch (this.pencil.getCurrentLayer().getType()) {
            case TILE:
                tileButton.disable();
                break;
            case BUILDING:
                buildingButton.disable();
                break;
            case UNIT:
                unitButton.disable();
                break;
            case PASSENGER:
                passengerButton.disable();
                break;
        }
        this.fixedButtons.add(tileButton);
        this.fixedButtons.add(buildingButton);
        this.fixedButtons.add(unitButton);
        this.fixedButtons.add(passengerButton);
    }

    public void createExitButtons() {
        clearButtons();
        // CLOSE BUTTON
        Vector2 back_pos = new Vector2(hudVars.exit_close_x, hudVars.exit_close_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
        // CONFIRM BUTTON
        Vector2 confirm_pos = new Vector2(hudVars.exit_confirm_x, hudVars.exit_confirm_y);
        this.buttons.add(new ButtonRectangle(confirm_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_CONFIRM, mainAssets.button_confirm, mainAssets.button_confirm_pressed, mainAssets.button_confirm));
    }

    public void createResizeButtons() {
        this.buttons = new ArrayList<Button>();
        this.toggleGroups = new ArrayList<ToggleGroup>();
        // BACK BUTTON
        Vector2 cancel_pos = new Vector2(hudVars.resize_cancel_x, hudVars.resize_cancel_y);
        this.buttons.add(new ButtonCircle(cancel_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
		 // PLAYER MINUS BUTTON
        Vector2 player_minus_pos = new Vector2(hudVars.player_x, hudVars.player_y);
        ButtonCircle minusPlayer = new ButtonCircle(player_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_PLAYER_MINUS, mainAssets.button_minus, mainAssets.button_minus_pressed, mainAssets.button_minus_disabled);
        if(this.map.getPlayersAmount() == Map.PLAYER_MIN) {
			minusPlayer.disable();
		}
        this.buttons.add(minusPlayer);
        // PLAYER PLUS BUTTON
        Vector2 player_plus_pos = new Vector2(hudVars.player_x + hudVars.player_width - Util.getSmallCircleButtonSize(), hudVars.player_y);
		ButtonCircle plusPlayer = new ButtonCircle(player_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_PLAYER_PLUS, mainAssets.button_plus, mainAssets.button_plus_pressed, mainAssets.button_minus_disabled);
		 if(this.map.getPlayersAmount() == Map.PLAYER_MAX) {
			plusPlayer.disable();
		}
        this.buttons.add(plusPlayer);
        // SIZE MINUS BUTTON
        Vector2 size_minus_pos = new Vector2(hudVars.size_x, hudVars.size_y);
        ButtonCircle size_minus = new ButtonCircle(size_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_SIZE_MINUS, mainAssets.button_minus, mainAssets.button_minus_pressed, mainAssets.button_minus_disabled);
        if (this.map.getSize() == Map.SIZE_MIN) {
            size_minus.disable();
        }
        this.buttons.add(size_minus);
        // SIZE PLUS BUTTON
        Vector2 size_plus_pos = new Vector2(hudVars.size_x + hudVars.size_width - Util.getSmallCircleButtonSize(), hudVars.size_y);
        ButtonCircle size_max = new ButtonCircle(size_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_SIZE_PLUS, mainAssets.button_plus, mainAssets.button_plus_pressed, mainAssets.button_minus_disabled);
        if (this.map.getSize() == Map.SIZE_MAX) {
            size_max.disable();
        }
        this.buttons.add(size_max);
        // OFFSETX BUTTONS
        Vector2 offsetx_minus_pos = new Vector2(hudVars.offset_x + Util.getCircleButtonSpace() / 4f, hudVars.offset_y);
        this.buttons.add(new ButtonCircle(offsetx_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_OFFSETX_MINUS, mainAssets.button_minus, mainAssets.button_minus_pressed, mainAssets.button_minus_disabled));
        Vector2 offsetx_plus_pos = new Vector2(hudVars.offset_x + (hudVars.offset_width / 2f) - Util.getSmallCircleButtonSize() - Util.getCircleButtonSpace() / 4f, hudVars.offset_y);
        this.buttons.add(new ButtonCircle(offsetx_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_OFFSETX_PLUS, mainAssets.button_plus, mainAssets.button_plus_pressed, mainAssets.button_minus_disabled));
        Vector2 offsety_minus_pos = new Vector2(hudVars.offset_x + (hudVars.offset_width / 2f) + Util.getCircleButtonSpace() / 4f, hudVars.offset_y);
        this.buttons.add(new ButtonCircle(offsety_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_OFFSETY_MINUS, mainAssets.button_minus, mainAssets.button_minus_pressed, mainAssets.button_minus_disabled));
        Vector2 offsety_plus_pos = new Vector2(hudVars.offset_x + hudVars.offset_width - Util.getSmallCircleButtonSize() - Util.getCircleButtonSpace() / 4f, hudVars.offset_y);
        this.buttons.add(new ButtonCircle(offsety_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_OFFSETY_PLUS, mainAssets.button_plus, mainAssets.button_plus_pressed, mainAssets.button_minus_disabled));
        // TILE TOGGLE GROUP
        ArrayList<GameObject> tiles = new ArrayList<GameObject>();
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(0), Tile.SEA, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(0), Tile.PLAIN, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(1), Tile.PLAIN, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(2), Tile.PLAIN, 0));
        Vector2 tile_pos = new Vector2(hudVars.tile_x, hudVars.tile_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.tile_width, hudVars.tile_height, ToggleGroup.TOGGLE_GROUP_TILE, tiles, 0, 4, Util.getCircleButtonSpace() / 4f, mainAssets));
        // CONFIRM BUTTON
        Vector2 confirm_pos = new Vector2(hudVars.resize_ok_x, hudVars.resize_ok_y);
        this.buttons.add(new ButtonRectangle(confirm_pos, Util.getCircleButtonSize(), ButtonRectangle.BUTTON_CONFIRM, mainAssets.button_confirm, mainAssets.button_confirm_pressed, mainAssets.button_confirm));
    }

    public void createChooseTileButton() {
        this.buttons = new ArrayList<Button>();
        this.toggleGroups = new ArrayList<ToggleGroup>();
        // CLOSE BUTTON
        Vector2 back_pos = new Vector2(hudVars.choose_close_x, hudVars.choose_close_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
        // SEASON BUTTONS
        Vector2 classic_pos = new Vector2(hudVars.choose_buttons_x + hudVars.choose_buttons_width / 6f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 winter_pos = new Vector2(hudVars.choose_buttons_x + 3 * hudVars.choose_buttons_width / 6f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 desert_pos = new Vector2(hudVars.choose_buttons_x + 5 * hudVars.choose_buttons_width / 6f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        ButtonCircle classic = new ButtonCircle(classic_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_SEASON + Season.CLA, mainAssets.button_classic, mainAssets.button_classic_pressed, mainAssets.button_classic_disabled);
        ButtonCircle winter = new ButtonCircle(winter_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_SEASON + Season.WIN, mainAssets.button_winter, mainAssets.button_winter_pressed, mainAssets.button_winter_disabled);
        ButtonCircle desert = new ButtonCircle(desert_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_SEASON + Season.DES, mainAssets.button_desert, mainAssets.button_desert_pressed, mainAssets.button_desert_disabled);
        switch (this.pencil.getCurrentTile().getSeason().getType()) {
            case Season.CLA:
                classic.disable();
                break;
            case Season.WIN:
                winter.disable();
                break;
            case Season.DES:
                desert.disable();
                break;
        }
        this.buttons.add(classic);
        this.buttons.add(winter);
        this.buttons.add(desert);
        if (Util.isVertical()) {
            initTileButtons(this.pencil.getCurrentTile().getSeason(), getCheckedTilePosition(this.pencil.getCurrentTile()), TOGGLE_COLUMNS_VERTICAL);
        } else {
            initTileButtons(this.pencil.getCurrentTile().getSeason(), getCheckedTilePosition(this.pencil.getCurrentTile()), TOGGLE_COLUMNS_HORIZONTAL);
        }
        // Orientation button
        Vector2 orientation_pos = new Vector2(hudVars.orientation_button_x, hudVars.orientation_button_y);
        ButtonCircle orientationButton = new ButtonCircle(orientation_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ORIENTATION, mainAssets.button_orientation, mainAssets.button_orientation_pressed, mainAssets.button_orientation_disabled);
        orientationButton.disable();
        this.buttons.add(orientationButton);
    }

    private void initTileButtons(Season season, int position, int columns) {
        ArrayList<GameObject> tiles = new ArrayList<GameObject>();
        // LINE 1
        tiles.add(new Tile(-1, -1, season, Tile.PLAIN, 0));
        tiles.add(new Tile(-1, -1, season, Tile.FOREST, 0));
        tiles.add(new Tile(-1, -1, season, Tile.SAND, 0));
        tiles.add(new Tile(-1, -1, season, Tile.SEA, 0));
        // LINE 2
        tiles.add(new Tile(-1, -1, season, Tile.MOUNTAINS, 0));
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_SN));
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_WE));
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_NE));
        // LINE 3
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_NW));
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_SE));
        tiles.add(new Tile(-1, -1, season, Tile.RIVER, Tile.RIVER_SW));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_SN));
        // LINE 4
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_WE));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_NE));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_NW));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_SE));
        // LINE 5
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.ROAD_SW));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.BRIDGE_SN));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.BRIDGE_WE));
        tiles.add(new Tile(-1, -1, season, Tile.ROAD, Tile.CROSSROAD));
        Vector2 tile_pos = new Vector2(hudVars.choose_toggle_x, hudVars.choose_toggle_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.choose_toggle_width, hudVars.choose_toggle_height, ToggleGroup.TOGGLE_GROUP_TILE, tiles, position, columns, Util.getCircleButtonSpace() / 4f, mainAssets));

    }

    public void createChooseBuildingButton() {
        this.buttons = new ArrayList<Button>();
        // CLOSE BUTTON
        Vector2 back_pos = new Vector2(hudVars.choose_close_x, hudVars.choose_close_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
        // ARMY BUTTONS
        Vector2 p0_pos = new Vector2(hudVars.choose_buttons_x + hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p1_pos = new Vector2(hudVars.choose_buttons_x + 3 * hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p2_pos = new Vector2(hudVars.choose_buttons_x + 5 * hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p3_pos = new Vector2(hudVars.choose_buttons_x + 7 * hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p4_pos = new Vector2(hudVars.choose_buttons_x + 9 * hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p5_pos = new Vector2(hudVars.choose_buttons_x + 11 * hudVars.choose_buttons_width / 12f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        ButtonCircle p0 = new ButtonCircle(p0_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GAI, mainAssets.button_p0, mainAssets.button_p0_pressed, mainAssets.button_p0_disabled);
		ButtonCircle p1 = new ButtonCircle(p1_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USA, mainAssets.button_p1, mainAssets.button_p1_pressed, mainAssets.button_p1_disabled);
        ButtonCircle p2 = new ButtonCircle(p2_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GER, mainAssets.button_p2, mainAssets.button_p2_pressed, mainAssets.button_p2_disabled);
        ButtonCircle p3 = new ButtonCircle(p3_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USS, mainAssets.button_p3, mainAssets.button_p3_pressed, mainAssets.button_p3_disabled);
        ButtonCircle p4 = new ButtonCircle(p4_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.JAP, mainAssets.button_p4, mainAssets.button_p4_pressed, mainAssets.button_p4_disabled);
        ButtonCircle p5 = new ButtonCircle(p5_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GBR, mainAssets.button_p5, mainAssets.button_p5_pressed, mainAssets.button_p5_disabled);
        switch (this.pencil.getCurrentBuilding().getOwner().getArmy().getType()) {
            case Army.USA:
                p1.disable();
                break;
            case Army.GER:
                p2.disable();
                break;
            case Army.USS:
                p3.disable();
                break;
            case Army.JAP:
                p4.disable();
                break;
            case Army.GBR:
                p5.disable();
                break;
            case Army.GAI:
                p0.disable();
                break;
        }
		this.buttons.add(p0);
        this.buttons.add(p1);
        this.buttons.add(p2);
		if(this.map.getPlayersAmount() >= 3) {
			this.buttons.add(p3);
			if(this.map.getPlayersAmount() >= 4) {
				this.buttons.add(p4);
				if(this.map.getPlayersAmount() >= 5) {
					this.buttons.add(p5);
				}
			}
		}
        if (Util.isVertical()) {
            initBuildingButtons(this.pencil.getCurrentBuilding().getOwner(), getCheckedBuildingPosition(this.pencil.getCurrentBuilding()), TOGGLE_COLUMNS_VERTICAL);
        } else {
            initBuildingButtons(this.pencil.getCurrentBuilding().getOwner(), getCheckedBuildingPosition(this.pencil.getCurrentBuilding()), TOGGLE_COLUMNS_HORIZONTAL);

        }
        // Orientation button
        Vector2 orientation_pos = new Vector2(hudVars.orientation_button_x, hudVars.orientation_button_y);
        ButtonCircle orientationButton = new ButtonCircle(orientation_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ORIENTATION, mainAssets.button_orientation, mainAssets.button_orientation_pressed, mainAssets.button_orientation_disabled);
        orientationButton.disable();
        this.buttons.add(orientationButton);
    }

    private void initBuildingButtons(Player player, int position, int columns) {
        this.toggleGroups = new ArrayList<ToggleGroup>();
        ArrayList<GameObject> buildings = new ArrayList<GameObject>();
        // LINE 1
        buildings.add(Building.createBuilding(-1, -1, player, Building.CAPITAL, Capital.CLASSIC));
        buildings.add(Building.createBuilding(-1, -1, player, Building.CITY, 0));
        buildings.add(Building.createBuilding(-1, -1, player, Building.FACTORY, 0));
        buildings.add(Building.createBuilding(-1, -1, player, Building.AIRPORT, 0));
        // LINE 2
        buildings.add(Building.createBuilding(-1, -1, player, Building.SHIPYARD, 0));
        buildings.add(Building.createBuilding(-1, -1, player, Building.CAPITAL, Capital.EIFFEL));
        buildings.add(Building.createBuilding(-1, -1, player, Building.CAPITAL, Capital.BRANDENBURG));
        buildings.add(Building.createBuilding(-1, -1, player, Building.CAPITAL, Capital.COLISSEUM));
        // LINE 3
        buildings.add(Building.createBuilding(-1, -1, player, Building.CAPITAL, Capital.BIG_BEN));
        Vector2 tile_pos = new Vector2(hudVars.choose_toggle_x, hudVars.choose_toggle_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.choose_toggle_width, hudVars.choose_toggle_height, ToggleGroup.TOGGLE_GROUP_BUILDING, buildings, position, columns, Util.getCircleButtonSpace() / 4f, mainAssets));
    }

    public void createChooseUnitButton() {
        this.buttons = new ArrayList<Button>();
        // CLOSE BUTTON
        Vector2 back_pos = new Vector2(hudVars.choose_close_x, hudVars.choose_close_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
        // ARMY BUTTONS
        Vector2 p1_pos = new Vector2(hudVars.choose_buttons_x + hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p2_pos = new Vector2(hudVars.choose_buttons_x + 3 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p3_pos = new Vector2(hudVars.choose_buttons_x + 5 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p4_pos = new Vector2(hudVars.choose_buttons_x + 7 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p5_pos = new Vector2(hudVars.choose_buttons_x + 9 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        ButtonCircle p1 = new ButtonCircle(p1_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USA, mainAssets.button_p1, mainAssets.button_p1_pressed, mainAssets.button_p1_disabled);
        ButtonCircle p2 = new ButtonCircle(p2_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GER, mainAssets.button_p2, mainAssets.button_p2_pressed, mainAssets.button_p2_disabled);
        ButtonCircle p3 = new ButtonCircle(p3_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USS, mainAssets.button_p3, mainAssets.button_p3_pressed, mainAssets.button_p3_disabled);
        ButtonCircle p4 = new ButtonCircle(p4_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.JAP, mainAssets.button_p4, mainAssets.button_p4_pressed, mainAssets.button_p4_disabled);
        ButtonCircle p5 = new ButtonCircle(p5_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GBR, mainAssets.button_p5, mainAssets.button_p5_pressed, mainAssets.button_p5_disabled);
        switch (this.pencil.getCurrentUnit().getOwner().getArmy().getType()) {
            case Army.USA:
                p1.disable();
                break;
            case Army.GER:
                p2.disable();
                break;
            case Army.USS:
                p3.disable();
                break;
            case Army.JAP:
                p4.disable();
                break;
            case Army.GBR:
                p5.disable();
                break;
        }
        this.buttons.add(p1);
        this.buttons.add(p2);
        if(this.map.getPlayersAmount() >= 3) {
			this.buttons.add(p3);
			if(this.map.getPlayersAmount() >= 4) {
				this.buttons.add(p4);
				if(this.map.getPlayersAmount() >= 5) {
					this.buttons.add(p5);
				}
			}
		}
        if (Util.isVertical()) {
            initUnitButtons(this.pencil.getCurrentUnit().getOwner(), getCheckedUnitPosition(this.pencil.getCurrentUnit()), TOGGLE_COLUMNS_VERTICAL);
        } else {
            initUnitButtons(this.pencil.getCurrentUnit().getOwner(), getCheckedUnitPosition(this.pencil.getCurrentUnit()), TOGGLE_COLUMNS_HORIZONTAL);
        }
        // Orientation button
        Vector2 orientation_pos = new Vector2(hudVars.orientation_button_x, hudVars.orientation_button_y);
        ButtonCircle orientationButton = new ButtonCircle(orientation_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ORIENTATION, mainAssets.button_orientation, mainAssets.button_orientation_pressed, mainAssets.button_orientation_disabled);
        this.buttons.add(orientationButton);
    }

    private void initUnitButtons(Player player, int position, int columns) {
        this.toggleGroups = new ArrayList<ToggleGroup>();
        ArrayList<GameObject> units = new ArrayList<GameObject>();
        // LINE 1
        units.add(Unit.createUnit(-1, -1, player, Unit.INFANTRY, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BAZOOKA, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.SNIPER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.JEEP, this.pencil.getCurrentOrientation()));
        // LINE 2
        units.add(Unit.createUnit(-1, -1, player, Unit.LIGHT_TANK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.HEAVY_TANK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.TRUCK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.ARTILLERY, this.pencil.getCurrentOrientation()));
        // LINE 3
        units.add(Unit.createUnit(-1, -1, player, Unit.HALFTRACK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BUNKER_MACHINE_GUN, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BUNKER_ARTILLERY, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.FLAK, this.pencil.getCurrentOrientation()));
        // LINE 4
        units.add(Unit.createUnit(-1, -1, player, Unit.FIGHTER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BOMBER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.TRANSPORTER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BATTLESHIP, this.pencil.getCurrentOrientation()));
        // LINE 5
        units.add(Unit.createUnit(-1, -1, player, Unit.DESTROYER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.LANDER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.AIRCRAFT_CARRIER, this.pencil.getCurrentOrientation()));
        Vector2 tile_pos = new Vector2(hudVars.choose_toggle_x, hudVars.choose_toggle_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.choose_toggle_width, hudVars.choose_toggle_height, ToggleGroup.TOGGLE_GROUP_BUILDING, units, position, columns, Util.getCircleButtonSpace() / 4f, mainAssets));

    }

    public void createChoosePassengerButton() {
        this.buttons = new ArrayList<Button>();
        // CLOSE BUTTON
        Vector2 back_pos = new Vector2(hudVars.choose_close_x, hudVars.choose_close_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, mainAssets.button_close, mainAssets.button_close_pressed, mainAssets.button_close));
        // ARMY BUTTONS
        Vector2 p1_pos = new Vector2(hudVars.choose_buttons_x + hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p2_pos = new Vector2(hudVars.choose_buttons_x + 3 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p3_pos = new Vector2(hudVars.choose_buttons_x + 5 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p4_pos = new Vector2(hudVars.choose_buttons_x + 7 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        Vector2 p5_pos = new Vector2(hudVars.choose_buttons_x + 9 * hudVars.choose_buttons_width / 10f - Util.getMediumCircleButtonSize() / 2f, hudVars.choose_buttons_y);
        ButtonCircle p1 = new ButtonCircle(p1_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USA, mainAssets.button_p1, mainAssets.button_p1_pressed, mainAssets.button_p1_disabled);
        ButtonCircle p2 = new ButtonCircle(p2_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GER, mainAssets.button_p2, mainAssets.button_p2_pressed, mainAssets.button_p2_disabled);
        ButtonCircle p3 = new ButtonCircle(p3_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.USS, mainAssets.button_p3, mainAssets.button_p3_pressed, mainAssets.button_p3_disabled);
        ButtonCircle p4 = new ButtonCircle(p4_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.JAP, mainAssets.button_p4, mainAssets.button_p4_pressed, mainAssets.button_p4_disabled);
        ButtonCircle p5 = new ButtonCircle(p5_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ARMY + Army.GBR, mainAssets.button_p5, mainAssets.button_p5_pressed, mainAssets.button_p5_disabled);
        switch (this.pencil.getCurrentPassenger().getOwner().getArmy().getType()) {
            case Army.USA:
                p1.disable();
                break;
            case Army.GER:
                p2.disable();
                break;
            case Army.USS:
                p3.disable();
                break;
            case Army.JAP:
                p4.disable();
                break;
            case Army.GBR:
                p5.disable();
                break;
        }
        this.buttons.add(p1);
        this.buttons.add(p2);
        if(this.map.getPlayersAmount() >= 3) {
			this.buttons.add(p3);
			if(this.map.getPlayersAmount() >= 4) {
				this.buttons.add(p4);
				if(this.map.getPlayersAmount() >= 5) {
					this.buttons.add(p5);
				}
			}
		}
        if (Util.isVertical()) {
            initPassengerButtons(this.pencil.getCurrentPassenger().getOwner(), getCheckedPassengerPosition(this.pencil.getCurrentPassenger()), TOGGLE_COLUMNS_VERTICAL);
        } else {
            initPassengerButtons(this.pencil.getCurrentPassenger().getOwner(), getCheckedPassengerPosition(this.pencil.getCurrentPassenger()), TOGGLE_COLUMNS_HORIZONTAL);
        }
        // Orientation button
        Vector2 orientation_pos = new Vector2(hudVars.orientation_button_x, hudVars.orientation_button_y);
        ButtonCircle orientationButton = new ButtonCircle(orientation_pos, Util.getMediumCircleButtonSize(), ButtonCircle.BUTTON_ORIENTATION, mainAssets.button_orientation, mainAssets.button_orientation_pressed, mainAssets.button_orientation_disabled);
        this.buttons.add(orientationButton);
    }

    private void initPassengerButtons(Player player, int position, int columns) {
        this.toggleGroups = new ArrayList<ToggleGroup>();
        ArrayList<GameObject> units = new ArrayList<GameObject>();
        // LINE 1
        units.add(Unit.createUnit(-1, -1, player, Unit.INFANTRY, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BAZOOKA, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.SNIPER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.JEEP, this.pencil.getCurrentOrientation()));
        // LINE 2
        units.add(Unit.createUnit(-1, -1, player, Unit.LIGHT_TANK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.HEAVY_TANK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.TRUCK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.ARTILLERY, this.pencil.getCurrentOrientation()));
        // LINE 3
        units.add(Unit.createUnit(-1, -1, player, Unit.HALFTRACK, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.FIGHTER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.BOMBER, this.pencil.getCurrentOrientation()));
        units.add(Unit.createUnit(-1, -1, player, Unit.TRANSPORTER, this.pencil.getCurrentOrientation()));


        Vector2 tile_pos = new Vector2(hudVars.choose_toggle_x, hudVars.choose_toggle_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.choose_toggle_width, hudVars.choose_toggle_height, ToggleGroup.TOGGLE_GROUP_BUILDING, units, position, columns, Util.getCircleButtonSpace() / 4f, mainAssets));

    }


    public void clearButtons() {
        this.fixedButtons = new ArrayList<ButtonCircle>();
        this.buttons = new ArrayList<Button>();
        this.toggleGroups = new ArrayList<ToggleGroup>();
    }

    public Button getTouchedButton(float x, float y) {
        if (this.buttons != null && !this.buttons.isEmpty()) {
            for (Button button : this.buttons) {
                if (button.isTouched(x, y)) {
                    return button;
                }
            }
        }
        // No dialog opened => can touch each buttons
        else {
            // Check fixed buttons
            if (fixedButtons != null && !fixedButtons.isEmpty()) {
                for (ButtonCircle button : fixedButtons) {
                    if (button.isLayerButton() && ((LayerButton) button).getVisibilityButton().isTouched(x, y)) {
                        return ((LayerButton) button).getVisibilityButton();
                    } else if (button.isTouched(x, y)) {
                        return button;
                    }
                }
            }
        }
        return null;
    }

    public void touchButton(Button button) {
        System.out.println("TOUCH : " + button.getTag());
        if (button.isEnabled()) {
            // BACK BUTTON
            if (button.getTag().equals(ButtonCircle.BUTTON_BACK)) {
                back();
            }
            // SAVE BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_SAVE)) {
                save();
            }
            // SETTINGS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PARAMS)) {
                openResizeDialog();
            }
            // CONFIRM BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_CONFIRM)) {
                if (this.isExit()) {
                    this.screen.back();
                } else if (this.isResize()) {
                    updateSize();
                    this.saved = false;
                }
            }
            // PARAMS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PARAMS)) {
                System.out.println("PARAMS");
            }
            // LAYER BUTTONS
            else if (button.getTag().equals(ButtonCircle.BUTTON_LAYER)) {
                switch (((LayerButton) button).getLayer().getType()) {
                    case TILE:
                        this.pencil.toTileLayer();
                        break;
                    case BUILDING:
                        this.pencil.toBuildingLayer();
                        break;
                    case UNIT:
                        this.pencil.toUnitLayer();
                        break;
                    case PASSENGER:
                        this.pencil.toPassengerLayer();
                        break;
                }
                createButtons();
            } else if (button.getTag().equals(ButtonCircle.BUTTON_VISIBILITY)) {
                Layer layer = ((LayerVisibilityButton) button).getLayerButton().getLayer();
                if (layer.isVisibile()) {
                    layer.hide();
                } else {
                    layer.show();
                }
                createButtons();
            }
            // DRAW BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_DRAW)) {
                touchDraw();
            }
            // CHOOSE BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_CHOOSE)) {
                touchChoose();
            }
            // ERASE BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_ERASE)) {
                touchErase();
            }
			// PLAYER PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PLAYER_PLUS)) {
                touchPlayerPlus();
            }
            // PLAYER MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PLAYER_MINUS)) {
                touchPlayerMinus();
            }
            // SIZE PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_SIZE_PLUS)) {
                touchPlus();
            }
            // SIZE MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_SIZE_MINUS)) {
                touchMinus();
            }
            // OFFSETX PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_OFFSETX_PLUS)) {
                touchOffsetXPlus();
            }
            // OFFSETX MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_OFFSETX_MINUS)) {
                touchOffsetXMinus();
            }
            // OFFSETY PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_OFFSETY_PLUS)) {
                touchOffsetYPlus();
            }
            // OFFSETY MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_OFFSETY_MINUS)) {
                touchOffsetYMinus();
            }
            // ORIENTATION BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_ORIENTATION)) {
                touchOrientation();
            }
            // ARMY BUTTON
            else if (button.getTag().substring(0, 5).equals(ButtonCircle.BUTTON_ARMY)) {
                touchArmy(Integer.parseInt(button.getTag().substring(5, 6)));
            }
            // SEASON BUTTON
            else if (button.getTag().substring(0, 7).equals(ButtonCircle.BUTTON_SEASON)) {
                touchSeason(Integer.parseInt(button.getTag().substring(7, 8)));
            }
        }
    }

    public void touchDraw() {
        this.pencil.draw();
        createButtons();
    }

    public void touchChoose() {
        this.pencil.choose();
        openChooseMenu();
    }

    public void touchErase() {
        this.pencil.erase();
        createButtons();
    }

    public void touchArmy(int army) {
        System.out.println("army : " + army);
        switch (this.pencil.getCurrentLayer().getType()) {
            case BUILDING:
                this.pencil.setCurrentBuilding(Building.createBuilding(-1, -1, players.get(army), this.pencil.getCurrentBuilding().getType(), this.pencil.getCurrentBuilding().getSubtype()));
                createChooseBuildingButton();
                break;
            case UNIT:
                this.pencil.setCurrentUnit(Unit.createUnit(-1, -1, players.get(army), this.pencil.getCurrentUnit().getType(), this.pencil.getCurrentOrientation()));
                createChooseUnitButton();
                break;
            case PASSENGER:
                this.pencil.setCurrentPassenger(Unit.createUnit(-1, -1, players.get(army), this.pencil.getCurrentPassenger().getType(), this.pencil.getCurrentOrientation()));
                createChoosePassengerButton();
                break;
        }
    }

    public void touchSeason(int season) {
        System.out.println("season : " + season);
        this.pencil.setCurrentTile(new Tile(-1, -1, this.screen.getController().getSeasons().get(season - 1), this.pencil.getCurrentTile().getType(), this.pencil.getCurrentTile().getSubtype()));
        createChooseTileButton();
    }

    public void touchOrientation() {
        this.pencil.updateOrientation();
        openChooseMenu();
    }

    public void updateMapName(String text) {
        if (!this.map.getName().equals(text) && !text.replaceAll(" ", "").isEmpty()) {
            // Map name doesn't exists
            if (this.screen.getController().getDatabaseInterface().checkMapName(text)) {
                MapFile.delete(this.map.getFile());
                this.map.updateName(text);
            }
        }
    }
	
	public void touchPlayerMinus() {
		this.resizePlayers = Math.max(Map.PLAYER_MIN, this.resizePlayers - 1);
        if (this.resizePlayers == Map.PLAYER_MIN) {
            disableButton(ButtonCircle.BUTTON_PLAYER_MINUS);
        }
        if (this.resizePlayers < Map.PLAYER_MAX) {
            enableButton(ButtonCircle.BUTTON_PLAYER_PLUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.resizePlayers);

	}

    public void touchPlayerPlus() {
        this.resizePlayers = Math.min(Map.PLAYER_MAX, this.resizePlayers + 1);
        if (this.resizePlayers == Map.PLAYER_MAX) {
            disableButton(ButtonCircle.BUTTON_PLAYER_PLUS);
        }
        if (this.resizePlayers > Map.PLAYER_MIN) {
            enableButton(ButtonCircle.BUTTON_PLAYER_MINUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.resizePlayers);
    }

    public void touchMinus() {
        this.resizeSize = Math.max(Map.SIZE_MIN, this.resizeSize - 1);
        if (this.resizeSize == Map.SIZE_MIN) {
            disableButton(ButtonCircle.BUTTON_SIZE_MINUS);
        }
        if (this.resizeSize < Map.SIZE_MAX) {
            enableButton(ButtonCircle.BUTTON_SIZE_PLUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.resizedMap.getPlayersAmount());
    }

    public void touchPlus() {
        this.resizeSize = Math.min(Map.SIZE_MAX, this.resizeSize + 1);
        if (this.resizeSize == Map.SIZE_MAX) {
            disableButton(ButtonCircle.BUTTON_SIZE_PLUS);
        }
        if (this.resizeSize > Map.SIZE_MIN) {
            enableButton(ButtonCircle.BUTTON_SIZE_MINUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.resizedMap.getPlayersAmount());
    }

    public void touchOffsetXPlus() {
        this.resizeOffsetX = Math.min(this.resizeSize - 1, this.resizeOffsetX + 1);
        System.out.println("TEST : " + this.resizeOffsetX);
        if (this.resizeOffsetX == this.resizeSize - 1) {
            disableButton(ButtonCircle.BUTTON_OFFSETX_PLUS);
        }
        if (this.resizeOffsetX > -(this.resizeSize - 1)) {
            enableButton(ButtonCircle.BUTTON_OFFSETX_MINUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.resizedMap.getPlayersAmount());
    }

    public void touchOffsetXMinus() {
        this.resizeOffsetX = Math.max(-(this.resizeSize - 1), this.resizeOffsetX - 1);
        if (this.resizeOffsetX == -(this.resizeSize - 1)) {
            disableButton(ButtonCircle.BUTTON_OFFSETX_MINUS);
        }
        if (this.resizeOffsetX < this.resizeSize - 1) {
            enableButton(ButtonCircle.BUTTON_OFFSETX_PLUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(),this.resizedMap.getPlayersAmount());
    }

    public void touchOffsetYPlus() {
        this.resizeOffsetY = Math.min(this.resizeSize - 1, this.resizeOffsetY + 1);
        if (this.resizeOffsetY == this.resizeSize - 1) {
            disableButton(ButtonCircle.BUTTON_OFFSETY_PLUS);
        }
        if (this.resizeOffsetY > -(this.resizeSize - 1)) {
            enableButton(ButtonCircle.BUTTON_OFFSETY_MINUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(),this.resizedMap.getPlayersAmount());
    }

    public void touchOffsetYMinus() {
        this.resizeOffsetY = Math.max(-(this.resizeSize - 1), this.resizeOffsetY - 1);
        if (this.resizeOffsetY == -(this.resizeSize - 1)) {
            disableButton(ButtonCircle.BUTTON_OFFSETY_MINUS);
        }
        if (this.resizeOffsetY < this.resizeSize - 1) {
            enableButton(ButtonCircle.BUTTON_OFFSETY_PLUS);
        }
        this.resizedMap = getResizedMap(this.resizeSize, this.resizeOffsetX, this.resizeOffsetY, (Tile) this.toggleGroups.get(0).getChecked().getObject(),this.resizedMap.getPlayersAmount());
    }

    private void disableButton(String tag) {
        if (buttons != null) {
            for (Button button : this.buttons) {
                if (button.getTag().equals(tag)) {
                    button.disable();
                }
            }
        }
    }

    private void enableButton(String tag) {
        if (buttons != null) {
            for (Button button : this.buttons) {
                if (button.getTag().equals(tag)) {
                    button.enable();
                }
            }
        }
    }

    public ToggleButton getTouchedToggleButton(float x, float y) {
        if (this.pencil.isChoose() || this.isResize()) {
            if (toggleGroups != null) {
                for (ToggleGroup group : toggleGroups) {
                    for (ToggleButton button : group.getButtons()) {
                        if (button.isTouched(x, y)) {
                            return button;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void touchToggleButton(ToggleButton button) {
        button.check();
        if (this.isPencil() && this.pencil.isChoose()) {
            switch (this.pencil.getCurrentLayer().getType()) {
                case TILE:
                    this.pencil.setCurrentTile((Tile) button.getObject());
                    break;
                case BUILDING:
                    this.pencil.setCurrentBuilding((Building) button.getObject());
                    break;
                case UNIT:
                    this.pencil.setCurrentUnit((Unit) button.getObject());
                    break;
                case PASSENGER:
                    this.pencil.setCurrentPassenger((Unit) button.getObject());
                    break;
            }
            back();
        }
    }

    public Square getTouchedSquare(float world_x, float world_y) {
        for (int row = this.map.getSize() - 1; row >= 0; row--) {
            for (int col = this.map.getSize() - 1; col >= 0; col--) {
                if (squareLayer[row][col].isTouched(world_x, world_y)) {
                    System.out.println("TOUCH : " + squareLayer[row][col]);
                    return squareLayer[row][col];
                }
            }
        }
        return null;
    }

    public void touchSquare(Square square) {
        if (!this.isExit() && !this.isResize()) {
            // DRAW
            if (pencil.isDraw()) {
                switch (this.pencil.getCurrentLayer().getType()) {
                    case TILE:
                        this.setTile(square.getRow(), square.getCol(), this.pencil.getCurrentTile());
                        break;
                    case BUILDING:
                        this.setBuilding(square.getRow(), square.getCol(), this.pencil.getCurrentBuilding());
                        break;
                    case UNIT:
                        this.setUnit(square.getRow(), square.getCol(), this.pencil.getCurrentUnit());
                        break;
                    case PASSENGER:
                        this.setPassenger(square.getRow(), square.getCol(), this.pencil.getCurrentPassenger());
                        break;
                }
            }
            // ERASE
            else if (this.pencil.isErase()) {
                switch (this.pencil.getCurrentLayer().getType()) {
                    case BUILDING:
                        this.setBuilding(square.getRow(), square.getCol(), null);
                        break;
                    case UNIT:
                        this.setUnit(square.getRow(), square.getCol(), null);
                        break;
                    case PASSENGER:
                        this.setPassenger(square.getRow(), square.getCol(), null);
                        break;
                }
            }
        }
    }

    public void openChooseMenu() {
        this.pencil.choose();
        switch (this.pencil.getCurrentLayer().getType()) {
            case TILE:
                this.createChooseTileButton();
                break;
            case BUILDING:
                this.createChooseBuildingButton();
                break;
            case UNIT:
                this.createChooseUnitButton();
                break;
            case PASSENGER:
                this.createChoosePassengerButton();
                break;
        }
    }

    public void closeChooseMenu() {
        this.pencil.draw();
        this.createButtons();
    }

    public void back() {
        System.out.println("BACK");
        if (this.pencil.isChoose()) {
            this.closeChooseMenu();
        } else {
            if (isExit()) {
                this.closeExitDialog();
            } else if (isResize()) {
                this.closeResizeDialog();
            } else {
                if (this.saved) {
                    this.screen.back();
                } else {
                    this.openExitDialog();
                }
            }
        }
    }

    private void openExitDialog() {
        this.state = EditorState.EXIT;
        createExitButtons();
    }

    private void closeExitDialog() {
        this.state = EditorState.PENCIL;
        createButtons();
    }

    private void openResizeDialog() {
        this.state = EditorState.RESIZE;
        createResizeButtons();
        this.resizeSize = this.map.getSize();
        this.resizeOffsetX = 0;
        this.resizeOffsetY = 0;
        this.resizedMap = this.map;
        this.resizePlayers = this.map.getPlayersAmount();
    }

    private void closeResizeDialog() {
        this.state = EditorState.PENCIL;
        createButtons();
    }

    private void updateSize() {
        // FIX BUG : If pencil is on unit/passenger/building belonging to the last player (deleted one), must re-init pencil
        if(this.map.getPlayersAmount() != this.resizePlayers) {
            if(this.pencil.getCurrentBuilding().getOwner().getOrder() == this.map.getPlayersAmount()) {
                Building defaultBuilding = Building.createBuilding(-1, -1, players.get(1), Building.CAPITAL, Capital.CLASSIC);
                this.pencil.setCurrentBuilding(defaultBuilding);
            }
            if(this.pencil.getCurrentUnit().getOwner().getOrder() == this.map.getPlayersAmount()) {
                Unit defaultUnit = Unit.createUnit(-1, -1, players.get(1), Unit.INFANTRY, this.pencil.getCurrentOrientation());
                this.pencil.setCurrentUnit(defaultUnit);
            }
            if(this.pencil.getCurrentPassenger().getOwner().getOrder() == this.map.getPlayersAmount()) {
                Unit defaultPassenger = Unit.createUnit(-1, -1, players.get(1), Unit.INFANTRY, Unit.Orientation.SOUTH);
                this.pencil.setCurrentPassenger(defaultPassenger);
            }
            // Remove units and passengers
            for (int row = this.map.getSize() - 1; row >= 0; row--) {
                for (int col = this.map.getSize() - 1; col >= 0; col--) {
                    Unit u = this.unitLayer[row][col];
                    Unit p = this.passengerLayer[row][col];
                    if(u != null && u.getOwner().getOrder() > this.resizePlayers) {
                        this.unitLayer[row][col] = null;
                    }
                    if(p != null && p.getOwner().getOrder() > this.resizePlayers) {
                        this.passengerLayer[row][col] = null;
                    }
                }
            }

        }
        // END FIX BUG
        updateSquareLayer(this.resizedMap.getSize());
        updateUnitsLayer(this.resizedMap.getSize(), this.resizeOffsetX, this.resizeOffsetY);
        this.map.updateMap(this.resizedMap.getSize(), this.resizedMap.getTiles(), this.resizedMap.getBuildings(), this.resizePlayers);
        closeResizeDialog();
    }

    private Map getResizedMap(int size, int offsetX, int offsetY, Tile defaultTile, int playersAmount) {
        System.out.println("SIZE : " + size);
        System.out.println("TILE : " + defaultTile);
        Map resizedMap = new Map(this.map.getId(), this.map.getName(), this.map.getFile(), Map.CUSTOM, false, 0);
        Tile[][] tiles = new Tile[size][size];
        Building[][] buildings = new Building[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if (row >= offsetX && row < this.map.getSize() + offsetX && col >= offsetY && col < this.map.getSize() + offsetY) {
                    Tile oldTile = this.map.getTile(row - offsetX, col - offsetY);
                    tiles[row][col] = new Tile(row, col, oldTile.getSeason(), oldTile.getType(), oldTile.getSubtype());
                    Building oldBuilding = this.map.getBuilding(row - offsetX, col - offsetY);
                    if (oldBuilding != null && oldBuilding.getOwner().getOrder() <= playersAmount) {
                        buildings[row][col] = Building.createBuilding(row, col, oldBuilding.getOwner(), oldBuilding.getType(), oldBuilding.getSubtype());
                        System.out.println("KKKL : " + oldBuilding.getOwner().getOrder() + " - " + playersAmount);
                    }
                } else {
                    tiles[row][col] = new Tile(row, col, defaultTile.getSeason(), defaultTile.getType(), defaultTile.getSubtype());
                }
            }
        }
        resizedMap.updateMap(size, tiles, buildings, playersAmount);
        return resizedMap;
    }

    private void updateSquareLayer(int size) {
        this.squareLayer = new Square[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                this.squareLayer[row][col] = new Square(row, col);
            }
        }
    }

    private void updateUnitsLayer(int size, int offsetX, int offsetY) {
        Unit[][] units = new Unit[size][size];
        Unit[][] passengers = new Unit[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if (row >= offsetX && row < this.map.getSize() + offsetX && col >= offsetY && col < this.map.getSize() + offsetY) {
                    Unit oldUnit = this.unitLayer[row - offsetX][col - offsetY];
                    if (oldUnit != null) {
                        units[row][col] = Unit.createUnit(row, col, oldUnit.getOwner(), oldUnit.getType(), oldUnit.getOrientation());
                    }
                    Unit oldPassenger = this.passengerLayer[row - offsetX][col - offsetY];
                    if (oldPassenger != null) {
                        passengers[row][col] = Unit.createUnit(row, col, oldPassenger.getOwner(), oldPassenger.getType(), oldPassenger.getOrientation());
                    }
                }
            }
        }
        this.unitLayer = units;
        this.passengerLayer = passengers;
    }

    public int getResizeSize() {
        return this.resizeSize;
    }

    public int getMapSize() {
        return this.map.getSize();
    }

    public float getMapWidth() {
        return getMapSize() * Util.getTileWidth();
    }

    public float getMapHeight() {
        return getMapSize() * Util.getTileHeight();
    }

    public void resume() {
        createButtons();
        if (this.isExit()) {
            openExitDialog();
        } else if (this.isResize()) {
            openResizeDialog();
        } else {
            if (this.pencil.isChoose()) {
                openChooseMenu();
            }
        }
    }

    public ArrayList<ButtonCircle> getFixedButtons() {
        return this.fixedButtons;
    }

    public ArrayList<Button> getButtons() {
        return this.buttons;
    }


    public ArrayList<ToggleGroup> getToggleGroups() {
        return this.toggleGroups;
    }

    public MainAssets getMainAssets() {
        return this.mainAssets;
    }

    public GameAssets getGameAssets() {
        return this.screen.getController().getGameAssets();
    }

    public EditorScreen getScreen() {
        return this.screen;
    }

    public EditorHUDVars getHUDVars() {
        return this.hudVars;
    }

    public Square[][] getSquareLayer() {
        return this.squareLayer;
    }

    public Unit[][] getUnitLayer() {
        return this.unitLayer;
    }

    public Unit[][] getPassengerLayer() {
        return this.passengerLayer;
    }

    public Map getMap() {
        return this.map;
    }

    public void setTile(int row, int col, Tile tile) {
        this.map.getTile(row, col).edit(tile.getSeason(), tile.getType(), tile.getSubtype());
        this.saved = false;
    }

    public void setBuilding(int row, int col, Building building) {
        if (building == null) {
            this.map.setBuilding(row, col, null);
        } else {
            this.map.setBuilding(row, col, Building.createBuilding(row, col, building.getOwner(), building.getType(), building.getSubtype()));
        }
        this.saved = false;
    }

    public void setUnit(int row, int col, Unit unit) {
        if (unit == null) {
            this.unitLayer[row][col] = null;
        } else {
            if (unit.canBeOnTile(map.getTile(row, col))) {
                this.unitLayer[row][col] = Unit.createUnit(row, col, unit.getOwner(), unit.getType(), unit.getOrientation());
            }
        }
        this.saved = false;
    }

    public void setPassenger(int row, int col, Unit passenger) {
        if (passenger == null) {
            this.passengerLayer[row][col] = null;
        } else {
            Unit transporter = this.unitLayer[row][col];
            if (transporter != null && passenger.canEmbarkIn(transporter)) {
                this.passengerLayer[row][col] = Unit.createUnit(row, col, passenger.getOwner(), passenger.getType(), passenger.getOrientation());
            }
        }
        this.saved = false;
    }

    private int getCheckedTilePosition(Tile tile) {
        int result = tile.getType() - 1;
        if (result == 5 && tile.getSubtype() > 0) {
            result = 4 + tile.getSubtype();
        } else if (result == 6 && tile.getSubtype() > 0) {
            result = 10 + tile.getSubtype();
        }
        return result;
    }

    private int getCheckedBuildingPosition(Building building) {
        int result = building.getType() - 1;
        if (result == 0 && building.getSubtype() > 0) {
            result += 4 + building.getSubtype();
        }
        return result;
    }

    private int getCheckedUnitPosition(Unit unit) {
        return unit.getType() - 1;
    }

    private int getCheckedPassengerPosition(Unit unit) {
        if(unit.getType() <= Unit.HALFTRACK) {
            return unit.getType() - 1;
        } else {
            return unit.getType() - 4;
        }
    }

    public Pencil getPencil() {
        return this.pencil;
    }

    public boolean isExit() {
        return this.state == EditorState.EXIT;
    }

    public boolean isResize() {
        return this.state == EditorState.RESIZE;
    }

    public boolean isPencil() {
        return this.state == EditorState.PENCIL;
    }

    public void save() {
        System.out.println("TEST : " + this.map.getSize());
        MapFile.mapToXml(this.map, this.unitLayer, this.passengerLayer);
        this.saved = true;
    }

    public Map getResizedMap() {
        return this.resizedMap;
    }

    public int getResizePlayers() {
        return this.resizePlayers;
    }

    public int getResizeOffsetX() {
        return this.resizeOffsetX;
    }

    public int getResizeOffsetY() {
        return this.resizeOffsetY;
    }
}