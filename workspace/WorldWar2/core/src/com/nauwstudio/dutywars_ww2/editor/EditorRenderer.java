package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.GameAssets;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.ToggleGroup;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

public class EditorRenderer {

    public static final float ZOOM_MAX = 2.0f;
    public static final float ZOOM_MIN = 0.5f;

    private Editor editor;
    SpriteBatch batch;

    private OrthographicCamera mainCamera;
    private OrthographicCamera worldCamera;

    private MainAssets mainAssets;
    private GameAssets gameAssets;
    private EditorHUDVars hudVars;

    public EditorRenderer(Editor editor) {
        try {
            this.editor = editor;
            // CAMERAS
            worldCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
            worldCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
            worldCamera.update();
            mainCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
            mainCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
            mainCamera.update();

            this.batch = new SpriteBatch();
            this.batch.setProjectionMatrix(this.worldCamera.combined);
            this.mainAssets = this.editor.getMainAssets();
            this.gameAssets = this.editor.getGameAssets();
            this.hudVars = this.editor.getHUDVars();
            centerCamera();
        } catch (Exception e) {
        }
    }

    public void resize(int width, int height) {
        worldCamera = new OrthographicCamera(width, height);
        worldCamera.setToOrtho(false, width, height);
        worldCamera.update();
        mainCamera = new OrthographicCamera(width, height);
        mainCamera.setToOrtho(false, width, height);
        mainCamera.update();
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        // REPLACE BUTTONS
        this.editor.createButtons();
        if (this.editor.getPencil().isChoose()) {
            this.editor.openChooseMenu();
        } else if (this.editor.isExit()) {
            this.editor.createExitButtons();
        } else if (this.editor.isResize()) {
            this.editor.createResizeButtons();
        }
    }

    public float getCameraPositionX() {
        return this.worldCamera.position.x;
    }

    public float getCameraPositionY() {
        return this.worldCamera.position.y;
    }

    public float getCameraOriginX() {
        return getCameraPositionX() - (getCameraZoomWidth() / 2f);
    }

    public float getCameraOriginY() {
        return getCameraPositionY() - (getCameraZoomHeight() / 2);
    }


    public float getCameraWidth() {
        return this.worldCamera.viewportWidth;
    }

    public float getCameraHeight() {
        return this.worldCamera.viewportHeight;
    }

    public float getCameraZoom() {
        return this.worldCamera.zoom;
    }

    public float getCameraZoomWidth() {
        return getCameraWidth() * this.worldCamera.zoom;
    }

    public float getCameraZoomHeight() {
        return getCameraHeight() * this.worldCamera.zoom;
    }

    public void moveCamera(float distX, float distY) {
        distX = distX * getCameraZoom();
        distY = distY * getCameraZoom();
        float border_left = Util.getTileWidth() / 2f - this.editor.getMapWidth() / 2f;
        float border_right = Util.getTileWidth() / 2f + this.editor.getMapWidth() / 2f;
        float border_bottom = 0;
        float border_top = this.editor.getMapHeight();
        this.worldCamera.translate(distX, distY);
        this.worldCamera.update();
        // Out of borders ?
        if (this.worldCamera.position.y < border_bottom) {
            this.worldCamera.translate(0, border_bottom - this.worldCamera.position.y);
        }
        if (this.worldCamera.position.y > border_top) {
            this.worldCamera.translate(0, border_top - this.worldCamera.position.y);
        }
        if (this.worldCamera.position.x < border_left) {
            this.worldCamera.translate(border_left - this.worldCamera.position.x, 0);
        }
        if (this.worldCamera.position.x > border_right) {
            this.worldCamera.translate(border_right - this.worldCamera.position.x, 0);
        }
        this.worldCamera.update();
    }

    public float getMenuCameraPositionX() {
        return this.mainCamera.position.x;
    }

    public float getMenuCameraPositionY() {
        return this.mainCamera.position.y;
    }

    public float getMenuCameraOriginX() {
        return getMenuCameraPositionX() - (getMenuCameraWidth() / 2f);
    }

    public float getMenuCameraOriginY() {
        return getMenuCameraPositionY() - (getMenuCameraHeight() / 2f);
    }

    public float getMenuCameraWidth() {
        return this.mainCamera.viewportWidth;
    }

    public float getMenuCameraHeight() {
        return this.mainCamera.viewportHeight;
    }

    public void pointCamera(Vector2 target) {
        int distX = (int) (target.x - this.worldCamera.position.x);
        int distY = (int) (target.y - this.worldCamera.position.y);
        this.worldCamera.translate(distX, distY);
        this.worldCamera.update();
    }

    public void centerCamera() {
        this.worldCamera.position.x = this.editor.getMapWidth() / 2f;
        this.worldCamera.position.y = this.editor.getMapHeight() / 2f;
        this.worldCamera.update();
    }

    public void zoomIn() {
        this.worldCamera.zoom = Math.max(this.worldCamera.zoom - 0.02f, ZOOM_MIN);
        worldCamera.update();
    }

    public void zoomOut() {
        this.worldCamera.zoom = Math.min(this.worldCamera.zoom + 0.02f, ZOOM_MAX);
        worldCamera.update();
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(25 / 255f, 25 / 255f, 25 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderWorld(delta);
        renderFixedButton(delta);
        if (this.editor.getPencil().isChoose()) {
            switch (this.editor.getPencil().getCurrentLayer().getType()) {
                case TILE:
                    renderChooseMenu(delta, "editor.choose_tile");
                    break;
                case BUILDING:
                    renderChooseMenu(delta, "editor.choose_building");
                    break;
                case UNIT:
                    renderChooseMenu(delta, "editor.choose_unit");
                    break;
                case PASSENGER:
                    renderChooseMenu(delta, "editor.choose_passenger");
                    break;
            }
        } else if (this.editor.isExit()) {
            renderExitDialog(delta);
        } else if (this.editor.isResize()) {
            renderResizeDialog(delta);
        }
        batch.setProjectionMatrix(mainCamera.combined);
    }

    public void renderWorld(float delta) {
        batch.setProjectionMatrix(worldCamera.combined);
        batch.begin();
        renderMapAndSquare(batch, delta);
        renderUnit(batch, delta);
        batch.end();
    }

    public void renderMapAndSquare(SpriteBatch batch, float delta) {
        for (int row = this.editor.getMapSize() - 1; row >= 0; row--) {
            for (int col = this.editor.getMapSize() - 1; col >= 0; col--) {
                if (this.editor.getPencil().getTileLayer().isVisibile()) {
                    this.editor.getMap().renderTile(batch, delta, row, col);
                }
                this.editor.getSquareLayer()[row][col].render(batch, delta, this.gameAssets);
                if (this.editor.getPencil().getBuildingLayer().isVisibile()) {
                    this.editor.getMap().renderBuilding(batch, delta, row, col);
                }
            }
        }
    }

    public void renderUnit(SpriteBatch batch, float delta) {
        for (int row = this.editor.getMapSize() - 1; row >= 0; row--) {
            for (int col = this.editor.getMapSize() - 1; col >= 0; col--) {
                if (this.editor.getPencil().getUnitLayer().isVisibile()) {
                    Unit u = this.editor.getUnitLayer()[row][col];
                    if (u != null) {
                        u.renderUnitOnly(batch, delta);
                    }
                }
                if (this.editor.getPencil().getPassengerLayer().isVisibile()) {
                    Unit p = this.editor.getPassengerLayer()[row][col];
                    if (p != null) {
                        p.renderUnitOnly(batch, delta);
                    }
                }
            }
        }
    }

    public void renderFixedButton(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        if(!this.editor.getFixedButtons().isEmpty()) {
            batch.draw(mainAssets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() + Util.getCircleButtonSpace() / 2f);
        }
        for (ButtonCircle b : this.editor.getFixedButtons()) {
            b.render(batch, this.mainAssets.font_medium);
        }
        if (this.editor.getPencil().isDraw()) {
            renderDrawObject();
        }
        batch.end();
    }

    public void renderDrawObject() {
        switch (this.editor.getPencil().getCurrentLayer().getType()) {
            case TILE:
                RenderUtil.drawTextureInCell(batch, this.editor.getPencil().getCurrentTile().getToggleTexture(), hudVars.state_choose_x, hudVars.state_y, Util.getCircleButtonSize(), Util.getCircleButtonSize(), 3 * Util.getCircleButtonSize() / 4f);
                break;
            case BUILDING:
                RenderUtil.drawTextureInCell(batch, this.editor.getPencil().getCurrentBuilding().getToggleTexture(), hudVars.state_choose_x, hudVars.state_y, Util.getCircleButtonSize(), Util.getCircleButtonSize(), 3 * Util.getCircleButtonSize() / 4f);
                break;
            case UNIT:
                RenderUtil.drawTextureInCell(batch, this.editor.getPencil().getCurrentUnit().getToggleTexture(), hudVars.state_choose_x, hudVars.state_y, Util.getCircleButtonSize(), Util.getCircleButtonSize(), 3 * Util.getCircleButtonSize() / 4f);
                break;
            case PASSENGER:
                RenderUtil.drawTextureInCell(batch, this.editor.getPencil().getCurrentPassenger().getToggleTexture(), hudVars.state_choose_x, hudVars.state_y, Util.getCircleButtonSize(), Util.getCircleButtonSize(), 3 * Util.getCircleButtonSize() / 4f);
                break;
        }
    }

    public void renderChooseMenu(float delta, String title) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        batch.draw(mainAssets.mission_bg, this.hudVars.choose_x, this.hudVars.choose_y, this.hudVars.choose_width, this.hudVars.choose_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_giant, this.mainAssets.stringBundle.get(title), this.hudVars.square_title_x, this.hudVars.square_title_y, this.hudVars.square_title_width, this.hudVars.square_title_height);
        for (Button b : this.editor.getButtons()) {
            b.render(batch);
        }
        for (ToggleGroup tg : this.editor.getToggleGroups()) {
            tg.render(batch);
        }
        batch.end();
    }

    public void renderExitDialog(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        batch.draw(mainAssets.mission_bg, this.hudVars.exit_x, this.hudVars.exit_y, this.hudVars.exit_width, this.hudVars.exit_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_bigbig, this.mainAssets.stringBundle.get("editor.exit_no_save"), this.hudVars.exit_text_x, this.hudVars.exit_text_y, this.hudVars.exit_text_width, this.hudVars.exit_text_height);
        for (Button b : this.editor.getButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    public void renderResizeDialog(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        batch.draw(mainAssets.mission_bg, this.hudVars.resize_x, this.hudVars.resize_y, this.hudVars.resize_width, this.hudVars.resize_height);
        RenderUtil.drawTextInCell(batch, mainAssets.font_giant, mainAssets.stringBundle.get("editor.edit_map"), this.hudVars.resize_title_x, this.hudVars.resize_title_y, this.hudVars.resize_title_width, this.hudVars.resize_title_height);
        RenderUtil.drawTextInCellLeft(batch, this.mainAssets.font_bigbig, this.mainAssets.stringBundle.get("main.players"), this.hudVars.player_title_x, this.hudVars.player_title_y, this.hudVars.player_title_width, this.hudVars.player_title_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.editor.getResizePlayers()+"", this.hudVars.player_x + this.hudVars.player_width/4f, this.hudVars.player_y, this.hudVars.player_width/2f, this.hudVars.player_height);
        RenderUtil.drawTextInCellLeft(batch, mainAssets.font_bigbig, mainAssets.stringBundle.get("main.size"), this.hudVars.size_title_x, this.hudVars.size_title_y, this.hudVars.size_title_width, this.hudVars.size_title_height);
        RenderUtil.drawTextInCell(batch, mainAssets.font_big, this.editor.getResizeSize() + "", this.hudVars.size_x + this.hudVars.size_width / 4f, this.hudVars.size_y, this.hudVars.size_width / 2f, this.hudVars.size_height);
        RenderUtil.drawTextInCellLeft(batch, mainAssets.font_bigbig, mainAssets.stringBundle.get("editor.offset_x"), this.hudVars.offset_title_x, this.hudVars.offset_title_y, this.hudVars.offset_title_width/2f, this.hudVars.offset_title_height);
        RenderUtil.drawTextInCellLeft(batch, mainAssets.font_bigbig, mainAssets.stringBundle.get("editor.offset_y"), this.hudVars.offset_title_x + hudVars.offset_title_width/2f, this.hudVars.offset_title_y, this.hudVars.offset_title_width/2f, this.hudVars.offset_title_height);
        RenderUtil.drawTextInCell(batch, mainAssets.font_big, this.editor.getResizeOffsetX()+"", this.hudVars.offset_x, this.hudVars.offset_y, this.hudVars.offset_width/2f, this.hudVars.offset_height);
        RenderUtil.drawTextInCell(batch, mainAssets.font_big, this.editor.getResizeOffsetY()+"", this.hudVars.offset_x + hudVars.offset_width/2f, this.hudVars.offset_y, this.hudVars.offset_width/2f, this.hudVars.offset_height);
        RenderUtil.drawTextInCellLeft(batch, mainAssets.font_bigbig, mainAssets.stringBundle.get("main.tile"), this.hudVars.tile_title_x, this.hudVars.tile_title_y, this.hudVars.tile_title_width, this.hudVars.tile_title_height);
        editor.getResizedMap().renderSmall(batch, this.hudVars.map_x, this.hudVars.map_y, this.hudVars.map_width);
        for (Button b : this.editor.getButtons()) {
            b.render(batch);
        }
        for (ToggleGroup tg : this.editor.getToggleGroups()) {
            tg.render(batch);
        }
        batch.end();
    }
}