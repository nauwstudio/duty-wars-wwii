package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;

public class LayerButton extends ButtonCircle {

    private MainAssets mainAssets;

    private Layer layer;
    private LayerVisibilityButton visibilityButton;

    private float visibility_x, visibility_y;

    public LayerButton(Vector2 position, float width, String tag, Layer layer,TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_disabled, MainAssets mainAssets) {
        super(position, width, tag, texture, texture_pressed, texture_disabled);
        this.mainAssets = mainAssets;
        this.layer = layer;
        initHUDVars();
        if(layer.isVisibile()) {
            createVisibilityButton(mainAssets.button_visible, mainAssets.button_visible_pressed);
        } else {
            createVisibilityButton(mainAssets.button_invisible, mainAssets.button_invisible_pressed);
        }
    }

    private void initHUDVars() {
        // Delete buttons
        visibility_x = this.position.x + this.width - Util.getTinyCircleButtonSize()/2f;
        visibility_y = this.position.y + Util.getCircleButtonSize() - Util.getTinyCircleButtonSize() + Util.getTinyCircleButtonSize()/4f;
    }

    public void createVisibilityButton(TextureRegion info, TextureRegion info_pressed) {
        Vector2 info_pos = new Vector2(this.visibility_x, this.visibility_y);
        this.visibilityButton = new LayerVisibilityButton(info_pos, Util.getTinyCircleButtonSize(), info, info_pressed, this);
    }

    @Override
    public void render(SpriteBatch batch, BitmapFont font) {
        super.render(batch);
        // Render name
        RenderUtil.drawTextInCell(batch, font, mainAssets.stringBundle.get(this.layer.getName()), this.position.x - this.width/4f, this.position.y - Util.getCircleButtonSpace()/6f, 6*this.width/4f, font.getLineHeight());
        // Render visibility button
        this.visibilityButton.render(batch);
    }

    @Override
    public boolean isLayerButton() {
        return true;
    }

    public Layer getLayer() {
        return this.layer;
    }

    public LayerVisibilityButton getVisibilityButton() {
        return this.visibilityButton;
    }
}
