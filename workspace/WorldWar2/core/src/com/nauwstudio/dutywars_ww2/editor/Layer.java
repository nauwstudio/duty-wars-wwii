package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;

/**
 * Created by nauwpie on 24/07/2018.
 */

public class Layer {

    public enum LayerType {
        TILE, BUILDING, UNIT, PASSENGER
    }

    private LayerType type;
    private boolean visibility;

    private MainAssets mainAssets;

    public Layer(LayerType type, MainAssets mainAssets) {
        this.type = type;
        this.visibility = true;
        this.mainAssets = mainAssets;
    }

    public boolean isVisibile() {
        return this.visibility;
    }

    public void show() {
        this.visibility = true;
    }

    public void hide() {
        this.visibility = false;
    }

    public TextureRegion getButtonTexture() {
        switch(this.type) {
            case TILE:
                return mainAssets.button_next_mission;
            case BUILDING:
                return mainAssets.button_next_mission;
            case UNIT:
                return mainAssets.button_next_mission;
            case PASSENGER:
                return mainAssets.button_next_mission;
            default:
                return mainAssets.button_next_mission;
        }
    }

    public TextureRegion getButtonTexturePressed() {
        switch(this.type) {
            case TILE:
                return mainAssets.button_next_mission_pressed;
            case BUILDING:
                return mainAssets.button_next_mission_pressed;
            case UNIT:
                return mainAssets.button_next_mission_pressed;
            case PASSENGER:
                return mainAssets.button_next_mission_pressed;
            default:
                return mainAssets.button_next_mission_pressed;
        }
    }

    public TextureRegion getButtonTextureDisabled() {
        switch(this.type) {
            case TILE:
                return mainAssets.button_next_mission;
            case BUILDING:
                return mainAssets.button_next_mission;
            case UNIT:
                return mainAssets.button_next_mission;
            case PASSENGER:
                return mainAssets.button_next_mission;
            default:
                return mainAssets.button_next_mission;
        }
    }

    public String getName() {
        switch(this.type) {
            case TILE:
                return "editor.tile";
            case BUILDING:
                return "editor.building";
            case UNIT:
                return "editor.unit";
            case PASSENGER:
                return "editor.passenger";
            default:
                return "editor.tile";
        }
    }

    public LayerType getType() {
        return this.type;
    }
}
