package com.nauwstudio.dutywars_ww2.editor;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.nauwstudio.dutywars_ww2.EditorScreen;
import com.nauwstudio.dutywars_ww2.GameScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Game;
import com.nauwstudio.dutywars_ww2.game.GameRenderer;

public class EditorInputHandler implements InputProcessor {

    EditorScreen screen;
    Editor editor;
    EditorRenderer renderer;

    public EditorInputHandler(EditorScreen screen) {
        this.screen = screen;
        this.editor = screen.getEditor();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.editor.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Touching button ?
        float button_x = this.renderer.getMenuCameraOriginX() + screenX;
        float button_y = this.renderer.getMenuCameraOriginY() + screenY;
        this.screen.touchedButton = this.editor.getTouchedButton(button_x, button_y);
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.press();
            disableGesture();
            return true;
        } else {
            // Touching toggle button ?
            this.screen.touchedToggle = this.editor.getTouchedToggleButton(button_x, button_y);
            if (this.screen.touchedToggle != null) {
                this.screen.touchedToggle.press();
                return true;
            } else {
                // Touching tile ?
                float world_x = this.renderer.getCameraOriginX() + screenX * this.renderer.getCameraZoom();
                float world_y = this.renderer.getCameraOriginY() + screenY * this.renderer.getCameraZoom();
                this.screen.touchedSquare = this.editor.getTouchedSquare(world_x, world_y);
                if (screen.touchedSquare != null) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Perform button action ?
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.editor.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            reset();
            return true;
        } else {
            // Perform radio button action ?
            if (this.screen.touchedToggle != null) {
                this.editor.touchToggleButton(this.screen.touchedToggle);
                this.screen.touchedToggle.release();
                this.screen.touchedToggle = null;
                return true;
            } else {
                // Perform selection action ?
                if (!this.editor.getPencil().isChoose() && this.screen.touchedSquare != null) {
                    float world_x = this.renderer.getCameraOriginX() + screenX * this.renderer.getCameraZoom();
                    float world_y = this.renderer.getCameraOriginY() + screenY * this.renderer.getCameraZoom();
                    // Perform select TILE action ?
                    if (this.screen.touchedSquare.isTouched(world_x, world_y)) {
                        this.editor.touchSquare(screen.touchedSquare);
                    }
                    reset();
                    return true;
                } else {
                    reset();
                    return false;
                }
            }
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount > 0) {
            this.renderer.zoomOut();
        } else {
            this.renderer.zoomIn();
        }
        return false;
    }

    public void disableGesture() {
        this.screen.can_pan = false;
        this.screen.can_pinch = false;
    }

    public void enableGesture() {
        this.screen.can_pan = true;
        this.screen.can_pinch = true;
    }

    public void reset() {
        enableGesture();
        this.screen.touchedButton = null;
        this.screen.touchedToggle = null;
        this.screen.touchedSquare = null;
    }
}
