package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlWriter;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Season;
import com.nauwstudio.dutywars_ww2.game.Tile;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by nauwpie on 2/01/2018.
 */

public class Util {

    // Zoom = number of tiles shown on height if vertical, width if horizontal
    public static final float BASIC_ZOOM = 12.0f;

    public static final float TILE_WIDTH = 256;
    public static final float TILE_HEIGHT = 148;
    public static final float TILE_RATIO = TILE_HEIGHT / TILE_WIDTH;

    public static final float MAP_TILE_WIDTH = 256;
    public static final float MAP_TILE_HEIGHT = 185;
    public static final float MAP_SHIFT = (MAP_TILE_HEIGHT - TILE_HEIGHT) / TILE_HEIGHT;

    public static final String PARAM_AUDIO_MUSIC = "music";
    public static final String PARAM_AUDIO_SOUND = "sound";
    public static final String PARAM_SPEED_HUMAN = "speed_human";
    public static final String PARAM_SPEED_ROBOT = "speed_robot";

    public static float getScreenWidth() {
        return Gdx.graphics.getWidth();
    }

    public static float getScreenHeight() {
        return Gdx.graphics.getHeight();
    }

    public static boolean isVertical() {
        return getScreenHeight() >= getScreenWidth();
    }

    public static float getTileHeight() {
        return Math.max(getScreenWidth(), getScreenHeight()) / BASIC_ZOOM;
    }

    public static float getTileWidth() {
        return getTileHeight() / TILE_RATIO;
    }

    public static float getActionBarHeight() {
        return getCircleButtonSize() + getCircleButtonSpace()/2f;
    }

    public static float getContentHeight() {
        return getScreenHeight() - getActionBarHeight();
    }

    public static float getCircleButtonSize() {
        return Math.min(Util.getScreenHeight(), Util.getScreenWidth()) / 7f;
    }

    public static float getSmallCircleButtonSize() {
        return 0.6f * Util.getCircleButtonSize();
    }

    public static float getMediumCircleButtonSize() {
        return 0.9f * Util.getCircleButtonSize();
    }

    public static float getTinyCircleButtonSize() {
        return 0.4f * Util.getCircleButtonSize();
    }

    public static float getCircleButtonSpace() {
        return getCircleButtonSize() / 2f;
    }

    public static float getRectangleButtonHeight() {
        return 9*getCircleButtonSize()/10f;
    }

    public static float getButtonsMenuHeight() {
        return getCircleButtonSize() + getCircleButtonSpace() / 2f;
    }

    // Map coord to screen coord
    public static Vector2 mapToWorldCoord(Vector2 map) {
        float x = (map.x - map.y) * (getTileWidth()/2f);
        float y = (map.x + map.y) * (getTileHeight()/2f);
        Vector2 iso = new Vector2(x, y);
        return iso;
    }

    public static int getStarsAmount(int score) {
        if(score >= 1 && score <= 25) {
            return 3;
        } else if(score >= 21 && score <= 50) {
            return 2;
        } else  if(score >= 41 && score <= 75) {
            return 1;
        } else {
            return 0;
        }
    }
}
