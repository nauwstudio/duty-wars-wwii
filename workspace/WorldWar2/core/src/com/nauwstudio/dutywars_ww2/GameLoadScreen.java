package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Robot;

import java.util.ArrayList;

public class GameLoadScreen implements Screen {

    private DWController controller;
    private I18NBundle stringBundle;
    private OrthographicCamera loadCamera;
    private SpriteBatch batch;

    private GameAssets assets;
    private ArrayList<Player> players;
    private com.nauwstudio.dutywars_ww2.game.Map map;
    private int mode;
    private Save save;
    private int loadCounter = 0;
    private Timer.Task loadTask;
    private float bullet_size;
    private float bullet_small_icon_size;
    private float bullet_big_icon_size;

    private boolean assetsLoaded;

    /*
    NEW GAME
     */
    public GameLoadScreen(DWController controller, Map map, int[] players_army, int[] players_type, int[] players_team, int[] players_order, int mode) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        this.save = null;
        this.players = getPlayers(players_army, players_type, players_team, players_order);
        this.map = map;
        this.mode = mode;
        initBundle();
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.batch = new SpriteBatch();
        this.batch.setProjectionMatrix(this.loadCamera.combined);
        this.loadTask = new Timer.Task() {
            @Override
            public void run() {
                loadCounter = (loadCounter + 1) % 6;
            }
        };
        this.bullet_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 14f;
        this.bullet_small_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 40f;
        this.bullet_big_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 25f;
        loadAssets();
    }

    /*
    RESUME GAME
     */
    public GameLoadScreen(DWController controller, Save save_summary) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        this.save = save_summary;
        this.players = this.save.getPlayers();
        this.map = save.getMap();
        this.mode = save_summary.getMode();
        initBundle();
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.batch = new SpriteBatch();
        this.batch.setProjectionMatrix(this.loadCamera.combined);
        this.loadTask = new Timer.Task() {
            @Override
            public void run() {
                loadCounter = (loadCounter + 1) % 6;
            }
        };
        this.bullet_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 14f;
        this.bullet_small_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 40f;
        this.bullet_big_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 25f;
        loadAssets();
    }


    private void loadAssets() {
        Timer.schedule(loadTask, 0.20f, 0.20f);
        this.assets.load();
        for (Player player : this.players) {
            if(!player.getArmy().isGAI()) {
                player.getArmy().getAssets().loadUnits();
                player.getArmy().getAssets().loadColors();
            }
        }

    }

    private void initAssets() {
        try {
            this.assets.init();
            for (Player player : this.players) {
                player.getArmy().getAssets().initUnits();
                player.getArmy().getAssets().initColors();
            }
        } catch (GdxRuntimeException exception) {

        }
    }

    /*
    Init players for a new game
     */
    public ArrayList<Player> getPlayers(int[] players_army, int[] players_type, int[] players_team, int[] players_order) {
        try {
            ArrayList<Player> players = new ArrayList<Player>();
            // GAIA player
            players.add(new Player(this.controller.getPlayer(Army.GAI).getArmy(), 0, 0, Player.GAIA_ORDER, Player.ALIVE));
            // REAL players
            for (int i = 0; i < players_army.length; i++) {
                // HUMAN
                if (players_type[i] == Player.HUMAN) {
                    players.add(new Player(this.controller.getPlayer(players_army[i]).getArmy(), players_team[i], 0, players_order[i], Player.ALIVE));
                }
                // ROBOT
                else {
                    players.add(new Robot(this.controller.getPlayer(players_army[i]).getArmy(), players_team[i], 0, players_order[i], Player.ALIVE));
                }
            }
            return players;
        } catch (Exception e) {
            return new ArrayList<Player>();
        }
    }

    public void initBundle() {
        FileHandle baseFileHandle = Gdx.files.internal("bundles/string");
        this.stringBundle = I18NBundle.createBundle(baseFileHandle);
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        //if (assetsLoaded) {
            // Render loader
            if (this.assets.update()) {
                initAssets();
                if (this.loadTask != null) {
                    this.loadTask.cancel();
                }
                if (this.save != null) {
                    this.controller.getDatabaseInterface().getSaveComplete(this.save);
                    this.controller.showResumeGameScreen(this.map, this.save);
                } else {

                    this.controller.showGameScreen(this.map, this.players, this.mode);
                }
            } else {
                renderLoading();
            }
            // Render screen
        /*
        } else {
            renderLoading();
            loadAssets();
            assetsLoaded = true;
        }
        */

    }

    private void renderLoading() {
        Gdx.gl.glClearColor(5 / 255f, 5 / 255f, 5 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(loadCamera.combined);
        batch.begin();
        RenderUtil.drawTextInCell(batch, this.controller.getMainAssets().font_giant, this.controller.getMainAssets().stringBundle.get("game.launch"), 0, Util.getScreenHeight() / 2f + this.controller.getMainAssets().font_giant.getXHeight(), Util.getScreenWidth(), this.controller.getMainAssets().font_giant.getXHeight());
        for (int i = 0; i < 6; i++) {
            if (i == loadCounter) {
                RenderUtil.drawTextureInCell(batch, this.controller.getMainAssets().bullet, Util.getScreenWidth() / 2f - 3 * bullet_size + i * bullet_size, Util.getScreenHeight() / 2f - this.controller.getMainAssets().font_giant.getXHeight() * 2, bullet_size, this.controller.getMainAssets().font_giant.getXHeight(), this.bullet_big_icon_size);
            } else {
                RenderUtil.drawTextureInCell(batch, this.controller.getMainAssets().bullet, Util.getScreenWidth() / 2f - 3 * bullet_size + i * bullet_size, Util.getScreenHeight() / 2f - this.controller.getMainAssets().font_giant.getXHeight() * 2, bullet_size, this.controller.getMainAssets().font_giant.getXHeight(), this.bullet_small_icon_size);
            }
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.bullet_size = Math.min(width, height) / 14f;
        this.bullet_small_icon_size = Math.min(width, height) / 40f;
        this.bullet_big_icon_size = Math.min(width, height) / 25f;
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {

    }

    public com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface getDatabase() {
        return this.controller.getDatabaseInterface();
    }

    public void back() {
        this.controller.toMainScreen();
    }
}
