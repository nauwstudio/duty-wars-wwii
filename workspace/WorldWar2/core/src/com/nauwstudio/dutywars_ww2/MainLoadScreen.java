package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.I18NBundle;

import java.io.File;

public class MainLoadScreen implements Screen {

    private DWController controller;
    private I18NBundle stringBundle;
    private BitmapFont font_giant;
    private OrthographicCamera loadCamera;
    private SpriteBatch batch;
    private boolean backgroundLoaded = false;

    public MainLoadScreen(DWController controller) {
        this.controller = controller;
        initBundle();
        initFonts();
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.batch = new SpriteBatch();
        this.batch.setProjectionMatrix(this.loadCamera.combined);

        // Load background assets
        this.controller.loadBackground();
    }

    public void initBundle() {
        FileHandle baseFileHandle = Gdx.files.internal("bundles/string");
        this.stringBundle = I18NBundle.createBundle(baseFileHandle);
    }

    public void initFonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/CutiveMono.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_giant = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_giant.size = 60;
        this.font_giant = generator.generateFont(ftfp_giant);
        generator.dispose();
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        // Render loader
        if (this.controller.stopMainAssetsLoading()) {
            if (backgroundLoaded) {
                this.controller.initMainAssets();
                this.controller.initSeasonAssets();
                this.controller.initPlayerAssets();
                this.controller.toMainScreen();
            } else {
                this.backgroundLoaded = true;
                this.controller.initBackground();
                this.controller.loadMainAssets();
                this.controller.loadSeasonAssets();
                this.controller.loadPlayerAssets();
            }
        }
        // Render screen
        else {
            Gdx.gl.glClearColor(5 / 255f, 5 / 255f, 5 / 255f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.setProjectionMatrix(loadCamera.combined);
            batch.begin();
            if (backgroundLoaded) {
                RenderUtil.drawBackground(batch, this.controller.getMainAssets());
            }
            batch.end();
        }
    }

    @Override
    public void resize(int width, int height) {
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
    }

    public com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface getDatabase() {
        return this.controller.getDatabaseInterface();
    }
}
