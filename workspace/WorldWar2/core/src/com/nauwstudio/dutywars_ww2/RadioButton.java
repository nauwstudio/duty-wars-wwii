package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class RadioButton {

    public static final String RADIO_SLOW = "slow";
    public static final String RADIO_NORMAL = "normal";
    public static final String RADIO_FAST = "fast";

    protected Vector2 position;
    protected float width, height;

    protected RadioGroup group;
    protected String tag;
    protected TextureRegion texture;
    protected TextureRegion texture_pressed;
    protected TextureRegion texture_checked;
    protected TextureRegion texture_checked_pressed;
    protected boolean checked = false;
    protected boolean pressed = false;
    protected String text;

    protected Rectangle bounds;

    public RadioButton(Vector2 position, float width, float height, RadioGroup group, String tag, String text, TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_checked, TextureRegion texture_checked_pressed) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.group = group;
        this.tag = tag;
        this.text = text;
        this.texture = texture;
        this.texture_pressed = texture_pressed;
        this.texture_checked = texture_checked;
        this.texture_checked_pressed = texture_checked_pressed;
        this.bounds = new Rectangle(position.x, position.y, this.width, this.height);
    }

    public String getTag() {
        return this.tag;
    }

    public boolean isTouched(float x, float y){
        return this.bounds.contains(new Vector2(x, y));
    }

    public void press() {
        this.pressed = true;
        // PLAY SOUND
        DWController.playSound(MainAssets.switchSound);
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public void release() {
        this.pressed = false;
    }

    public void check() {
        this.group.unCheckAll();
        this.checked = true;
    }

    public void uncheck() {
        this.checked = false;
    }

    public boolean isChecked() {
        return this.checked;
    }

    public RadioGroup getGroup() {
        return this.group;
    }

    public void render(SpriteBatch batch, BitmapFont font, BitmapFont font_pressed) {
        if (pressed) {
            RenderUtil.drawTextInCell(batch, font_pressed, this.text, this.position.x + this.height, this.position.y, this.width-this.height, this.height);
            if(checked) {
                RenderUtil.drawTextureInCell(batch, texture_checked_pressed, this.position.x, this.position.y, this.height, this.height, 4*this.height/5f);
            } else {
                RenderUtil.drawTextureInCell(batch, texture_pressed, this.position.x, this.position.y, this.height, this.height, 4*this.height/5f);
            }
        } else {
            RenderUtil.drawTextInCell(batch, font, this.text, this.position.x + this.height, this.position.y, this.width-this.height, this.height);
            if(checked) {
                RenderUtil.drawTextureInCell(batch, texture_checked, this.position.x, this.position.y, this.height, this.height, 4*this.height/5f);
            } else {
                RenderUtil.drawTextureInCell(batch, texture, this.position.x, this.position.y, this.height, this.height, 4*this.height/5f);
            }
        }

    }
}
