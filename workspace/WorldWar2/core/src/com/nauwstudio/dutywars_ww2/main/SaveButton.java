package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.I18NBundle;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Map;

import java.text.MessageFormat;

public class SaveButton extends ButtonRectangle {

    private Save save;
    private SaveDeleteButton deleteButton;
    private MainAssets assets;

    private float delete_x, delete_y;
    private float save_map_x, save_map_y, save_map_width, save_map_height;
    private float save_title_x, save_title_y, save_title_width, save_title_height;
    private float save_map_name_x, save_map_name_y, save_map_name_width, save_map_name_height;
    private float save_turn_x, save_turn_y, save_turn_width, save_turn_height;
    private float save_players_x, save_players_y, save_players_width, save_players_height, save_player_tab_width;

    public SaveButton(Vector2 position, float width, float height, Save save, TextureRegion texture, TextureRegion texture_pressed, TextureRegion delete, TextureRegion delete_pressed) {
        super(position, width, height, BUTTON_SAVE, texture, texture_pressed, null);
        this.save = save;
        initHUDVars();
        createDeleteButton(delete, delete_pressed);
    }

    private void initHUDVars() {
        // Delete buttons
        delete_x = this.position.x + this.width - 0.7f * Util.getCircleButtonSize();
        delete_y = this.position.y + this.height - 0.7f * Util.getCircleButtonSize();
        // Save armies
        this.save_players_x = this.position.x + Util.getCircleButtonSpace()/4f;
        this.save_players_width = (this.width - 2*Util.getCircleButtonSpace()/4f)/(save.getPlayers().size()-1);
        this.save_players_height = Util.getCircleButtonSize();
        this.save_players_y = this.position.y + Util.getCircleButtonSpace()/4f;
        // Save map icon
        this.save_map_x = this.position.x + Util.getCircleButtonSpace()/4f;
        this.save_map_width = this.width/2.5f;
        this.save_map_height = this.height - save_players_height;
        this.save_map_y = this.save_players_y + save_players_height + Util.getCircleButtonSpace()/4f;
        // Save title
        this.save_title_x = this.save_map_x + this.save_map_width + Util.getCircleButtonSpace()/4f;
        this.save_title_width = this.width - this.save_map_width;
        this.save_title_height = (this.height - this.save_players_height)/3f;
        this.save_title_y = this.position.y + this.height - this.save_title_height;
        // Save map name
        this.save_map_name_x = this.save_map_x + this.save_map_width + Util.getCircleButtonSpace()/4f;
        this.save_map_name_width = this.width - this.save_map_width;
        this.save_map_name_height = (this.height - this.save_players_height)/4f;
        this.save_map_name_y = this.save_title_y - this.save_map_name_height;
        // Save turn
        this.save_turn_x = this.save_map_x + this.save_map_width + Util.getCircleButtonSpace()/4f;
        this.save_turn_width = this.width - this.save_map_width;
        this.save_turn_height = (this.height - this.save_players_height) / 4f;
        this.save_turn_y = this.save_map_name_y - this.save_turn_height;

    }

    public void render(SpriteBatch batch, MainAssets assets, BitmapFont big, BitmapFont small, I18NBundle bundle) {
        // Render background
        super.render(batch);
        // Render delete button
        this.deleteButton.render(batch);
        // Render map
        this.save.getMap().renderSmall(batch, this.save_map_x, this.save_map_y, this.save_map_width);
        // Render title
        RenderUtil.drawTextInCellLeft(batch, big, MessageFormat.format(bundle.get("main.game"), new String[]{save.getId() + ""}), save_title_x, save_title_y, save_title_width, save_title_height);
        // Render map name
        if(save.getMap().getType() == Map.CUSTOM) {
            RenderUtil.drawTextInCellLeft(batch, small, save.getMap().getName(), save_map_name_x, save_map_name_y, save_map_name_width, save_map_name_height);
        } else {
            RenderUtil.drawTextInCellLeft(batch, small, bundle.get(save.getMap().getName()), save_map_name_x, save_map_name_y, save_map_name_width, save_map_name_height);
        }
            // Render day
        RenderUtil.drawTextInCellLeft(batch, small, MessageFormat.format(bundle.get("main.day"), new String[]{save.getDay() + ""}), save_turn_x, save_turn_y, save_turn_width, save_turn_height);
        // Render players
        for(int i = 1 ; i < save.getPlayers().size() ; i ++) {
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[save.getPlayers().get(i).getArmy().getType()-1], save_players_x + (i-1)*save_players_width, save_players_y, save_players_width, save_players_height, Util.getCircleButtonSize());
            RenderUtil.drawTextureInCell(batch, save.getPlayers().get(i).getTexture(assets), save_players_x + (i-1)*save_players_width + save_players_width/2f, save_players_y + save_players_height -7*Util.getCircleButtonSpace()/8f, Util.getCircleButtonSize()/2f, save_players_height/2f, Util.getCircleButtonSize()/2f);
        }
    }

    public Save getSave() {
        return this.save;
    }

    public ButtonCircle getDeleteButton() {
        return this.deleteButton;
    }

    @Override
    public boolean isSaveButton() {
        return true;
    }

    public void createDeleteButton(TextureRegion delete, TextureRegion delete_pressed) {
        Vector2 close_pos = new Vector2(this.delete_x, this.delete_y);
        this.deleteButton = new SaveDeleteButton(close_pos, Util.getSmallCircleButtonSize(), delete, delete_pressed, this);

    }
}
