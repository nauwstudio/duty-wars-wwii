package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.MainScreen;
import com.nauwstudio.dutywars_ww2.Util;

public class MainInputHandler implements InputProcessor {

    MainScreen screen;
    Main main;
    MainRenderer renderer;

    public MainInputHandler(MainScreen screen) {
        this.screen = screen;
        this.main = screen.getMain();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.main.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        this.main.message = "DOWN";
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Touching button ?
        float button_x = this.renderer.getMenuCameraOriginX() + screenX;
        float button_y = this.renderer.getMenuCameraOriginY() + screenY;
        this.screen.touchedButton = this.main.getTouchedButton(button_x, button_y);
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.press();
            return true;
        } else {
            this.screen.touchedToggle = this.main.getTouchedToggleButton(button_x, button_y);
            if (this.screen.touchedToggle != null) {
                this.screen.touchedToggle.press();
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.main.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        } else {
            if (this.screen.touchedToggle != null) {
                // Perform button action
                this.main.touchToggleButton(this.screen.touchedToggle);
                this.screen.touchedToggle.release();
                this.screen.touchedToggle = null;
                return true;
            } else {
                reset();
                return false;
            }
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void disableGesture() {
        this.screen.can_pan = false;
    }

    public void enableGesture() {
        this.screen.can_pan = true;
    }

    public void reset() {
        enableGesture();
        this.screen.touchedButton = null;
    }
}
