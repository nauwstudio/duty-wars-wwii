package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.I18NBundle;
import com.nauwstudio.dutywars_ww2.ArmyAssets;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Map;

public class MapButton extends ButtonRectangle {

    private Map map;
    private MapDeleteButton deleteButton;
    private MapExportButton exportButton;

    private float delete_x, delete_y;
    private float export_x, export_y;
    private float map_x, map_y, map_width, map_height;
    private float map_name_x, map_name_y, map_name_width, map_name_height;
    private float map_players_x, map_players_y, map_players_width, map_players_height;
    private float map_buildings_x, map_buildings_y, map_buildings_width, map_buildings_height, map_buildings_icon_size;

    public MapButton(Vector2 position, float width, float height, Map map, MainAssets assets) {
        super(position, width, height, BUTTON_MAP, assets.button_save, assets.button_save_pressed, null);
        this.map = map;
        initHUDVars();
        createDeleteButton(assets.button_close, assets.button_close_pressed);
        createExportButton(assets.button_export, assets.button_export_pressed);
    }

    private void initHUDVars() {
        // Delete button
        delete_x = this.position.x + this.width - 0.7f * Util.getCircleButtonSize();
        delete_y = this.position.y + this.height - 0.7f * Util.getCircleButtonSize();
        // Export button
        export_x = this.delete_x - Util.getSmallCircleButtonSize() - 0.7f * Util.getSmallCircleButtonSize();
        export_y = delete_y;
        // Map name
        this.map_name_x = this.position.x + Util.getCircleButtonSpace()/4f;
        this.map_name_width = this.export_x - this.map_name_x - Util.getCircleButtonSpace()/4f;
        this.map_name_height = 0.8f * Util.getCircleButtonSize();
        this.map_name_y = this.position.y + this.height - this.map_name_height;
        // Map icon
        this.map_x = this.position.x + Util.getCircleButtonSpace()/4f;
        this.map_width = this.width/2.5f;
        this.map_height = this.height - map_name_height;
        this.map_y = this.position.y + Util.getCircleButtonSpace()/2f;
        // Map players
        this.map_players_x = this.map_x + this.map_width + Util.getCircleButtonSpace()/4f;
        this.map_players_width = this.width - this.map_width - Util.getCircleButtonSpace()/2f;
        this.map_players_height = (this.height-this.map_name_height)/2f;
        this.map_players_y = this.map_name_y - this.map_players_height;
        // Map buildings
        this.map_buildings_x = this.map_players_x;
        this.map_buildings_width = this.map_players_width;
        this.map_buildings_height = (this.height-this.map_name_height)/2f;
        this.map_buildings_y = this.position.y + Util.getCircleButtonSpace()/4f;
        this.map_buildings_icon_size = Util.getSmallCircleButtonSize();
    }

    public void render(SpriteBatch batch, MainAssets assets, ArmyAssets armyAssets, BitmapFont big, BitmapFont small, I18NBundle bundle) {
        // Render background
        super.render(batch);
        // Render delete button
        this.deleteButton.render(batch);
        // Render export button
        this.exportButton.render(batch);
        // Render name
        RenderUtil.drawTextInCell(batch, big, map.getName(), map_name_x, map_name_y, map_name_width, map_name_height);
        // Render map
        this.map.renderSmall(batch, this.map_x, this.map_y, this.map_width);
        // Render players
        for(int i = 0; i < this.map.getPlayersAmount() ; i ++) {
            RenderUtil.drawTextureInCell(batch, assets.human_alive, map_players_x + i*2*map_players_width/9f, map_players_y, map_players_width/9f, map_players_height, Util.getTinyCircleButtonSize());
        }
        // Draw buildings
        RenderUtil.drawTextureInCell(batch, armyAssets.capital, this.map_buildings_x + 0 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y + this.map_buildings_height / 2f, this.map_buildings_width / 9f, this.map_buildings_height / 2f, this.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, assets.font_medium, "" + this.map.getCapitalAmount(), this.map_buildings_x + 0 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y, this.map_buildings_width / 9f, this.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, armyAssets.city, this.map_buildings_x + 1 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y + this.map_buildings_height / 2f, this.map_buildings_width / 9f, this.map_buildings_height / 2f, this.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, assets.font_medium, "" + this.map.getCityAmount(), this.map_buildings_x + 1 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y, this.map_buildings_width / 9f, this.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, armyAssets.factory, this.map_buildings_x + 2 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y + this.map_buildings_height / 2f, this.map_buildings_width / 9f, this.map_buildings_height / 2f, this.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, assets.font_medium, "" + this.map.getFactoryAmount(), this.map_buildings_x + 2 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y, this.map_buildings_width / 9f, this.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, armyAssets.airport, this.map_buildings_x + 3 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y + this.map_buildings_height / 2f, this.map_buildings_width / 9f, this.map_buildings_height / 2f, this.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, assets.font_medium, "" + this.map.getAirportAmount(), this.map_buildings_x + 3 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y, this.map_buildings_width / 9f, this.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, armyAssets.shipyard, this.map_buildings_x + 4 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y + this.map_buildings_height / 2f, this.map_buildings_width / 9f, this.map_buildings_height / 2f, this.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, assets.font_medium, "" + this.map.getShipyardAmount(), this.map_buildings_x + 4 * 2 * (this.map_buildings_width / 9f), this.map_buildings_y, this.map_buildings_width / 9f, this.map_buildings_height / 2f);

    }

    public Map getMap() {
        return this.map;
    }

    public ButtonCircle getDeleteButton() {
        return this.deleteButton;
    }

    public ButtonCircle getExportButton() {
        return this.exportButton;
    }

    @Override
    public boolean isMapButton() {
        return true;
    }

    public void createDeleteButton(TextureRegion delete, TextureRegion delete_pressed) {
        Vector2 delete_pos = new Vector2(this.delete_x, this.delete_y);
        this.deleteButton = new MapDeleteButton(delete_pos, Util.getSmallCircleButtonSize(), delete, delete_pressed, this);
    }

    public void createExportButton(TextureRegion export, TextureRegion export_pressed) {
        Vector2 export_pos = new Vector2(this.export_x, this.export_y);
        this.exportButton = new MapExportButton(export_pos, Util.getSmallCircleButtonSize(), export, export_pressed, this);
    }
}
