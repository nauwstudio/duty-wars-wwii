package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.ToggleGroup;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;

import java.text.MessageFormat;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class MainRenderer {

    private Main main;
    SpriteBatch batch;

    private OrthographicCamera mainCamera;
    private OrthographicCamera saveCamera;

    private MainAssets assets;
    private MainHUDVars hudVars;

    public MainRenderer(Main main) {
        this.main = main;
        // CAMERAS
        mainCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        mainCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        mainCamera.update();
        saveCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        saveCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        saveCamera.update();

        this.batch = new SpriteBatch();
        this.assets = this.main.getAssets();
        this.hudVars = this.main.getHUDVars();
    }

    public float getMenuCameraPositionX() {
        return this.mainCamera.position.x;
    }

    public float getMenuCameraPositionY() {
        return this.mainCamera.position.y;
    }

    public float getMenuCameraOriginX() {
        return getMenuCameraPositionX() - (getMenuCameraWidth() / 2f);
    }

    public float getMenuCameraOriginY() {
        return getMenuCameraPositionY() - (getMenuCameraHeight() / 2f);
    }

    public float getMenuCameraWidth() {
        return this.mainCamera.viewportWidth;
    }

    public float getMenuCameraHeight() {
        return this.mainCamera.viewportHeight;
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(1 / 255f, 1 / 255f, 1 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderBackground(delta);
        // Campaign saves screen
        if (main.isCampaign()) {
            renderCampaignSave(delta);
            renderSaveButton(delta);
        }
        // Versus saves screen
        else if (main.isVersus()) {
            renderVersusSave(delta);
            renderSaveButton(delta);
        }
        // Custom maps screen
        else if (main.isEditor()) {
            renderCustomMap(delta);
            renderMapButton(delta);
        }
        renderButton(delta);
        if (this.main.getSaveToDelete() != null) {
            renderSaveToDelete(delta, this.main.getSaveToDelete());
            renderDialogButton(delta);
        } else if (this.main.getMapToDelete() != null) {
            renderMapToDelete(delta, this.main.getMapToDelete());
            renderDialogButton(delta);
        } else if(this.main.isCreateMap()) {
            renderCreateMap(delta);
            renderDialogButton(delta);
        }
    }

    private void renderBackground(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        RenderUtil.drawBackground(batch, assets);
        if (this.main.isMain()) {
            RenderUtil.drawMainTitle(batch, assets.title);
        }
        batch.end();
    }

    private void renderButton(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        for (Button b : this.main.getFixedButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    private void renderCampaignSave(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw bg
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("main.campaign"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        // Draw saves
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("main.campaign_save"), this.hudVars.active_game_x, this.hudVars.active_game_y, this.hudVars.active_game_width, this.hudVars.active_game_height);
        batch.end();
    }

    private void renderVersusSave(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw bg
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("main.versus"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        // Draw saves
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("main.versus_save"), this.hudVars.active_game_x, this.hudVars.active_game_y, this.hudVars.active_game_width, this.hudVars.active_game_height);
        batch.end();
    }

    private void renderCustomMap(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw bg
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("main.map_editor"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        // Draw saves
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("main.custom_maps"), this.hudVars.active_game_x, this.hudVars.active_game_y, this.hudVars.active_game_width, this.hudVars.active_game_height);
        batch.end();
    }

    private void renderSaveButton(float delta) {
        /*Cut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) this.hudVars.active_game_y);
        batch.setProjectionMatrix(saveCamera.combined);
        batch.begin();
        for (SaveButton b : this.main.getSaveButtons()) {
            b.render(batch, assets, this.assets.font_big, this.assets.font_medium, this.assets.stringBundle);
        }
        batch.end();
        /*Uncut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) Util.getScreenHeight());
    }

    private void renderMapButton(float delta) {
        /*Cut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) this.hudVars.active_game_y);
        batch.setProjectionMatrix(saveCamera.combined);
        batch.begin();
        for (MapButton b : this.main.getMapButtons()) {
            b.render(batch, assets, this.main.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets(), this.assets.font_big, this.assets.font_medium, this.assets.stringBundle);
        }
        batch.end();
        /*Uncut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) Util.getScreenHeight());
    }

    private void renderSaveToDelete(float delta, Save save) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.mission_bg, this.hudVars.delete_x, this.hudVars.delete_y, this.hudVars.delete_width, this.hudVars.delete_height);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_big, MessageFormat.format(this.assets.stringBundle.get("main.game"), new String[]{save.getId() + ""}), this.hudVars.delete_title_x, this.hudVars.delete_title_y, this.hudVars.delete_title_width, this.hudVars.delete_title_height);
        // Draw text
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("main.delete_save"), this.hudVars.delete_text_x, this.hudVars.delete_text_y, this.hudVars.delete_text_width, this.hudVars.delete_text_height);
        batch.end();
    }

    private void renderMapToDelete(float delta, Map map) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.mission_bg, this.hudVars.delete_x, this.hudVars.delete_y, this.hudVars.delete_width, this.hudVars.delete_height);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_bigbig, map.getName(), this.hudVars.delete_title_x, this.hudVars.delete_title_y, this.hudVars.delete_title_width, this.hudVars.delete_title_height);
        // Draw text
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("main.delete_map"), this.hudVars.delete_text_x, this.hudVars.delete_text_y, this.hudVars.delete_text_width, this.hudVars.delete_text_height);
        batch.end();
    }

    private void renderDialogButton(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        for (Button b : this.main.getDialogButtons()) {
            b.render(batch);
        }
        for (ToggleGroup tg : this.main.getToggleGroups()) {
            tg.render(batch);
        }
        batch.end();
    }

    public void renderCreateMap(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        batch.draw(assets.mission_bg, this.hudVars.create_map_x, this.hudVars.create_map_y, this.hudVars.create_map_width, this.hudVars.create_map_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("main.create_map"), this.hudVars.create_map_title_x, this.hudVars.create_map_title_y, this.hudVars.create_map_title_width, this.hudVars.create_map_title_height);
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("main.name"), this.hudVars.name_title_x, this.hudVars.name_title_y, this.hudVars.name_title_width, this.hudVars.name_title_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.main.getCreateMapName()+"", this.hudVars.name_x, this.hudVars.name_y, this.hudVars.name_width, this.hudVars.name_height);
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("main.size"), this.hudVars.size_title_x, this.hudVars.size_title_y, this.hudVars.size_title_width, this.hudVars.size_title_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.main.getCreateMapSize()+"", this.hudVars.size_x + this.hudVars.size_width/4f, this.hudVars.size_y, this.hudVars.size_width/2f, this.hudVars.size_height);
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("main.players"), this.hudVars.player_title_x, this.hudVars.player_title_y, this.hudVars.player_title_width, this.hudVars.player_title_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.main.getCreateMapPlayersAmount()+"", this.hudVars.player_x + this.hudVars.player_width/4f, this.hudVars.player_y, this.hudVars.player_width/2f, this.hudVars.player_height);
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("main.tile"), this.hudVars.tile_title_x, this.hudVars.tile_title_y, this.hudVars.tile_title_width, this.hudVars.tile_title_height);
        main.getCreatedMap().renderSmall(batch, this.hudVars.map_x, this.hudVars.map_y, this.hudVars.map_width);
        batch.end();
    }

    public float getSaveCameraPositionX() {
        return this.saveCamera.position.x;
    }

    public float getSaveCameraPositionY() {
        return this.saveCamera.position.y;
    }

    public float getSaveCameraOriginX() {
        return getSaveCameraPositionX() - (getSaveCameraWidth() / 2f);
    }

    public float getSaveCameraOriginY() {
        return getSaveCameraPositionY() - (getSaveCameraHeight() / 2f);
    }

    public float getSaveCameraWidth() {
        return this.saveCamera.viewportWidth;
    }

    public float getSaveCameraHeight() {
        return this.saveCamera.viewportHeight;
    }

    public void moveSaveCamera(float distY) {
        // Move only if no dialog open
        if (this.main.getSaveToDelete() == null) {
            float border_top = this.getSaveCameraHeight() / 2f;
            float last_save_y = this.main.getSaveButtons().get(this.main.getSaveButtons().size() - 1).getPosition().y;
            float border_bottom = Math.min(this.getSaveCameraHeight() / 2f, this.getSaveCameraHeight() / 2f + last_save_y - Util.getCircleButtonSpace());
            this.saveCamera.translate(0, distY);
            this.saveCamera.update();
            // Out of borders ?
            if (this.saveCamera.position.y < border_bottom) {
                this.saveCamera.translate(0, border_bottom - this.saveCamera.position.y);
            }
            if (this.saveCamera.position.y > border_top) {
                this.saveCamera.translate(0, border_top - this.saveCamera.position.y);
            }
            this.saveCamera.update();
        }
    }

    public void moveMapCamera(float distY) {
        // Move only if no dialog open
        if (this.main.getMapToDelete() == null) {
            float border_top = this.getSaveCameraHeight() / 2f;
            float last_map_y = this.main.getMapButtons().get(this.main.getMapButtons().size() - 1).getPosition().y;
            float border_bottom = Math.min(this.getSaveCameraHeight() / 2f, this.getSaveCameraHeight() / 2f + last_map_y - Util.getCircleButtonSpace());
            this.saveCamera.translate(0, distY);
            this.saveCamera.update();
            // Out of borders ?
            if (this.saveCamera.position.y < border_bottom) {
                this.saveCamera.translate(0, border_bottom - this.saveCamera.position.y);
            }
            if (this.saveCamera.position.y > border_top) {
                this.saveCamera.translate(0, border_top - this.saveCamera.position.y);
            }
            this.saveCamera.update();
        }
    }

    public void resize(int width, int height) {
        try {
            // UPDATE HUD VARIABLES
            this.hudVars.initHUDVariables();
            mainCamera = new OrthographicCamera(width, height);
            mainCamera.setToOrtho(false, width, height);
            mainCamera.update();
            saveCamera = new OrthographicCamera(width, (int) this.hudVars.active_game_y);
            saveCamera.setToOrtho(false, width, (int) this.hudVars.active_game_y);
            saveCamera.update();
            // REPLACE BUTTONS
            if (this.main.isCampaign()) {
                this.main.createCampaignSavesButtons();
                if (this.main.getSaveToDelete() != null) {
                    this.main.createDeleteSaveButtons();
                }
            } else if (this.main.isVersus()) {
                this.main.createVersusSavesButtons();
                if (this.main.getSaveToDelete() != null) {
                    this.main.createDeleteSaveButtons();
                }
            } else if (this.main.isEditor()) {
                this.main.createMapEditorButtons();
                if (this.main.getMapToDelete() != null) {
                    this.main.createDeleteSaveButtons();
                } else if(this.main.isCreateMap()) {
                    this.main.createCreateMapButtons();
                }
            } else {
                this.main.createMainButtons();
            }
        } catch (Exception e) {
            main.message = "resize() : " + e.toString();
        }
    }
}
