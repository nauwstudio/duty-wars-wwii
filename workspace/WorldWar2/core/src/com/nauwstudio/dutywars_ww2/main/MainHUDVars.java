package com.nauwstudio.dutywars_ww2.main;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class MainHUDVars {

    private Main main;

    // BUTTONS
    float versus_x, versus_y;
    float campaign_x, campaign_y;
    float map_editor_x, map_editor_y;
    float settings_x, settings_y;
    // COMMON
    float title_x, title_y, title_width, title_height;
    // SAVES
    float new_game_x, new_game_y;
    float back_x, back_y;
    float import_x, import_y;
    float active_game_x, active_game_y, active_game_width, active_game_height;
    float save_x, save_y, save_width, save_height;
    // DELETE
    public float delete_x, delete_y, delete_width, delete_height;
    public float delete_close_x, delete_close_y;
    public float delete_title_x, delete_title_y, delete_title_width, delete_title_height;
    public float delete_text_x, delete_text_y, delete_text_width, delete_text_height;
    public float delete_ok_x, delete_ok_y;
    // CREATE MAP DIALOG
    float create_map_x, create_map_y, create_map_width, create_map_height;
    float create_map_cancel_x, create_map_cancel_y;
    float create_map_ok_x, create_map_ok_y;
    float create_map_title_x, create_map_title_y, create_map_title_width, create_map_title_height;
    float name_title_x, name_title_y, name_title_width, name_title_height;
    float name_x, name_y, name_width, name_height;
    float size_title_x, size_title_y, size_title_width, size_title_height;
    float size_x, size_y, size_width, size_height;
    float player_title_x, player_title_y, player_title_width, player_title_height;
    float player_x, player_y, player_width, player_height;
    float tile_title_x, tile_title_y, tile_title_width, tile_title_height;
    float tile_x, tile_y, tile_width, tile_height;
    float map_x, map_y, map_width, map_height;

    public MainHUDVars(Main main) {
        this.main = main;
        initHUDVariables();
    }

    public void initHUDVariables() {
        try {
            float button_ratio = this.main.getAssets().button_versus.getRegionWidth() / (float) this.main.getAssets().button_versus.getRegionHeight();
            if (Util.isVertical()) {
                versus_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                versus_y = Util.getScreenHeight() / 2f - (Util.getRectangleButtonHeight()) / 2f;
                campaign_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                campaign_y = versus_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
                map_editor_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                map_editor_y = campaign_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
                settings_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                settings_y = map_editor_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            } else {
                versus_x = Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                versus_y = Util.getScreenHeight() / 2f - (Util.getRectangleButtonHeight()) / 2f;
                campaign_x = 3 * Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                campaign_y = versus_y;
                map_editor_x = Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                map_editor_y = versus_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
                settings_x = 3 * Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
                settings_y = map_editor_y;
            }
            this.title_x = 0;
            this.title_width = Util.getScreenWidth();
            this.title_height = Util.getCircleButtonSize();
            this.title_y = Util.getScreenHeight() - this.title_height;
            this.new_game_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.new_game_y = this.title_y - Util.getCircleButtonSize();
            this.back_x = Util.getCircleButtonSpace() / 4f;
            this.back_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
            this.import_x = Util.getScreenWidth() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
            this.import_y = this.back_y;
            this.active_game_x = 0;
            this.active_game_width = Util.getScreenWidth();
            this.active_game_height = Util.getCircleButtonSize();
            this.active_game_y = this.new_game_y - this.active_game_height;
            this.save_width = Math.min(Util.getScreenHeight(), Util.getScreenWidth()) - Util.getCircleButtonSpace();
            this.save_x = Util.getScreenWidth() / 2f - this.save_width / 2f;
            this.save_height = 3 * Util.getCircleButtonSize();
            this.save_y = Util.getScreenHeight() - this.save_height * 2;
            initDeleteSaveVars(button_ratio);
            initCreateMapVars(button_ratio);
        } catch (Exception e) {
            main.message = "initHUD : " + e.toString();
        }
    }

    private void initDeleteSaveVars(float button_ratio) {
        this.delete_width = Math.min(Util.getScreenHeight(), Util.getScreenWidth()) - Util.getCircleButtonSpace() / 2f;
        this.delete_height = this.delete_width / 2f;
        this.delete_x = Util.getScreenWidth() / 2f - this.delete_width / 2f;
        this.delete_y = Util.getScreenHeight() / 2f - this.delete_height / 2f;
        // delete close
        delete_close_x = this.delete_x + this.delete_width - 0.7f * Util.getCircleButtonSize();
        delete_close_y = this.delete_y + this.delete_height - 0.7f * Util.getCircleButtonSize();
        // delete title
        this.delete_title_width = this.delete_width;
        this.delete_title_height = this.delete_height / 3f;
        this.delete_title_x = this.delete_x;
        this.delete_title_y = this.delete_y + this.delete_height - this.delete_title_height;
        // delete text
        this.delete_text_width = this.delete_width;
        this.delete_text_height = this.delete_height / 3f;
        this.delete_text_x = this.delete_x;
        this.delete_text_y = this.delete_title_y - this.delete_text_height;
        // delete button
        delete_ok_x = this.delete_x + this.delete_width / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        delete_ok_y = this.delete_y + Util.getCircleButtonSpace() / 4f;

    }

    private void initCreateMapVars(float button_ratio) {
        this.create_map_width = Util.getScreenWidth() - Util.getCircleButtonSpace();
        this.create_map_x = Util.getScreenWidth() / 2f - this.create_map_width / 2f;
        this.create_map_height = Util.getScreenHeight() - Util.getCircleButtonSpace();
        this.create_map_y = Util.getScreenHeight() / 2f - this.create_map_height / 2f;
        this.create_map_cancel_x = this.create_map_x + this.create_map_width - 0.7f * Util.getCircleButtonSize();
        this.create_map_cancel_y = this.create_map_y + this.create_map_height - 0.7f * Util.getCircleButtonSize();
        this.create_map_ok_x = this.create_map_x + this.create_map_width / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        this.create_map_ok_y = this.create_map_y + Util.getCircleButtonSpace() / 4f;
        if (Util.isVertical()) {
            this.create_map_title_width = this.create_map_width;
            this.create_map_title_x = create_map_x;
            this.create_map_title_height = this.create_map_height / 9f;
            this.create_map_title_y = this.create_map_y + this.create_map_height - this.create_map_title_height;
            this.name_title_x = this.create_map_x + Util.getCircleButtonSpace()/4f;
            this.name_title_width = this.create_map_width - Util.getCircleButtonSpace()/2f;
            this.name_title_height = (this.create_map_height - this.create_map_title_height - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace()/4f)/11f;
            this.name_title_y = this.create_map_title_y - this.name_title_height;
            this.name_x = this.name_title_x;
            this.name_width = this.create_map_width - Util.getCircleButtonSpace()/2f;
            this.name_height = this.name_title_height;
            this.name_y = this.name_title_y - this.name_height;
			this.player_title_x = this.create_map_x + Util.getCircleButtonSpace()/4f;
            this.player_title_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.player_title_height = this.name_title_height;
            this.player_title_y = this.name_y - this.player_title_height;
            this.player_x = this.player_title_x;
            this.player_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.player_height = this.name_title_height;
            this.player_y = this.player_title_y - this.player_height;
            this.size_title_x = this.create_map_x + this.create_map_width/2f + Util.getCircleButtonSpace()/4f;
            this.size_title_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.size_title_height = this.name_title_height;
            this.size_title_y = this.player_title_y;
            this.size_x = this.size_title_x;
            this.size_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.size_height = this.name_title_height;
            this.size_y = this.size_title_y - this.size_height;
            this.tile_title_x = this.name_title_x;
            this.tile_title_width = this.name_title_width;
            this.tile_title_height = this.name_title_height;
            this.tile_title_y = this.size_y - this.tile_title_height;
            this.tile_x = this.create_map_x + Util.getCircleButtonSpace();
            this.tile_width = this.create_map_width - Util.getCircleButtonSpace()*2;
            this.tile_height = this.name_title_height;
            this.tile_y = this.tile_title_y - this.tile_height + Util.getCircleButtonSpace()/2f;
            this.map_x = this.create_map_x + Util.getCircleButtonSpace()*1.5f;
            this.map_width = this.create_map_width - Util.getCircleButtonSpace()*3;
            this.map_height = 5*this.name_title_height;
            this.map_y = this.tile_y - this.map_height;
        } else {
            this.create_map_title_width = this.create_map_width;
            this.create_map_title_x = create_map_x;
            this.create_map_title_height = this.create_map_height / 8f;
            this.create_map_title_y = this.create_map_y + this.create_map_height - this.create_map_title_height;
            this.name_title_x = this.create_map_x + Util.getCircleButtonSpace()/4f;
            this.name_title_width = this.size_title_width;
            this.name_title_height = (this.create_map_height - this.create_map_title_height - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace())/6f;
            this.name_title_y = this.create_map_title_y - this.name_title_height;
            this.name_x = this.name_title_x;
            this.name_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.name_height = this.name_title_height;
            this.name_y = this.name_title_y - this.name_height;
            this.player_title_x = this.create_map_x + Util.getCircleButtonSpace()/4f;
            this.player_title_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.player_title_height = this.name_title_height;
            this.player_title_y = this.name_y - this.player_title_height;
            this.player_x = this.player_title_x + Util.getCircleButtonSpace()/4f;
            this.player_width = this.create_map_width/2f - Util.getCircleButtonSpace();
            this.player_height = this.name_title_height;
            this.player_y = this.player_title_y - this.player_height;
            this.size_title_x = this.create_map_x + Util.getCircleButtonSpace()/4f;;
            this.size_title_width = this.create_map_width/2f - Util.getCircleButtonSpace()/2f;
            this.size_title_height = this.name_title_height;
            this.size_title_y = this.player_y - this.size_title_height;
            this.size_x = this.size_title_x + Util.getCircleButtonSpace()/4f;
            this.size_width = this.create_map_width/2f - Util.getCircleButtonSpace();
            this.size_height = this.name_title_height;
            this.size_y = this.size_title_y - this.size_height;
            //
            this.tile_title_x = this.size_title_x + this.create_map_width/2 + Util.getCircleButtonSpace()/4f;
            this.tile_title_width = this.size_title_width;
            this.tile_title_height = this.name_title_height;
            this.tile_title_y = this.create_map_title_y - this.tile_title_height;
            this.tile_x = this.create_map_x + create_map_width/2f + Util.getCircleButtonSpace();
            this.tile_width = this.create_map_width/2f - Util.getCircleButtonSpace()*2;
            this.tile_height = this.name_title_height;
            this.tile_y = this.tile_title_y - this.tile_height + Util.getCircleButtonSpace()/2f;
            this.map_x = this.create_map_x + create_map_width/2f + Util.getCircleButtonSpace()*1.5f;
            this.map_width = this.create_map_width/2f - Util.getCircleButtonSpace()*3;
            this.map_height = 4*this.name_title_height;
            this.map_y = this.tile_y - this.map_height - Util.getCircleButtonSpace()/2f;
        }
    }
}
