package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.MainScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.campaign.Campaign;
import com.nauwstudio.dutywars_ww2.campaign.CampaignRenderer;

public class MainGestureDetector implements GestureListener {

    MainScreen screen;
    Main main;
    MainRenderer renderer;

    public MainGestureDetector(MainScreen screen) {
        this.screen = screen;
        this.main = screen.getMain();
        this.renderer = screen.getRenderer();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return true;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        // Reverse y-axis
        y = Util.getScreenHeight() - y;
        // Release button
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
        }
        // Move button camera
        if (this.main.getSaveButtons() != null && !this.main.getSaveButtons().isEmpty()&& y < this.main.getHUDVars().active_game_y) {
            this.renderer.moveSaveCamera(deltaY * 1.0f);
            return true;
        } else if (this.main.getMapButtons() != null && !this.main.getMapButtons().isEmpty()&& y < this.main.getHUDVars().active_game_y) {
            this.renderer.moveMapCamera(deltaY * 1.0f);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {
    }
}
