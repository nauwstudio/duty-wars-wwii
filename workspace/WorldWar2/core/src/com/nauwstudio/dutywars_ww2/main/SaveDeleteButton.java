package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.campaign.Mission;

public class SaveDeleteButton extends ButtonCircle {

    private SaveButton saveButton;

    public SaveDeleteButton(Vector2 position, float width, TextureRegion texture, TextureRegion texture_pressed, SaveButton saveButton) {
        super(position, width, BUTTON_DELETE_SAVE, texture, texture_pressed, null);
        this.saveButton = saveButton;
    }

    public void render(SpriteBatch batch, MainAssets assets) {
        super.render(batch);
    }

    public SaveButton getSaveButton() {
        return this.saveButton;
    }

    @Override
    public boolean isDeleteSaveButton() {
        return true;
    }
}
