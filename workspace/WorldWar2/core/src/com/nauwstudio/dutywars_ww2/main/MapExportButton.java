package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;

public class MapExportButton extends ButtonCircle {

    private MapButton mapButton;

    public MapExportButton(Vector2 position, float width, TextureRegion texture, TextureRegion texture_pressed, MapButton mapButton) {
        super(position, width, BUTTON_EXPORT_MAP, texture, texture_pressed, null);
        this.mapButton = mapButton;
    }

    public void render(SpriteBatch batch, MainAssets assets) {
        super.render(batch);
    }

    public MapButton getMapButton() {
        return this.mapButton;
    }

    @Override
    public boolean isDeleteSaveButton() {
        return true;
    }
}
