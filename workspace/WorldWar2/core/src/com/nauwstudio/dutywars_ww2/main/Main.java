package com.nauwstudio.dutywars_ww2.main;

import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.MainScreen;
import com.nauwstudio.dutywars_ww2.MapFile;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.ToggleButton;
import com.nauwstudio.dutywars_ww2.ToggleGroup;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Game;
import com.nauwstudio.dutywars_ww2.game.GameObject;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Tile;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;

import java.util.ArrayList;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class Main {

    private enum MainState {
        MAIN, VERSUS, CAMPAIGN, EDITOR
    }

    private MainScreen screen;

    private ArrayList<Button> fixedButtons;
    private ArrayList<SaveButton> saveButtons;
    private ArrayList<MapButton> mapButtons;
    private ArrayList<Button> dialogButtons;
    private ArrayList<ToggleGroup> toggleGroups;
    private Save saveToDelete = null;
    private Map mapToDelete = null;

    public String message;

    private MainAssets assets;
    public MainHUDVars hudVars;

    private MainState state;

    private ArrayList<Save> campaignSaves = new ArrayList<Save>();
    private ArrayList<Save> versusSaves = new ArrayList<Save>();
    private ArrayList<Map> customMaps = new ArrayList<Map>();

    private boolean createMap = false;
    private String createMapName = "";
    private int createMapSize = 10;
    private int createMapPlayersAmount = 2;
    private Map createdMap;

    public Main(MainScreen screen, MainAssets assets) {
        this.screen = screen;
        this.assets = assets;
        this.hudVars = new MainHUDVars(this);
        if (this.screen.getController().isMusic()) {
            this.screen.getController().startMenuMusic();
        }
        this.state = MainState.MAIN;
        createMainButtons();
        if (isCreateMap()) {
            createCreateMapButtons();
        }
        //TODO
        //tmxToXml();
    }

    private void tmxToXml() {
        // TRANSFORM TMX MAP HERE
        // 2 PLAYERS
        /*
        MapFile.tmxToXml("dual.tmx");
        System.out.println("MAP CREATED : " + "dual");
        MapFile.tmxToXml("infinite.tmx");
        System.out.println("MAP CREATED : " + "infinite");
        MapFile.tmxToXml("plus.tmx");
        System.out.println("MAP CREATED : " + "plus");
        MapFile.tmxToXml("australia.tmx");
        System.out.println("MAP CREATED : " + "australia");
        MapFile.tmxToXml("unknown.tmx");
        System.out.println("MAP CREATED : " + "unknown");
        // 3 PLAYERS
        MapFile.tmxToXml("triangle.tmx");
        MapFile.tmxToXml("poseidon.tmx");
        MapFile.tmxToXml("africa.tmx");
        MapFile.tmxToXml("maze.tmx");
        MapFile.tmxToXml("caribbean.tmx");
        // 4 PLAYERS
        MapFile.tmxToXml("square.tmx");
        MapFile.tmxToXml("spiral.tmx");
        MapFile.tmxToXml("rivers.tmx");
        MapFile.tmxToXml("eight.tmx");
        MapFile.tmxToXml("america.tmx");
        // 5 PLAYERS
        MapFile.tmxToXml("pentagon.tmx");
        MapFile.tmxToXml("europe.tmx");
        MapFile.tmxToXml("star.tmx");
        MapFile.tmxToXml("islands.tmx");
        MapFile.tmxToXml("world.tmx");
        // CAMPAIGN
        MapFile.tmxToXml("britain.tmx");
        System.out.println("MAP CREATED : " + "britain");
        MapFile.tmxToXml("tobruk.tmx");
        System.out.println("MAP CREATED : " + "tobruk");
        MapFile.tmxToXml("pearl_harbor.tmx");
        System.out.println("MAP CREATED : " + "pearl_harbor");
        MapFile.tmxToXml("hong_kong.tmx");
        System.out.println("MAP CREATED : " + "hong_kong");
        MapFile.tmxToXml("midway.tmx");
        System.out.println("MAP CREATED : " + "midway");
        MapFile.tmxToXml("stalingrad.tmx");
        System.out.println("MAP CREATED : " + "stalingrad");
        MapFile.tmxToXml("guadalcanal.tmx");
        System.out.println("MAP CREATED : " + "guadalcanal");
        MapFile.tmxToXml("el_alamein.tmx");
        System.out.println("MAP CREATED : " + "el_alamein");
        MapFile.tmxToXml("capri.tmx");
        System.out.println("MAP CREATED : " + "capri");
        MapFile.tmxToXml("kursk.tmx");
        System.out.println("MAP CREATED : " + "kursk");
        MapFile.tmxToXml("husky.tmx");
        System.out.println("MAP CREATED : " + "husky");
        MapFile.tmxToXml("kharkov.tmx");
        System.out.println("MAP CREATED : " + "kharkov");
        MapFile.tmxToXml("leningrad.tmx");
        MapFile.tmxToXml("crimea.tmx");
        MapFile.tmxToXml("roma.tmx");
        */
        MapFile.tmxToXml("omaha_beach.tmx");
        /*
        MapFile.tmxToXml("paris.tmx");
        MapFile.tmxToXml("market_garden.tmx");
        MapFile.tmxToXml("bulge.tmx");
        MapFile.tmxToXml("luzon.tmx");
        MapFile.tmxToXml("iwo_jima.tmx");
        MapFile.tmxToXml("vienna.tmx");
        MapFile.tmxToXml("okinawa.tmx");
        MapFile.tmxToXml("berlin.tmx");
        MapFile.tmxToXml("sakhalin.tmx");
        System.out.println("MAP CREATED : " + "sakhalin");
        */
    }

    public void createMainButtons() {
        clearButtons();
        // VERSUS BUTTON
        Vector2 versus_pos = new Vector2(hudVars.versus_x, hudVars.versus_y);
        this.fixedButtons.add(new ButtonRectangle(versus_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_VERSUS, assets.button_versus, assets.button_versus_pressed, assets.button_versus));
        // CAMPAIGN BUTTON
        Vector2 campaign_pos = new Vector2(hudVars.campaign_x, hudVars.campaign_y);
        this.fixedButtons.add(new ButtonRectangle(campaign_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_CAMPAIGN, assets.button_campaign, assets.button_campaign_pressed, assets.button_campaign));
        // MAP EDITOR BUTTON
        Vector2 map_editor_pos = new Vector2(hudVars.map_editor_x, hudVars.map_editor_y);
        this.fixedButtons.add(new ButtonRectangle(map_editor_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_MAP_EDITOR, assets.button_map_editor, assets.button_map_editor_pressed, assets.button_map_editor_disabled));
        // SETTINGS BUTTON
        Vector2 settings_pos = new Vector2(hudVars.settings_x, hudVars.settings_y);
        this.fixedButtons.add(new ButtonRectangle(settings_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_SETTINGS, assets.button_settings, assets.button_settings_pressed, assets.button_settings));
    }

    public void createCampaignSavesButtons() {
        clearButtons();
        // NEW GAME
        Vector2 new_game_pos = new Vector2(hudVars.new_game_x, hudVars.new_game_y);
        this.fixedButtons.add(new ButtonRectangle(new_game_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_NEW_CAMPAIGN_GAME, assets.button_new_game, assets.button_new_game_pressed, assets.button_new_game));
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.fixedButtons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
        // SAVES BUTTON
        System.out.println("lol:" + this.campaignSaves.size());
        for (int i = 0; i < this.campaignSaves.size(); i++) {
            Vector2 save_pos = new Vector2(hudVars.save_x, hudVars.save_y - i * (hudVars.save_height + Util.getCircleButtonSpace()));
            this.saveButtons.add(new SaveButton(save_pos, hudVars.save_width, hudVars.save_height, this.campaignSaves.get(i), assets.button_save, assets.button_save_pressed, assets.button_close, assets.button_close_pressed));
        }
    }

    public void createVersusSavesButtons() {
        clearButtons();
        // NEW GAME
        Vector2 new_game_pos = new Vector2(hudVars.new_game_x, hudVars.new_game_y);
        this.fixedButtons.add(new ButtonRectangle(new_game_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_NEW_VERSUS_GAME, assets.button_new_game, assets.button_new_game_pressed, assets.button_new_game));
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.fixedButtons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
        // SAVES BUTTON
        for (int i = 0; i < this.versusSaves.size(); i++) {
            Vector2 save_pos = new Vector2(hudVars.save_x, hudVars.save_y - i * (hudVars.save_height + Util.getCircleButtonSpace()));
            this.saveButtons.add(new SaveButton(save_pos, hudVars.save_width, hudVars.save_height, this.versusSaves.get(i), assets.button_save, assets.button_save_pressed, assets.button_close, assets.button_close_pressed));
        }
    }

    public void createDeleteSaveButtons() {
        // CANCEL
        Vector2 back_pos = new Vector2(hudVars.delete_close_x, hudVars.delete_close_y);
        this.dialogButtons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_close, assets.button_close_pressed, assets.button_close));
        // CONFIRM
        Vector2 new_game_pos = new Vector2(hudVars.delete_ok_x, hudVars.delete_ok_y);
        this.dialogButtons.add(new ButtonRectangle(new_game_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_CONFIRM, assets.button_confirm, assets.button_confirm_pressed, assets.button_new_game));
    }

    public void createMapEditorButtons() {
        clearButtons();
        // NEW MAP
        Vector2 new_map_pos = new Vector2(hudVars.new_game_x, hudVars.new_game_y);
        this.fixedButtons.add(new ButtonRectangle(new_map_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_NEW_MAP, assets.button_new_map, assets.button_new_map_pressed, assets.button_new_map));
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.fixedButtons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
        // IMPORT BUTTON
        Vector2 import_pos = new Vector2(hudVars.import_x, hudVars.import_y);
        this.fixedButtons.add(new ButtonCircle(import_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_IMPORT_MAP, assets.button_import, assets.button_import_pressed, assets.button_import));
        // SAVES BUTTON
        for (int i = 0; i < this.customMaps.size(); i++) {
            Vector2 map_pos = new Vector2(hudVars.save_x, hudVars.save_y - i * (hudVars.save_height + Util.getCircleButtonSpace()));
            this.mapButtons.add(new MapButton(map_pos, hudVars.save_width, hudVars.save_height, this.customMaps.get(i), assets));
        }
    }

    public void createCreateMapButtons() {
        this.dialogButtons = new ArrayList<Button>();
        // BACK BUTTON
        Vector2 cancel_pos = new Vector2(hudVars.create_map_cancel_x, hudVars.create_map_cancel_y);
        this.dialogButtons.add(new ButtonCircle(cancel_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_close, assets.button_close_pressed, assets.button_close));
		// PLAYER MINUS BUTTON
        Vector2 player_minus_pos = new Vector2(hudVars.player_x, hudVars.player_y);
        ButtonCircle minusPlayer = new ButtonCircle(player_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_PLAYER_MINUS, assets.button_minus, assets.button_minus_pressed, assets.button_minus_disabled);
        minusPlayer.disable();
        this.dialogButtons.add(minusPlayer);
        // PLAYER PLUS BUTTON
        Vector2 player_plus_pos = new Vector2(hudVars.player_x + hudVars.player_width - Util.getSmallCircleButtonSize(), hudVars.player_y);
        this.dialogButtons.add(new ButtonCircle(player_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_PLAYER_PLUS, assets.button_plus, assets.button_plus_pressed, assets.button_minus_disabled));
        // SIZE MINUS BUTTON
        Vector2 size_minus_pos = new Vector2(hudVars.size_x, hudVars.size_y);
        this.dialogButtons.add(new ButtonCircle(size_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_SIZE_MINUS, assets.button_minus, assets.button_minus_pressed, assets.button_minus_disabled));
        // SIZE PLUS BUTTON
        Vector2 size_plus_pos = new Vector2(hudVars.size_x + hudVars.size_width - Util.getSmallCircleButtonSize(), hudVars.size_y);
        this.dialogButtons.add(new ButtonCircle(size_plus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_SIZE_PLUS, assets.button_plus, assets.button_plus_pressed, assets.button_minus_disabled));

        // TILE TOGGLE GROUP
        ArrayList<GameObject> tiles = new ArrayList<GameObject>();
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(0), Tile.SEA, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(0), Tile.PLAIN, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(1), Tile.PLAIN, 0));
        tiles.add(new Tile(-1, -1, this.screen.getController().getSeasons().get(2), Tile.PLAIN, 0));
        Vector2 tile_pos = new Vector2(hudVars.tile_x, hudVars.tile_y);
        this.toggleGroups.add(new ToggleGroup(tile_pos, hudVars.tile_width, hudVars.tile_height, ToggleGroup.TOGGLE_GROUP_TILE, tiles, 1, 4, Util.getCircleButtonSpace() / 4f, assets));
        // CONFIRM BUTTON
        Vector2 confirm_pos = new Vector2(hudVars.create_map_ok_x, hudVars.create_map_ok_y);
        this.dialogButtons.add(new ButtonRectangle(confirm_pos, Util.getCircleButtonSize(), ButtonRectangle.BUTTON_CONFIRM, assets.button_confirm, assets.button_confirm_pressed, assets.button_confirm));
    }

    public void clearButtons() {
        this.fixedButtons = new ArrayList<Button>();
        this.saveButtons = new ArrayList<SaveButton>();
        this.mapButtons = new ArrayList<MapButton>();
        this.dialogButtons = new ArrayList<Button>();
        this.toggleGroups = new ArrayList<ToggleGroup>();
    }

    public Button getTouchedButton(float x, float y) {
        // A dialog is open => can touch dialog buttons only.
        if (dialogButtons != null && !dialogButtons.isEmpty()) {
            for (Button button : dialogButtons) {
                if (button.isTouched(x, y)) {
                    return button;
                }
            }
        }
        // No dialog opened => can touch each buttons.
        else {
            // Check fixed buttons
            if (fixedButtons != null && !fixedButtons.isEmpty()) {
                for (Button button : fixedButtons) {
                    if (button.isTouched(x, y)) {
                        return button;
                    }
                }
            }
            // Check save buttons
            if (saveButtons != null && !saveButtons.isEmpty()) {
                for (SaveButton button : saveButtons) {
                    if (button.getDeleteButton().isTouched(x + this.screen.getRenderer().getSaveCameraOriginX(), y + this.screen.getRenderer().getSaveCameraOriginY())) {
                        return button.getDeleteButton();
                    } else if (button.isTouched(x + this.screen.getRenderer().getSaveCameraOriginX(), y + this.screen.getRenderer().getSaveCameraOriginY())) {
                        return button;
                    }
                }
            }
            // Check map buttons
            if (mapButtons != null && !mapButtons.isEmpty()) {
                for (MapButton button : mapButtons) {
                    if (button.getDeleteButton().isTouched(x + this.screen.getRenderer().getSaveCameraOriginX(), y + this.screen.getRenderer().getSaveCameraOriginY())) {
                        return button.getDeleteButton();
                    } else if (button.getExportButton().isTouched(x + this.screen.getRenderer().getSaveCameraOriginX(), y + this.screen.getRenderer().getSaveCameraOriginY())) {
                        return button.getExportButton();
                    } else if (button.isTouched(x + this.screen.getRenderer().getSaveCameraOriginX(), y + this.screen.getRenderer().getSaveCameraOriginY())) {
                        return button;
                    }
                }
            }
        }
        return null;
    }

    public void touchButton(Button button) {
        if (button.isEnabled()) {
            // VERSUS BUTTON
            if (button.getTag().equals(ButtonRectangle.BUTTON_VERSUS)) {
                touchVersus();
            }
            // CAMPAIGN BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_CAMPAIGN)) {
                touchCampaign();
            }
            // SETTINGS BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_SETTINGS)) {
                this.screen.getController().toSettingsScreen();
            }
            // BACK BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_BACK)) {
                back();
            }
            // NEW CAMPAIGN GAME BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_NEW_CAMPAIGN_GAME)) {
                this.screen.getController().toCampaignScreen(0);
            }
            // NEW VERSUS GAME BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_NEW_VERSUS_GAME)) {
                this.screen.getController().toVersusScreen();
            }
            // SAVE BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_SAVE)) {
                this.screen.getController().loadResumeGameScreen(((SaveButton) button).getSave());
            }
            // DELETE SAVE BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_DELETE_SAVE)) {
                this.saveToDelete = ((SaveDeleteButton) button).getSaveButton().getSave();
                this.createDeleteSaveButtons();
            }
            // CONFIRM BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_CONFIRM)) {
                // DELETE SAVE ?
                if (this.saveToDelete != null) {
                    this.screen.getController().deleteSave(this.saveToDelete.getId());
                    if (this.saveToDelete.getMode() == Game.MODE_CAMPAIGN) {
                        this.saveToDelete = null;
                        touchCampaign();
                    } else {
                        this.saveToDelete = null;
                        touchVersus();
                    }
                }
                // DELETE MAP
                else if (this.mapToDelete != null) {
                    this.screen.getController().deleteMap(this.mapToDelete);
                    this.mapToDelete = null;
                    touchMapEditor();
                }
                // CREATE MAP
                else if (this.isCreateMap()) {
                    if (this.createMapName != null && !this.createMapName.replaceAll(" ", "").isEmpty()) {
                        if (this.screen.getController().getDatabaseInterface().checkMapName(this.createMapName)) {
                            Tile bg = (Tile) this.toggleGroups.get(0).getChecked().getObject();
                            this.screen.getController().createMap(this.createMapName, this.createMapSize, bg.getSeason().getType(), bg.getType(), this.createMapPlayersAmount);
                        } else {
                            this.screen.getController().showMessage("Map already exists");
                        }
                    } else {
                        this.screen.getController().showMessage("Map name cannot be empty");
                    }
                }
            }
            // MAP EDITOR BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_MAP_EDITOR)) {
                touchMapEditor();
            }
            // MAP IMPORT BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_IMPORT_MAP)) {
                this.screen.getController().openImportFolder(this);
            }
            // MAP BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_MAP)) {
                if (this.screen.getController().checkMapSaves(((MapButton) button).getMap().getId())) {
                    this.screen.getController().loadMapEditorScreen(((MapButton) button).getMap());
                } else {
                    this.screen.getController().showMessage(this.assets.stringBundle.get("main.cant_edit_map"));
                }
            }
            // CREATE MAP BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_NEW_MAP)) {
                this.createMap = true;
                this.createCreateMapButtons();
                this.createdMap = getResizedMap(this.createMapSize, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.createMapPlayersAmount);
                touchEditName();
            }
            // DELETE MAP BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_DELETE_MAP)) {
                if (this.screen.getController().checkMapSaves(((MapDeleteButton) button).getMapButton().getMap().getId())) {
                    this.mapToDelete = ((MapDeleteButton) button).getMapButton().getMap();
                    this.createDeleteSaveButtons();
                } else {
                    this.screen.getController().showMessage(this.assets.stringBundle.get("main.cant_delete_map"));
                }
            }
            // EXPORT MAP BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_EXPORT_MAP)) {
                this.screen.getController().exportMap(((MapExportButton) button).getMapButton().getMap().getFile());
            }
			// PLAYER PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PLAYER_PLUS)) {
                touchPlayerPlus();
            }
            // PLAYER MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_PLAYER_MINUS)) {
                touchPlayerMinus();
            }
            // SIZE PLUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_SIZE_PLUS)) {
                touchPlus();
            }
            // SIZE MINUS BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_SIZE_MINUS)) {
                touchMinus();
            }
        }
    }

    public void touchCampaign() {
        // Getting campaign save
        ArrayList<Save> saves = this.screen.getController().getSavesSummary(Game.MODE_CAMPAIGN);
        // INIT MAP FROM SAVES
        for (Save save : saves) {
            try {
                MapFile mapFile = new MapFile(save.getMap().getType(), MapFile.FOLDER + save.getMap().getFile());
                save.getMap().initMap(mapFile.getSize(), this.screen.getController().getSeasons(), mapFile.getTileLayer(), save.getBuildings());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (saves != null && !saves.isEmpty()) {
            this.state = MainState.CAMPAIGN;
            this.campaignSaves = saves;
            createCampaignSavesButtons();
        } else {
            this.screen.getController().toCampaignScreen(0);
        }
    }

    public void touchVersus() {
        // Getting versus save
        ArrayList<Save> saves = this.screen.getController().getSavesSummary(Game.MODE_VERSUS);
        // INIT MAP FROM SAVES
        for (Save save : saves) {
            try {
                MapFile mapFile = new MapFile(save.getMap().getType(), MapFile.FOLDER + save.getMap().getFile());
                save.getMap().initMap(mapFile.getSize(), this.screen.getController().getSeasons(), mapFile.getTileLayer(), save.getBuildings());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (saves != null && !saves.isEmpty()) {
            this.state = MainState.VERSUS;
            this.versusSaves = saves;
            createVersusSavesButtons();
        } else {
            this.screen.getController().toVersusScreen();
        }
    }

    public void touchMapEditor() {
        // Check file access permission
        if (this.screen.getController().checkPermission()) {
            this.state = MainState.EDITOR;
            // Getting custom maps
            ArrayList<Map> maps = this.screen.getController().getMapsByType(Map.CUSTOM);
            // INIT MAP
            for (Map map : maps) {
                // Load map
                map.initMap(this.screen.getController().getSeasons(), this.screen.getController().getPlayers());
            }
            System.out.println("MAP EDITOR SIZE : " + maps.size());
            this.customMaps = maps;
            createMapEditorButtons();
        } else {
            this.screen.getController().getPermission();
        }
    }

    public void touchEditName() {
        this.screen.openTextInput(this.assets.stringBundle.get("main.name_dialog_title"), this.createMapName, "");
    }

    public void touchMinus() {
        this.createMapSize = Math.max(Map.SIZE_MIN, this.createMapSize - 1);
        if (this.createMapSize == Map.SIZE_MIN) {
            disableButton(ButtonCircle.BUTTON_SIZE_MINUS);
        }
        if (this.createMapSize < Map.SIZE_MAX) {
            enableButton(ButtonCircle.BUTTON_SIZE_PLUS);
        }
        // Update map
        this.createdMap = getResizedMap(this.createMapSize, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.createMapPlayersAmount);
    }

    public void touchPlus() {
        this.createMapSize = Math.min(Map.SIZE_MAX, this.createMapSize + 1);
        if (this.createMapSize == Map.SIZE_MAX) {
            disableButton(ButtonCircle.BUTTON_SIZE_PLUS);
        }
        if (this.createMapSize > Map.SIZE_MIN) {
            enableButton(ButtonCircle.BUTTON_SIZE_MINUS);
        }
        // Update map
        this.createdMap = getResizedMap(this.createMapSize, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.createMapPlayersAmount);
    }

    public void touchPlayerMinus() {
        this.createMapPlayersAmount = Math.max(Map.PLAYER_MIN, this.createMapPlayersAmount - 1);
        if (this.createMapPlayersAmount == Map.PLAYER_MIN) {
            disableButton(ButtonCircle.BUTTON_PLAYER_MINUS);
        }
        if (this.createMapPlayersAmount < Map.PLAYER_MAX) {
            enableButton(ButtonCircle.BUTTON_PLAYER_PLUS);
        }
    }

    public void touchPlayerPlus() {
        this.createMapPlayersAmount = Math.min(Map.PLAYER_MAX, this.createMapPlayersAmount + 1);
        if (this.createMapPlayersAmount == Map.PLAYER_MAX) {
            disableButton(ButtonCircle.BUTTON_PLAYER_PLUS);
        }
        if (this.createMapPlayersAmount > Map.PLAYER_MIN) {
            enableButton(ButtonCircle.BUTTON_PLAYER_MINUS);
        }
        // Update map
        this.createdMap = getResizedMap(this.createMapSize, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.createMapPlayersAmount);
    }

    private void disableButton(String tag) {
        if (dialogButtons != null) {
            for (Button button : this.dialogButtons) {
                if (button.getTag().equals(tag)) {
                    button.disable();
                }
            }
        }
    }

    private void enableButton(String tag) {
        if (dialogButtons != null) {
            for (Button button : this.dialogButtons) {
                if (button.getTag().equals(tag)) {
                    button.enable();
                }
            }
        }
    }

    public ToggleButton getTouchedToggleButton(float x, float y) {
        if (isCreateMap()) {
            if (toggleGroups != null) {
                for (ToggleGroup group : toggleGroups) {
                    for (ToggleButton button : group.getButtons()) {
                        if (button.isTouched(x, y)) {
                            return button;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void touchToggleButton(ToggleButton button) {
        if (isCreateMap()) {
            button.check();
            // Update map
            this.createdMap = getResizedMap(this.createMapSize, (Tile) this.toggleGroups.get(0).getChecked().getObject(), this.createMapPlayersAmount);
        }
    }

    public void updateCreateMapName(String text) {
        this.createMapName = text;
    }

    public void back() {
        if (isCampaign()) {
            if (this.saveToDelete != null) {
                this.saveToDelete = null;
                this.dialogButtons = new ArrayList<Button>();
            } else {
                this.campaignSaves = null;
                this.state = MainState.MAIN;
                this.createMainButtons();
            }
        } else if (isVersus()) {
            if (this.saveToDelete != null) {
                this.saveToDelete = null;
                this.dialogButtons = new ArrayList<Button>();
            } else {
                this.versusSaves = null;
                this.state = MainState.MAIN;
                this.createMainButtons();
            }
        } else if (isEditor()) {
            if (this.mapToDelete != null) {
                this.mapToDelete = null;
                this.dialogButtons = new ArrayList<Button>();
            } else if (this.isCreateMap()) {
                this.createMap = false;
                this.dialogButtons = new ArrayList<Button>();
                this.createMapName = "";
                this.createMapSize = 10;
            } else {
                this.customMaps = null;
                this.state = MainState.MAIN;
                this.createMainButtons();
            }
        } else {
            this.screen.exit();
        }
    }

    public void resume() {
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        // REPLACE BUTTONS
        if (isCampaign()) {
            this.createCampaignSavesButtons();
            if (this.getSaveToDelete() != null) {
                this.createDeleteSaveButtons();
            }
        } else if (isVersus()) {
            this.createVersusSavesButtons();
            if (this.getSaveToDelete() != null) {
                this.createDeleteSaveButtons();
            }
        } else if (isEditor()) {
            System.out.println("RESUME RESUME");
            this.createMapEditorButtons();
            if (this.getMapToDelete() != null) {
                this.createDeleteSaveButtons();
            } else if (isCreateMap()) {
                System.out.println("AHAHAHAH");
                this.createCreateMapButtons();
            }
        } else {
            this.createMainButtons();
        }
    }

    public MainAssets getAssets() {
        return this.assets;
    }

    public MainHUDVars getHUDVars() {
        return this.hudVars;
    }

    public ArrayList<Button> getFixedButtons() {
        return this.fixedButtons;
    }

    public ArrayList<SaveButton> getSaveButtons() {
        return this.saveButtons;
    }

    public ArrayList<MapButton> getMapButtons() {
        return this.mapButtons;
    }

    public ArrayList<Button> getDialogButtons() {
        return this.dialogButtons;
    }

    public ArrayList<ToggleGroup> getToggleGroups() {
        return this.toggleGroups;
    }

    public ArrayList<Save> getCampaignSaves() {
        return this.campaignSaves;
    }

    public ArrayList<Save> getVersusSaves() {
        return this.versusSaves;
    }

    public ArrayList<Map> getCustomMaps() {
        return this.customMaps;
    }

    public Save getSaveToDelete() {
        return this.saveToDelete;
    }

    public Map getMapToDelete() {
        return this.mapToDelete;
    }

    public MainScreen getScreen() {
        return this.screen;
    }

    public boolean isCreateMap() {
        return this.createMap;
    }

    public int getCreateMapSize() {
        return this.createMapSize;
    }

    public int getCreateMapPlayersAmount() {
        return this.createMapPlayersAmount;
    }

    public String getCreateMapName() {
        return this.createMapName;
    }

    public boolean isMain() {
        return this.state == MainState.MAIN;
    }

    public boolean isVersus() {
        return this.state == MainState.VERSUS;
    }

    public boolean isCampaign() {
        return this.state == MainState.CAMPAIGN;
    }

    public boolean isEditor() {
        return this.state == MainState.EDITOR;
    }

    private Map getResizedMap(int size, Tile defaultTile, int playersAmount) {
        Map resizedMap = new Map(0, this.createMapName, this.createMapName + ".xml", Map.CUSTOM, false, 0);
        Tile[][] tiles = new Tile[size][size];
        Building[][] buildings = new Building[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                tiles[row][col] = new Tile(row, col, defaultTile.getSeason(), defaultTile.getType(), defaultTile.getSubtype());
            }
        }
        resizedMap.updateMap(size, tiles, buildings, playersAmount);
        return resizedMap;
    }

    public Map getCreatedMap() {
        return this.createdMap;
    }
}
