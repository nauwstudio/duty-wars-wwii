package com.nauwstudio.dutywars_ww2;


import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;

import java.io.File;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class ArmyAssets {

    public static final String BUILDINGS_ATLAS = "textures/buildings_";
    public static final String UNITS_ATLAS = "textures/units_";
    public static final String BUTTONS_ATLAS = "textures/buttons_";
    public static final String WORLD_ATLAS = "textures/world_";
    public static final String ATLAS = ".atlas";

    // Buildings
    public TextureRegion capital, city, factory, airport, shipyard, eiffel, colisseum, big_ben, brandenburg, hofburg;
    // Infantry
    public TextureRegion infantry_S, infantry_W, infantry_N, infantry_E, infantry_HD;
    public Animation infantry_S_fire, infantry_W_fire, infantry_N_fire, infantry_E_fire;
    public TextureRegion button_infantry, button_infantry_pressed, button_infantry_disabled;
    // Bazooka
    public TextureRegion bazooka_S, bazooka_W, bazooka_N, bazooka_E, bazooka_HD;
    public Animation bazooka_S_fire, bazooka_W_fire, bazooka_N_fire, bazooka_E_fire;
    public TextureRegion button_bazooka, button_bazooka_pressed, button_bazooka_disabled;
    // Sniper
    public TextureRegion sniper_S, sniper_W, sniper_N, sniper_E, sniper_HD;
    public Animation sniper_S_fire, sniper_W_fire, sniper_N_fire, sniper_E_fire;
    public TextureRegion button_sniper, button_sniper_pressed, button_sniper_disabled;
    // Jeep
    public TextureRegion jeep_S, jeep_W, jeep_N, jeep_E, jeep_HD;
    public Animation jeep_S_fire, jeep_W_fire, jeep_N_fire, jeep_E_fire;
    public TextureRegion button_jeep, button_jeep_pressed, button_jeep_disabled;
    // Light tank
    public TextureRegion light_tank_S, light_tank_W, light_tank_N, light_tank_E, light_tank_HD;
    public Animation light_tank_S_fire, light_tank_W_fire, light_tank_N_fire, light_tank_E_fire;
    public TextureRegion button_light_tank, button_light_tank_pressed, button_light_tank_disabled;
    // Heavy tank
    public TextureRegion heavy_tank_S, heavy_tank_W, heavy_tank_N, heavy_tank_E, heavy_tank_HD;
    public Animation heavy_tank_S_fire, heavy_tank_W_fire, heavy_tank_N_fire, heavy_tank_E_fire;
    public TextureRegion button_heavy_tank, button_heavy_tank_pressed, button_heavy_tank_disabled;
    // Truck
    public TextureRegion truck_S, truck_W, truck_N, truck_E, truck_HD;
    public TextureRegion button_truck, button_truck_pressed, button_truck_disabled;
    // Artillery
    public TextureRegion artillery_S, artillery_W, artillery_N, artillery_E, artillery_HD;
    public Animation artillery_S_fire, artillery_W_fire, artillery_N_fire, artillery_E_fire;
    public TextureRegion button_artillery, button_artillery_pressed, button_artillery_disabled;
    // Halftrack
    public TextureRegion halftrack_S, halftrack_W, halftrack_N, halftrack_E, halftrack_HD;
    public Animation halftrack_S_fire, halftrack_W_fire, halftrack_N_fire, halftrack_E_fire;
    public TextureRegion button_halftrack, button_halftrack_pressed, button_halftrack_disabled;
    // Machine gun bunker
    public TextureRegion bunker_machine_gun_S, bunker_machine_gun_W, bunker_machine_gun_N, bunker_machine_gun_E, bunker_machine_gun_HD;
    public Animation bunker_machine_gun_S_fire, bunker_machine_gun_W_fire, bunker_machine_gun_N_fire, bunker_machine_gun_E_fire;
    public TextureRegion button_bunker_machine_gun, button_bunker_machine_gun_pressed, button_bunker_machine_gun_disabled;
    // Artillery bunker
    public TextureRegion bunker_artillery_S, bunker_artillery_W, bunker_artillery_N, bunker_artillery_E, bunker_artillery_HD;
    public Animation bunker_artillery_S_fire, bunker_artillery_W_fire, bunker_artillery_N_fire, bunker_artillery_E_fire;
    public TextureRegion button_bunker_artillery, button_bunker_artillery_pressed, button_bunker_artillery_disabled;
    // Flak
    public TextureRegion flak_S, flak_W, flak_N, flak_E, flak_HD;
    public Animation flak_S_fire, flak_W_fire, flak_N_fire, flak_E_fire;
    public TextureRegion button_flak, button_flak_pressed, button_flak_disabled;
    // Fighter
    public TextureRegion fighter_S, fighter_W, fighter_N, fighter_E, fighter_HD;
    public Animation fighter_S_fire, fighter_W_fire, fighter_N_fire, fighter_E_fire;
    public TextureRegion button_fighter, button_fighter_pressed, button_fighter_disabled;
    // Bomber
    public TextureRegion bomber_S, bomber_W, bomber_N, bomber_E, bomber_HD;
    public Animation bomber_S_fire, bomber_W_fire, bomber_N_fire, bomber_E_fire;
    public TextureRegion button_bomber, button_bomber_pressed, button_bomber_disabled;
    // Transporter
    public TextureRegion transporter_S, transporter_W, transporter_N, transporter_E, transporter_HD;
    public TextureRegion button_transporter, button_transporter_pressed, button_transporter_disabled;
    // Lander
    public TextureRegion lander_S, lander_W, lander_N, lander_E, lander_HD;
    public TextureRegion button_lander, button_lander_pressed, button_lander_disabled;
    // Destroyer
    public TextureRegion destroyer_S, destroyer_W, destroyer_N, destroyer_E, destroyer_HD;
    public Animation destroyer_S_fire, destroyer_W_fire, destroyer_N_fire, destroyer_E_fire;
    public TextureRegion button_destroyer, button_destroyer_pressed, button_destroyer_disabled;
    // Battleship
    public TextureRegion battleship_S, battleship_W, battleship_N, battleship_E, battleship_HD;
    public Animation battleship_S_fire, battleship_W_fire, battleship_N_fire, battleship_E_fire;
    public TextureRegion button_battleship, button_battleship_pressed, button_battleship_disabled;
    // Lander
    public TextureRegion aircraft_carrier_S, aircraft_carrier_W, aircraft_carrier_N, aircraft_carrier_E, aircraft_carrier_HD;
    public TextureRegion button_aircraft_carrier, button_aircraft_carrier_pressed, button_aircraft_carrier_disabled;
    // Color
    public TextureRegion logo, banner, icon;
    public TextureRegion[] defence;
    public TextureRegion[] life;

    private AssetManager manager;
    private String army;

    public ArmyAssets(AssetManager manager, String army) {
        this.manager = manager;
        this.army = army;
    }

    public void loadBuildings() {
        this.manager.load(BUILDINGS_ATLAS + army + ATLAS, TextureAtlas.class);
    }

    public void loadUnits() {
        if (!this.army.equals(Army.GAI_ATLAS)) {
            this.manager.load(UNITS_ATLAS + army + ATLAS, TextureAtlas.class);
            this.manager.load(BUTTONS_ATLAS + army + ATLAS, TextureAtlas.class);
        }
    }

    public void loadColors() {
        this.manager.load(WORLD_ATLAS + army + ATLAS, TextureAtlas.class);
    }

    public TextureAtlas getAtlas(String atlas) {
        return this.manager.get(atlas, TextureAtlas.class);
    }

    public TextureRegion getRegion(TextureAtlas atlas, String name) {
        return atlas.findRegion(name);
    }

    public void initBuildings() {
        TextureAtlas atlas = getAtlas(BUILDINGS_ATLAS + army + ATLAS);
        capital = getRegion(atlas, "0001");
        city = getRegion(atlas, "0002");
        factory = getRegion(atlas, "0003");
        airport = getRegion(atlas, "0004");
        shipyard = getRegion(atlas, "0005");
        eiffel = getRegion(atlas, "0006");
        brandenburg = getRegion(atlas, "0007");
        colisseum = getRegion(atlas, "0008");
        big_ben = getRegion(atlas, "0009");
        hofburg = getRegion(atlas, "0009");
    }

    public void initUnits() {
        if (!this.army.equals(Army.GAI_ATLAS)) {
            TextureAtlas atlas = getAtlas(UNITS_ATLAS + army + ATLAS);
            TextureAtlas buttonAtlas = getAtlas(BUTTONS_ATLAS + army + ATLAS);
            initInfantry(atlas, buttonAtlas);
            initBazooka(atlas, buttonAtlas);
            initSniper(atlas, buttonAtlas);
            initJeep(atlas, buttonAtlas);
            initLightTank(atlas, buttonAtlas);
            initHeavyTank(atlas, buttonAtlas);
            initTruck(atlas, buttonAtlas);
            initArtillery(atlas, buttonAtlas);
            initHalftrack(atlas, buttonAtlas);
            initBunkerMachineGun(atlas, buttonAtlas);
            initBunkerArtillery(atlas, buttonAtlas);
            initFlak(atlas, buttonAtlas);
            initFighter(atlas, buttonAtlas);
            initBomber(atlas, buttonAtlas);
            initTransporter(atlas, buttonAtlas);
            initLander(atlas, buttonAtlas);
            initDestroyer(atlas, buttonAtlas);
            initBattleship(atlas, buttonAtlas);
            initAircraftCarrier(atlas, buttonAtlas);
        }
    }

    public void initColors() {
        if (!this.army.equals(Army.GAI_ATLAS)) {
            TextureAtlas atlas = getAtlas(WORLD_ATLAS + army + ATLAS);
            initBanner(atlas);
            initDefence(atlas);
            initLife(atlas);
        }
    }

    private void initInfantry(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        infantry_S = getRegion(atlas, "0001");
        infantry_W = getRegion(atlas, "0002");
        infantry_N = getRegion(atlas, "0003");
        infantry_E = getRegion(atlas, "0004");
        infantry_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0005"), infantry_S});
        infantry_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        infantry_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0006"), infantry_W});
        infantry_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        infantry_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0007"), infantry_N});
        infantry_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        infantry_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0008"), infantry_E});
        infantry_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_infantry = getRegion(buttonAtlas,"0001");
        button_infantry_pressed = getRegion(buttonAtlas,"0002");
        button_infantry_disabled = getRegion(buttonAtlas,"0003");
        infantry_HD = getRegion(atlas, "0189");
    }

    private void initBazooka(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        bazooka_S = getRegion(atlas, "0009");
        bazooka_W = getRegion(atlas, "0010");
        bazooka_N = getRegion(atlas, "0011");
        bazooka_E = getRegion(atlas, "0012");
        bazooka_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0013"), getRegion(atlas, "0017"), getRegion(atlas, "0021"), bazooka_S});
        bazooka_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0014"), getRegion(atlas, "0018"), getRegion(atlas, "0022"), bazooka_W});
        bazooka_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0015"), getRegion(atlas, "0019"), getRegion(atlas, "0023"), bazooka_N});
        bazooka_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0016"), getRegion(atlas, "0020"), getRegion(atlas, "0024"), bazooka_E});
        button_bazooka = getRegion(buttonAtlas,"0004");
        button_bazooka_pressed = getRegion(buttonAtlas,"0005");
        button_bazooka_disabled = getRegion(buttonAtlas,"0006");
        bazooka_HD = getRegion(atlas, "0190");
    }

    private void initSniper(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        sniper_S = getRegion(atlas, "0025");
        sniper_W = getRegion(atlas, "0026");
        sniper_N = getRegion(atlas, "0027");
        sniper_E = getRegion(atlas, "0028");
        sniper_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0029"), sniper_S});
        sniper_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0030"), sniper_W});
        sniper_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0031"), sniper_N});
        sniper_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0032"), sniper_E});
        button_sniper = getRegion(buttonAtlas,"0007");
        button_sniper_pressed = getRegion(buttonAtlas,"0008");
        button_sniper_disabled = getRegion(buttonAtlas,"0009");
        sniper_HD = getRegion(atlas, "0191");
    }

    private void initJeep(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        jeep_S = getRegion(atlas, "0033");
        jeep_W = getRegion(atlas, "0034");
        jeep_N = getRegion(atlas, "0035");
        jeep_E = getRegion(atlas, "0036");
        jeep_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0037"), jeep_S});
        jeep_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        jeep_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0038"), jeep_W});
        jeep_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        jeep_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0039"), jeep_N});
        jeep_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        jeep_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0040"), jeep_E});
        jeep_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_jeep = getRegion(buttonAtlas,"0010");
        button_jeep_pressed = getRegion(buttonAtlas,"0011");
        button_jeep_disabled = getRegion(buttonAtlas,"0012");
        jeep_HD = getRegion(atlas, "0192");
    }

    private void initLightTank(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        light_tank_S = getRegion(atlas, "0041");
        light_tank_W = getRegion(atlas, "0042");
        light_tank_N = getRegion(atlas, "0043");
        light_tank_E = getRegion(atlas, "0044");
        light_tank_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0045"), getRegion(atlas, "0049"), getRegion(atlas, "0053"), light_tank_S});
        light_tank_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0046"), getRegion(atlas, "0050"), getRegion(atlas, "0054"), light_tank_W});
        light_tank_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0047"), getRegion(atlas, "0051"), getRegion(atlas, "0055"), light_tank_N});
        light_tank_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0048"), getRegion(atlas, "0052"), getRegion(atlas, "0056"), light_tank_E});
        button_light_tank = getRegion(buttonAtlas,"0013");
        button_light_tank_pressed = getRegion(buttonAtlas,"0014");
        button_light_tank_disabled = getRegion(buttonAtlas,"0015");
        light_tank_HD = getRegion(atlas, "0193");
    }

    private void initHeavyTank(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        heavy_tank_S = getRegion(atlas, "0057");
        heavy_tank_W = getRegion(atlas, "0058");
        heavy_tank_N = getRegion(atlas, "0059");
        heavy_tank_E = getRegion(atlas, "0060");
        heavy_tank_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0061"), getRegion(atlas, "0065"), getRegion(atlas, "0069"), heavy_tank_S});
        heavy_tank_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0062"), getRegion(atlas, "0066"), getRegion(atlas, "0070"), heavy_tank_W});
        heavy_tank_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0063"), getRegion(atlas, "0067"), getRegion(atlas, "0071"), heavy_tank_N});
        heavy_tank_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0064"), getRegion(atlas, "0068"), getRegion(atlas, "0072"), heavy_tank_E});
        button_heavy_tank = getRegion(buttonAtlas,"0016");
        button_heavy_tank_pressed = getRegion(buttonAtlas,"0017");
        button_heavy_tank_disabled = getRegion(buttonAtlas,"0018");
        heavy_tank_HD = getRegion(atlas, "0194");
    }

    private void initTruck(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        truck_S = getRegion(atlas, "0073");
        truck_W = getRegion(atlas, "0074");
        truck_N = getRegion(atlas, "0075");
        truck_E = getRegion(atlas, "0076");
        button_truck = getRegion(buttonAtlas,"0019");
        button_truck_pressed = getRegion(buttonAtlas,"0020");
        button_truck_disabled = getRegion(buttonAtlas,"0021");
        truck_HD = getRegion(atlas, "0195");
    }

    private void initArtillery(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        artillery_S = getRegion(atlas, "0077");
        artillery_W = getRegion(atlas, "0078");
        artillery_N = getRegion(atlas, "0079");
        artillery_E = getRegion(atlas, "0080");
        artillery_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0081"), getRegion(atlas, "0085"), getRegion(atlas, "0089"), artillery_S});
        artillery_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0082"), getRegion(atlas, "0086"), getRegion(atlas, "0090"), artillery_W});
        artillery_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0083"), getRegion(atlas, "0087"), getRegion(atlas, "0091"), artillery_N});
        artillery_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0084"), getRegion(atlas, "0088"), getRegion(atlas, "0092"), artillery_E});
        button_artillery = getRegion(buttonAtlas,"0022");
        button_artillery_pressed = getRegion(buttonAtlas,"0023");
        button_artillery_disabled = getRegion(buttonAtlas,"0024");
        artillery_HD = getRegion(atlas, "0196");
    }

    private void initHalftrack(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        halftrack_S = getRegion(atlas, "0093");
        halftrack_W = getRegion(atlas, "0094");
        halftrack_N = getRegion(atlas, "0095");
        halftrack_E = getRegion(atlas, "0096");
        halftrack_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0097"), halftrack_S});
        halftrack_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        halftrack_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0098"), halftrack_W});
        halftrack_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        halftrack_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0099"), halftrack_N});
        halftrack_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        halftrack_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0100"), halftrack_E});
        halftrack_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_halftrack = getRegion(buttonAtlas,"0025");
        button_halftrack_pressed = getRegion(buttonAtlas,"0026");
        button_halftrack_disabled = getRegion(buttonAtlas,"0027");
        halftrack_HD = getRegion(atlas, "0197");
    }

    private void initBunkerMachineGun(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        bunker_machine_gun_S = getRegion(atlas, "0101");
        bunker_machine_gun_W = getRegion(atlas, "0102");
        bunker_machine_gun_N = getRegion(atlas, "0103");
        bunker_machine_gun_E = getRegion(atlas, "0104");
        bunker_machine_gun_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0105"), bunker_machine_gun_S});
        bunker_machine_gun_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        bunker_machine_gun_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0106"), bunker_machine_gun_W});
        bunker_machine_gun_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        bunker_machine_gun_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0107"), bunker_machine_gun_N});
        bunker_machine_gun_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        bunker_machine_gun_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0108"), bunker_machine_gun_E});
        bunker_machine_gun_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_bunker_machine_gun = getRegion(buttonAtlas,"0025");
        button_bunker_machine_gun_pressed = getRegion(buttonAtlas,"0026");
        button_bunker_machine_gun_disabled = getRegion(buttonAtlas,"0027");
        bunker_machine_gun_HD = getRegion(atlas, "0198");
    }

    private void initBunkerArtillery(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        bunker_artillery_S = getRegion(atlas, "0109");
        bunker_artillery_W = getRegion(atlas, "0110");
        bunker_artillery_N = getRegion(atlas, "0111");
        bunker_artillery_E = getRegion(atlas, "0112");
        bunker_artillery_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0113"), getRegion(atlas, "0117"), getRegion(atlas, "0121"), bunker_artillery_S});
        bunker_artillery_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0114"), getRegion(atlas, "0118"), getRegion(atlas, "0122"), bunker_artillery_W});
        bunker_artillery_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0115"), getRegion(atlas, "0119"), getRegion(atlas, "0123"), bunker_artillery_N});
        bunker_artillery_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0116"), getRegion(atlas, "0120"), getRegion(atlas, "0124"), bunker_artillery_E});
        button_bunker_artillery = getRegion(buttonAtlas,"0022");
        button_bunker_artillery_pressed = getRegion(buttonAtlas,"0023");
        button_bunker_artillery_disabled = getRegion(buttonAtlas,"0024");
        bunker_artillery_HD = getRegion(atlas, "0199");
    }

    private void initFlak(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        flak_S = getRegion(atlas, "0125");
        flak_W = getRegion(atlas, "0126");
        flak_N = getRegion(atlas, "0127");
        flak_E = getRegion(atlas, "0128");
        flak_S_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0129"), flak_S});
        flak_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        flak_W_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0130"), flak_W});
        flak_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        flak_N_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0131"), flak_N});
        flak_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        flak_E_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0132"), flak_E});
        flak_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_flak = getRegion(buttonAtlas,"0025");
        button_flak_pressed = getRegion(buttonAtlas,"0026");
        button_flak_disabled = getRegion(buttonAtlas,"0027");
        flak_HD = getRegion(atlas, "0200");
    }

    private void initFighter(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        fighter_S = getRegion(atlas, "0133");
        fighter_W = getRegion(atlas, "0134");
        fighter_N = getRegion(atlas, "0135");
        fighter_E = getRegion(atlas, "0136");
        fighter_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0137"), fighter_S});
        fighter_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        fighter_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0138"), fighter_W});
        fighter_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        fighter_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0139"), fighter_N});
        fighter_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        fighter_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0140"), fighter_E});
        fighter_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_fighter = getRegion(buttonAtlas,"0028");
        button_fighter_pressed = getRegion(buttonAtlas,"0029");
        button_fighter_disabled = getRegion(buttonAtlas,"0030");
        fighter_HD = getRegion(atlas, "0201");
    }

    private void initBomber(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        bomber_S = getRegion(atlas, "0141");
        bomber_W = getRegion(atlas, "0142");
        bomber_N = getRegion(atlas, "0143");
        bomber_E = getRegion(atlas, "0144");
        bomber_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0145"), getRegion(atlas, "0149"), getRegion(atlas, "0153"), bomber_S});
        bomber_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0146"), getRegion(atlas, "0150"), getRegion(atlas, "0154"), bomber_W});
        bomber_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0147"), getRegion(atlas, "0151"), getRegion(atlas, "0155"), bomber_N});
        bomber_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0148"), getRegion(atlas, "0152"), getRegion(atlas, "0156"), bomber_E});
        button_bomber = getRegion(buttonAtlas,"0031");
        button_bomber_pressed = getRegion(buttonAtlas,"0032");
        button_bomber_disabled = getRegion(buttonAtlas,"0033");
        bomber_HD = getRegion(atlas, "0202");
    }

    private void initTransporter(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        transporter_S = getRegion(atlas, "0157");
        transporter_W = getRegion(atlas, "0158");
        transporter_N = getRegion(atlas, "0159");
        transporter_E = getRegion(atlas, "0160");
        button_transporter = getRegion(buttonAtlas,"0034");
        button_transporter_pressed = getRegion(buttonAtlas,"0035");
        button_transporter_disabled = getRegion(buttonAtlas,"0036");
        transporter_HD = getRegion(atlas, "0203");
    }

    private void initLander(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        lander_S = getRegion(atlas, "0185");
        lander_W = getRegion(atlas, "0186");
        lander_N = getRegion(atlas, "0187");
        lander_E = getRegion(atlas, "0188");
        button_lander = getRegion(buttonAtlas,"0043");
        button_lander_pressed = getRegion(buttonAtlas,"0044");
        button_lander_disabled = getRegion(buttonAtlas,"0045");
        lander_HD = getRegion(atlas, "0206");
    }

    private void initDestroyer(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        destroyer_S = getRegion(atlas, "0177");
        destroyer_W = getRegion(atlas, "0178");
        destroyer_N = getRegion(atlas, "0179");
        destroyer_E = getRegion(atlas, "0180");
        destroyer_S_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0181"), destroyer_S});
        destroyer_S_fire.setPlayMode(Animation.PlayMode.LOOP);
        destroyer_W_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0182"), destroyer_W});
        destroyer_W_fire.setPlayMode(Animation.PlayMode.LOOP);
        destroyer_N_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0183"), destroyer_N});
        destroyer_N_fire.setPlayMode(Animation.PlayMode.LOOP);
        destroyer_E_fire = new Animation(0.15f, new TextureRegion[]{getRegion(atlas, "0184"), destroyer_E});
        destroyer_E_fire.setPlayMode(Animation.PlayMode.LOOP);
        button_destroyer = getRegion(buttonAtlas,"0040");
        button_destroyer_pressed = getRegion(buttonAtlas,"0041");
        button_destroyer_disabled = getRegion(buttonAtlas,"0042");
        destroyer_HD = getRegion(atlas, "0205");
    }

    private void initBattleship(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        battleship_S = getRegion(atlas, "0161");
        battleship_W = getRegion(atlas, "0162");
        battleship_N = getRegion(atlas, "0163");
        battleship_E = getRegion(atlas, "0164");
        battleship_S_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0165"), getRegion(atlas, "0169"), getRegion(atlas, "0173"), battleship_S});
        battleship_W_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0166"), getRegion(atlas, "0170"), getRegion(atlas, "0174"), battleship_W});
        battleship_N_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0167"), getRegion(atlas, "0171"), getRegion(atlas, "0175"), battleship_N});
        battleship_E_fire = new Animation(0.1f, new TextureRegion[]{getRegion(atlas, "0168"), getRegion(atlas, "0172"), getRegion(atlas, "0176"), battleship_E});
        button_battleship = getRegion(buttonAtlas,"0037");
        button_battleship_pressed = getRegion(buttonAtlas,"0038");
        button_battleship_disabled = getRegion(buttonAtlas,"0039");
        battleship_HD = getRegion(atlas, "0204");
    }

    private void initAircraftCarrier(TextureAtlas atlas, TextureAtlas buttonAtlas) {
        aircraft_carrier_S = getRegion(atlas, "0207");
        aircraft_carrier_W = getRegion(atlas, "0208");
        aircraft_carrier_N = getRegion(atlas, "0209");
        aircraft_carrier_E = getRegion(atlas, "0210");
        button_aircraft_carrier = getRegion(buttonAtlas,"0046");
        button_aircraft_carrier_pressed = getRegion(buttonAtlas,"0047");
        button_aircraft_carrier_disabled = getRegion(buttonAtlas,"0048");
        aircraft_carrier_HD = getRegion(atlas, "0211");
    }

    private void initBanner(TextureAtlas atlas) {
        banner = getRegion(atlas,"0001");
        logo = getRegion(atlas,"0007");
        icon = getRegion(atlas,"0019");
    }

    private void initDefence(TextureAtlas atlas) {
        defence = new TextureRegion[5];
        defence[1] = getRegion(atlas,"0002");
        defence[2] = getRegion(atlas,"0003");
        defence[3] = getRegion(atlas,"0004");
        defence[4] = getRegion(atlas,"0005");
        defence[0] = getRegion(atlas,"0006");
    }

    private void initLife(TextureAtlas atlas) {
        life = new TextureRegion[11];
        life[0] = getRegion(atlas,"0018");
        life[1] = getRegion(atlas,"0008");
        life[2] = getRegion(atlas,"0009");
        life[3] = getRegion(atlas,"0010");
        life[4] = getRegion(atlas,"0011");
        life[5] = getRegion(atlas,"0012");
        life[6] = getRegion(atlas,"0013");
        life[7] = getRegion(atlas,"0014");
        life[8] = getRegion(atlas,"0015");
        life[9] = getRegion(atlas,"0016");
        life[10] = getRegion(atlas,"0017");
    }

    public boolean update() {
        return this.manager.update();
    }

    public void dispose() {
        this.manager.dispose();
    }
}
