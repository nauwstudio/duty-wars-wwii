package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;

/**
 * Created by nauwpie on 2/01/2018.
 */

public class RenderUtil {

    public static void drawMainTitle(SpriteBatch batch, TextureRegion texture) {
        float title_width = Math.min(Util.getScreenWidth(), Util.getScreenHeight());
        float title_height = title_width * (texture.getRegionHeight() / (float) texture.getRegionWidth());
        batch.draw(texture, Util.getScreenWidth()/2 - title_width/2, Util.getScreenHeight() - title_height - Util.getCircleButtonSpace(), title_width, title_height);
    }

    public static void drawBackground(SpriteBatch batch, MainAssets assets) {
        if(Util.isVertical()) {
            float bg_width = Util.getScreenHeight() * (assets.background.getWidth()/(float) assets.background.getHeight());
            batch.draw(assets.background, Util.getScreenWidth() / 2f - bg_width / 2f, 0, bg_width, Util.getScreenHeight());
        } else {
            float bg_height = Util.getScreenWidth() * (assets.background_horizontal.getHeight() / (float) assets.background_horizontal.getWidth());
            batch.draw(assets.background_horizontal, 0 , Util.getScreenHeight()/2 - bg_height/2, Util.getScreenWidth(), bg_height);
        }
    }

    public static void drawTextureInCell(SpriteBatch batch, TextureRegion texture, float cell_x, float cell_y, float cell_width, float cell_height, float icon_width) {
        drawIconInCell(batch, texture, cell_x, cell_y, cell_width, cell_height, icon_width, icon_width * (texture.getRegionHeight() / (float) texture.getRegionWidth()));
    }

    public static void drawTextureInCellBottom(SpriteBatch batch, TextureRegion texture, float cell_x, float cell_y, float cell_width, float cell_height, float icon_width) {
        drawIconInCellBottom(batch, texture, cell_x, cell_y, cell_width, cell_height, icon_width, icon_width * (texture.getRegionHeight() / (float) texture.getRegionWidth()));
    }

    public static void drawIconInCell(SpriteBatch batch, TextureRegion texture, float cell_x, float cell_y, float cell_width, float cell_height, float icon_width, float icon_height) {
        batch.draw(texture, cell_x + cell_width / 2f - icon_width / 2f, cell_y + cell_height / 2f - icon_height / 2f, icon_width, icon_height);
    }

    public static void drawIconInCellBottom(SpriteBatch batch, TextureRegion texture, float cell_x, float cell_y, float cell_width, float cell_height, float icon_width, float icon_height) {
        batch.draw(texture, cell_x + cell_width / 2f - icon_width / 2f, cell_y, icon_width, icon_height);
    }

    public static void drawTextInCell(SpriteBatch batch, BitmapFont font, String text, float cell_x, float cell_y, float cell_width, float cell_height) {
        font.draw(batch, text, cell_x, cell_y + cell_height / 2f + font.getXHeight() / 2f, cell_width, Align.center, true);
    }

    public static void drawTextInCellLeft(SpriteBatch batch, BitmapFont font, String text, float cell_x, float cell_y, float cell_width, float cell_height) {
        font.draw(batch, text, cell_x, cell_y + cell_height / 2f + font.getXHeight() / 2f, cell_width, Align.left, true);
    }

    public static void drawTextInCellRight(SpriteBatch batch, BitmapFont font, String text, float cell_x, float cell_y, float cell_width, float cell_height) {
        font.draw(batch, text, cell_x, cell_y + cell_height / 2f + font.getXHeight() / 2f, cell_width, Align.right, true);
    }

    public static void drawTextInCellTop(SpriteBatch batch, BitmapFont font, String text, float cell_x, float cell_y, float cell_width, float cell_height) {
        font.draw(batch, text, cell_x, cell_y + cell_height + font.getXHeight(), cell_width, Align.center, true);
    }
}
