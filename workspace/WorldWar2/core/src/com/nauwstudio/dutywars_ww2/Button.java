package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public abstract class Button {

    protected Vector2 position;
    protected float width, height;

    protected String tag;
    protected TextureRegion texture;
    protected TextureRegion texture_pressed;
    protected TextureRegion texture_disabled;
    protected boolean pressed = false;
    protected boolean enabled = true;

    public Button(Vector2 position, float width, float height, String tag, TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_disabled) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.tag = tag;
        this.texture = texture;
        this.texture_pressed = texture_pressed;
        this.texture_disabled = texture_disabled;
    }

    public String getTag() {
        return this.tag;
    }

    public abstract boolean isTouched(float x, float y);

    public void press() {
        if (this.enabled) {
            this.pressed = true;
            // PLAY SOUND
            DWController.playSound(MainAssets.buttonSound);
        }
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public void release() {
        this.pressed = false;
    }

    public void disable() {
        this.enabled = false;
    }

    public void enable() {
        this.enabled = true;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void render(SpriteBatch batch) {
        if (enabled) {
            if (pressed) {
                batch.draw(this.texture_pressed, position.x, position.y, width, height);
            } else {
                batch.draw(this.texture, position.x, position.y, width, height);
            }
        } else {
            batch.draw(this.texture_disabled, position.x, position.y, width, height);
        }
    }
}
