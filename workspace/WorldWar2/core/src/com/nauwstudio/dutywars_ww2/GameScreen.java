package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.nauwstudio.dutywars_ww2.game.*;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;

public class GameScreen implements Screen {

    private DWController controller;

    private Game game;
    private GameRenderer renderer;
    private GameAssets assets;

    private int saveID = 0;
    private int mode = 0;
    private float runtime = 0;

    // Input handler vars
    private GameGestureDetector gestureDetector;
    private GameInputHandler inputHandler;
    public Button touchedButton;
    public Square touchedSquare;
    public CheckBox touchedCheckBox;
    public RadioButton touchedRadioButton;
    public boolean can_pan;
    public boolean can_pinch;

    public GameScreen(DWController controller, Map map, ArrayList<Player> players, int mode) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        // SOUNDS
        if (this.controller.isMusic()) {
            this.controller.stopMenuMusic();
            startBackgroundMusic();
        }
        this.game = new Game(this, this.assets, map, players, mode);
        this.renderer = new GameRenderer(this.game);
        initInputHandler();
        this.saveID = 0;
        this.mode = mode;
    }

    public GameScreen(DWController controller, Map map, Save save) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        if (this.controller.isMusic()) {
            this.controller.stopMenuMusic();
            startBackgroundMusic();
        }
        this.game = new Game(this, this.assets, map, save);
        this.renderer = new GameRenderer(this.game);
        initInputHandler();
        this.saveID = save.getId();
        this.mode = save.getMode();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new GameGestureDetector(this);
        this.inputHandler = new GameInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Game getGame() {
        return game;
    }

    public DWController getController() {
        return this.controller;
    }

    public GameRenderer getRenderer() {
        return renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
    }

    @Override
    public void render(float delta) {
        if (this.game.isRunning() || this.game.isRobotPlaying()) {
            this.runtime += delta;
            this.game.update(delta);
        }
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
        this.game.showMenu();
    }

    @Override
    public void resume() {
        this.game.resume();
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        this.stopBackgroundMusic();
        this.assets.dispose();
    }

    public com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface getDatabase() {
        return this.controller.getDatabaseInterface();
    }

    public void toMenuScreen() {
        this.controller.toMainScreen();
    }

    public void toVersusScreen() {
        this.controller.toVersusScreen();
    }

    public void toCampaignScreen(int map_unlocked) {
        this.controller.toCampaignScreen(map_unlocked);
    }

    public void saveGame() {
        // PLAYERS
        System.out.println("SAVE : " + game);
        ArrayList<Player> players = this.game.getPlayers();
        // BUILDINGS
        ArrayList<Building> buildings = new ArrayList<Building>();
        for (Player p : players) {
            buildings.addAll(p.getBuildings());
        }
        buildings.addAll(this.game.getGaiaPlayer().getBuildings());
        // UNITS
        ArrayList<Unit> units = new ArrayList<Unit>();
        for (Player p : players) {
            for (Unit unit : p.getUnits()) {
                units.add(unit);
                if (unit.getPassenger() != null) {
                    units.add(unit.getPassenger());
                }
            }
        }
        Save save = new Save(this.saveID, this.mode, this.game.getMap(), players, buildings, units, this.game.getTurn());
        this.saveID = this.controller.saveGame(save);
    }

    public void deleteSave() {
        this.controller.deleteSave(this.saveID);
    }

    public void startBackgroundMusic() {
        if (this.controller.isMusic()) {
            this.assets.backgroundMusic.play();
        }
    }

    public void stopBackgroundMusic() {
        this.assets.backgroundMusic.stop();
    }

}
