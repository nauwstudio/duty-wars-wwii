package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.game.Game;

public class ButtonRectangle extends Button {

    public static final String BUTTON_VERSUS = "versus";
    public static final String BUTTON_CAMPAIGN = "campaign";
    public static final String BUTTON_MAP_EDITOR = "map_editor";
    public static final String BUTTON_SETTINGS = "settings";
    public static final String BUTTON_START = "start";
    public static final String BUTTON_EXIT = "exit";
    public static final String BUTTON_MAP = "map";

    public static final String BUTTON_SAVE = "save";
    public static final String BUTTON_NEW_CAMPAIGN_GAME = "new_campaign_game";
    public static final String BUTTON_NEW_VERSUS_GAME = "new_versus_game";
    public static final String BUTTON_CONFIRM = "confirm";

    public static final String BUTTON_NEW_MAP = "new_map";

    public static final String BUTTON_HELP = "help";
    public static final String BUTTON_ABOUT = "about";
    public static final String BUTTON_LOG = "log";
    public static final String BUTTON_LEADERBOARD = "leaderboard";

    protected Rectangle bounds;

    public ButtonRectangle(Vector2 position, float height, String tag, TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_disabled) {
        super(position, height * (texture.getRegionWidth()/(float)texture.getRegionHeight()), height, tag, texture, texture_pressed, texture_disabled);
        this.bounds = new Rectangle(position.x, position.y, this.width, this.height);
    }

    public ButtonRectangle(Vector2 position, float width, float height, String tag, TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_disabled) {
        super(position, width, height, tag, texture, texture_pressed, texture_disabled);
        this.bounds = new Rectangle(position.x, position.y, this.width, this.height);
    }

    public boolean isTouched(float x, float y) {
        return this.bounds.contains(new Vector2(x, y));
    }

    public boolean isSaveButton() {
        return false;
    }

    public boolean isMapButton() {
        return false;
    }
}
