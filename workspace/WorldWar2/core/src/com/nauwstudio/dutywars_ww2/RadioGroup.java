package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Created by nauwpie on 12/06/2018.
 */

public class RadioGroup {

    public static final String RADIO_GROUP_USER = "user";
    public static final String RADIO_GROUP_ROBOT = "robot";

    private ArrayList<RadioButton> buttons;

    protected Vector2 position;
    protected float width, height;
    protected String tag;
    private int size;
    private float elemHeight;

    public RadioGroup(Vector2 position, float width, float height, String tag, String[] labels, String[] texts, int checked, MainAssets assets) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.tag = tag;
        this.size = labels.length;
        this.elemHeight = this.height/this.size;
        this.buttons = new ArrayList<RadioButton>();
        for(int i = 0; i < this.size; i ++) {
            Vector2 radio_pos = new Vector2(position.x, position.y + (this.size-i-1)*this.elemHeight);
            this.buttons.add(new RadioButton(radio_pos,this.width, this.elemHeight, this, labels[i], texts[i], assets.radioButton, assets.radioButton_pressed, assets.radioButton_checked, assets.radioButton_checked_pressed));
        }
        this.buttons.get(checked).check();
    }

    public String getTag() {
        return this.tag;
    }

    public ArrayList<RadioButton> getButtons(){
        return this.buttons;
    }

    public void unCheckAll() {
        for(RadioButton button : this.buttons) {
            button.uncheck();
        }
    }

    public void render(SpriteBatch batch, BitmapFont font, BitmapFont font_pressed) {
        for(RadioButton radioButton : this.buttons) {
            radioButton.render(batch, font, font_pressed);
        }
    }

    public int getCheckedPosition() {
        for(int i = 0; i < this.size ; i ++) {
            if(this.buttons.get(i).isChecked()) {
                return i;
            }
        }
        return 0;
    }
}

