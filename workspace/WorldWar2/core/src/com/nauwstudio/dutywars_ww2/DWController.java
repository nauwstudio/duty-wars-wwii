package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.game.*;
import com.nauwstudio.dutywars_ww2.game.units.Unit;
import com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface;
import com.nauwstudio.dutywars_ww2.interfaces.AdsInterface;
import com.nauwstudio.dutywars_ww2.interfaces.FileInterface;
import com.nauwstudio.dutywars_ww2.interfaces.GooglePlayInterface;
import com.nauwstudio.dutywars_ww2.interfaces.WebInterface;
import com.nauwstudio.dutywars_ww2.main.Main;

import java.util.ArrayList;

public class DWController extends Game implements ApplicationListener {

    private DatabaseInterface databaseInterface;
    private GooglePlayInterface googlePlayInterface;
    private WebInterface webInterface;
    private AdsInterface adsInterface;
    private FileInterface fileInterface;
    private AssetManager assetManager;
    private MainAssets mainAssets;
    private GameAssets gameAssets;
    private ArrayList<Season> seasons;
    private ArrayList<Player> players;

    private static boolean music;
    private static boolean sound;

    // For ctaching import file return
    private Main main;

    public DWController(DatabaseInterface databaseInterface, GooglePlayInterface googlePlayInterface, WebInterface webInterface, AdsInterface adsInterface, FileInterface fileInterface) {
        this.databaseInterface = databaseInterface;
        this.googlePlayInterface = googlePlayInterface;
        this.webInterface = webInterface;
        this.adsInterface = adsInterface;
        this.fileInterface = fileInterface;
        this.assetManager = new AssetManager();
        this.mainAssets = new MainAssets(this.assetManager);
        this.gameAssets = new GameAssets(this.assetManager);
        initParams();
        initSeasons();
        initPlayers();
        this.adsInterface.showLauncherAd();
    }

    @Override
    public void create() {
        setScreen(new MainLoadScreen(this));
    }

    public void toMainScreen() {
        setScreen(new MainScreen(this));
    }

    public void toVersusScreen() {
        setScreen(new VersusScreen(this));
    }

    public void toCampaignScreen(int map_unlocked) {
        setScreen(new CampaignScreen(this, map_unlocked));
    }

    public void toSettingsScreen() {
        setScreen(new SettingsScreen(this));
    }

    public void startCampaignGame(Map map, int[] players_army) {
        final int[] players_type = new int[players_army.length];
        final int[] players_team = new int[players_army.length];
        final int[] players_order = new int[players_army.length];
        // 1 VS 1
        if (players_army.length == 2) {
            players_type[0] = Player.HUMAN;
            players_type[1] = Player.ROBOT;
            players_team[0] = 1;
            players_team[1] = 2;
            players_order[0] = 1;
            players_order[1] = 2;
        }
        // 2 VS 1
        else {
            players_type[0] = Player.HUMAN;
            players_type[1] = Player.ROBOT;
            players_type[2] = Player.HUMAN;
            players_team[0] = 1;
            players_team[1] = 2;
            players_team[2] = 1;
            players_order[0] = 1;
            players_order[1] = 2;
            players_order[2] = 3;
        }
        loadGameScreen(map, players_army, players_type, players_team, players_order, com.nauwstudio.dutywars_ww2.game.Game.MODE_CAMPAIGN);
    }

    public void startVersusGame(Map map, int[] players_army, int[] players_type, int[] players_team, int[] players_order) {
        loadGameScreen(map, players_army, players_type, players_team, players_order, com.nauwstudio.dutywars_ww2.game.Game.MODE_VERSUS);
    }

    public void loadGameScreen(Map map, int[] players_army, int[] players_type, int[] players_team, int[] players_order, int mode) {
        setScreen(new GameLoadScreen(this, map, players_army, players_type, players_team, players_order, mode));
    }

    public void showGameScreen(Map map, ArrayList<Player> players, int mode) {
        setScreen(new GameScreen(this, map, players, mode));
    }

    public void loadResumeGameScreen(Save save_summary) {
        this.setScreen(new GameLoadScreen(this, save_summary));
    }

    public void showResumeGameScreen(Map map, Save save) {
        System.out.println("Show resume game screen");
        setScreen(new GameScreen(this, map, save));
    }

    public void loadMapEditorScreen(Map map) {
        this.setScreen(new EditorLoadScreen(this, map));
    }

    public void showMapEditorScreen(Map map, ArrayList<Player> players) {
        System.out.println("Show Map editor screen");
        setScreen(new EditorScreen(this, map, players));
    }

    public int saveGame(Save save) {
        return this.databaseInterface.saveGame(save);
    }

    public DatabaseInterface getDatabaseInterface() {
        return this.databaseInterface;
    }

    public AssetManager getAssetManager() {
        return this.assetManager;
    }

    public MainAssets getMainAssets() {
        return this.mainAssets;
    }

    public GameAssets getGameAssets() {
        return this.gameAssets;
    }

    public void loadBackground() {
        this.mainAssets.loadBackground();
    }

    public void initBackground() {
        this.mainAssets.initBackground();
    }

    public void loadMainAssets() {
        this.mainAssets.load();
    }

    public void initMainAssets() {
        this.mainAssets.init();
    }

    public void loadSeasonAssets() {
        for (Season season : this.seasons) {
            season.getAssets().load();
        }
    }

    public void initSeasonAssets() {
        for (Season season : this.seasons) {
            season.getAssets().init();
        }
    }

    public ArrayList<Season> getSeasons() {
        return this.seasons;
    }

    public void loadPlayerAssets() {
        for (Player player : this.players) {
            player.getArmy().getAssets().loadBuildings();
        }
    }

    public void initPlayerAssets() {
        for (Player player : this.players) {
            player.getArmy().getAssets().initBuildings();
        }
    }

    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    public Player getPlayer(int order) {
        return this.players.get(order);
    }

    public boolean stopMainAssetsLoading() {
        return this.mainAssets.update();
    }

    public boolean paramEnabled(String key) {
        String value = this.databaseInterface.getParam(key);
        if (value.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public void enableParam(String key) {
        this.databaseInterface.updateParam(key, "1");
    }

    public String getParamValue(String key) {
        String value = this.databaseInterface.getParam(key);
        return value;
    }

    public float getSpeedParam(String key) {
        switch(Integer.parseInt(getParamValue(key))) {
            case 1:
                return Unit.SPEED_NORMAL;
            case 2:
                return Unit.SPEED_FAST;
            default:
                return Unit.SPEED_SLOW;
        }
    }

    public void disableParam(String key) {
        this.databaseInterface.updateParam(key, "0");
    }

    public void updateParam(String key, String value) {
        this.databaseInterface.updateParam(key, value);
    }

    public void enableMusic() {
        this.enableParam(Util.PARAM_AUDIO_MUSIC);
        this.music = true;
    }

    public void disableMusic() {
        this.disableParam(Util.PARAM_AUDIO_MUSIC);
        this.music = false;
    }

    public void enableSound() {
        this.enableParam(Util.PARAM_AUDIO_SOUND);
        this.sound = true;
    }

    public void disableSound() {
        this.disableParam(Util.PARAM_AUDIO_SOUND);
        this.sound = false;
    }

    public void updateHumanSpeed(int speed) {
        Unit.updateHumanSpeed(speed);
        this.updateParam(Util.PARAM_SPEED_HUMAN, speed + "");
    }

    public void updateRobotSpeed(int speed) {
        Unit.updateRobotSpeed(speed);
        this.updateParam(Util.PARAM_SPEED_ROBOT, speed + "");
    }

    public void startMenuMusic() {
        if(isMusic()) {
            this.getMainAssets().menuMusic.play();
        }
    }

    public void stopMenuMusic() {
        this.getMainAssets().menuMusic.stop();
    }

    public static boolean isMusic() {
        return music;
    }

    public static boolean isSound() {
        return sound;
    }

    public static long playSound(Sound sound) {
        if (isSound()) {
            return sound.play();
        } else {
            return 0;
        }
    }

    public static void stopSound(Sound sound, long id) {
        if (isSound()) {
            sound.stop(id);
        }
    }

    private void initParams() {
        // MUSIC
        if (paramEnabled(Util.PARAM_AUDIO_MUSIC)) {
            this.music = true;
        } else {
            this.music = false;
        }
        // SOUNDS
        if (paramEnabled(Util.PARAM_AUDIO_SOUND)) {
            this.sound = true;
        } else {
            this.sound = false;
        }
        // HUMAN UNIT SPEED
        Unit.HUMAN_SPEED = getSpeedParam(Util.PARAM_SPEED_HUMAN);
        // ROBOT UNIT SPEED
        Unit.ROBOT_SPEED = getSpeedParam(Util.PARAM_SPEED_ROBOT);
    }

    public void initSeasons() {
        this.seasons = new ArrayList<Season>();
        this.seasons.add(new Season(this.assetManager, Season.CLA));
        this.seasons.add(new Season(this.assetManager, Season.WIN));
        this.seasons.add(new Season(this.assetManager, Season.DES));
    }

    public void initPlayers() {
        this.players = new ArrayList<Player>();
        this.players.add(new Player(new Army(this.gameAssets, Army.GAI)));
        this.players.add(new Player(new Army(this.gameAssets, Army.USA)));
        this.players.add(new Player(new Army(this.gameAssets, Army.GER)));
        this.players.add(new Player(new Army(this.gameAssets, Army.USS)));
        this.players.add(new Player(new Army(this.gameAssets, Army.JAP)));
        this.players.add(new Player(new Army(this.gameAssets, Army.GBR)));

    }

    public Map getMap(int mapID) {
        return this.databaseInterface.getMap(mapID);
    }

    public ArrayList<Map> getMapsByType(int mapType) {
        return this.databaseInterface.getMapsByType(mapType);
    }

    public ArrayList<Mission> getMissions() {
        return this.databaseInterface.getMissions();
    }

    public ArrayList<Save> getSavesSummary(int mode) {
        return this.databaseInterface.getSavesSummary(this, mode);
    }

    public void deleteSave(int saveID) {
        this.databaseInterface.deleteSave(saveID);
    }

    public void deleteMap(Map map) {
        this.databaseInterface.deleteMap(map.getId());
        MapFile.delete(map.getFile());
    }

    public void createMap(String name, int size, int season, int type, int playersAmount) {
        if(this.databaseInterface.checkMapName(name)) {
            MapFile.create(name, size, season, type, playersAmount);
            Map map = this.databaseInterface.createMap(name);
            map.initMap(getSeasons(), getPlayers());
            loadMapEditorScreen(map);
        }
    }

    public boolean checkMapSaves(int mapId) {
        return this.databaseInterface.checkMapSaves(mapId);
    }


    public boolean isLogged() {
        return this.googlePlayInterface.isSignedIn();
    }

    public void logIn() {
        this.googlePlayInterface.logIn();
    }

    public void logOut() {
        this.googlePlayInterface.logOut();
    }

    public void showAllLeaderboards() {
        this.googlePlayInterface.getAllLeaderboards();
    }

    public void showLeaderboard(int missionId) {
        this.googlePlayInterface.getLeaderboard(missionId);
    }

    public void submitScore(int missionId, int score) {
        this.googlePlayInterface.submitScore(missionId, score);
    }

    public void help() {
        this.webInterface.help();
    }

    public void web() {
        this.webInterface.web();
    }

    public void facebook() {
        this.webInterface.facebook();
    }

    public void googlePlay() {
        this.webInterface.googlePlay();
    }

    public String getExportFolder() {
        return this.fileInterface.getExportFolder();
    }

    public void openImportFolder(Main main) {
        this.main = main;
        this.fileInterface.importFile();
    }

    public void exportMap(String fileName) {
        showMessage("Exporting : " + fileName + " ...");
        MapFile.exportMap(getExportFolder(), fileName);
        showMessage("Export done !");
    }

    public void importMap(String fileName) {
        showMessage("Importing : " + fileName + " ...");
        String mapName = fileName.substring(0, fileName.lastIndexOf("."));
        if(this.databaseInterface.checkMapName(mapName)) {
            if(MapFile.checkMapExtension(fileName)) {
                if(MapFile.checkMapFormat(this, fileName)) {
                    MapFile.importMap(fileName);
                    databaseInterface.createMap(mapName);
                    showMessage("Import done !");
                } else {
                    MapFile.deleteTemp(fileName);
                    showMessage("Import error : wrong file format");
                }
            } else {
                MapFile.deleteTemp(fileName);
                showMessage("Import error : wrong file extension");
            }
        } else {
            MapFile.deleteTemp(fileName);
            showMessage("Import error : map already exists");
        }
        if(this.main != null) {
            this.main.touchMapEditor();
            this.main = null;
        }
    }

    public boolean checkPermission() {
        return this.fileInterface.checkPermission();
    }

    public void getPermission() {
        this.fileInterface.getPermission();
    }


    public void showMessage(String message) {
        this.fileInterface.showMessage(message);
    }
}
