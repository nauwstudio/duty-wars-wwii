package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.File;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class GameAssets {

    public final String WORLD_ATLAS = "textures/world";
    public static final String SOUNDS_FOLDER = "sounds/";
    public final String ATLAS = ".atlas";
    public static final String MP3 = ".mp3";

    public TextureRegion tile_selected;
    public TextureRegion tile_target_blue;
    public TextureRegion tile_target_red;
    public TextureRegion tile_target_green;
    public TextureRegion tile_scope_blue;
    public TextureRegion tile_scope_red;

    public TextureRegion menu_bg;
    public TextureRegion menu_dollar;
    public TextureRegion menu_shield;
    public TextureRegion menu_life;
    public TextureRegion menu_ammo;
    public TextureRegion menu_gas;

    public TextureRegion reload;
    public Animation low_ammo;
    public Animation low_gas;
    public Animation low_ammo_and_gas;

    public TextureRegion banner;

    public TextureRegion unit_feet;
    public TextureRegion unit_motor;
    public TextureRegion unit_air;
    public TextureRegion unit_naval;
    public TextureRegion unit_fixed;

    public TextureRegion unit_move_scope;
    public TextureRegion unit_attack_scope;

    public TextureRegion star_full;

    // SOUNDS
    public Music backgroundMusic;
    public Sound victory;
    public Sound defeat;
    public Sound star;
    public Sound capture;

    public Sound men_move;
    public Sound infantry_fire;
    public Sound bazooka_fire;
    public Sound sniper_fire;
    public Sound jeep_move, jeep_fire;
    public Sound light_tank_move, light_tank_fire;
    public Sound heavy_tank_move, heavy_tank_fire;
    public Sound halftrack_move, halftrack_fire;
    public Sound truck_move;
    public Sound artillery_move, artillery_fire;
    // Plane
    public Sound fighter_move, fighter_fire;
    public Sound bomber_move, bomber_fire;
    // Boat
    public Sound boat_move;
    public Sound battleship_fire;
    public Sound destroyer_fire;
    public Sound lander_move;

    private AssetManager manager;

    public GameAssets(AssetManager manager) {
        this.manager = manager;
    }

    public AssetManager getAssetManager() {
        return this.manager;
    }

    public void load() {
        // TEXTURES
        this.manager.load(WORLD_ATLAS + ATLAS, TextureAtlas.class);
        // SOUNDS
        this.manager.load(SOUNDS_FOLDER + "background" + MP3, Music.class);
        this.manager.load(SOUNDS_FOLDER + "victory" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "defeat" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "star" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "capture" + MP3, Sound.class);

        // Ground
        this.manager.load(SOUNDS_FOLDER + "men_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "heavy_tank_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "light_tank_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "infantry_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "bazooka_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "sniper_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "jeep_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "jeep_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "light_tank_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "heavy_tank_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "halftrack_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "halftrack_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "truck_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "artillery_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "artillery_fire" + MP3, Sound.class);
        // Air
        this.manager.load(SOUNDS_FOLDER + "fighter_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "fighter_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "bomber_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "bomber_fire" + MP3, Sound.class);
        // Sea
        this.manager.load(SOUNDS_FOLDER + "boat_move" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "battleship_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "destroyer_fire" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "lander_move" + MP3, Sound.class);
    }

    public void finishLoading() {
        this.manager.finishLoading();
    }

    public boolean update() {
        return this.manager.update();
    }

    public void dispose() {
        this.manager.dispose();
    }

    public TextureAtlas getAtlas(String atlas) {
        return this.manager.get(atlas, TextureAtlas.class);

    }

    public TextureRegion getRegion(TextureAtlas atlas, String name) {
        return atlas.findRegion(name);
    }

    private Music getMusic(String name) {
        Music music = this.manager.get(SOUNDS_FOLDER + name + MP3, Music.class);
        return music;
    }

    private Sound getSound(String name) {
        Sound sound = this.manager.get(SOUNDS_FOLDER + name + MP3, Sound.class);
        return sound;
    }

    public void init() {
        initTextures();
        initSounds();
    }

    public void initTextures() {
        TextureAtlas atlas = getAtlas(WORLD_ATLAS + ATLAS);
        initTile(atlas);
        initMenu(atlas);
    }

    public void initTile(TextureAtlas atlas) {
        tile_selected = getRegion(atlas, "0001");
        tile_target_blue = getRegion(atlas, "0002");
        tile_target_red = getRegion(atlas, "0003");
        tile_target_green = getRegion(atlas, "0004");
        tile_scope_blue = getRegion(atlas, "0024");
        tile_scope_red = getRegion(atlas, "0025");
        reload = getRegion(atlas, "0020");
        low_gas = new Animation(1.0f, new TextureRegion[]{getRegion(atlas, "0015"), getRegion(atlas, "0022")});
        low_gas.setPlayMode(Animation.PlayMode.LOOP);
        low_ammo = new Animation(1.0f, new TextureRegion[]{getRegion(atlas, "0016"), getRegion(atlas, "0022")});
        low_ammo.setPlayMode(Animation.PlayMode.LOOP);
        low_ammo_and_gas = new Animation(1.0f, new TextureRegion[]{getRegion(atlas, "0020"), getRegion(atlas, "0022")});
        low_ammo_and_gas.setPlayMode(Animation.PlayMode.LOOP);
    }

    public void initMenu(TextureAtlas atlas) {
        menu_dollar = getRegion(atlas, "0010");
        menu_shield = getRegion(atlas, "0011");
        menu_life = getRegion(atlas, "0012");
        menu_gas = getRegion(atlas, "0013");
        menu_ammo = getRegion(atlas, "0014");
        menu_bg = getRegion(atlas, "0019");
        unit_attack_scope = getRegion(atlas, "0017");
        unit_move_scope = getRegion(atlas, "0018");
        unit_feet = getRegion(atlas, "0005");
        unit_motor = getRegion(atlas, "0006");
        unit_air = getRegion(atlas, "0007");
        unit_naval = getRegion(atlas, "0008");
        unit_fixed = getRegion(atlas, "0009");
        star_full = getRegion(atlas, "0021");
        banner = getRegion(atlas, "0023");
    }

    public void initSounds() {
        backgroundMusic = getMusic("background");
        backgroundMusic.setLooping(true);
        victory = getSound("victory");
        defeat = getSound("defeat");
        star = getSound("star");
        capture = getSound("capture");
        // UNITS
        men_move = getSound("men_move");
        infantry_fire = getSound("infantry_fire");
        bazooka_fire = getSound("bazooka_fire");
        sniper_fire = getSound("sniper_fire");
        jeep_move = getSound("jeep_move");
        jeep_fire = getSound("jeep_fire");
        light_tank_move = getSound("light_tank_move");
        light_tank_fire = getSound("light_tank_fire");
        heavy_tank_move = getSound("heavy_tank_move");
        heavy_tank_fire = getSound("heavy_tank_fire");
        halftrack_move = getSound("halftrack_move");
        halftrack_fire = getSound("halftrack_fire");
        truck_move = getSound("truck_move");
        artillery_move = getSound("artillery_move");
        artillery_fire = getSound("artillery_fire");
        // Air
        fighter_move = getSound("fighter_move");
        fighter_fire = getSound("fighter_fire");
        bomber_move = getSound("bomber_move");
        bomber_fire = getSound("bomber_fire");
        // Sea
        boat_move = getSound("boat_move");
        battleship_fire = getSound("battleship_fire");
        destroyer_fire = getSound("destroyer_fire");
        lander_move = getSound("lander_move");
    }
}
