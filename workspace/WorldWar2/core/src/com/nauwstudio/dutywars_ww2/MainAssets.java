package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.I18NBundle;

import java.io.File;
import java.util.Random;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class MainAssets {

    public static final String BACKGROUNDS_FOLDER = "textures/backgrounds/";
    public static final String MENU_ATLAS = "textures/menu";
    public static final String BUTTON_ATLAS = "textures/buttons";
    public static final String SOUNDS_FOLDER = "sounds/";
    public static final String BUNDLES_FOLDER = "bundles/";
    public static final String FONTS_FOLDER = "fonts/";
    public static final String ATLAS = ".atlas";
    public static final String PNG = ".png";
    public static final String MP3 = ".mp3";
    public static final String TTF = ".ttf";

    public static final int BACKGROUNDS_AMOUNT = 4;

    // Background
    public int background_pos = 1;
    public Texture background;
    public Texture background_horizontal;

    // Common
    public TextureRegion button_back, button_back_pressed;
    public TextureRegion button_next, button_next_pressed;
    public TextureRegion button_start_game, button_start_game_pressed;
    public TextureRegion button_close, button_close_pressed;
    public TextureRegion button_import, button_import_pressed;
    public TextureRegion button_export, button_export_pressed;
    public TextureRegion toggle_checked, toggle_checked_pressed, toggle_pressed;
    // Main
    public TextureRegion title;
    public TextureRegion button_versus, button_versus_pressed;
    public TextureRegion button_campaign, button_campaign_pressed;
    public TextureRegion button_settings, button_settings_pressed;
    public TextureRegion button_map_editor, button_map_editor_pressed, button_map_editor_disabled;
    public TextureRegion button_save, button_save_pressed;
    public TextureRegion button_new_game, button_new_game_pressed;
    public TextureRegion button_confirm, button_confirm_pressed;
    public TextureRegion human_alive, human_dead;
    public TextureRegion robot_alive, robot_dead;
    public TextureRegion button_new_map, button_new_map_pressed;
    // Versus
    public TextureRegion locked;
    public TextureRegion[] button_army, button_army_pressed;
    public TextureRegion[] button_type, button_type_pressed;
    public TextureRegion[] button_team, button_team_pressed;
    public TextureRegion button_plus, button_plus_pressed;
    public TextureRegion button_minus, button_minus_pressed, button_minus_disabled;
    // Campaign
    public TextureRegion button_mission, button_mission_pressed;
    public TextureRegion button_mission_completed, button_mission_completed_presssed;
    public TextureRegion button_next_mission, button_next_mission_pressed;
    public TextureRegion button_leaderboard, button_leaderboard_pressed, button_leaderboard_disabled;
    public TextureRegion[] world_map;
    public TextureRegion mission_bg;
    public TextureRegion[] armies_logo;
    public TextureRegion star_full, star_empty;
    public TextureRegion bullet;
    // Settings
    public TextureRegion checkBox, checkBox_pressed, checkBox_checked, checkBox_checked_pressed;
    public TextureRegion radioButton, radioButton_pressed, radioButton_checked, radioButton_checked_pressed;
    public TextureRegion button_help, button_help_pressed;
    public TextureRegion button_about, button_about_pressed;
    public TextureRegion button_log_in, button_log_in_pressed;
    public TextureRegion button_log_out, button_log_out_pressed;
    public TextureRegion button_all_leaderboard, button_all_leaderboard_pressed, button_all_leaderboard_disabled;
    public TextureRegion button_web, button_web_pressed;
    public TextureRegion button_facebook, button_facebook_pressed;
    public TextureRegion button_google, button_google_pressed;
    // Game
    public TextureRegion button_menu, button_menu_pressed;
    public TextureRegion button_stat, button_stat_pressed;
    public TextureRegion button_end, button_end_pressed;
    public TextureRegion button_info, button_info_pressed;
    public TextureRegion button_drop, button_drop_pressed, button_drop_disabled;
    public TextureRegion button_move_scope, button_move_scope_pressed;
    public TextureRegion button_reload, button_reload_pressed, button_reload_disabled;
    public TextureRegion button_capture, button_capture_pressed, button_capture_disabled;
    public TextureRegion button_exit_game, button_exit_game_pressed;
// Editor
    public TextureRegion button_p1, button_p1_pressed, button_p1_disabled;
    public TextureRegion button_p2, button_p2_pressed, button_p2_disabled;
    public TextureRegion button_p3, button_p3_pressed, button_p3_disabled;
    public TextureRegion button_p4, button_p4_pressed, button_p4_disabled;
    public TextureRegion button_p5, button_p5_pressed, button_p5_disabled;
    public TextureRegion button_p0, button_p0_pressed, button_p0_disabled;
    public TextureRegion button_empty, button_empty_pressed, button_empty_disabled;
    public TextureRegion button_classic, button_classic_pressed, button_classic_disabled;
    public TextureRegion button_winter, button_winter_pressed, button_winter_disabled;
    public TextureRegion button_desert, button_desert_pressed, button_desert_disabled;
    public TextureRegion button_building, button_building_pressed, button_building_disabled;
    public TextureRegion button_unit, button_unit_pressed, button_unit_disabled;
    public TextureRegion button_passenger, button_passenger_pressed, button_passenger_disabled;
    public TextureRegion button_map_save, button_map_save_pressed, button_map_save_disabled;
    public TextureRegion button_map_pencil, button_map_pencil_pressed, button_map_pencil_disabled;
    public TextureRegion button_map_eraser, button_map_eraser_pressed, button_map_eraser_disabled;
    public TextureRegion button_map_params, button_map_params_pressed, button_map_params_disabled;
    public TextureRegion button_visible, button_visible_pressed;
    public TextureRegion button_invisible, button_invisible_pressed;
    public TextureRegion button_orientation, button_orientation_pressed, button_orientation_disabled;




    // SOUNDS
    public Music menuMusic;
    public static Sound buttonSound;
    public static Sound switchSound;
    // BUNDLES
    public I18NBundle stringBundle;
    // FONTS
    public BitmapFont font_small;
    public BitmapFont font_medium;
    public BitmapFont font_big;
    public BitmapFont font_big_gray;
    public BitmapFont font_bigbig;
    public BitmapFont font_giant;

    private AssetManager manager;

    public MainAssets(AssetManager manager) {
        this.manager = manager;
        // Getting random background
        Random ran = new Random();
        this.background_pos = ran.nextInt(BACKGROUNDS_AMOUNT) + 1;
    }

    public AssetManager getAssetManager() {
        return this.manager;
    }

    public void loadBackground() {
        System.out.println("TEST + " + File.separator);
        this.manager.load(BACKGROUNDS_FOLDER + String.format("%04d", background_pos) + PNG, Texture.class);
        this.manager.load(BACKGROUNDS_FOLDER + String.format("%04d", background_pos + 10) + PNG, Texture.class);
    }

    public void load() {
        // TEXTURES
        this.manager.load(MENU_ATLAS + ATLAS, TextureAtlas.class);
        this.manager.load(BUTTON_ATLAS + ATLAS, TextureAtlas.class);
        // SOUNDS
        this.manager.load(SOUNDS_FOLDER + "music" + MP3, Music.class);
        this.manager.load(SOUNDS_FOLDER + "touch" + MP3, Sound.class);
        this.manager.load(SOUNDS_FOLDER + "switch" + MP3, Sound.class);
    }

    public TextureAtlas getAtlas(String atlas) {
        return this.manager.get(atlas, TextureAtlas.class);
    }

    public TextureRegion getRegion(TextureAtlas atlas, String name) {
        return atlas.findRegion(name);
    }

    public boolean update() {
        return this.manager.update();
    }

    public void dispose() {
        this.manager.dispose();
    }

    private Texture getBackground(String name) {
        Texture background = this.manager.get(BACKGROUNDS_FOLDER + name + PNG, Texture.class);
        background.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return background;
    }

    private Music getMusic(String name) {
        Music sound = this.manager.get(SOUNDS_FOLDER + name + MP3, Music.class);
        return sound;
    }

    private Sound getSound(String name) {
        Sound sound = this.manager.get(SOUNDS_FOLDER + name + MP3, Sound.class);
        return sound;
    }

    private I18NBundle getBundle(String name) {
        return I18NBundle.createBundle(Gdx.files.internal(BUNDLES_FOLDER + name));
    }

    private FreeTypeFontGenerator getFont(String name) {
        return new FreeTypeFontGenerator(Gdx.files.internal(FONTS_FOLDER + name + TTF));
    }

    public void initBackground() {
        background = getBackground(String.format("%04d", background_pos));
        background_horizontal = getBackground(String.format("%04d", background_pos + 10));
    }

    public void init() {
        // TEXTURES
        initTextures();
        // SOUNDS
        initSounds();
        // BUNDLES
        initBundles();
        // FONTS
        initFonts();
    }

    public void initTextures() {
        TextureAtlas atlas = getAtlas(MENU_ATLAS + ATLAS);
        title = getRegion(atlas, "0049");
        star_full = getRegion(atlas, "0001");
        star_empty = getRegion(atlas, "0002");
        bullet = getRegion(atlas, "0003");
        mission_bg = getRegion(atlas, "0009");
        human_alive = getRegion(atlas, "0044");
        human_dead = getRegion(atlas, "0045");
        robot_alive = getRegion(atlas, "0046");
        robot_dead = getRegion(atlas, "0047");
        locked = getRegion(atlas, "0048");
        button_save = getRegion(atlas, "0042");
        button_save_pressed = getRegion(atlas, "0043");
        toggle_pressed = getRegion(atlas,"0050");
        toggle_checked = getRegion(atlas,"0051");
        toggle_checked_pressed = getRegion(atlas,"0052");
        initArmyLogo(atlas);
        initWorldMap(atlas);
        initButtons();
    }

    public void initSounds() {
        menuMusic = getMusic("music");
        menuMusic.setLooping(true);
        buttonSound = getSound("touch");
        switchSound = getSound("switch");
    }

    public void initBundles() {
        this.stringBundle = getBundle("string");
    }

    public void initFonts() {
        FreeTypeFontGenerator generator = getFont("CutiveMono");
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_small = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_small.size = 20;
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_medium = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_medium.size = 25;
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_big = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_big.size = 35;
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_big_gray = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_big_gray.size = 35;
        ftfp_big_gray.color = Color.LIGHT_GRAY;
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_bigbig = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_bigbig.size = 45;
        FreeTypeFontGenerator.FreeTypeFontParameter ftfp_giant = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfp_giant.size = 60;
        this.font_small = generator.generateFont(ftfp_small);
        this.font_medium = generator.generateFont(ftfp_medium);
        this.font_big = generator.generateFont(ftfp_big);
        this.font_big_gray = generator.generateFont(ftfp_big_gray);
        this.font_bigbig = generator.generateFont(ftfp_bigbig);
        this.font_giant = generator.generateFont(ftfp_giant);
        generator.dispose();
    }

    private void initArmyLogo(TextureAtlas atlas) {
        armies_logo = new TextureRegion[5];
        armies_logo[0] = getRegion(atlas, "0004");
        armies_logo[1] = getRegion(atlas, "0005");
        armies_logo[2] = getRegion(atlas, "0006");
        armies_logo[3] = getRegion(atlas, "0007");
        armies_logo[4] = getRegion(atlas, "0008");
    }

    private void initWorldMap(TextureAtlas atlas) {
        world_map = new TextureRegion[32];
        world_map[0] = getRegion(atlas, "0010");
        world_map[1] = getRegion(atlas, "0011");
        world_map[2] = getRegion(atlas, "0012");
        world_map[3] = getRegion(atlas, "0013");
        world_map[4] = getRegion(atlas, "0014");
        world_map[5] = getRegion(atlas, "0015");
        world_map[6] = getRegion(atlas, "0016");
        world_map[7] = getRegion(atlas, "0017");
        world_map[8] = getRegion(atlas, "0018");
        world_map[9] = getRegion(atlas, "0019");
        world_map[10] = getRegion(atlas, "0020");
        world_map[11] = getRegion(atlas, "0021");
        world_map[12] = getRegion(atlas, "0022");
        world_map[13] = getRegion(atlas, "0023");
        world_map[14] = getRegion(atlas, "0024");
        world_map[15] = getRegion(atlas, "0025");
        world_map[16] = getRegion(atlas, "0026");
        world_map[17] = getRegion(atlas, "0027");
        world_map[18] = getRegion(atlas, "0028");
        world_map[19] = getRegion(atlas, "0029");
        world_map[20] = getRegion(atlas, "0030");
        world_map[21] = getRegion(atlas, "0031");
        world_map[22] = getRegion(atlas, "0032");
        world_map[23] = getRegion(atlas, "0033");
        world_map[24] = getRegion(atlas, "0034");
        world_map[25] = getRegion(atlas, "0035");
        world_map[26] = getRegion(atlas, "0036");
        world_map[27] = getRegion(atlas, "0037");
        world_map[28] = getRegion(atlas, "0038");
        world_map[29] = getRegion(atlas, "0039");
        world_map[30] = getRegion(atlas, "0040");
        world_map[31] = getRegion(atlas, "0041");
    }

    private void initButtons() {
        TextureAtlas atlas = getAtlas(BUTTON_ATLAS + ATLAS);
        button_versus = getRegion(atlas, "0032");
        button_versus_pressed = getRegion(atlas, "0033");
        button_campaign = getRegion(atlas, "0034");
        button_campaign_pressed = getRegion(atlas, "0035");
        button_map_editor = getRegion(atlas, "0093");
        button_map_editor_pressed = getRegion(atlas, "0094");
        button_map_editor_disabled = getRegion(atlas, "0095");
        button_settings = getRegion(atlas, "0036");
        button_settings_pressed = getRegion(atlas, "0037");
        button_new_game = getRegion(atlas, "0038");
        button_new_game_pressed = getRegion(atlas, "0039");
        button_new_map = getRegion(atlas, "0110");
        button_new_map_pressed = getRegion(atlas, "0111");
        button_confirm = getRegion(atlas, "0040");
        button_confirm_pressed = getRegion(atlas, "0041");

        button_back = getRegion(atlas, "0022");
        button_back_pressed = getRegion(atlas, "0023");
        button_next = getRegion(atlas, "0070");
        button_next_pressed = getRegion(atlas, "0071");
        button_start_game = getRegion(atlas, "0024");
        button_start_game_pressed = getRegion(atlas, "0025");

        button_close = getRegion(atlas, "0018");
        button_close_pressed = getRegion(atlas, "0019");
        button_import = getRegion(atlas, "0163");
        button_import_pressed = getRegion(atlas, "0164");
        button_export = getRegion(atlas, "0112");
        button_export_pressed = getRegion(atlas, "0113");
        // CAMPAIGN SCREEN
        button_next_mission = getRegion(atlas, "0026");
        button_next_mission_pressed = getRegion(atlas, "0027");
        button_mission = getRegion(atlas, "0028");
        button_mission_pressed = getRegion(atlas, "0029");
        button_mission_completed = getRegion(atlas, "0030");
        button_mission_completed_presssed = getRegion(atlas, "0031");
        button_leaderboard = getRegion(atlas, "0081");
        button_leaderboard_pressed = getRegion(atlas, "0082");
        button_leaderboard_disabled = getRegion(atlas, "0083");
        // VERSUS SCREEN
        button_army = new TextureRegion[6];
        button_army_pressed = new TextureRegion[6];
        button_army[0] = getRegion(atlas, "0068");
        button_army_pressed[0] = getRegion(atlas, "0069");
        button_army[1] = getRegion(atlas, "0042");
        button_army_pressed[1] = getRegion(atlas, "0043");
        button_army[2] = getRegion(atlas, "0044");
        button_army_pressed[2] = getRegion(atlas, "0045");
        button_army[3] = getRegion(atlas, "0046");
        button_army_pressed[3] = getRegion(atlas, "0047");
        button_army[4] = getRegion(atlas, "0048");
        button_army_pressed[4] = getRegion(atlas, "0049");
        button_army[5] = getRegion(atlas, "0050");
        button_army_pressed[5] = getRegion(atlas, "0051");
        button_type = new TextureRegion[2];
        button_type_pressed = new TextureRegion[2];
        button_type[0] = getRegion(atlas, "0052");
        button_type_pressed[0] = getRegion(atlas, "0053");
        button_type[1] = getRegion(atlas, "0054");
        button_type_pressed[1] = getRegion(atlas, "0055");
        button_team = new TextureRegion[6];
        button_team_pressed = new TextureRegion[6];
        button_team[0] = getRegion(atlas, "0066");
        button_team_pressed[0] = getRegion(atlas, "0067");
        button_team[1] = getRegion(atlas, "0058");
        button_team_pressed[1] = getRegion(atlas, "0059");
        button_team[2] = getRegion(atlas, "0060");
        button_team_pressed[2] = getRegion(atlas, "0061");
        button_team[3] = getRegion(atlas, "0062");
        button_team_pressed[3] = getRegion(atlas, "0063");
        button_team[4] = getRegion(atlas, "0064");
        button_team_pressed[4] = getRegion(atlas, "0065");
        button_team[5] = getRegion(atlas, "0056");
        button_team_pressed[5] = getRegion(atlas, "0057");

        button_plus = getRegion(atlas, "0076");
        button_plus_pressed = getRegion(atlas, "0077");
        button_minus = getRegion(atlas, "0078");
        button_minus_pressed = getRegion(atlas, "0079");
        button_minus_disabled = getRegion(atlas, "0080");
        // SETTINGS SCREEN
        checkBox = getRegion(atlas, "0072");
        checkBox_pressed = getRegion(atlas, "0073");
        checkBox_checked = getRegion(atlas, "0074");
        checkBox_checked_pressed = getRegion(atlas, "0075");
        radioButton = getRegion(atlas, "0106");
        radioButton_pressed = getRegion(atlas, "0107");
        radioButton_checked = getRegion(atlas, "0108");
        radioButton_checked_pressed = getRegion(atlas, "0109");
        button_log_in = getRegion(atlas, "0084");
        button_log_in_pressed = getRegion(atlas, "0085");
        button_log_out = getRegion(atlas, "0086");
        button_log_out_pressed = getRegion(atlas, "0087");
        button_all_leaderboard = getRegion(atlas, "0088");
        button_all_leaderboard_pressed = getRegion(atlas, "0089");
        button_all_leaderboard_disabled = getRegion(atlas, "0090");
        button_help = getRegion(atlas, "0091");
        button_help_pressed = getRegion(atlas, "0092");
        button_about = getRegion(atlas, "0096");
        button_about_pressed = getRegion(atlas, "0097");
        button_web = getRegion(atlas, "0098");
        button_web_pressed = getRegion(atlas, "0099");
        button_facebook = getRegion(atlas, "0100");
        button_facebook_pressed = getRegion(atlas, "0101");
        button_google = getRegion(atlas, "0102");
        button_google_pressed = getRegion(atlas, "0103");
        // GAME
        button_menu = getRegion(atlas, "0001");
        button_menu_pressed = getRegion(atlas, "0002");
        button_stat = getRegion(atlas, "0003");
        button_stat_pressed = getRegion(atlas, "0004");
        button_end = getRegion(atlas, "0005");
        button_end_pressed = getRegion(atlas, "0006");
        button_info = getRegion(atlas, "0007");
        button_info_pressed = getRegion(atlas, "0008");
        button_drop = getRegion(atlas, "0009");
        button_drop_pressed = getRegion(atlas, "0010");
        button_drop_disabled = getRegion(atlas, "0011");
        button_reload = getRegion(atlas, "0012");
        button_reload_pressed = getRegion(atlas, "0013");
        button_reload_disabled = getRegion(atlas, "0014");
        button_move_scope = getRegion(atlas, "0104");
        button_move_scope_pressed = getRegion(atlas, "0105");
        button_capture = getRegion(atlas, "0015");
        button_capture_pressed = getRegion(atlas, "0016");
        button_capture_disabled = getRegion(atlas, "0017");
        button_exit_game = getRegion(atlas, "0020");
        button_exit_game_pressed = getRegion(atlas, "0021");
        // EDITOR
        button_p1 = getRegion(atlas, "0114");
        button_p1_pressed = getRegion(atlas, "0115");
        button_p1_disabled = getRegion(atlas, "0116");
        button_p2 = getRegion(atlas, "0117");
        button_p2_pressed = getRegion(atlas, "0118");
        button_p2_disabled = getRegion(atlas, "0119");
        button_p3 = getRegion(atlas, "0120");
        button_p3_pressed = getRegion(atlas, "0121");
        button_p3_disabled = getRegion(atlas, "0122");
        button_p4 = getRegion(atlas, "0123");
        button_p4_pressed = getRegion(atlas, "0124");
        button_p4_disabled = getRegion(atlas, "0125");
        button_p5 = getRegion(atlas, "0126");
        button_p5_pressed = getRegion(atlas, "0127");
        button_p5_disabled = getRegion(atlas, "0128");
        button_p0 = getRegion(atlas, "0129");
        button_p0_pressed = getRegion(atlas, "0130");
        button_p0_disabled = getRegion(atlas, "0131");
        button_empty = getRegion(atlas, "0132");
        button_empty_pressed = getRegion(atlas, "0133");
        button_empty_disabled = getRegion(atlas, "0134");
        button_classic = getRegion(atlas, "0135");
        button_classic_pressed = getRegion(atlas, "0136");
        button_classic_disabled = getRegion(atlas, "0137");
        button_winter = getRegion(atlas, "0165");
        button_winter_pressed = getRegion(atlas, "0166");
        button_winter_disabled = getRegion(atlas, "0167");
        button_desert = getRegion(atlas, "0168");
        button_desert_pressed = getRegion(atlas, "0169");
        button_desert_disabled = getRegion(atlas, "0170");
        button_building = getRegion(atlas, "0138");
        button_building_pressed = getRegion(atlas, "0139");
        button_building_disabled = getRegion(atlas, "0140");
        button_unit = getRegion(atlas, "0141");
        button_unit_pressed = getRegion(atlas, "0142");
        button_unit_disabled = getRegion(atlas, "0143");
        button_passenger = getRegion(atlas, "0144");
        button_passenger_pressed = getRegion(atlas, "0145");
        button_passenger_disabled = getRegion(atlas, "0146");
        button_map_save = getRegion(atlas, "0147");
        button_map_save_pressed = getRegion(atlas, "0148");
        button_map_save_disabled = getRegion(atlas, "0149");
        button_map_pencil = getRegion(atlas, "0150");
        button_map_pencil_pressed = getRegion(atlas, "0151");
        button_map_pencil_disabled = getRegion(atlas, "0152");
        button_map_eraser = getRegion(atlas, "0153");
        button_map_eraser_pressed = getRegion(atlas, "0154");
        button_map_eraser_disabled = getRegion(atlas, "0155");
        button_map_params = getRegion(atlas, "0156");
        button_map_params_pressed = getRegion(atlas, "0157");
        button_map_params_disabled = getRegion(atlas, "0158");
        button_visible = getRegion(atlas, "0159");
        button_visible_pressed = getRegion(atlas, "0160");
        button_invisible = getRegion(atlas, "0161");
        button_invisible_pressed = getRegion(atlas, "0162");
        button_orientation = getRegion(atlas, "0171");
        button_orientation_pressed = getRegion(atlas, "0172");
        button_orientation_disabled = getRegion(atlas, "0173");
    }
}
