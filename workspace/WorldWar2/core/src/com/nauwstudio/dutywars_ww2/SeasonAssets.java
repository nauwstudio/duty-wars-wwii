package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Season;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class SeasonAssets {

    public static final String TILES_ATLAS = "textures/tiles_";
    public static final String ATLAS = ".atlas";

    public TextureRegion plain, forest, sand, sea, mountains, sea_s, sea_w, sea_sw;
    public TextureRegion river_sn, river_we, river_ne, river_nw, river_se, river_sw;
    public TextureRegion road_sn, road_we, road_ne, road_nw, road_se, road_sw, bridge_sn, bridge_we, crossroad;

    private AssetManager manager;
    private String season;

    public SeasonAssets(AssetManager manager, String season) {
        this.manager = manager;
        this.season = season;
    }

    public void load() {
        this.manager.load(TILES_ATLAS + season + ATLAS, TextureAtlas.class);
    }

    public TextureAtlas getAtlas(String atlas) {
        return this.manager.get(atlas, TextureAtlas.class);
    }

    public TextureRegion getRegion(TextureAtlas atlas, String name) {
        return atlas.findRegion(name);
    }

    public void init() {
        TextureAtlas atlas = getAtlas(TILES_ATLAS + season + ATLAS);
        plain = getRegion(atlas, "0001");
        forest = getRegion(atlas, "0002");
        sand = getRegion(atlas, "0003");
        sea = getRegion(atlas, "0004");
        sea_s = getRegion(atlas, "0021");
        sea_w = getRegion(atlas, "0022");
        sea_sw = getRegion(atlas, "0023");
        mountains = getRegion(atlas, "0005");
        river_sn = getRegion(atlas, "0006");
        river_we = getRegion(atlas, "0007");
        river_ne = getRegion(atlas, "0008");
        river_nw = getRegion(atlas, "0009");
        river_se = getRegion(atlas, "0010");
        river_sw = getRegion(atlas, "0011");
        road_sn = getRegion(atlas, "0014");
        road_we = getRegion(atlas, "0015");
        road_ne = getRegion(atlas, "0016");
        road_nw = getRegion(atlas, "0017");
        road_se = getRegion(atlas, "0018");
        road_sw = getRegion(atlas, "0019");
        bridge_sn = getRegion(atlas, "0012");
        bridge_we = getRegion(atlas, "0013");
        crossroad = getRegion(atlas, "0020");
    }

    public void finishLoading() {
        this.manager.finishLoading();
    }

    public boolean update() {
        return this.manager.update();
    }

    public void dispose() {
        this.manager.dispose();
    }
}
