package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.*;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class Army {

    public static final int GAI = 0;
    public static final int USA = 1;
    public static final int GER = 2;
    public static final int USS = 3;
    public static final int JAP = 4;
    public static final int GBR = 5;

    public static final String GAI_ATLAS = "gai";
    public static final String USA_ATLAS = "usa";
    public static final String GER_ATLAS = "ger";
    public static final String USS_ATLAS = "uss";
    public static final String JAP_ATLAS = "jap";
    public static final String GBR_ATLAS = "gbr";

    public static final int[] ARMIES = {GAI, USA, GER, USS, JAP, GBR};

    private int type;
    private GameAssets gameAssets;
    private ArmyAssets assets;

    public Army(GameAssets assets, int type) {
        this.gameAssets = assets;
        this.type = type;
        switch (this.type) {
            case GAI:
                this.assets = new ArmyAssets(assets.getAssetManager(), GAI_ATLAS);
                break;
            case USA:
                this.assets = new ArmyAssets(assets.getAssetManager(), USA_ATLAS);
                break;
            case GER:
                this.assets = new ArmyAssets(assets.getAssetManager(), GER_ATLAS);
                break;
            case USS:
                this.assets = new ArmyAssets(assets.getAssetManager(), USS_ATLAS);
                break;
            case JAP:
                this.assets = new ArmyAssets(assets.getAssetManager(), JAP_ATLAS);
                break;
            case GBR:
                this.assets = new ArmyAssets(assets.getAssetManager(), GBR_ATLAS);
                break;
        }
    }

    public GameAssets getGameAssets() {
        return this.gameAssets;
    }

    /*
    Army summary !! BE CAREFUL ! NOT ASSETS. CALL updateAssets when loading game !
     */
    public Army(int type) {
        this.type = type;
    }

    public boolean isGAI() {
        return this.type == GAI;
    }

    public int getType() {
        return this.type;
    }

    public TextureRegion getLogoTexture() {
        return this.assets.logo;
    }

    public TextureRegion getIconTexture() {
        return this.assets.icon;
    }

    public TextureRegion getBannerTexture() {
        if(this.isGAI()) {
            // TODO get banner from world
            return this.getAssets().banner;
        } else {
            return this.assets.banner;
        }
    }

    public TextureRegion getDefenceTexture(int defence) {
        return this.assets.defence[defence];
    }

    public TextureRegion getLifeTexture(int life) {
        if (life <= 0) {
            life = 0;
        } else if (life <= 100){
            life = (life - 1) / 10 + 1;
        } else {
            life = 10;
        }
        return this.assets.life[life];
    }

    public ArmyAssets getAssets() {
        return this.assets;
    }
}
