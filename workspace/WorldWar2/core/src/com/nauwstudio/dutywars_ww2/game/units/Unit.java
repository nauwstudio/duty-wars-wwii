package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.GameAssets;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Tile;
import com.nauwstudio.dutywars_ww2.game.Game;
import com.nauwstudio.dutywars_ww2.game.GameObject;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;

import java.util.ArrayList;

import static com.nauwstudio.dutywars_ww2.game.units.Unit.Orientation.EAST;
import static com.nauwstudio.dutywars_ww2.game.units.Unit.Orientation.NORTH;
import static com.nauwstudio.dutywars_ww2.game.units.Unit.Orientation.SOUTH;
import static com.nauwstudio.dutywars_ww2.game.units.Unit.Orientation.WEST;

/**
 * Created by nauwpie on 16/11/2017.
 */

public abstract class Unit extends GameObject {

    // EXTRAS
    public static final String EXTRA_PLAYER = "player";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_ORIENTATION = "orientation";
    // TYPE
    public static final int INFANTRY = 1;
    public static final int BAZOOKA = 2;
    public static final int SNIPER = 3;
    public static final int JEEP = 4;
    public static final int LIGHT_TANK = 5;
    public static final int HEAVY_TANK = 6;
    public static final int TRUCK = 7;
    public static final int ARTILLERY = 8;
    public static final int HALFTRACK = 9;
    public static final int BUNKER_MACHINE_GUN = 10;
    public static final int BUNKER_ARTILLERY = 11;
    public static final int FLAK = 12;
    public static final int FIGHTER = 13;
    public static final int BOMBER = 14;
    public static final int TRANSPORTER = 15;
    public static final int BATTLESHIP = 16;
    public static final int DESTROYER = 17;
    public static final int LANDER = 18;
    public static final int AIRCRAFT_CARRIER = 19;
    // PRICE
    public static final int INFANTRY_PRICE = 1000;
    public static final int BAZOOKA_PRICE = 3000;
    public static final int SNIPER_PRICE = 2000;
    public static final int JEEP_PRICE = 4000;
    public static final int LIGHT_TANK_PRICE = 7000;
    public static final int HEAVY_TANK_PRICE = 16000;
    public static final int TRUCK_PRICE = 5000;
    public static final int ARTILLERY_PRICE = 6000;
    public static final int HALFTRACK_PRICE = 6000;
    public static final int BUNKER_MACHINE_GUN_PRICE = 0;
    public static final int BUNKER_ARTILLERY_PRICE = 0;
    public static final int FLAK_PRICE = 0;
    public static final int FIGHTER_PRICE = 20000;
    public static final int BOMBER_PRICE = 22000;
    public static final int TRANSPORTER_PRICE = 18000;
    public static final int LANDER_PRICE = 12000;
    public static final int DESTROYER_PRICE = 18000;
    public static final int BATTLESHIP_PRICE = 28000;
    public static final int AIRCRAFT_CARRIER_PRICE = 30000;
    // MOVE SCOPE
    public static final int INFANTRY_MOVE_SCOPE = 2;
    public static final int BAZOOKA_MOVE_SCOPE = 2;
    public static final int SNIPER_MOVE_SCOPE = 2;
    public static final int JEEP_MOVE_SCOPE = 5;
    public static final int LIGHT_TANK_MOVE_SCOPE = 4;
    public static final int HEAVY_TANK_MOVE_SCOPE = 4;
    public static final int TRUCK_MOVE_SCOPE = 4;
    public static final int ARTILLERY_MOVE_SCOPE = 3;
    public static final int HALFTRACK_MOVE_SCOPE = 4;
    public static final int BUNKER_MACHINE_GUN_MOVE_SCOPE = 0;
    public static final int BUNKER_ARTILLERY_MOVE_SCOPE = 0;
    public static final int FLAK_MOVE_SCOPE = 0;
    public static final int FIGHTER_MOVE_SCOPE = 6;
    public static final int BOMBER_MOVE_SCOPE = 4;
    public static final int TRANSPORTER_MOVE_SCOPE = 5;
    public static final int LANDER_MOVE_SCOPE = 5;
    public static final int DESTROYER_MOVE_SCOPE = 5;
    public static final int BATTLESHIP_MOVE_SCOPE = 4;
    public static final int AIRCRAFT_CARRIER_MOVE_SCOPE = 4;

    // ATTACK SCOPE
    public static final int[] INFANTRY_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] BAZOOKA_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] SNIPER_ATTACK_SCOPE = new int[]{2, 3};
    public static final int[] JEEP_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] LIGHT_TANK_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] HEAVY_TANK_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] TRUCK_ATTACK_SCOPE = new int[]{0, 0};
    public static final int[] ARTILLERY_ATTACK_SCOPE = new int[]{2, 4};
    public static final int[] HALFTRACK_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] BUNKER_MACHINE_GUN_ATTACK_SCOPE = new int[]{1, 2};
    public static final int[] BUNKER_ARTILLERY_ATTACK_SCOPE = new int[]{2, 5};
    public static final int[] FLAK_ATTACK_SCOPE = new int[]{1, 4};
    public static final int[] FIGHTER_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] BOMBER_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] TRANSPORTER_ATTACK_SCOPE = new int[]{0, 0};
    public static final int[] LANDER_ATTACK_SCOPE = new int[]{0, 0};
    public static final int[] DESTROYER_ATTACK_SCOPE = new int[]{1, 1};
    public static final int[] BATTLESHIP_ATTACK_SCOPE = new int[]{2, 6};
    public static final int[] AIRCRAFT_CARRIER_ATTACK_SCOPE = new int[]{0, 0};
    // AMMO
    public static final int INFANTRY_AMMO = 20;
    public static final int BAZOOKA_AMMO = 5;
    public static final int SNIPER_AMMO = 15;
    public static final int JEEP_AMMO = 25;
    public static final int LIGHT_TANK_AMMO = 12;
    public static final int HEAVY_TANK_AMMO = 9;
    public static final int TRUCK_AMMO = 0;
    public static final int ARTILLERY_AMMO = 12;
    public static final int HALFTRACK_AMMO = 15;
    public static final int BUNKER_MACHINE_GUN_AMMO = 20;
    public static final int BUNKER_ARTILLERY_AMMO = 10;
    public static final int FLAK_AMMO = 15;
    public static final int FIGHTER_AMMO = 10;
    public static final int BOMBER_AMMO = 5;
    public static final int TRANSPORTER_AMMO = 0;
    public static final int LANDER_AMMO = 0;
    public static final int DESTROYER_AMMO = 20;
    public static final int BATTLESHIP_AMMO = 10;
    public static final int AIRCRAFT_CARRIER_AMMO = 0;
    // GAS
    public static final int INFANTRY_GAS = 99;
    public static final int BAZOOKA_GAS = 99;
    public static final int SNIPER_GAS = 99;
    public static final int JEEP_GAS = 40;
    public static final int LIGHT_TANK_GAS = 35;
    public static final int HEAVY_TANK_GAS = 30;
    public static final int TRUCK_GAS = 35;
    public static final int ARTILLERY_GAS = 25;
    public static final int HALFTRACK_GAS = 30;
    public static final int BUNKER_MACHINE_GUN_GAS = 0;
    public static final int BUNKER_ARTILLERY_GAS = 0;
    public static final int FLAK_GAS = 0;
    public static final int FIGHTER_GAS = 40;
    public static final int BOMBER_GAS = 30;
    public static final int TRANSPORTER_GAS = 35;
    public static final int LANDER_GAS = 50;
    public static final int DESTROYER_GAS = 40;
    public static final int BATTLESHIP_GAS = 35;
    public static final int AIRCRAFT_CARRIER_GAS = 35;
    // LIFE
    public static final int LIFE = 100;
    // DAMAGE
    public static final int[] INFANTRY_DMG = {55, 45, 55, 12, 5, 2, 14, 15, 15, 2, 2, 5, 0, 0, 0, 2, 4, 4, 1};
    public static final int[] BAZOOKA_DMG = {65, 55, 65, 85, 55, 15, 75, 70, 65, 20, 20, 30, 0, 0, 0, 5, 15, 15, 3};
    public static final int[] SNIPER_DMG = {55, 45, 60, 12, 5, 2, 14, 15, 15, 2, 2, 5, 0, 0, 0, 2, 4, 4, 1};
    public static final int[] JEEP_DMG = {65, 55, 60, 55, 10, 5, 15, 15, 15, 5, 5, 10, 0, 0, 0, 2, 6, 6, 1};
    public static final int[] LIGHT_TANK_DMG = {75, 70, 75, 85, 55, 25, 75, 70, 65, 20, 20, 30, 0, 0, 0, 10, 20, 20, 5};
    public static final int[] HEAVY_TANK_DMG = {105, 95, 105, 90, 70, 55, 85, 90, 90, 40, 40, 50, 0, 0, 0, 20, 30, 30, 10};
    public static final int[] TRUCK_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public static final int[] ARTILLERY_DMG = {90, 85, 85, 80, 70, 45, 70, 75, 80, 25, 25, 40, 0, 0, 0, 30, 50, 50, 15};
    public static final int[] HALFTRACK_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 60, 65, 0, 0, 0, 0};
    public static final int[] BUNKER_MACHINE_GUN_DMG = {65, 55, 60, 55, 10, 5, 15, 15, 15, 4, 4, 10, 0, 0, 0, 3, 6, 6, 2};
    public static final int[] BUNKER_ARTILLERY_DMG = {90, 85, 85, 80, 70, 45, 70, 75, 80, 25, 25, 40, 0, 0, 0, 30, 50, 50, 20};
    public static final int[] FLAK_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 85, 75, 80, 0, 0, 0, 0};
    public static final int[] FIGHTER_DMG = {80, 80, 80, 70, 60, 50, 65, 70, 70, 20, 20, 30, 55, 70, 70, 35, 45, 55, 25};
    public static final int[] BOMBER_DMG = {120, 120, 120, 110, 105, 95, 105, 105, 105, 40, 40, 50, 0, 0, 0, 75, 80, 90, 50};
    public static final int[] TRANSPORTER_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public static final int[] BATTLESHIP_DMG = {90, 90, 90, 85, 80, 60, 80, 85, 80, 35, 35, 45, 0, 0, 0, 65, 75, 85, 50};
    public static final int[] DESTROYER_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 80, 70, 75, 0, 0, 0, 0};
    public static final int[] LANDER_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public static final int[] AIRCRAFT_CARRIER_DMG = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static final int DIE_STEP = 10;
    public static final int ATTACKED_STEP = 10;
    public static final int RELOAD_STEP = 10;

    public static final float SPEED_SLOW = 0.03f;
    public static final float SPEED_NORMAL = 0.06f;
    public static final float SPEED_FAST = 0.09f;

    public static float HUMAN_SPEED;
    public static float ROBOT_SPEED;

    public enum UnitState {
        IDLE, MOVE, FIRE, STOP
    }

    public enum Orientation {
        SOUTH, WEST, NORTH, EAST
    }

    public enum Kind {
        GROUND, AIR, NAVAL, FIXED
    }

    protected int type;
    protected Kind kind;
    protected Player owner;
    protected int price;
    protected int moveScope;
    protected int[] attackScope;

    protected int life;
    protected int defence;
    protected int ammo;
    protected int gas;
    protected int[] damage;

    protected Sound fireSound;
    protected Sound moveSound;

    protected Unit passenger;

    protected UnitState state;
    protected Orientation orientation;
    // Move values
    private ArrayList<Vector2> path;
    private Vector2 step;
    private int moveAmount;
    private Game game;
    // Attack/Defend values
    private float fireDeltaTime = 0.0f;
    //private float firedDeltaTime = 0.0f;
    // Die values
    private int die_step = DIE_STEP;
    // Attacked values
    private int attacked_step = ATTACKED_STEP;
    // Reload values
    private int reload_step = RELOAD_STEP;
    // Ways for moving
    private ArrayList<ArrayList<int[]>> ways;
    // Move sound id
    private long moveSoundId;

    public Unit(int row, int col, Orientation orientation, Player owner, int type, Kind kind, int price, int moveScope, int[] attackScope, int life, int ammo, int gas, int[] damage, Sound soundFire, Sound soundMove) {
        super(row, col);
        this.type = type;
        this.kind = kind;
        this.owner = owner;
        this.price = price;
        this.moveScope = moveScope;
        this.attackScope = attackScope;
        this.life = life;
        this.ammo = ammo;
        this.gas = gas;
        this.damage = damage;
        this.fireSound = soundFire;
        this.moveSound = soundMove;
        this.orientation = orientation;
        idle();
        updateTextureRatio();

    }

    public void update(float delta) {
        switch (this.state) {
            case IDLE:
                // DO NOTHING
                break;
            case STOP:
                // DO NOTHING
                break;
            case MOVE:
                updateMove(delta);
                break;
            case FIRE:
                updateFire(delta);
                break;
        }
    }

    private void updateMove(float delta) {
        if (!this.owner.isHuman()) {
            this.game.pointCameraOn(this.position);
        }
        // At step
        if (isAtStep(this.position, this.step)) {
            this.position = step;
            this.path.remove(0);
            // Move is ended
            if (this.path.isEmpty()) {
                // Check : Air unit die if gas = 0
                if (this.isAirUnit() && this.gas == 0) {
                    stop();
                    this.game.killUnit(this);
                }
                // Check : Unit can do something after path (capture, attack, embark or drop)
                else if (canDoSomethingAfterMoving(this.game)) {
                    this.state = UnitState.IDLE;
                } else {
                    stop();
                }
                DWController.stopSound(this.getSoundMove(), moveSoundId);
                this.game = null;
            }
            // Move is not ended
            else {
                this.step = this.path.get(0);
                changeDirection(this.step);
            }
        }
        // Not at step
        else {
            Vector2 next = getNextMove();
            this.position.x += next.x;
            this.position.y += next.y;
        }
    }

    private Vector2 getNextMove() {
        float speed;
        // Speed
        if (this.owner.isHuman()) {
            speed = HUMAN_SPEED;
        } else {
            speed = ROBOT_SPEED;
        }
        switch (this.orientation) {
            case SOUTH:
                return Util.mapToWorldCoord(new Vector2(0, -speed));
            case WEST:
                return Util.mapToWorldCoord(new Vector2(-speed, 0));
            case NORTH:
                return Util.mapToWorldCoord(new Vector2(0, speed));
            case EAST:
                return Util.mapToWorldCoord(new Vector2(speed, 0));
            default:
                return new Vector2(this.position.x, this.position.y);
        }
    }

    private void updateFire(float delta) {
        this.fireDeltaTime += delta;
    }

    public void move(ArrayList<int[]> move, Game game) {
        this.game = game;
        moveSoundId = DWController.playSound(this.getSoundMove());
        this.setRow(move.get(move.size() - 1)[0]);
        this.setCol(move.get(move.size() - 1)[1]);
        if (this.passenger != null) {
            this.passenger.setRow(move.get(move.size() - 1)[0]);
            this.passenger.setCol(move.get(move.size() - 1)[1]);
        }
        this.gas -= move.size() - 1;
        this.moveAmount -= move.size() - 1;
        // Create path (convert to iso + removing non-node step to avoid freezing bug)
        this.path = new ArrayList<Vector2>();
        Vector2 origin = new Vector2(move.get(0)[0], move.get(0)[1]);
        this.path.add(Util.mapToWorldCoord(origin));
        for (int i = 1; i < move.size() - 1; i++) {
            Vector2 previous = new Vector2(move.get(i - 1)[0], move.get(i - 1)[1]);
            Vector2 node = new Vector2(move.get(i)[0], move.get(i)[1]);
            Vector2 next = new Vector2(move.get(i + 1)[0], move.get(i + 1)[1]);
            if (!isSameDirection(previous, node, next)) {
                this.path.add(Util.mapToWorldCoord(node));
            }
        }
        Vector2 destination = new Vector2(move.get(move.size() - 1)[0], move.get(move.size() - 1)[1]);
        this.path.add(Util.mapToWorldCoord(destination));
        this.step = this.path.get(0);
        this.state = UnitState.MOVE;
    }

    public boolean isSameDirection(Vector2 previous, Vector2 node, Vector2 next) {
        // Calculate new orientation
        Orientation a = null;
        Orientation b = null;
        if (node.x > previous.x) {
            a = Orientation.EAST;
        } else if (node.x < previous.x) {
            a = Orientation.WEST;
        } else if (node.y > previous.y) {
            a = Orientation.NORTH;
        } else if (node.y < previous.y) {
            a = Orientation.SOUTH;
        }
        if (next.x > node.x) {
            b = Orientation.EAST;
        } else if (next.x < node.x) {
            b = Orientation.WEST;
        } else if (next.y > node.y) {
            b = Orientation.NORTH;
        } else if (next.y < node.y) {
            b = Orientation.SOUTH;
        }
        return a == b;
    }

    public void attack(final Unit enemy, final int damage, Game game) {
        aimTarget(enemy.getPosition());
        fireDeltaTime = 0.0f;
        ammo--;
        state = UnitState.FIRE;
        DWController.playSound(this.getSoundFire());
        enemy.getAttacked(damage);
    }

    public void getAttacked(final int damage) {
        // Attacked animation
        this.attacked_step = 1;
        Timer.Task attackedTask = new Timer.Task() {
            @Override
            public void run() {
                attacked_step++;
                if (attacked_step == ATTACKED_STEP) {
                    hurt(damage);
                    this.cancel();
                }
            }
        };
        Timer.schedule(attackedTask, 0.0f, 0.05f, ATTACKED_STEP);
    }

    public void die() {
        // Die animation
        this.die_step = 1;
        Timer.Task dieTask = new Timer.Task() {
            @Override
            public void run() {
                die_step++;
                if (die_step == DIE_STEP) {
                    removeFromOwnerUnitList();
                    this.cancel();
                }
            }
        };
        Timer.schedule(dieTask, 0.0f, 0.025f, DIE_STEP);
    }

    public boolean isAlive() {
        return this.life > 0;
    }

    public void removeFromOwnerUnitList() {
        this.owner.removeUnit(this);
    }

    public boolean isAtStep(Vector2 position, Vector2 step) {
        Vector2 next = getNextMove();
        switch (this.orientation) {
            case SOUTH:
                if (position.x + next.x >= step.x && position.y + next.y <= step.y) {
                    return true;
                } else {
                    return false;
                }
            case WEST:
                if (position.x + next.x <= step.x && position.y + next.y <= step.y) {
                    return true;
                } else {
                    return false;
                }
            case NORTH:
                if (position.x + next.x <= step.x && position.y + next.y >= step.y) {
                    return true;
                } else {
                    return false;
                }
            case EAST:
                if (position.x + next.x >= step.x && position.y + next.y >= step.y) {
                    return true;
                } else {
                    return false;
                }
            default:
                return true;
        }
    }

    public void changeDirection(Vector2 destination) {
        // Calculate new orientation
        if (destination.x > this.position.x && destination.y > this.position.y) {
            this.orientation = Orientation.EAST;
        } else if (destination.x < this.position.x && destination.y < this.position.y) {
            this.orientation = Orientation.WEST;
        } else if (destination.x < this.position.x && destination.y > this.position.y) {
            this.orientation = Orientation.NORTH;
        } else if (destination.x > this.position.x && destination.y < this.position.y) {
            this.orientation = SOUTH;
        }
        updateTextureRatio();
    }

    public void aimTarget(Vector2 target) {
        if (!this.isFixedUnit()) {
            // Calculate new orientation
            if (target.x > this.position.x && target.y > this.position.y) {
                this.orientation = Orientation.EAST;
            } else if (target.x < this.position.x && target.y < this.position.y) {
                this.orientation = Orientation.WEST;
            } else if (target.x < this.position.x && target.y > this.position.y) {
                this.orientation = Orientation.NORTH;
            } else if (target.x > this.position.x && target.y < this.position.y) {
                this.orientation = SOUTH;
            } else if (target.x > this.position.x && target.y == this.position.y) {
                this.orientation = Orientation.EAST;
            } else if (target.x < this.position.x && target.y == this.position.y) {
                this.orientation = Orientation.WEST;
            } else if (target.x == this.position.x && target.y < this.position.y) {
                this.orientation = SOUTH;
            } else if (target.x == this.position.x && target.y > this.position.y) {
                this.orientation = Orientation.NORTH;
            }
        }
        updateTextureRatio();
    }

    public void renderUnitOnly(SpriteBatch batch, float delta) {
        batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
    }

    @Override
    public void render(SpriteBatch batch, float delta, GameAssets assets) {
        switch (this.state) {
            case IDLE:
                if (isDying()) {
                    Color c = batch.getColor();
                    batch.setColor(c.r, c.g, c.b, 1f / die_step);
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                    batch.setColor(c.r, c.g, c.b, 1f);
                } else if (isAttacked() && attacked_step % 2 == 1) {
                    Color c = batch.getColor();
                    batch.setColor(c.r, c.g, c.b, 1f / attacked_step);
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                    batch.setColor(c.r, c.g, c.b, 1f);
                } else {
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                }
                renderInfo(batch, delta, assets);
                break;
            case STOP:
                if (isDying()) {
                    Color c = batch.getColor();
                    batch.setColor(c.r, c.g, c.b, 1f / die_step);
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                    batch.setColor(c.r, c.g, c.b, 1f);
                } else if (isAttacked() && attacked_step % 2 == 1) {
                    Color c = batch.getColor();
                    batch.setColor(c.r, c.g, c.b, 1f / attacked_step);
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                    batch.setColor(c.r, c.g, c.b, 1f);
                } else {
                    Color c = batch.getColor();
                    batch.setColor(Color.LIGHT_GRAY);
                    batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                    batch.setColor(c);
                }
                renderInfo(batch, delta, assets);
                break;
            case MOVE:
                batch.draw(getTextureIdle(), position.x, position.y, width, width * textureRatio);
                //renderInfo(batch, delta);
                break;
            case FIRE:
                batch.draw(getTextureFire(fireDeltaTime), position.x, position.y, width, width * (getTextureFire(fireDeltaTime).getRegionHeight() / (float) getTextureFire(fireDeltaTime).getRegionWidth()));
                //renderInfo(batch, delta);
                break;
        }
    }

    protected void renderInfo(SpriteBatch batch, float delta, GameAssets assets) {
        // Render defence
        batch.draw(owner.getArmy().getDefenceTexture(this.defence), position.x + width / 6f, position.y, width / 4f, width / 4f);
        // Render life
        batch.draw(owner.getArmy().getLifeTexture(this.life), position.x - width / 6f + width - width / 4f, position.y, width / 4f, width / 4f);
        // Render passenger
        if (this.passenger != null) {
            renderPassenger(batch, delta, assets);
        }
        // Render reloading
        if (isReloading()) {
            renderReloading(batch, delta, assets);
        } else {
            if (isLowGas() && isLowAmmo()) {
                renderLowAmmoAndGas(batch, delta, assets);
            } else if (isLowGas()) {
                renderLowGas(batch, delta, assets);
            } else if (isLowAmmo()) {
                renderLowAmmo(batch, delta, assets);
            }
        }
    }

    private void renderReloading(SpriteBatch batch, float delta, GameAssets assets) {
        batch.draw(assets.reload, position.x + width / 4f, position.y + ((height / 2f) * (reload_step / 10f)), width / 2f, width / 2f);
    }

    private void renderLowGas(SpriteBatch batch, float delta, GameAssets assets) {
        batch.draw((TextureRegion) assets.low_gas.getKeyFrame(delta), position.x + width / 6f, position.y + height - width / 4f, width / 4f, width / 4f);
    }

    private void renderLowAmmo(SpriteBatch batch, float delta, GameAssets assets) {
        batch.draw((TextureRegion) assets.low_ammo.getKeyFrame(delta), position.x + width / 6f, position.y + height - width / 4f, width / 4f, width / 4f);
    }

    private void renderLowAmmoAndGas(SpriteBatch batch, float delta, GameAssets assets) {
        batch.draw((TextureRegion) assets.low_ammo_and_gas.getKeyFrame(delta), position.x + width / 6f, position.y + height - width / 4f, width / 4f, width / 4f);
    }

    private void renderPassenger(SpriteBatch batch, float delta, GameAssets assets) {
        batch.draw(this.getPassenger().getButtonTexture(), position.x - width / 6f + width - width / 4f, position.y + height - width / 4f, width / 4f, width / 4f);

    }


    public void idle() {
        this.state = UnitState.IDLE;
        this.moveAmount = this.moveScope;
    }

    public boolean isIdle() {
        return this.state == UnitState.IDLE;
    }

    public boolean isMoving() {
        return this.state == UnitState.MOVE;
    }

    public boolean isFiring() {
        return this.state == UnitState.FIRE;
    }

    private void updateTextureRatio() {
        this.textureRatio = getTextureIdle().getRegionHeight() / (float) getTextureIdle().getRegionWidth();
    }

    public boolean canDoSomethingAfterMoving(Game world) {
        // Undirect unit can do nothing except sniper which can capture
        if (isUndirectUnit()) {
            if (canCapture(world)) {
                return true;
            } else {
                return this.moveAmount > 0;
            }
        } else if (isDirectUnit()) {
            // Direct unit can attack or capture
            if (canCapture(world) || canAttack(world)) {
                return true;
            } else {
                return this.moveAmount > 0;
            }
        } else {
            // Transport unit can drop or reload
            if (this.canDrop(world) || canReload(world)) {
                return true;
            } else {
                return this.moveAmount > 0;
            }
        }
    }

    public void stop() {
        this.state = UnitState.STOP;
        this.moveAmount = 0;
    }

    public boolean isStop() {
        return this.state == UnitState.STOP;
    }

    public boolean isGroundUnit() {
        return this.kind == Kind.GROUND;
    }

    public boolean isAirUnit() {
        return this.kind == Kind.AIR;
    }

    public boolean isNavalUnit() {
        return this.kind == Kind.NAVAL;
    }

    public boolean isFeetUnit() {
        return this.type == INFANTRY || this.type == BAZOOKA || this.type == SNIPER;
    }

    public boolean isFixedUnit() {
        return this.kind == Kind.FIXED;
    }

    public boolean isAntiAirUnit() {
        return this.type == HALFTRACK || this.type == DESTROYER || this.type == FIGHTER || this.type == FLAK;
    }

    public boolean isTransportUnit() {
        return this.type == TRUCK || this.type == TRANSPORTER || this.type == LANDER || this.type == AIRCRAFT_CARRIER;
    }

    public boolean isReloadUnit() {
        return this.type == TRUCK || this.type == AIRCRAFT_CARRIER;
    }

    public boolean isDirectUnit() {
        return this.attackScope[0] == 1 && this.attackScope[1] == 1;
    }

    public boolean isUndirectUnit() {
        return this.attackScope[0] >= 1 && this.attackScope[1] > 1;
    }

    public int getType() {
        return this.type;
    }

    public Player getOwner() {
        return this.owner;
    }

    public int getMoveScope() {
        return this.moveScope;
    }

    public int[] getAttackScope() {
        return this.attackScope;
    }

    public int getPrice() {
        return this.price;
    }

    public int getLife() {
        return this.life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getDefence() {
        return this.defence;
    }

    public int getAmmo() {
        return this.ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public void setGas(int gas) {
        this.gas = gas;
    }

    public int getGas() {
        return this.gas;
    }

    public int[] getDamage() {
        return this.damage;
    }

    public void updateDefence(int defence) {
        if (!this.isAirUnit() && !this.isFixedUnit()) {
            this.defence = defence;
        } else {
            this.defence = 0;
        }
    }

    public static Unit createUnit(int row, int col, Player player, int type, Orientation orientation) {
        switch (type) {
            case Unit.INFANTRY:
                return new Infantry(row, col, player, orientation);
            case Unit.BAZOOKA:
                return new Bazooka(row, col, player, orientation);
            case Unit.SNIPER:
                return new Sniper(row, col, player, orientation);
            case Unit.JEEP:
                return new Jeep(row, col, player, orientation);
            case Unit.LIGHT_TANK:
                return new LightTank(row, col, player, orientation);
            case Unit.HEAVY_TANK:
                return new HeavyTank(row, col, player, orientation);
            case Unit.TRUCK:
                return new Truck(row, col, player, orientation);
            case Unit.ARTILLERY:
                return new Artillery(row, col, player, orientation);
            case Unit.HALFTRACK:
                return new Halftrack(row, col, player, orientation);
            case Unit.BUNKER_MACHINE_GUN:
                return new BunkerMachineGun(row, col, player, orientation);
            case Unit.BUNKER_ARTILLERY:
                return new BunkerArtillery(row, col, player, orientation);
            case Unit.FLAK:
                return new Flak(row, col, player, orientation);
            case Unit.FIGHTER:
                return new Fighter(row, col, player, orientation);
            case Unit.BOMBER:
                return new Bomber(row, col, player, orientation);
            case Unit.TRANSPORTER:
                return new Transporter(row, col, player, orientation);
            case Unit.LANDER:
                return new Lander(row, col, player, orientation);
            case Unit.DESTROYER:
                return new Destroyer(row, col, player, orientation);
            case Unit.BATTLESHIP:
                return new Battleship(row, col, player, orientation);
            case Unit.AIRCRAFT_CARRIER:
                return new AircraftCarrier(row, col, player, orientation);
            default:
                return null;
        }
    }

    public abstract TextureRegion getTextureIdle();

    @Override
    public TextureRegion getToggleTexture() {
        return getTextureIdle();
    }

    public abstract TextureRegion getTextureFire(float delta);

    public abstract TextureRegion getButtonTexture();

    public abstract TextureRegion getButtonPressedTexture();

    public abstract TextureRegion getButtonDisabledTexture();

    public TextureRegion getTypeTexture(GameAssets assets) {
        if (this.isFeetUnit()) {
            return assets.unit_feet;
        } else if (this.isAirUnit()) {
            return assets.unit_air;
        } else if (this.isNavalUnit()) {
            return assets.unit_naval;
        } else if (this.isFixedUnit()) {
            return assets.unit_fixed;
        } else {
            return assets.unit_motor;
        }
    }

    public TextureRegion[] getCanAttackTypeTexture(GameAssets assets) {
        TextureRegion[] result = new TextureRegion[]{};
        if (!this.isTransportUnit()) {
            if (this.isAntiAirUnit()) {
                if (this.type == FIGHTER) {
                    result = new TextureRegion[]{assets.unit_feet, assets.unit_motor, assets.unit_air, assets.unit_naval, assets.unit_fixed};
                } else {
                    result = new TextureRegion[]{assets.unit_air};
                }
            } else {
                result = new TextureRegion[]{assets.unit_feet, assets.unit_motor, assets.unit_naval, assets.unit_fixed};
            }
        }
        return result;
    }

    public int getMoveDistance() {
        return Math.min(this.moveAmount, this.gas);
    }

    public boolean canBeOnTile(Tile tile) {
        // Plane can go everywhere
        switch (this.kind) {
            case AIR:
                return true;
            case NAVAL:
                return tile.isSea();
            case GROUND:
                if (tile.isSea()) {
                    return false;
                } else {
                    if (((tile.isMountains() || tile.isRiver()) && !isFeetUnit())) {
                        return false;
                    } else {
                        return true;
                    }
                }
            case FIXED:
                if(!tile.isRiver() && !tile.isSea()) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    public boolean isEnemyUnit(Unit unit) {
        return this.owner.getTeam() != unit.getOwner().getTeam();
    }

    public boolean canBeAttackedBy(Unit unit) {
        // Transport unit ?
        if (unit != null && !unit.isTransportUnit()) {
            // Target is an enemy unit ?
            if (this.isEnemyUnit(unit)) {
                if (this.isAirUnit()) {
                    if (unit.isAntiAirUnit()) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    if (unit.isAntiAirUnit() && unit.getType() != FIGHTER) {
                        return false;
                    } else {
                        return true;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isEmpty() {
        return this.passenger == null;
    }

    public boolean canEmbarkIn(Unit unit) {
        if (unit != null && unit.isTransportUnit() && unit.isEmpty() && this.getOwner().equals(unit.getOwner())) {
            if (this.isGroundUnit()) {
                if (unit.getType() == LANDER) {
                    return true;
                } else {
                    if (this.isFeetUnit()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if(this.isAirUnit()) {
                if(unit.getType() == AIRCRAFT_CARRIER) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void addPassenger(Unit unit) {
        if (unit.canEmbarkIn(this)) {
            unit.stop();
            this.passenger = unit;
            this.getOwner().removeUnit(unit);
            this.passenger.setRow(this.row);
            this.passenger.setCol(this.col);
        }
    }

    public void dropPassenger(int row, int col) {
        if (this.passenger != null) {
            this.passenger.position = Util.mapToWorldCoord(new Vector2(row, col));
            this.passenger.setRow(row);
            this.passenger.setCol(col);
            this.passenger.stop();
            this.getOwner().addUnit(this.passenger);
            this.passenger = null;
        }
        this.stop();
    }

    public Unit getPassenger() {
        return this.passenger;
    }

    public boolean canAttack(Game world) {
        if (!this.isTransportUnit() && this.ammo > 0) {
            for (int row = 0; row < world.getMapSize(); row++) {
                for (int col = 0; col < world.getMapSize(); col++) {
                    if (world.isRedTarget(this, row, col)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public boolean canCapture(Game world) {
        Building building = world.getBuildingLayer()[this.getRow()][this.getCol()];
        if (this.isFeetUnit() && building != null && !building.getOwner().isAllied(this.getOwner())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canReload(Game world) {
        if (this.type == TRUCK) {
            for (int row = 0; row < world.getMapSize(); row++) {
                for (int col = 0; col < world.getMapSize(); col++) {
                    int manhattanDistance = Math.abs(this.getRow() - row) + Math.abs(this.getCol() - col);
                    // 1. Check if square is in 1 scope
                    if (manhattanDistance == 1) {
                        Unit neighbor = world.getUnitLayer()[row][col];
                        // 2. Check if unit have same owner
                        if (neighbor != null && neighbor.isGroundUnit()  && this.getOwner().equals(world.getUnitLayer()[row][col].getOwner())) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else if (this.type == AIRCRAFT_CARRIER) {
            for (int row = 0; row < world.getMapSize(); row++) {
                for (int col = 0; col < world.getMapSize(); col++) {
                    int manhattanDistance = Math.abs(this.getRow() - row) + Math.abs(this.getCol() - col);
                    // 1. Check if square is in 1 scope
                    if (manhattanDistance == 1) {
                        Unit neighbor = world.getUnitLayer()[row][col];
                        // 2. Check if unit have same owner
                        if (neighbor != null && neighbor.isAirUnit()  && this.getOwner().equals(world.getUnitLayer()[row][col].getOwner())) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public boolean canDrop(Game world) {
        if (this.isTransportUnit() && this.passenger != null) {
            for (int row = 0; row < world.getMapSize(); row++) {
                for (int col = 0; col < world.getMapSize(); col++) {
                    if (world.isDropTarget(this, row, col)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public void reload() {
        this.gas = this.getMaxGas();
        this.ammo = this.getMaxAmmo();
        // Reload animation
        this.reload_step = 0;
        Timer.Task captureTask = new Timer.Task() {
            @Override
            public void run() {
                reload_step++;
                if (reload_step == RELOAD_STEP) {
                    this.cancel();
                }
            }
        };
        Timer.schedule(captureTask, 0.0f, 0.025f, RELOAD_STEP);
    }

    public int getMaxGas() {
        switch (type) {
            case Unit.INFANTRY:
                return INFANTRY_GAS;
            case Unit.BAZOOKA:
                return BAZOOKA_GAS;
            case Unit.SNIPER:
                return SNIPER_GAS;
            case Unit.JEEP:
                return JEEP_GAS;
            case Unit.LIGHT_TANK:
                return LIGHT_TANK_GAS;
            case Unit.HEAVY_TANK:
                return HEAVY_TANK_GAS;
            case Unit.TRUCK:
                return TRUCK_GAS;
            case Unit.ARTILLERY:
                return ARTILLERY_GAS;
            case HALFTRACK:
                return HALFTRACK_GAS;
            case FIGHTER:
                return FIGHTER_GAS;
            case BOMBER:
                return BOMBER_GAS;
            case TRANSPORTER:
                return TRANSPORTER_GAS;
            case LANDER:
                return LANDER_GAS;
            case DESTROYER:
                return DESTROYER_GAS;
            case BATTLESHIP:
                return BATTLESHIP_GAS;
            case AIRCRAFT_CARRIER:
                return AIRCRAFT_CARRIER_GAS;
            default:
                return 0;
        }
    }

    public int getMaxAmmo() {
        switch (type) {
            case Unit.INFANTRY:
                return INFANTRY_AMMO;
            case Unit.BAZOOKA:
                return BAZOOKA_AMMO;
            case Unit.SNIPER:
                return SNIPER_AMMO;
            case Unit.JEEP:
                return JEEP_AMMO;
            case Unit.LIGHT_TANK:
                return LIGHT_TANK_AMMO;
            case Unit.HEAVY_TANK:
                return HEAVY_TANK_AMMO;
            case Unit.TRUCK:
                return TRUCK_AMMO;
            case Unit.ARTILLERY:
                return ARTILLERY_AMMO;
            case HALFTRACK:
                return HALFTRACK_AMMO;
            case BUNKER_MACHINE_GUN:
                return BUNKER_MACHINE_GUN_AMMO;
            case BUNKER_ARTILLERY:
                return BUNKER_ARTILLERY_AMMO;
            case FLAK:
                return FLAK_AMMO;
            case FIGHTER:
                return FIGHTER_AMMO;
            case BOMBER:
                return BOMBER_AMMO;
            case TRANSPORTER:
                return TRANSPORTER_AMMO;
            case LANDER:
                return LANDER_AMMO;
            case DESTROYER:
                return DESTROYER_AMMO;
            case BATTLESHIP:
                return BATTLESHIP_AMMO;
            case AIRCRAFT_CARRIER:
                return AIRCRAFT_CARRIER_AMMO;
            default:
                return 0;
        }
    }

    public void heal(int amount) {
        life = Math.min(this.life + amount, LIFE);
    }

    public void hurt(int amount) {
        life = Math.max(this.life - amount, 0);
    }

    private boolean isDying() {
        return this.die_step < DIE_STEP;
    }

    private boolean isAttacked() {
        return this.attacked_step < ATTACKED_STEP;
    }

    private boolean isReloading() {
        return this.reload_step < RELOAD_STEP;
    }

    public boolean isLowGas() {
        return this.gas <= this.getMaxGas() / 5f && this.getMaxGas() > 0;
    }

    public boolean isLowAmmo() {
        return this.ammo <= this.getMaxAmmo() / 5f && this.getMaxAmmo() > 0;
    }

    public abstract String getDescription();

    public void updateWays(Game game) {
        disposeWays();
        ArrayList<int[]> noMove = new ArrayList<int[]>();
        noMove.add(new int[]{row, col});
        ways.add(noMove);
        int depth = getMoveDistance();
        int length = 1;
        while (depth > 0) {
            ArrayList<ArrayList<int[]>> al = (ArrayList<ArrayList<int[]>>) ways.clone();
            for (ArrayList<int[]> move : al) {
                // Checking only longest moves
                if (move.size() == length) {
                    // Getting last tile of the path
                    int[] tile = move.get(move.size() - 1);
                    // NORTH
                    checkMoveValidity(game, move, tile[0] + 1, tile[1]);
                    // SOUTH
                    checkMoveValidity(game, move, tile[0] - 1, tile[1]);
                    // EAST
                    checkMoveValidity(game, move, tile[0], tile[1] + 1);
                    // WEST
                    checkMoveValidity(game, move, tile[0], tile[1] - 1);
                }
            }
            length++;
            depth--;
        }
        ways.remove(0);
    }

    private void checkMoveValidity(Game game, ArrayList<int[]> move, int row, int col) {
        ArrayList<int[]> nextMove = (ArrayList<int[]>) move.clone();
        // 1. Check that row and col are in map bounds
        if (row >= 0 && row < game.getMapSize() && col >= 0 && col < game.getMapSize()) {
            // 2. Check that tile is empty
            if (canGoOnTarget(game, row, col)) {
                    nextMove.add(new int[]{row, col});
                    ways.add(nextMove);
            }
        }
    }

    public boolean canGoOnTarget(Game game, int row, int col) {
        // 1. Check that target is empty
        if (game.getUnit(row, col) == null) {
            // 2. Check if unit can be on tile or if there is a building(for shipyard)
            if (canBeOnTile(game.getMap().getTile(row, col)) || game.getBuildingLayer()[row][col] != null && game.getBuildingLayer()[row][col].getType() == Building.SHIPYARD) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public ArrayList<ArrayList<int[]>> getWays() {
        return this.ways;
    }

    public void disposeWays() {
        this.ways = new ArrayList<ArrayList<int[]>>();
    }

    public boolean isHurt() {
        return life < Unit.LIFE;
    }

    public Orientation getOrientation() {
        return this.orientation;
    }

    public int getOrientationValue() {
        switch (this.orientation) {
            case SOUTH:
                return 0;
            case WEST:
                return 1;
            case NORTH:
                return 2;
            case EAST:
                return 3;
            default:
                return 0;
        }
    }

    public static Orientation valueToOrientation(int value) {
        switch (value) {
            case 0:
                return SOUTH;
            case 1:
                return WEST;
            case 2:
                return NORTH;
            case 3:
                return EAST;
            default:
                return SOUTH;
        }
    }

    public int getMoveAmount() {
        return this.moveAmount;
    }

    public Sound getSoundFire() {
        return this.fireSound;
    }

    public Sound getSoundMove() {
        return this.moveSound;
    }

    public static int getHumanSpeedPosition() {
        if (HUMAN_SPEED == SPEED_FAST) {
            return 2;
        } else if (HUMAN_SPEED == SPEED_NORMAL) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void updateHumanSpeed(int speed) {
        switch (speed) {
            case 2:
                HUMAN_SPEED = SPEED_FAST;
                break;
            case 1:
                HUMAN_SPEED = SPEED_NORMAL;
                break;
            case 0:
                HUMAN_SPEED = SPEED_SLOW;
                break;
        }
    }

    public static int getRobotSpeedPosition() {
        if (ROBOT_SPEED == SPEED_FAST) {
            return 2;
        } else if (ROBOT_SPEED == SPEED_NORMAL) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void updateRobotSpeed(int speed) {
        switch (speed) {
            case 2:
                ROBOT_SPEED = SPEED_FAST;
                break;
            case 1:
                ROBOT_SPEED = SPEED_NORMAL;
                break;
            case 0:
                ROBOT_SPEED = SPEED_SLOW;
                break;
        }
    }

    public boolean isUnit() {
        return true;
    }

}
