package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.GameAssets;

public class Square extends GameObject {

    private Target target;
    private Scope scope;
    protected boolean selected = false;

    public enum Target {
        NOTHING, BLUE, RED, GREEN
    }

    public enum Scope {
        NOTHING, BLUE, RED
    }

    public Square(int row, int col) {
        super(row, col);
        this.target = Target.NOTHING;
        this.scope = Scope.NOTHING;
    }

    @Override
    public void render(SpriteBatch batch, float delta, GameAssets assets) {
        if (selected) {
            batch.draw(assets.tile_selected, position.x, position.y, width, height);
        } else {
            switch (this.target) {
                case NOTHING:
                    switch (this.scope) {
                        case NOTHING:
                            break;
                        case BLUE:
                            batch.draw(assets.tile_scope_blue, position.x, position.y, width, height);
                            break;
                        case RED:
                            batch.draw(assets.tile_scope_red, position.x, position.y, width, height);
                            break;
                    }
                    break;
                case BLUE:
                    batch.draw(assets.tile_target_blue, position.x, position.y, width, height);
                    break;
                case RED:
                    batch.draw(assets.tile_target_red, position.x, position.y, width, height);
                    break;
                case GREEN:
                    batch.draw(assets.tile_target_green, position.x, position.y, width, height);
                    break;
            }
        }
    }

    public void select() {
        this.selected = true;
    }

    public void release() {
        this.selected = false;
        this.target = Target.NOTHING;
        this.scope = Scope.NOTHING;
    }

    public void targetBlue() {
        this.target = Target.BLUE;
    }

    public boolean isBlueTarget() {
        return this.target == Target.BLUE;
    }

    public void targetRed() {
        this.target = Target.RED;
    }

    public boolean isRedTarget() {
        return this.target == Target.RED;
    }

    public void targetGreen() {
        this.target = Target.GREEN;
    }

    public boolean isGreenTarget() {
        return this.target == Target.GREEN;
    }

    public void scopeBlue() {
        this.scope = Scope.BLUE;
    }
    public void scopeRed() {
        this.scope = Scope.RED;
    }

    @Override
    public TextureRegion getTexture() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public TextureRegion getToggleTexture() {
        return getTexture();
    }
}
