package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class LightTank extends Unit{

	private static final String NAME_USA = "lighttank.usa";
	private static final String NAME_GER = "lighttank.ger";
	private static final String NAME_USS = "lighttank.uss";
	private static final String NAME_JAP = "lighttank.jap";
	private static final String NAME_GBR = "lighttank.gbr";
	private static final String DESC_USA = "lighttank_desc.usa";
	private static final String DESC_GER = "lighttank_desc.ger";
	private static final String DESC_USS = "lighttank_desc.uss";
	private static final String DESC_JAP = "lighttank_desc.jap";
	private static final String DESC_GBR = "lighttank_desc.gbr";

	public LightTank(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, LIGHT_TANK, Kind.GROUND, LIGHT_TANK_PRICE, LIGHT_TANK_MOVE_SCOPE, LIGHT_TANK_ATTACK_SCOPE, LIFE, LIGHT_TANK_AMMO, LIGHT_TANK_GAS, LIGHT_TANK_DMG, owner.getArmy().getGameAssets().light_tank_fire, owner.getArmy().getGameAssets().light_tank_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().light_tank_S;
			case WEST:
				return this.owner.getArmy().getAssets().light_tank_W;
			case NORTH:
				return this.owner.getArmy().getAssets().light_tank_N;
			case EAST:
				return this.owner.getArmy().getAssets().light_tank_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().light_tank_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().light_tank_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().light_tank_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().light_tank_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().light_tank_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_light_tank;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_light_tank_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_light_tank_disabled;
	}

	@Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case com.nauwstudio.dutywars_ww2.game.Army.USA:
				return NAME_USA;
			case com.nauwstudio.dutywars_ww2.game.Army.GER:
				return NAME_GER;
			case com.nauwstudio.dutywars_ww2.game.Army.USS:
				return NAME_USS;
			case com.nauwstudio.dutywars_ww2.game.Army.JAP:
				return NAME_JAP;
            case com.nauwstudio.dutywars_ww2.game.Army.GBR:
                return NAME_GBR;
		}
		return "";
	}

	@Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}

}
