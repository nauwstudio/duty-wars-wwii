package com.nauwstudio.dutywars_ww2.game.buildings;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Capital extends Building {

    public static final String NAME = "building.capital";
    public static final String NAME_EIFFEL = "building.eiffel";
    public static final String NAME_BRANDEBOURG = "building.brandebourg";
    public static final String NAME_COLISSEUM = "building.colisseum";
    public static final String NAME_BIG_BEN = "building.big_ben";


    public static final int CLASSIC = 0;
    public static final int EIFFEL = 1;
    public static final int BRANDENBURG = 2;
    public static final int COLISSEUM = 3;
    public static final int BIG_BEN = 4;

    public Capital(int row, int col, Player owner, int subtype) {
        super(row, col, owner, CAPITAL, CAPITAL_MONEY, CAPITAL_DEFENCE);
        this.subtype = subtype;
        this.textureRatio = getTexture().getRegionHeight() / (float) getTexture().getRegionWidth();
    }

    @Override
    public TextureRegion getTexture() {
        switch (subtype) {
            case EIFFEL:
                return this.owner.getArmy().getAssets().eiffel;
            case BRANDENBURG:
                return this.owner.getArmy().getAssets().brandenburg;
            case COLISSEUM:
                return this.owner.getArmy().getAssets().colisseum;
            case BIG_BEN:
                return this.owner.getArmy().getAssets().big_ben;
            case CLASSIC:
                return this.owner.getArmy().getAssets().capital;
            default:
                return this.owner.getArmy().getAssets().capital;
        }
    }

    public String getName() {
        switch (subtype) {
            case EIFFEL:
                return NAME_EIFFEL;
            case BRANDENBURG:
                return NAME_BRANDEBOURG;
            case COLISSEUM:
                return NAME_COLISSEUM;
            case BIG_BEN:
                return NAME_BIG_BEN;
            case CLASSIC:
                return NAME;
            default:
                return NAME;
        }
    }

    @Override
    public boolean isCapital() {
        return true;
    }
}
