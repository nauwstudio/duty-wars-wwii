package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Sniper extends Unit{

	private static final String NAME_USA = "sniper.usa";
	private static final String NAME_GER = "sniper.ger";
	private static final String NAME_USS = "sniper.uss";
	private static final String NAME_JAP = "sniper.jap";
	private static final String NAME_GBR = "sniper.gbr";
	private static final String DESC_USA = "sniper_desc.usa";
	private static final String DESC_GER = "sniper_desc.ger";
	private static final String DESC_USS = "sniper_desc.uss";
	private static final String DESC_JAP = "sniper_desc.jap";
	private static final String DESC_GBR = "sniper_desc.gbr";

	public Sniper(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, SNIPER, Kind.GROUND, SNIPER_PRICE, SNIPER_MOVE_SCOPE, SNIPER_ATTACK_SCOPE, LIFE, SNIPER_AMMO, SNIPER_GAS, SNIPER_DMG, owner.getArmy().getGameAssets().sniper_fire, owner.getArmy().getGameAssets().men_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().sniper_S;
			case WEST:
				return this.owner.getArmy().getAssets().sniper_W;
			case NORTH:
				return this.owner.getArmy().getAssets().sniper_N;
			case EAST:
				return this.owner.getArmy().getAssets().sniper_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().sniper_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().sniper_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().sniper_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().sniper_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().sniper_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_sniper;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_sniper_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_sniper_disabled;
	}

	@Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return NAME_USA;
			case Army.GER:
				return NAME_GER;
			case Army.USS:
				return NAME_USS;
			case Army.JAP:
				return NAME_JAP;
			case Army.GBR:
				return NAME_GBR;
		}
		return "";
	}

	@Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}
}
