package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Flak extends Unit {

	private static final String NAME_USA = "flak.usa";
	private static final String NAME_GER = "flak.ger";
	private static final String NAME_USS = "flak.uss";
	private static final String NAME_JAP = "flak.jap";
	private static final String NAME_GBR = "flak.gbr";
	private static final String DESC_USA = "flak_desc.usa";
	private static final String DESC_GER = "flak_desc.ger";
	private static final String DESC_USS = "flak_desc.uss";
	private static final String DESC_JAP = "flak_desc.jap";
	private static final String DESC_GBR = "flak_desc.gbr";

	public Flak(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, FLAK, Kind.FIXED, FLAK_PRICE, FLAK_MOVE_SCOPE, FLAK_ATTACK_SCOPE, LIFE, FLAK_AMMO, FLAK_GAS, FLAK_DMG, owner.getArmy().getGameAssets().destroyer_fire, owner.getArmy().getGameAssets().infantry_fire);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().flak_S;
			case WEST:
				return this.owner.getArmy().getAssets().flak_W;
			case NORTH:
				return this.owner.getArmy().getAssets().flak_N;
			case EAST:
				return this.owner.getArmy().getAssets().flak_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().flak_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().flak_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().flak_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().flak_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().flak_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_flak;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_flak_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_flak_disabled;
	}

	@Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return NAME_USA;
			case Army.GER:
				return NAME_GER;
			case Army.USS:
				return NAME_USS;
			case Army.JAP:
				return NAME_JAP;
			case Army.GBR:
				return NAME_GBR;
		}
		return "";
	}

	@Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}
}
