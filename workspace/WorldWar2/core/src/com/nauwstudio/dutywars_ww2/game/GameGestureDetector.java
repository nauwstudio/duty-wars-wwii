package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.GameScreen;
import com.nauwstudio.dutywars_ww2.Util;

public class GameGestureDetector implements GestureListener {

    GameScreen screen;
    Game game;
    GameRenderer renderer;

    public GameGestureDetector(GameScreen screen) {
        this.screen = screen;
        this.game = screen.getGame();
        this.renderer = screen.getRenderer();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if(this.game.isRobotPlaying()) {
            return false;
        } else {
            // Reverse y-axis
            y = Util.getScreenHeight() - y;
            // Release button
            if (this.screen.touchedButton != null) {
                this.screen.touchedButton.release();
                this.screen.touchedButton = null;
            }
            if (this.screen.touchedSquare != null) {
                //this.screen.touchedSquare.release();
                this.screen.touchedSquare = null;
            }
            // Move button camera
            if (y < Util.getButtonsMenuHeight() && this.game.getSelectedBuilding() != null && this.game.getBottomButtons().size() > 4) {
                this.screen.can_pan = false;
                this.renderer.moveButtonCamera(-deltaX);
                return true;
            }
            // Move game camera if can pan
            if (this.screen.can_pan && this.game.isRunning()) {
                this.renderer.moveCamera(-deltaX, deltaY);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        if (this.game.isRunning() && this.screen.can_pinch) {
            screen.touchedSquare = null;
            if (initialPointer1.dst(initialPointer2) > pointer1.dst(pointer2)) {
                this.renderer.zoomOut();
            } else {
                this.renderer.zoomIn();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void pinchStop() {

    }
}
