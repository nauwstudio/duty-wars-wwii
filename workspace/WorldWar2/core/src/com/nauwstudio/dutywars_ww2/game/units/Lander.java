package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Lander extends Unit{

	private static final String NAME = "lander";
	private static final String DESC = "lander_desc";

	public Lander(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, LANDER, Kind.NAVAL, LANDER_PRICE, LANDER_MOVE_SCOPE, LANDER_ATTACK_SCOPE, LIFE, LANDER_AMMO, LANDER_GAS, LANDER_DMG, owner.getArmy().getGameAssets().infantry_fire, owner.getArmy().getGameAssets().lander_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().lander_S;
			case WEST:
				return this.owner.getArmy().getAssets().lander_W;
			case NORTH:
				return this.owner.getArmy().getAssets().lander_N;
			case EAST:
				return this.owner.getArmy().getAssets().lander_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		return getTextureIdle();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().lander_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_lander;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_lander_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_lander_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}


}
