package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.Util;

public class Tile extends GameObject {

    // TYPE
    public static final int PLAIN = 1;
    public static final int FOREST = 2;
    public static final int SAND = 3;
    public static final int SEA = 4;
    public static final int MOUNTAINS = 5;
    public static final int RIVER = 6;
    public static final int ROAD = 7;
    // SUBTYPE RIVER
    public static final int RIVER_SN = 1;
    public static final int RIVER_WE = 2;
    public static final int RIVER_NE = 3;
    public static final int RIVER_NW = 4;
    public static final int RIVER_SE = 5;
    public static final int RIVER_SW = 6;
    // SUBTYPE ROAD
    public static final int ROAD_SN = 1;
    public static final int ROAD_WE = 2;
    public static final int ROAD_NE = 3;
    public static final int ROAD_NW = 4;
    public static final int ROAD_SE = 5;
    public static final int ROAD_SW = 6;
    public static final int BRIDGE_SN = 7;
    public static final int BRIDGE_WE = 8;
    public static final int CROSSROAD = 9;
    // DEFENCE
    public static final int PLAIN_DEFENCE = 1;
    public static final int FOREST_DEFENCE = 2;
    public static final int SAND_DEFENCE = 0;
    public static final int SEA_DEFENCE = 0;
    public static final int MOUNTAINS_DEFENCE = 4;
    public static final int RIVER_DEFENCE = 0;
    public static final int ROAD_DEFENCE = 0;

    private Season season;
    private int type;
    private int subtype;

    public Tile(int row, int col, Season season, int type, int subtype) {
        super(row, col);
        this.season = season;
        this.type = type;
        this.subtype = subtype;
        this.textureRatio = getTexture().getRegionHeight() / (float) getTexture().getRegionWidth();
    }

    @Override
    public void render(SpriteBatch batch, float delta, com.nauwstudio.dutywars_ww2.GameAssets assets) {
        batch.draw(this.getTexture(), position.x, position.y - Util.MAP_SHIFT * this.height, width, width * this.textureRatio);
    }

    public void renderSmall(SpriteBatch batch, float x, float y, float width, int size) {
        float small_tile_width = width / size;
        float ratio = small_tile_width / this.width;
        float pos_x = x + position.x * ratio + width / 2 - small_tile_width / 2;
        float pos_y = y + position.y * ratio - Util.MAP_SHIFT * this.height * ratio;
        batch.draw(this.getTexture(), pos_x, pos_y, small_tile_width, small_tile_width * this.textureRatio);

    }

    public TextureRegion getTexture() {
        switch (this.type) {
            case PLAIN:
                return this.season.getAssets().plain;
            case FOREST:
                return this.season.getAssets().forest;
            case SAND:
                return this.season.getAssets().sand;
            case SEA:
                if (this.row == 0 && this.col == 0) {
                    return this.season.getAssets().sea_sw;
                } else if (this.col == 0) {
                    return this.season.getAssets().sea_s;
                } else if (this.row == 0) {
                    return this.season.getAssets().sea_w;
                } else {
                    return this.season.getAssets().sea;
                }
            case MOUNTAINS:
                return this.season.getAssets().mountains;
            case RIVER:
                switch (subtype) {
                    case RIVER_SN:
                        return this.season.getAssets().river_sn;
                    case RIVER_WE:
                        return this.season.getAssets().river_we;
                    case RIVER_NW:
                        return this.season.getAssets().river_nw;
                    case RIVER_NE:
                        return this.season.getAssets().river_ne;
                    case RIVER_SE:
                        return this.season.getAssets().river_se;
                    case RIVER_SW:
                        return this.season.getAssets().river_sw;
                }
            case ROAD:
                switch (subtype) {
                    case ROAD_SN:
                        return this.season.getAssets().road_sn;
                    case ROAD_WE:
                        return this.season.getAssets().road_we;
                    case ROAD_NW:
                        return this.season.getAssets().road_nw;
                    case ROAD_NE:
                        return this.season.getAssets().road_ne;
                    case ROAD_SE:
                        return this.season.getAssets().road_se;
                    case ROAD_SW:
                        return this.season.getAssets().road_sw;
                    case BRIDGE_SN:
                        return this.season.getAssets().bridge_sn;
                    case BRIDGE_WE:
                        return this.season.getAssets().bridge_we;
                    case CROSSROAD:
                        return this.season.getAssets().crossroad;
                }
            default:
                return null;
        }
    }

    public String getName() {
        switch (this.type) {
            case PLAIN:
                return "tile.plain";
            case FOREST:
                return "tile.forest";
            case SAND:
                return "tile.beach";
            case SEA:
                return "tile.sea";
            case MOUNTAINS:
                return "tile.mountains";
            case RIVER:
                return "tile.river";
            case ROAD:
                return "tile.road";
            default:
                return null;
        }
    }

    public boolean isRiver() {
        return this.type == RIVER;
    }

    public boolean isSea() {
        return this.type == SEA;
    }

    public boolean isMountains() {
        return this.type == MOUNTAINS;
    }

    public int getDefence() {
        switch (this.type) {
            case PLAIN:
                return PLAIN_DEFENCE;
            case FOREST:
                return FOREST_DEFENCE;
            case SAND:
                return SAND_DEFENCE;
            case SEA:
                return SEA_DEFENCE;
            case MOUNTAINS:
                return MOUNTAINS_DEFENCE;
            case RIVER:
                return RIVER_DEFENCE;
            case ROAD:
                return ROAD_DEFENCE;
            default:
                return 0;
        }
    }

    public Season getSeason() {
        return this.season;
    }

    public int getType() {
        return this.type;
    }

    public int getSubtype() {
        return this.subtype;
    }

    public void edit(Season season, int type, int subtype) {
        this.season = season;
        this.type = type;
        this.subtype = subtype;
        this.textureRatio = getTexture().getRegionHeight() / (float) getTexture().getRegionWidth();
    }

    @Override
    public TextureRegion getToggleTexture() {
        return getTexture();
    }
}
