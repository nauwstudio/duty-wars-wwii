package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Truck extends Unit{

	private static final String NAME = "truck";
	private static final String DESC = "truck_desc";

	public Truck(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, TRUCK, Kind.GROUND, TRUCK_PRICE, TRUCK_MOVE_SCOPE, TRUCK_ATTACK_SCOPE, LIFE, TRUCK_AMMO, TRUCK_GAS, TRUCK_DMG, owner.getArmy().getGameAssets().infantry_fire, owner.getArmy().getGameAssets().truck_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().truck_S;
			case WEST:
				return this.owner.getArmy().getAssets().truck_W;
			case NORTH:
				return this.owner.getArmy().getAssets().truck_N;
			case EAST:
				return this.owner.getArmy().getAssets().truck_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		return getTextureIdle();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().truck_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_truck;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_truck_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_truck_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}


}
