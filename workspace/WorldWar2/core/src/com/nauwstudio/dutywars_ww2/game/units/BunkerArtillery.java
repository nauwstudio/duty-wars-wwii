package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class BunkerArtillery extends Unit {

	public static final String NAME = "bunker_artillery";
	public static final String DESC = "bunker_artillery_desc";

	public BunkerArtillery(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, BUNKER_ARTILLERY, Kind.FIXED, BUNKER_ARTILLERY_PRICE, BUNKER_ARTILLERY_MOVE_SCOPE, BUNKER_ARTILLERY_ATTACK_SCOPE, LIFE, BUNKER_ARTILLERY_AMMO, BUNKER_ARTILLERY_GAS, BUNKER_ARTILLERY_DMG, owner.getArmy().getGameAssets().artillery_fire, owner.getArmy().getGameAssets().infantry_fire);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().bunker_artillery_S;
			case WEST:
				return this.owner.getArmy().getAssets().bunker_artillery_W;
			case NORTH:
				return this.owner.getArmy().getAssets().bunker_artillery_N;
			case EAST:
				return this.owner.getArmy().getAssets().bunker_artillery_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_artillery_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_artillery_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_artillery_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_artillery_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().bunker_artillery_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_bunker_artillery;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_bunker_artillery_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_bunker_artillery_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
