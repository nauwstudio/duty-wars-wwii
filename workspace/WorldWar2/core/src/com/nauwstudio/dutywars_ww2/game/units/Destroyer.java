package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Destroyer extends Unit {

	private static final String NAME = "destroyer";
	private static final String DESC = "destroyer_desc";

	public Destroyer(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, DESTROYER, Kind.NAVAL, DESTROYER_PRICE, DESTROYER_MOVE_SCOPE, DESTROYER_ATTACK_SCOPE, LIFE, DESTROYER_AMMO, DESTROYER_GAS, DESTROYER_DMG, owner.getArmy().getGameAssets().destroyer_fire, owner.getArmy().getGameAssets().boat_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().destroyer_S;
			case WEST:
				return this.owner.getArmy().getAssets().destroyer_W;
			case NORTH:
				return this.owner.getArmy().getAssets().destroyer_N;
			case EAST:
				return this.owner.getArmy().getAssets().destroyer_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().destroyer_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().destroyer_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().destroyer_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().destroyer_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().destroyer_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_destroyer;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_destroyer_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_destroyer_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
