package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Halftrack extends Unit {

	private static final String NAME = "halftrack";
	private static final String DESC = "halftrack_desc";

	public Halftrack(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, HALFTRACK, Kind.GROUND, HALFTRACK_PRICE, HALFTRACK_MOVE_SCOPE, HALFTRACK_ATTACK_SCOPE, LIFE, HALFTRACK_AMMO, HALFTRACK_GAS, HALFTRACK_DMG, owner.getArmy().getGameAssets().halftrack_fire, owner.getArmy().getGameAssets().halftrack_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().halftrack_S;
			case WEST:
				return this.owner.getArmy().getAssets().halftrack_W;
			case NORTH:
				return this.owner.getArmy().getAssets().halftrack_N;
			case EAST:
				return this.owner.getArmy().getAssets().halftrack_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().halftrack_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().halftrack_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().halftrack_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().halftrack_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().halftrack_S;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_halftrack;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_halftrack_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_halftrack_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
