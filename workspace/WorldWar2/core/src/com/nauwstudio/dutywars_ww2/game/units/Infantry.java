package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.GameAssets;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Infantry extends Unit {

    private static final String NAME_USA = "infantry.usa";
    private static final String NAME_GER = "infantry.ger";
    private static final String NAME_USS = "infantry.uss";
    private static final String NAME_JAP = "infantry.jap";
    private static final String NAME_GBR = "infantry.gbr";
    private static final String DESC_USA = "infantry_desc.usa";
    private static final String DESC_GER = "infantry_desc.ger";
    private static final String DESC_USS = "infantry_desc.uss";
    private static final String DESC_JAP = "infantry_desc.jap";
    private static final String DESC_GBR = "infantry_desc.gbr";

    public Infantry(int row, int col, Player owner, Orientation orientation) {
        super(row, col, orientation, owner, INFANTRY, Kind.GROUND, INFANTRY_PRICE, INFANTRY_MOVE_SCOPE, INFANTRY_ATTACK_SCOPE, LIFE, INFANTRY_AMMO, INFANTRY_GAS, INFANTRY_DMG, owner.getArmy().getGameAssets().infantry_fire, owner.getArmy().getGameAssets().men_move);
    }

    @Override
    public TextureRegion getTextureIdle() {
        switch (orientation) {
            case SOUTH:
                return this.owner.getArmy().getAssets().infantry_S;
            case WEST:
                return this.owner.getArmy().getAssets().infantry_W;
            case NORTH:
                return this.owner.getArmy().getAssets().infantry_N;
            case EAST:
                return this.owner.getArmy().getAssets().infantry_E;
            default:
                return getTexture();
        }
    }

    @Override
    public TextureRegion getTextureFire(float delta) {
        switch (orientation) {
            case SOUTH:
                return (TextureRegion) this.owner.getArmy().getAssets().infantry_S_fire.getKeyFrame(delta);
            case WEST:
                return (TextureRegion) this.owner.getArmy().getAssets().infantry_W_fire.getKeyFrame(delta);
            case NORTH:
                return (TextureRegion) this.owner.getArmy().getAssets().infantry_N_fire.getKeyFrame(delta);
            case EAST:
                return (TextureRegion) this.owner.getArmy().getAssets().infantry_E_fire.getKeyFrame(delta);

        }
        return getTexture();
    }

    @Override
    public TextureRegion getTexture() {
       return this.owner.getArmy().getAssets().infantry_HD;
    }

    @Override
    public TextureRegion getButtonTexture() {
        return this.owner.getArmy().getAssets().button_infantry;
    }

    @Override
    public TextureRegion getButtonPressedTexture() {
        return this.owner.getArmy().getAssets().button_infantry_pressed;
    }

    @Override
    public TextureRegion getButtonDisabledTexture() {
        return this.owner.getArmy().getAssets().button_infantry_disabled;
    }

    @Override
    public String getName() {
        switch (this.owner.getArmy().getType()) {
            case Army.USA:
                return NAME_USA;
            case Army.GER:
                return NAME_GER;
            case Army.USS:
                return NAME_USS;
            case Army.JAP:
                return NAME_JAP;
            case Army.GBR:
                return NAME_GBR;
        }
        return "";
    }

    @Override
    public String getDescription() {
        switch (this.owner.getArmy().getType()) {
            case Army.USA:
                return DESC_USA;
            case Army.GER:
                return DESC_GER;
            case Army.USS:
                return DESC_USS;
            case Army.JAP:
                return DESC_JAP;
            case Army.GBR:
                return DESC_GBR;
        }
        return "";
    }
}
