package com.nauwstudio.dutywars_ww2.game.buildings;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Shipyard extends Building {

    public static final String NAME = "building.shipyard";

    public Shipyard(int row, int col, Player owner) {
        super(row, col, owner, SHIPYARD, SHIPYARD_MONEY, SHIPYARD_DEFENCE);
        this.textureRatio = getTexture().getRegionHeight() / (float) getTexture().getRegionWidth();
    }

    @Override
    public TextureRegion getTexture() {
        return this.owner.getArmy().getAssets().shipyard;
    }

    public String getName() {
        return NAME;
    }

    @Override
    public boolean isShipyard() {
        return true;
    }
}
