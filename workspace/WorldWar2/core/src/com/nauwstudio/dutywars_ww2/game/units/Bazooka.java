package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Bazooka extends Unit {

	private static final String NAME_USA = "bazooka.usa";
	private static final String NAME_GER = "bazooka.ger";
	private static final String NAME_USS = "bazooka.uss";
	private static final String NAME_JAP = "bazooka.jap";
	private static final String NAME_GBR = "bazooka.gbr";
	private static final String DESC_USA = "bazooka_desc.usa";
	private static final String DESC_GER = "bazooka_desc.ger";
	private static final String DESC_USS = "bazooka_desc.uss";
	private static final String DESC_JAP = "bazooka_desc.jap";
	private static final String DESC_GBR = "bazooka_desc.gbr";

	public Bazooka(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, BAZOOKA, Kind.GROUND, BAZOOKA_PRICE, BAZOOKA_MOVE_SCOPE, BAZOOKA_ATTACK_SCOPE, LIFE, BAZOOKA_AMMO, BAZOOKA_GAS, BAZOOKA_DMG, owner.getArmy().getGameAssets().bazooka_fire, owner.getArmy().getGameAssets().men_move);
		}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().bazooka_S;
			case WEST:
				return this.owner.getArmy().getAssets().bazooka_W;
			case NORTH:
				return this.owner.getArmy().getAssets().bazooka_N;
			case EAST:
				return this.owner.getArmy().getAssets().bazooka_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bazooka_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().bazooka_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bazooka_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().bazooka_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().bazooka_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_bazooka;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_bazooka_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_bazooka_disabled;
	}

	@Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return NAME_USA;
			case Army.GER:
				return NAME_GER;
			case Army.USS:
				return NAME_USS;
			case Army.JAP:
				return NAME_JAP;
			case Army.GBR:
				return NAME_GBR;
		}
		return "";
	}

	@Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}
}
