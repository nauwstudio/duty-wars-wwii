package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class BunkerMachineGun extends Unit {

	public static final String NAME = "bunker_machine_gun";
	public static final String DESC = "bunker_machine_gun_desc";

	public BunkerMachineGun(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, BUNKER_MACHINE_GUN, Kind.FIXED, BUNKER_MACHINE_GUN_PRICE, BUNKER_MACHINE_GUN_MOVE_SCOPE, BUNKER_MACHINE_GUN_ATTACK_SCOPE, LIFE, BUNKER_MACHINE_GUN_AMMO, BUNKER_MACHINE_GUN_GAS, BUNKER_MACHINE_GUN_DMG, owner.getArmy().getGameAssets().jeep_fire, owner.getArmy().getGameAssets().infantry_fire);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().bunker_machine_gun_S;
			case WEST:
				return this.owner.getArmy().getAssets().bunker_machine_gun_W;
			case NORTH:
				return this.owner.getArmy().getAssets().bunker_machine_gun_N;
			case EAST:
				return this.owner.getArmy().getAssets().bunker_machine_gun_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_machine_gun_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_machine_gun_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_machine_gun_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().bunker_machine_gun_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().bunker_machine_gun_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_bunker_machine_gun;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_bunker_machine_gun_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_bunker_machine_gun_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
