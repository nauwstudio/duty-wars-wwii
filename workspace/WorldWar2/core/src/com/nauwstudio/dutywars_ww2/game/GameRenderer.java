package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.I18NBundle;
import com.nauwstudio.dutywars_ww2.*;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

public class GameRenderer {

    public static final float ZOOM_MAX = 2.0f;
    public static final float ZOOM_MIN = 0.5f;

    private Game game;
    SpriteBatch batch;

    private OrthographicCamera worldCamera;
    private OrthographicCamera menuCamera;
    private OrthographicCamera buttonCamera;

    private MainAssets mainAssets;
    private GameAssets assets;
    private GameHUDVars hudVars;

    public GameRenderer(Game game) {
        try {
            this.game = game;
            // CAMERAS
            worldCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
            worldCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
            worldCamera.update();
            menuCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
            menuCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
            menuCamera.update();
            buttonCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
            buttonCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
            buttonCamera.update();

            this.batch = new SpriteBatch();
            this.batch.setProjectionMatrix(this.worldCamera.combined);
            this.mainAssets = this.game.getMainAssets();
            this.assets = this.game.getAssets();
            this.hudVars = this.game.getHUDVars();
            centerCamera();
        } catch (Exception e) {
        }
    }

    public void resize(int width, int height) {
        worldCamera = new OrthographicCamera(width, height);
        worldCamera.setToOrtho(false, width, height);
        worldCamera.update();
        menuCamera = new OrthographicCamera(width, height);
        menuCamera.setToOrtho(false, width, height);
        menuCamera.update();
        buttonCamera = new OrthographicCamera(width, height);
        buttonCamera.setToOrtho(false, width, height);
        buttonCamera.update();
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        // REPLACE BUTTONS
        // World is running
        if (this.game.isRunning()) {
            if (!this.game.isRobotPlaying()) {
                // A unit was selected
                if (this.game.getSelectedUnit() != null) {
                    this.game.selectUnit(this.game.getSquareLayer()[this.game.getSelectedUnit().getRow()][this.game.getSelectedUnit().getCol()]);
                }
                // A building was selected
                else if (this.game.getSelectedBuilding() != null) {
                    this.game.selectBuilding(this.game.getSquareLayer()[this.game.getSelectedBuilding().getRow()][this.game.getSelectedBuilding().getCol()]);
                }
                // A tile was selected
                else if (this.game.getSelectedTile() != null) {

                }
                // Nothing was selected
                else {
                    this.game.createMainButtons();
                }
            }
        }
        // World is menu
        else if (this.game.isMenu()) {
            this.game.createMenuButtons();
        }
        // World is stat
        else if (this.game.isStat()) {
            this.game.createStatButtons();
        }
        // World is info
        else if (this.game.isInfo()) {
            this.game.createInfoButtons();
        }
        // World is ended
        else if (this.game.isEnded()) {
            this.game.createEndGameButtons();
        }

    }

    public float getCameraPositionX() {
        return this.worldCamera.position.x;
    }

    public float getCameraPositionY() {
        return this.worldCamera.position.y;
    }

    public float getCameraOriginX() {
        return getCameraPositionX() - (getCameraZoomWidth() / 2f);
    }

    public float getCameraOriginY() {
        return getCameraPositionY() - (getCameraZoomHeight() / 2);
    }


    public float getCameraWidth() {
        return this.worldCamera.viewportWidth;
    }

    public float getCameraHeight() {
        return this.worldCamera.viewportHeight;
    }

    public float getCameraZoom() {
        return this.worldCamera.zoom;
    }

    public float getCameraZoomWidth() {
        return getCameraWidth() * this.worldCamera.zoom;
    }

    public float getCameraZoomHeight() {
        return getCameraHeight() * this.worldCamera.zoom;
    }

    public void moveCamera(float distX, float distY) {
        distX = distX * getCameraZoom();
        distY = distY * getCameraZoom();
        float border_left = Util.getTileWidth() / 2f - this.game.getMapWidth() / 2f;
        float border_right = Util.getTileWidth() / 2f + this.game.getMapWidth() / 2f;
        float border_bottom = 0;
        float border_top = this.game.getMapHeight();
        this.worldCamera.translate(distX, distY);
        this.worldCamera.update();
        // Out of borders ?
        if (this.worldCamera.position.y < border_bottom) {
            this.worldCamera.translate(0, border_bottom - this.worldCamera.position.y);
        }
        if (this.worldCamera.position.y > border_top) {
            this.worldCamera.translate(0, border_top - this.worldCamera.position.y);
        }
        if (this.worldCamera.position.x < border_left) {
            this.worldCamera.translate(border_left - this.worldCamera.position.x, 0);
        }
        if (this.worldCamera.position.x > border_right) {
            this.worldCamera.translate(border_right - this.worldCamera.position.x, 0);
        }
        this.worldCamera.update();
    }

    public void pointCamera(Vector2 target) {
        int distX = (int) (target.x - this.worldCamera.position.x);
        int distY = (int) (target.y - this.worldCamera.position.y);
        this.worldCamera.translate(distX, distY);
        this.worldCamera.update();
    }

    public void centerCamera() {
        this.worldCamera.position.x = this.game.getMapWidth() / 2f;
        this.worldCamera.position.y = this.game.getMapHeight() / 2f;
        this.worldCamera.update();
    }

    public void zoomIn() {
        this.worldCamera.zoom = Math.max(this.worldCamera.zoom - 0.02f, ZOOM_MIN);
        worldCamera.update();
    }

    public void zoomOut() {
        this.worldCamera.zoom = Math.min(this.worldCamera.zoom + 0.02f, ZOOM_MAX);
        worldCamera.update();
    }

    public float getButtonCameraPositionX() {
        return this.buttonCamera.position.x;
    }

    public float getButtonCameraPositionY() {
        return this.buttonCamera.position.y;
    }

    public float getButtonCameraOriginX() {
        return getButtonCameraPositionX() - (getButtonCameraWidth() / 2f);
    }

    public float getButtonCameraOriginY() {
        return getButtonCameraPositionY() - (getButtonCameraHeight() / 2f);
    }

    public float getButtonCameraWidth() {
        return this.buttonCamera.viewportWidth;
    }

    public float getButtonCameraHeight() {
        return this.buttonCamera.viewportHeight;
    }

    public void moveButtonCamera(float distX) {
        float border_left = this.getButtonCameraWidth() / 2f;
        float last_button_x = this.game.getBottomButtons().get(this.game.getBottomButtons().size() - 1).getPosition().x;
        float border_right = Math.max(this.getButtonCameraWidth() / 2f, last_button_x + Util.getCircleButtonSize() + Util.getCircleButtonSpace() / 4f - this.getButtonCameraWidth() / 2f);
        this.buttonCamera.translate(distX, 0);
        this.buttonCamera.update();
        // Out of borders ?
        if (this.buttonCamera.position.x < border_left) {
            this.buttonCamera.translate(border_left - this.buttonCamera.position.x, 0);
        }
        if (this.buttonCamera.position.x > border_right) {
            this.buttonCamera.translate(border_right - this.buttonCamera.position.x, 0);
        }
        this.buttonCamera.update();
    }

    public void resetButtonCamera() {
        this.buttonCamera.position.x = getButtonCameraWidth() / 2f;
        this.buttonCamera.update();
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(25 / 255f, 25 / 255f, 25 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderWorld(delta);
        switch (this.game.getState()) {
            case RUNNING:
                renderHUD(delta);
                renderButton(delta);
                break;
            case IA_PLAYING:
                renderHUD(delta);
                break;
            case MENU:
                renderMenu(delta);
                renderButton(delta);
                break;
            case STAT:
                renderStat(delta);
                renderButton(delta);
                break;
            case INFO:
                if (this.game.getInfoUnit() != null) {
                    renderInfo(delta, this.game.getInfoUnit());
                    renderButton(delta);
                } else {
                    // Error, close info pop up
                    this.game.back();
                }
                break;
            case SWITCH:
                renderSwitch(delta);
                break;
            case END:
                renderEndGame(delta);
                renderButton(delta);
                break;
        }
        batch.setProjectionMatrix(menuCamera.combined);
    }

    public void renderWorld(float delta) {
        batch.setProjectionMatrix(worldCamera.combined);
        batch.begin();
        renderMap(batch, delta);
        renderSquare(batch, delta);
        renderUnit(batch, delta);
        batch.end();
    }

    public void renderSquare(SpriteBatch batch, float delta) {
        for (int row = this.game.getSquareLayer().length - 1; row >= 0; row--) {
            for (int col = this.game.getSquareLayer().length - 1; col >= 0; col--) {
                if (this.game.getSquareLayer()[row][col] != null) {
                    this.game.getSquareLayer()[row][col].render(batch, delta, assets);
                }
            }
        }

    }

    public void renderMap(SpriteBatch batch, float delta) {
        for (int row = this.game.getBuildingLayer().length - 1; row >= 0; row--) {
            for (int col = this.game.getBuildingLayer().length - 1; col >= 0; col--) {
                this.game.getMap().renderTile(batch, delta, row, col);
                if (this.game.getBuildingLayer()[row][col] != null) {
                    this.game.getBuildingLayer()[row][col].render(batch, delta, assets);
                }
            }
        }
    }

    public void renderUnit(SpriteBatch batch, float delta) {

        for (Player p : this.game.getPlayers()) {
            for (com.nauwstudio.dutywars_ww2.game.units.Unit u : p.getUnits()) {
                u.render(batch, delta, assets);
            }
        }

    }

    public void renderHUD(float delta) {

        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        renderTopScreen(batch, delta);
        if (this.game.isRobotPlaying()) {
            renderOpponentPlaying(delta, this.game.getCurrentPlayer());
        } else {
            // Drawing selected unit
            if (this.game.getSelectedUnit() != null) {
                renderSelectedUnit(batch, delta, this.game.getSelectedUnit());
            }
            // Drawing selected building
            else if (this.game.getSelectedBuilding() != null) {
                renderSelectedBuilding(batch, delta, this.game.getSelectedBuilding());
            }
            // Drawing selected tile
            else if (this.game.getSelectedTile() != null) {
                renderSelectedTile(batch, delta, this.game.getSelectedTile());
            }
        }
        batch.end();
    }

    public void renderButton(float delta) {
        batch.setProjectionMatrix(buttonCamera.combined);
        batch.begin();
        for (ButtonCircle b : this.game.getBottomButtons()) {
            b.render(batch, this.mainAssets.font_medium);
        }
        batch.end();
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        for (Button b : this.game.getFixedButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    public void renderTopScreen(SpriteBatch batch, float delta) {
        if (this.game.getCurrentPlayer() != null) {
            // Drawing top bg
            batch.draw(this.game.getCurrentPlayer().getArmy().getBannerTexture(), hudVars.top_x, hudVars.top_y, hudVars.top_width, hudVars.top_height);
            RenderUtil.drawTextureInCell(batch, this.game.getCurrentPlayer().getArmy().getLogoTexture(), hudVars.top_x, hudVars.top_y, hudVars.top_height, hudVars.top_height, hudVars.top_height);
            // Draw money on top middle
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getCurrentPlayer().getMoney() + " $", hudVars.top_x, hudVars.top_y, hudVars.top_width, hudVars.top_height);
            // Draw turn on top right
            RenderUtil.drawTextInCellRight(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.day") + " " + this.game.getDay() + " ", hudVars.top_x, hudVars.top_y, hudVars.top_width, hudVars.top_height);
        }
    }

    public void renderSelectedTile(SpriteBatch batch, float delta, com.nauwstudio.dutywars_ww2.game.Tile tile) {
        // Drawing bottom bg
        batch.draw(this.game.getAssets().banner, 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f);
        // Drawing selected building
        batch.draw(tile.getTexture(), Util.getCircleButtonSpace() / 2f, Util.getCircleButtonSpace() / 2f, tile.getWidth(), tile.getWidth() * tile.getTextureRatio());
        // Name
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get(tile.getName()), hudVars.bottom_x, hudVars.bottom_tab_y + 2 * hudVars.bottom_tab_height, hudVars.bottom_width, hudVars.bottom_tab_height);
        // Tile defence (left)
        RenderUtil.drawTextureInCell(batch, assets.menu_shield, hudVars.bottom_tab_x, hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + tile.getDefence(), hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
    }

    public void renderSelectedBuilding(SpriteBatch batch, float delta, com.nauwstudio.dutywars_ww2.game.buildings.Building building) {
        float h = 0;
        if (building.canCreateUnit() && building.getOwner().equals(this.game.getCurrentPlayer())) {
            h = Util.getButtonsMenuHeight();
        }
        // Drawing bottom bg
        if (building.getOwner().getArmy().isGAI()) {
            batch.draw(this.game.getAssets().banner, 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f + h);

        } else {
            batch.draw(building.getOwner().getArmy().getBannerTexture(), 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f + h);
        }
        // Drawing selected building
        batch.draw(building.getTexture(), Util.getCircleButtonSpace() / 2f, h + (Util.getCircleButtonSpace() / 2f), building.getWidth(), building.getWidth() * building.getTextureRatio());
        // Name
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get(building.getName()), hudVars.bottom_x, h + hudVars.bottom_tab_y + 2 * hudVars.bottom_tab_height, hudVars.bottom_width, hudVars.bottom_tab_height);
        // Building defence (left)
        RenderUtil.drawTextureInCell(batch, assets.menu_shield, hudVars.bottom_tab_x, h + hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + building.getDefence(), hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon, h + hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
        // Building money (right)
        RenderUtil.drawTextureInCell(batch, assets.menu_dollar, hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + building.getMoney(), hudVars.bottom_tab_x + 2 * hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y + 0.5f * hudVars.bottom_tab_height, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
    }

    public void renderSelectedUnit(SpriteBatch batch, float delta, com.nauwstudio.dutywars_ww2.game.units.Unit unit) {
        float h = Util.getButtonsMenuHeight();
        // Drawing bottom bg
        batch.draw(unit.getOwner().getArmy().getBannerTexture(), 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f + h);
        // Drawing selected unit
        batch.draw(unit.getTexture(), Util.getCircleButtonSpace() / 2f, h + (Util.getCircleButtonSpace() / 2f), unit.getWidth(), unit.getWidth() * unit.getTextureRatio());
        // Drawing tab info
        // Name
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get(unit.getName()), hudVars.bottom_x, h + hudVars.bottom_tab_y + 2 * hudVars.bottom_tab_height, hudVars.bottom_width, hudVars.bottom_tab_height);
        // Unit defence (top-left)
        RenderUtil.drawTextureInCell(batch, assets.menu_shield, hudVars.bottom_tab_x, h + hudVars.bottom_tab_y + hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + unit.getDefence(), hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon, h + hudVars.bottom_tab_y + hudVars.bottom_tab_height, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
        // Unit life (top-right)
        RenderUtil.drawTextureInCell(batch, assets.menu_life, hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y + hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + unit.getLife(), hudVars.bottom_tab_x + 2 * hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y + hudVars.bottom_tab_height, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
        // Unit ammo (bottom-left)
        RenderUtil.drawTextureInCell(batch, assets.menu_ammo, hudVars.bottom_tab_x, h + hudVars.bottom_tab_y, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + unit.getAmmo(), hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon, h + hudVars.bottom_tab_y, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
        // Unit gas (bottom-right)
        RenderUtil.drawTextureInCell(batch, assets.menu_gas, hudVars.bottom_tab_x + hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y, hudVars.bottom_tab_cell_icon, hudVars.bottom_tab_height, hudVars.bottom_tab_cell_icon);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, "" + unit.getGas(), hudVars.bottom_tab_x + 2 * hudVars.bottom_tab_cell_icon + hudVars.bottom_tab_cell_text, h + hudVars.bottom_tab_y, hudVars.bottom_tab_cell_text, hudVars.bottom_tab_height);
    }

    public void renderMenu(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        batch.draw(assets.menu_bg, hudVars.bg_x, hudVars.bg_y, hudVars.bg_width, this.hudVars.bg_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_giant, this.mainAssets.stringBundle.get("game.menu"), hudVars.bg_title_x, hudVars.bg_title_y, hudVars.bg_title_width, hudVars.bg_title_height);
        // Draw sounds title
        RenderUtil.drawTextInCellLeft(batch, this.mainAssets.font_bigbig, this.mainAssets.stringBundle.get("settings.audio"), this.hudVars.sound_title_x, this.hudVars.sound_title_y, this.hudVars.sound_title_width, this.hudVars.sound_title_height);
        for (CheckBox c : this.game.getCheckBoxes()) {
            c.render(batch, this.mainAssets.font_big, this.mainAssets.font_big_gray);
        }
        // Draw speeds title
        RenderUtil.drawTextInCellLeft(batch, this.mainAssets.font_bigbig, this.mainAssets.stringBundle.get("settings.speed"), this.hudVars.speed_title_x, this.hudVars.speed_title_y, this.hudVars.speed_title_width, this.hudVars.speed_title_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get("settings.speed_user"), this.hudVars.speed_subtitle_x, this.hudVars.speed_subtitle_y, this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get("settings.speed_robot"), this.hudVars.speed_subtitle_x + this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_y, this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_height);
        for (RadioGroup c : this.game.getRadioGroups()) {
            c.render(batch, this.mainAssets.font_big, this.mainAssets.font_big_gray);
        }
        batch.end();
    }

    public void renderStat(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.menu_bg, hudVars.bg_x, hudVars.bg_y, hudVars.bg_width, hudVars.bg_height);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_giant, this.mainAssets.stringBundle.get("game.stats"), hudVars.bg_title_x, hudVars.bg_title_y, hudVars.bg_title_width, hudVars.bg_title_height);
        // Draw content
        if (Util.isVertical()) {
            renderStatVertical(batch, delta);
        } else {
            renderStatHorizontal(batch, delta);
        }
        batch.end();
    }

    private void renderStatVertical(SpriteBatch batch, float delta) {
        // Draw row headers
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.team"), hudVars.bg_x, hudVars.bg_y + 6 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().capital, hudVars.bg_x, hudVars.bg_y + 5 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().city, hudVars.bg_x, hudVars.bg_y + 4 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().factory, hudVars.bg_x, hudVars.bg_y + 3 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().airport, hudVars.bg_x, hudVars.bg_y + 2 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().shipyard, hudVars.bg_x, hudVars.bg_y + 1 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.money"), hudVars.bg_x, hudVars.bg_y + 0 * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height);
        // Draw col headers
        for (int i = 0; i < this.game.getPlayers().size(); i++) {
            RenderUtil.drawTextureInCell(this.batch, this.game.getPlayers().get(i).getArmy().getIconTexture(), hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 7 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
            RenderUtil.drawTextureInCell(batch, this.game.getPlayers().get(i).getTexture(this.mainAssets), hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width + hudVars.stat_cell_width / 2, hudVars.bg_y + 7 * hudVars.stat_cell_height + hudVars.stat_cell_height / 2f, hudVars.stat_icon_size / 2f, hudVars.stat_col_header_height / 2f, hudVars.stat_icon_size / 2f);
        }
        // Draw cell content
        for (int i = 0; i < this.game.getPlayers().size(); i++) {
            // Col content
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getTeam() + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 6 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getBuildingsAmount()[Building.CAPITAL - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 5 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getBuildingsAmount()[Building.CITY - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 4 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getBuildingsAmount()[Building.FACTORY - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 3 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getBuildingsAmount()[Building.AIRPORT - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 2 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getBuildingsAmount()[Building.SHIPYARD - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 1 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get(i).getMoney() + "", hudVars.bg_x + hudVars.stat_row_header_width + i * hudVars.stat_cell_width, hudVars.bg_y + 0 * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
        }
    }

    private void renderStatHorizontal(SpriteBatch batch, float delta) {
        // Draw row headers
        for (int i = 0; i < this.game.getPlayers().size(); i++) {
            RenderUtil.drawTextureInCell(this.batch, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getArmy().getIconTexture(), hudVars.bg_x, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_row_header_width, hudVars.stat_cell_height, hudVars.stat_icon_size);
            RenderUtil.drawTextureInCell(batch, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getTexture(this.mainAssets), hudVars.bg_x + hudVars.stat_cell_width / 2, hudVars.bg_y + i * hudVars.stat_cell_height + hudVars.stat_cell_height / 2f, hudVars.stat_icon_size / 2f, hudVars.stat_col_header_height / 2f, hudVars.stat_icon_size / 2f);
        }
        // Draw col headers
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.team"), hudVars.bg_x + hudVars.stat_row_header_width + 0 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().capital, hudVars.bg_x + hudVars.stat_row_header_width + 1 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().city, hudVars.bg_x + hudVars.stat_row_header_width + 2 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().factory, hudVars.bg_x + hudVars.stat_row_header_width + 3 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().airport, hudVars.bg_x + hudVars.stat_row_header_width + 4 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
        RenderUtil.drawTextureInCell(batch, this.game.getGaiaPlayer().getArmy().getAssets().shipyard, hudVars.bg_x + hudVars.stat_row_header_width + 5 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height, hudVars.stat_icon_size);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.money"), hudVars.bg_x + hudVars.stat_row_header_width + 6 * hudVars.stat_cell_width, hudVars.bg_title_y - hudVars.stat_col_header_height, hudVars.stat_cell_width, hudVars.stat_col_header_height);
        // Draw cell content
        for (int i = 0; i < this.game.getPlayers().size(); i++) {
            // Col content
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getTeam() + "", hudVars.bg_x + hudVars.stat_row_header_width + 0 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getBuildingsAmount()[com.nauwstudio.dutywars_ww2.game.buildings.Building.CAPITAL - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + 1 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getBuildingsAmount()[com.nauwstudio.dutywars_ww2.game.buildings.Building.CITY - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + 2 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getBuildingsAmount()[com.nauwstudio.dutywars_ww2.game.buildings.Building.FACTORY - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + 3 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getBuildingsAmount()[com.nauwstudio.dutywars_ww2.game.buildings.Building.AIRPORT - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + 4 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getBuildingsAmount()[com.nauwstudio.dutywars_ww2.game.buildings.Building.SHIPYARD - 1] + "", hudVars.bg_x + hudVars.stat_row_header_width + 5 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.game.getPlayers().get((this.game.getPlayers().size() - 1) - i).getMoney() + "", hudVars.bg_x + hudVars.stat_row_header_width + 6 * hudVars.stat_cell_width, hudVars.bg_y + i * hudVars.stat_cell_height, hudVars.stat_cell_width, hudVars.stat_cell_height);
        }
    }


    public void renderInfo(float delta, Unit unit) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.menu_bg, hudVars.bg_x, hudVars.bg_y, hudVars.bg_width, hudVars.bg_height);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get(unit.getName()), hudVars.bg_title_x, hudVars.bg_title_y, hudVars.bg_title_width, hudVars.bg_title_height);
        // Draw texture
        RenderUtil.drawTextureInCell(batch, unit.getTexture(), hudVars.info_map_x, hudVars.info_map_y, hudVars.info_map_width, hudVars.info_map_height, hudVars.info_map_icon_size);
        // Draw move and attack scopes
        RenderUtil.drawTextureInCell(batch, assets.unit_move_scope, hudVars.info_scope_x, hudVars.info_scope_y, hudVars.info_scope_cell_size, hudVars.info_scope_height, hudVars.info_scope_icon_size);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, unit.getMoveScope() + "", hudVars.info_scope_x + 1 * hudVars.info_scope_cell_size, hudVars.info_scope_y, hudVars.info_scope_cell_size, hudVars.info_scope_height);
        RenderUtil.drawTextureInCell(batch, assets.unit_attack_scope, hudVars.info_scope_x + 2 * hudVars.info_scope_cell_size, hudVars.info_scope_y, hudVars.info_scope_cell_size, hudVars.info_scope_height, hudVars.info_scope_icon_size);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, unit.getAttackScope()[0] + " - " + unit.getAttackScope()[1], hudVars.info_scope_x + 3 * hudVars.info_scope_cell_size, hudVars.info_scope_y, hudVars.info_scope_cell_size, hudVars.info_scope_height);
        // Draw type
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.type"), hudVars.info_type_x, hudVars.info_type_y, 3 * hudVars.info_type_cell_size, hudVars.info_type_height);
        RenderUtil.drawTextureInCell(batch, unit.getTypeTexture(assets), hudVars.info_type_x + 3 * hudVars.info_type_cell_size, hudVars.info_type_y, hudVars.info_type_cell_size, hudVars.info_type_height, hudVars.info_type_icon_size);
        // Draw attack
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.attack"), hudVars.info_attack_x, hudVars.info_attack_y, 3 * hudVars.info_attack_cell_size, hudVars.info_attack_height);
        for (int i = 0; i < unit.getCanAttackTypeTexture(assets).length; i++) {
            RenderUtil.drawTextureInCell(batch, unit.getCanAttackTypeTexture(assets)[i], hudVars.info_attack_x + (3 + i) * hudVars.info_attack_cell_size, hudVars.info_attack_y, hudVars.info_attack_cell_size, hudVars.info_attack_height, hudVars.info_attack_icon_size);
        }
        // Draw description
        this.mainAssets.font_small.draw(batch, this.mainAssets.stringBundle.get(unit.getDescription()), this.hudVars.info_description_x, this.hudVars.info_description_y + this.hudVars.info_description_height, this.hudVars.info_description_width, Align.topLeft, true);
        batch.end();
    }

    public void renderSwitch(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Drawing banner
        batch.draw(this.game.getCurrentPlayer().getArmy().getBannerTexture(), hudVars.switch_x, hudVars.switch_y, hudVars.switch_width, hudVars.switch_height);
        RenderUtil.drawTextureInCell(batch, this.game.getCurrentPlayer().getArmy().getLogoTexture(), hudVars.switch_x, hudVars.switch_y, hudVars.switch_height, hudVars.switch_height, hudVars.switch_height);
        // Draw day on middle
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_giant, this.mainAssets.stringBundle.get("game.day") + " " + this.game.getDay(), hudVars.switch_x + hudVars.switch_height, hudVars.switch_y, hudVars.switch_width - hudVars.switch_height, hudVars.switch_height);
        batch.end();
    }

    public void renderEndGame(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        batch.draw(assets.menu_bg, hudVars.end_x, hudVars.end_y, hudVars.end_width, this.hudVars.end_height);
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get("game.end"), hudVars.end_title_x, hudVars.end_title_y, hudVars.end_title_width, hudVars.end_title_height);
        for (int i = 0; i < this.game.getWinners().size(); i++) {
            RenderUtil.drawTextureInCell(batch, this.game.getWinners().get(i).getArmy().getIconTexture(), hudVars.end_winners_tab_x + i * hudVars.end_winners_tab_width / this.game.getWinners().size(), hudVars.end_winners_tab_y, hudVars.end_winners_tab_width / this.game.getWinners().size(), hudVars.end_winners_tab_height, hudVars.end_winners_tab_icon_size);
        }
        if (this.game.getWinners().size() > 1) {
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.end_text_multi"), hudVars.end_text_x, hudVars.end_text_y, hudVars.end_text_width, hudVars.end_text_height);
        } else {
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.end_text_solo"), hudVars.end_text_x, hudVars.end_text_y, hudVars.end_text_width, hudVars.end_text_height);
        }
        // Render score if campaign mode
        if (this.game.isCampaignMode()) {
            // Draw best score
            RenderUtil.drawTextInCell(batch, this.mainAssets.font_medium, this.mainAssets.stringBundle.get("game.score") + " " + game.getScore(), this.hudVars.end_score_x, this.hudVars.end_score_y, this.hudVars.end_score_width / 2f, this.hudVars.end_score_height);
            // RENDER STARS
            for (int i = 0; i < 3; i++) {
                if (i < this.game.getShowingStar()) {
                    RenderUtil.drawTextureInCell(batch, assets.star_full, this.hudVars.end_score_x + this.hudVars.end_score_width / 2f + i * this.hudVars.end_score_width / 6f, this.hudVars.end_score_y, this.hudVars.end_score_width / 6f, this.hudVars.end_score_height, this.hudVars.end_score_icon_size);
                }
            }
        }
        batch.end();
    }

    public void renderOpponentPlaying(float delta, Player player) {
        // Drawing army banner
        batch.draw(this.game.getAssets().banner, 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f);
        // Drawing army logo
        RenderUtil.drawTextureInCell(batch, player.getArmy().getIconTexture(), 0, 0, Util.getCircleButtonSize() * 1.5f, Util.getCircleButtonSize() * 1.5f, Util.getCircleButtonSize());
        // Name
        RenderUtil.drawTextInCell(batch, this.mainAssets.font_big, this.mainAssets.stringBundle.get("game.robot_playing"), 0, 0, Util.getScreenWidth(), Util.getCircleButtonSize() * 1.5f);
    }
}