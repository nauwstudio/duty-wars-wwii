package com.nauwstudio.dutywars_ww2.game.buildings;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class City extends Building{

	public static final String NAME = "building.city";


	public City(int row, int col, Player owner) {
		super(row, col, owner, CITY, CITY_MONEY, CITY_DEFENCE);
		this.textureRatio = getTexture().getRegionHeight() / (float)getTexture().getRegionWidth();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().city;
	}

	public String getName() {
		return NAME;
	}
	
	@Override
	public boolean isCity() {
		return true;
	}
}
