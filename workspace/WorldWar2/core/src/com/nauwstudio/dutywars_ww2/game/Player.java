package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;

public class Player {

    public static final int HUMAN = 1;
    public static final int ROBOT = 2;

    public static final int ALIVE = 1;
    public static final int DEAD = 2;

    public static final int GAIA_ORDER = 0;

    protected Army army;
    protected int team;
    protected int money;
    protected int order;
    protected ArrayList<Unit> units;
    protected ArrayList<Building> buildings;
    protected int status;
    protected boolean onlyArmyPlayer;

    public Player(Army army) {
        this.army = army;
    }

    public Player(Army army, int team, int money, int order, int status) {
        this.army = army;
        this.team = team;
        this.money = money;
        this.order = order;
        this.units = new ArrayList<Unit>();
        this.buildings = new ArrayList<Building>();
        this.status = status;
    }

    public ArrayList<Unit> getUnits() {
        return this.units;
    }

    public void addUnit(Unit unit) {
        this.units.add(unit);
    }

    public void removeUnit(Unit unit) {
        this.units.remove(unit);
    }

    public ArrayList<Building> getBuildings() {
        return this.buildings;
    }

    public void addBuilding(Building buidling) {
        this.buildings.add(buidling);
    }

    public void removeBuilding(Building building) {
        this.buildings.remove(building);
    }

    public boolean haveCapital() {
        for (Building building : this.getBuildings()) {
            if (building.isCapital()) {
                return true;
            }
        }
        return false;
    }

    public Vector2 getCapitalPosition() {
        Vector2 result = null;
        for (Building building : this.getBuildings()) {
            if (building.isCapital()) {
                result = building.getPosition();
            }
        }
        if(result == null) {
            if (this.buildings == null || this.buildings.isEmpty() && !this.units.isEmpty()) {
                result = this.getUnits().get(this.units.size()-1).getPosition();
            } else if(!this.buildings.isEmpty()) {
                result = this.getBuildings().get(this.buildings.size()-1).getPosition();
            }
        }
        return result;
    }

    public boolean haveUnit() {
        return !this.units.isEmpty();
    }

    public void addMoney(int amount) {
        this.money += amount;
    }

    public void removeMoney(int amount) {
        this.money -= amount;
    }

    public int getMoney() {
        return this.money;
    }

    public int getMoneyByTurn() {
        int moneyByTurn = 0;
        for (Building building : this.getBuildings()) {
            moneyByTurn += building.getMoney();
        }
        return moneyByTurn;
    }

    public Army getArmy() {
        return this.army;
    }

    public int getOrder() {
        return this.order;
    }

    public void play(Game game) {
    }

    public boolean isHuman() {
        return true;
    }

    public int getType() {
        return HUMAN;
    }

    public int getTeam() {
        return this.team;
    }

    public boolean isAllied(Player player) {
        return this.team == player.team;
    }

    public void die() {
        this.units = new ArrayList<Unit>();
        this.buildings = new ArrayList<Building>();
        this.status = DEAD;
    }

    public boolean isAlive() {
        return this.status == ALIVE;
    }

    public int[] getBuildingsAmount() {
        int[] amount = new int[]{0, 0, 0, 0, 0};
        for (Building building : this.buildings) {
            amount[building.getType() - 1] = amount[building.getType() - 1] + 1;
        }
        return amount;
    }

    public void setOnlyArmyPlayer(boolean onlyArmyPlayer) {
        this.onlyArmyPlayer = onlyArmyPlayer;
    }

    public boolean isOnlyArmyPlayer() {
        return this.onlyArmyPlayer;
    }

    public TextureRegion getTexture(MainAssets assets) {
        if (this.isHuman()) {
            if (isAlive()) {
                return assets.human_alive;
            } else {
                return assets.human_dead;
            }
        } else {
            if (isAlive()) {
                return assets.robot_alive;
            } else {
                return assets.robot_dead;
            }
        }
    }

    public int getTruckNumber() {
        int amount = 0;
        for (Unit unit : this.units) {
            if (unit.getType() == Unit.TRUCK) {
                amount++;
            }
        }
        return amount;
    }

    public int getTransporterNumber() {
        int amount = 0;
        for (Unit unit : this.units) {
            if (unit.getType() == Unit.TRANSPORTER) {
                amount++;
            }
        }
        return amount;
    }

    public int getLanderNumber() {
        int amount = 0;
        for (Unit unit : this.units) {
            if (unit.getType() == Unit.LANDER) {
                amount++;
            }
        }
        return amount;
    }

    public int getAircraftCarrierNumber() {
        int amount = 0;
        for (Unit unit : this.units) {
            if (unit.getType() == Unit.AIRCRAFT_CARRIER) {
                amount++;
            }
        }
        return amount;
    }

    public int getAntiAirUnitAmount() {
        int antiAirUnitAmount = 0;
        for (Unit unit : this.units) {
            if (unit.isAntiAirUnit()) {
                antiAirUnitAmount++;
            }
        }
        return antiAirUnitAmount;
    }

    public int getAirUnitAmount() {
        int airUnitAmount = 0;
        for (Unit unit : this.units) {
            if (unit.isAirUnit()) {
                airUnitAmount ++;
            }
        }
        return airUnitAmount;
    }

    public int getUnitAmount(int unitType) {
        int unitAmount = 0;
        for (Unit unit : this.units) {
            if (unit.getType() == unitType) {
                unitAmount ++;
            }
        }
        return unitAmount;
    }

    @Override
    public String toString() {
        String army = "";
        switch (this.army.getType()) {
            case Army.USA:
                army = "USA";
                break;
            case Army.GER:
                army = "GER";
                break;
            case Army.USS:
                army = "USS";
                break;
            case Army.JAP:
                army = "JAP";
                break;
            case Army.GBR:
                army = "GBR";
                break;
        }

        return "Player " + army + " (" + this.getOrder() + ")";
    }
}
