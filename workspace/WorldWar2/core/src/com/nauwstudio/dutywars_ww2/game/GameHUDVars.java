package com.nauwstudio.dutywars_ww2.game;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class GameHUDVars {

    private Game game;

    // BUTTONS HUD
    float main_line, main3_col1, main3_col2, main3_col3, main2_col1, main2_col2, main4_col1, main4_col2, main4_col3, main4_col4;
    float create_line, create_col1, create_col2, create_col3, create_col4, create_col5, create_col6, create_col7, create_col8, create_col9;

    // CLOSE TILE BUTTON POSITION
    float close_tile_x;
    float close_tile_y;

    // CLOSE UNIT BUTTON POSITION
    float close_unit_x;
    float close_unit_y;

    // CLOSE MENU BUTTON POSITION
    float close_menu_x;
    float close_menu_y;

    // COMMON HUD
    public float bg_x, bg_y, bg_width, bg_height;
    public float bg_title_x, bg_title_y, bg_title_width, bg_title_height;

    // TOP HUD
    public float top_x, top_y, top_width, top_height;

    // BOTTOM HUD
    public float bottom_x, bottom_y, bottom_width, bottom_height;
    public float bottom_icon_size, bottom_tab_x, bottom_tab_width, bottom_tab_height, bottom_tab_y, bottom_tab_cell_icon, bottom_tab_cell_text;

    // SWITCH HUD
    public float switch_x, switch_y, switch_width, switch_height;

    // MENU HUD
    public float menu_exit_x, menu_exit_y;

    // STAT HUD
    public float stat_cell_width, stat_cell_height, stat_row_header_width, stat_col_header_height, stat_icon_size;

    // SETTINGS HUD
    // SOUNDS CHECK BOXES
    float sound_title_x, sound_title_y, sound_title_width, sound_title_height;
    float music_checkbox_x, music_checkbox_y, music_checkbox_width, music_checkbox_height;
    float effects_checkbox_x, effects_checkbox_y, effects_checkbox_width, effects_checkbox_height;
    // SPEEDS RADIO BUTTONS
    float speed_title_x, speed_title_y, speed_title_width, speed_title_height;
    float speed_subtitle_x, speed_subtitle_y, speed_subtitle_width, speed_subtitle_height;
    float speed_user_x, speed_user_y, speed_user_width, speed_user_height;
    float speed_robot_x, speed_robot_y, speed_robot_width, speed_robot_height;
    float menu_help_x, menu_help_y;

    // INFOS HUD
    public float info_map_x, info_map_y, info_map_width, info_map_height, info_map_icon_size;
    public float info_scope_x, info_scope_y, info_scope_width, info_scope_height, info_scope_cell_size, info_scope_icon_size;
    public float info_type_x, info_type_y, info_type_width, info_type_height, info_type_cell_size, info_type_icon_size;
    public float info_attack_x, info_attack_y, info_attack_width, info_attack_height, info_attack_cell_size, info_attack_icon_size;
    public float info_description_x, info_description_y, info_description_width, info_description_height;

    // END GAME HUD
    public float end_x, end_y, end_width, end_height;
    public float end_title_x, end_title_y, end_title_width, end_title_height;
    public float end_winners_tab_x, end_winners_tab_y, end_winners_tab_width, end_winners_tab_height, end_winners_tab_icon_size;
    public float end_text_x, end_text_y, end_text_width, end_text_height;
    public float end_score_x, end_score_y, end_score_width, end_score_height, end_score_icon_size;
    public float button_finish_game_x, button_finish_game_y;

    public GameHUDVars(Game game) {
        this.game = game;
        initHUDVariables();
    }

    public void initHUDVariables() {
        // CLOSE TILE BUTTONS POSITIONS
        close_tile_x = Util.getScreenWidth() - 0.7f * Util.getCircleButtonSize();
        close_tile_y = 1.5f * Util.getCircleButtonSize() - 0.7f * Util.getCircleButtonSize();
        // CLOSE UNIT BUTTONS POSITIONS
        close_unit_x = Util.getScreenWidth() - 0.7f * Util.getCircleButtonSize();
        close_unit_y = Util.getButtonsMenuHeight() + 1.5f * Util.getCircleButtonSize() - 0.7f * Util.getCircleButtonSize();
        // CLOSE MENU BUTTONS POSITIONS
        close_menu_x = Util.getScreenWidth() - 0.7f * Util.getCircleButtonSize();
        close_menu_y = Util.getScreenHeight() - 0.7f * Util.getCircleButtonSize();
        //
        this.bg_x = 0;
        this.bg_width = Util.getScreenWidth();
        this.bg_height = Util.getScreenHeight();
        this.bg_y = 0;
        if(Util.isVertical()) {
            // Title
            this.bg_title_x = this.bg_x;
            this.bg_title_width = this.bg_width;
            this.bg_title_height = this.bg_height/9f;
            this.bg_title_y = this.bg_y + this.bg_height - this.bg_title_height;
        } else {
            // Title
            this.bg_title_x = this.bg_x;
            this.bg_title_width = this.bg_width;
            this.bg_title_height = this.bg_height/6f;
            this.bg_title_y = this.bg_y + this.bg_height - this.bg_title_height;
        }
        initTopVars();
        initBottomVars();
        initButtonVars();
        initSwitchVars();
        initMenuVars();
        initStatVars();
        initInfoVars();
        initEndGameVars();
    }

    private void initTopVars() {
        this.top_x = this.bg_x;
        this.top_width = this.bg_width;
        this.top_height = Util.getCircleButtonSpace();
        this.top_y = Util.getScreenHeight() - this.top_height;
    }

    private void initBottomVars() {
        this.bottom_x = this.bg_x;
        this.bottom_width = this.bg_width;
        this.bottom_height = Util.getCircleButtonSize() * 1.5f;
        this.bottom_y = this.bg_y;
        this.bottom_icon_size = Util.getCircleButtonSpace() + Util.getTileWidth();
        this.bottom_tab_x = this.bottom_x + this.bottom_icon_size;
        this.bottom_tab_width = close_tile_x - this.bottom_icon_size;
        this.bottom_tab_height = this.bottom_height/3f;
        this.bottom_tab_y = this.bg_y;
        this.bottom_tab_cell_icon = this.bottom_tab_height;
        this.bottom_tab_cell_text = (this.bottom_tab_width - (this.bottom_tab_cell_icon * 2)) / 2f;
    }

    private void initButtonVars() {
        // MAIN BUTTONS POSITIONS
        main_line = Util.getCircleButtonSpace() / 4f;
        main3_col2 = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f;
        main3_col1 = main3_col2 - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
        main3_col3 = main3_col2 + Util.getCircleButtonSpace() + Util.getCircleButtonSize();
        main2_col1 = Util.getScreenWidth() / 2f - Util.getCircleButtonSpace() / 2f - Util.getCircleButtonSize();
        main2_col2 = Util.getScreenWidth() / 2f + Util.getCircleButtonSpace() / 2f;
        main4_col2 = Util.getScreenWidth() / 2f - Util.getCircleButtonSpace() / 2f - Util.getCircleButtonSize();
        main4_col3 = Util.getScreenWidth() / 2f + Util.getCircleButtonSpace() / 2f;
        main4_col1 = main4_col2 - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
        main4_col4 = main4_col3 + Util.getCircleButtonSpace() + Util.getCircleButtonSize();

        // CREATE BUTTONS POSITIONS
        create_line = Util.getCircleButtonSpace() / 4f;
        create_col1 = Util.getCircleButtonSpace() / 4f;
        create_col2 = create_col1 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col3 = create_col2 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col4 = create_col3 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col5 = create_col4 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col6 = create_col5 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col7 = create_col6 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col8 = create_col7 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
        create_col9 = create_col8 + Util.getCircleButtonSize() + Util.getCircleButtonSpace();
    }

    private void initSwitchVars() {
        this.switch_x = this.bg_x;
        this.switch_width = this.bg_width;
        this.switch_height = Util.getCircleButtonSize()*3;
        this.switch_y = Util.getScreenHeight()/2f-this.switch_height/2f;
    }

    private void initMenuVars() {
        float button_ratio = this.game.getMainAssets().button_exit_game.getRegionWidth() / (float) this.game.getMainAssets().button_exit_game.getRegionHeight();
        float divider = 5.0f;
        if (Util.isVertical()) {
            divider = 8.0f;
            // AUDIO
            this.sound_title_x = Util.getCircleButtonSize();
            this.sound_title_height = Util.getContentHeight() / divider;
            this.sound_title_width = Util.getScreenWidth();
            this.sound_title_y = this.bg_title_y - this.sound_title_height;
            // MUSIC CHECKBOX
            this.music_checkbox_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.music_checkbox_height = Util.getContentHeight() / (divider * 2);
            this.music_checkbox_x = Util.getCircleButtonSpace();
            this.music_checkbox_y = this.sound_title_y - this.music_checkbox_height;
            // EFFECTS CHECKBOX
            this.effects_checkbox_width = this.music_checkbox_width;
            this.effects_checkbox_height = this.music_checkbox_height;
            this.effects_checkbox_x = this.music_checkbox_x + this.music_checkbox_width + Util.getCircleButtonSpace();
            this.effects_checkbox_y = this.sound_title_y - this.effects_checkbox_height;
            // SPEED
            this.speed_title_x = Util.getCircleButtonSize();
            this.speed_title_height = Util.getContentHeight()/divider;
            this.speed_title_width = Util.getScreenWidth();
            this.speed_title_y = this.music_checkbox_y - this.speed_title_height;
            // subtitle
            this.speed_subtitle_x = 0;
            this.speed_subtitle_height = Util.getContentHeight() / (divider * 2);
            this.speed_subtitle_width = Util.getScreenWidth();
            this.speed_subtitle_y = this.speed_title_y - this.speed_subtitle_height;
            // USER SPEED RADIOGROUP
            this.speed_user_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_user_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_user_x = Util.getCircleButtonSpace();
            this.speed_user_y = this.speed_subtitle_y - this.speed_user_height;
            // ROBOT SPEED RADIOGROUP
            this.speed_robot_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_robot_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_robot_x = this.speed_user_x + this.speed_user_width + Util.getCircleButtonSpace();
            this.speed_robot_y = this.speed_subtitle_y - this.speed_robot_height;
        } else {
            // AUDIO
            this.sound_title_x = Util.getCircleButtonSpace();
            this.sound_title_height = Util.getContentHeight() / (divider*2f);
            this.sound_title_width = 2.5f * Util.getCircleButtonSize();
            this.sound_title_y = this.bg_title_y - this.sound_title_height;
            // MUSIC CHECKBOX
            this.music_checkbox_width = (Util.getScreenWidth() - this.sound_title_x - this.sound_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.music_checkbox_height = Util.getContentHeight() / (divider*2f);
            this.music_checkbox_x = this.sound_title_x + this.sound_title_width + Util.getCircleButtonSpace();
            this.music_checkbox_y = this.sound_title_y;
            // EFFECTS CHECKBOX
            this.effects_checkbox_width = this.music_checkbox_width;
            this.effects_checkbox_height = this.music_checkbox_height;
            this.effects_checkbox_x = this.music_checkbox_x + this.music_checkbox_width + Util.getCircleButtonSpace();
            this.effects_checkbox_y = this.music_checkbox_y ;
            // SPEED
            this.speed_title_x = Util.getCircleButtonSpace();
            this.speed_title_height = Util.getContentHeight() / (divider*2f);
            this.speed_title_width = 2.5f * Util.getCircleButtonSize();
            this.speed_title_y = this.sound_title_y - this.speed_title_height - Util.getCircleButtonSpace()/2f;
            // subtitle
            this.speed_subtitle_x = this.speed_title_x + this.speed_title_width + Util.getCircleButtonSpace();
            this.speed_subtitle_height = Util.getContentHeight() / (divider * 2f);
            this.speed_subtitle_width = (Util.getScreenWidth() -  Util.getCircleButtonSpace() - this.speed_subtitle_x);
            this.speed_subtitle_y = this.speed_title_y;
            // USER SPEED RADIOGROUP
            this.speed_user_width = (Util.getScreenWidth() - this.speed_title_x - this.speed_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_user_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_user_x = this.speed_title_x + this.speed_title_width + Util.getCircleButtonSpace();
            this.speed_user_y = this.speed_title_y - this.speed_user_height;
            // ROBOT SPEED RADIOGROUP
            this.speed_robot_width = (Util.getScreenWidth() - this.speed_title_x - this.speed_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_robot_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_robot_x = this.speed_user_x + this.speed_user_width + Util.getCircleButtonSpace();
            this.speed_robot_y = this.speed_subtitle_y - this.speed_robot_height;
        }
        // HELP BUTTON
        this.menu_help_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        this.menu_help_y = this.speed_user_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
        // EXIT GAME BUTTON POSITION
        this.menu_exit_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
        this.menu_exit_y = Util.getCircleButtonSpace() / 4f;
    }

    private void initStatVars() {
        if (Util.isVertical()) {
            this.stat_cell_height = (this.bg_height - this.bg_title_height)/8f;
            this.stat_cell_width = (this.bg_width - this.stat_cell_height) / (float) this.game.getPlayers().size();
            this.stat_row_header_width = this.bg_width / 6f;
            this.stat_col_header_height = this.stat_cell_height;
            this.stat_icon_size = 4*Math.min(this.stat_row_header_width, this.stat_col_header_height)/5f;
        }
        // Horizontal screen
        else {
            this.stat_cell_width = this.bg_width / 8f;
            this.stat_row_header_width = this.stat_cell_width;
            this.stat_col_header_height = (this.bg_height - this.bg_title_height)/6f;
            this.stat_cell_height = (this.bg_height - this.bg_title_height - this.stat_col_header_height) / (float) this.game.getPlayers().size();
            this.stat_icon_size = 4*Math.min(this.stat_row_header_width, this.stat_col_header_height)/5f;
        }
    }

    private void initInfoVars() {
        if(Util.isVertical()) {
            // Info map
            this.info_map_x = this.bg_x;
            this.info_map_width = this.bg_width;
            this.info_map_height = 3 * this.bg_title_height;
            this.info_map_y = this.bg_title_y - this.info_map_height;
            this.info_map_icon_size = 4*this.info_map_width/5f;
            // Info scope
            this.info_scope_x = this.bg_x + Util.getCircleButtonSpace()/4f;
            this.info_scope_width = this.bg_width - 2* this.info_scope_x;
            this.info_scope_height = this.bg_title_height;
            this.info_scope_y = this.info_map_y - this.info_scope_height;
            this.info_scope_cell_size = this.info_scope_width/4f;
            this.info_scope_icon_size = this.info_scope_cell_size;
            // Info type
            this.info_type_x = this.bg_x;
            this.info_type_width = this.bg_width;
            this.info_type_height = this.bg_title_height;
            this.info_type_y = this.info_scope_y - this.info_type_height;
            this.info_type_cell_size = this.info_type_width/8f;
            this.info_type_icon_size = 4*Math.min(this.info_type_height, this.info_type_cell_size)/5f;
            // Info attack
            this.info_attack_x = this.bg_x;
            this.info_attack_width = this.bg_width;
            this.info_attack_height = this.bg_title_height;
            this.info_attack_y = this.info_type_y - this.info_attack_height;
            this.info_attack_cell_size = this.info_attack_width/8f;
            this.info_attack_icon_size = 4*Math.min(this.info_attack_height, this.info_attack_cell_size)/5f;
            // Info description
            this.info_description_x = this.bg_x + Util.getCircleButtonSpace() / 4f;
            this.info_description_width = this.bg_width - Util.getCircleButtonSpace() / 2f;
            this.info_description_height = 2 * this.bg_title_height - Util.getCircleButtonSpace() / 2f;
            this.info_description_y = this.info_attack_y - this.info_description_height - Util.getCircleButtonSpace() / 4f;

        } else {
            // Info map
            this.info_map_x = this.bg_x;
            this.info_map_width = this.bg_width / 2f;
            this.info_map_height = 3 * this.bg_title_height;
            this.info_map_y = this.bg_title_y - this.info_map_height;
            this.info_map_icon_size = 4*this.info_map_width/5f;
            // Info scope
            this.info_scope_x = this.bg_x + Util.getCircleButtonSpace()/4f;
            this.info_scope_width = this.bg_width/2 - this.info_scope_x;
            this.info_scope_height = this.bg_title_height;
            this.info_scope_y = this.info_map_y - this.info_scope_height;
            this.info_scope_cell_size = this.info_scope_width/4f;
            this.info_scope_icon_size = this.info_scope_cell_size;
            // Info type
            this.info_type_x = this.bg_x + this.bg_width/2f;
            this.info_type_width = this.bg_width/2f;
            this.info_type_height = this.bg_title_height;
            this.info_type_y = this.bg_title_y - this.info_type_height;
            this.info_type_cell_size = this.info_type_width/8f;
            this.info_type_icon_size = 4*Math.min(this.info_type_height, this.info_type_cell_size)/5f;
            // Info attack
            this.info_attack_x = this.bg_x + this.bg_width/2f;
            this.info_attack_width = this.bg_width/2f;
            this.info_attack_height = this.bg_title_height;
            this.info_attack_y = this.info_type_y - this.info_attack_height;
            this.info_attack_cell_size = this.info_attack_width/8f;
            this.info_attack_icon_size = 4*Math.min(this.info_attack_height, this.info_attack_cell_size)/5f;
            // Info description
            this.info_description_x = this.bg_x + this.bg_width/2f + Util.getCircleButtonSpace() / 4f;
            this.info_description_width = this.bg_width/2f - Util.getCircleButtonSpace() / 2f;
            this.info_description_height = 2 * this.bg_title_height - Util.getCircleButtonSpace() / 2f;
            this.info_description_y = this.info_attack_y - this.info_description_height - Util.getCircleButtonSpace() / 4f;
        }
    }

    private void initEndGameVars() {
        this.end_width = Math.min(Util.getScreenHeight(), Util.getScreenWidth()) - Util.getCircleButtonSpace()/2f;
        this.end_height = this.end_width;
        this.end_x = Util.getScreenWidth()/2f - this.end_width/2f;
        this.end_y = Util.getScreenHeight()/2f - this.end_height/2f;
        this.end_title_width = this.end_width;
        this.end_title_height = this.end_height/5f;
        this.end_title_x = this.end_x;
        this.end_title_y = this.end_y + this.end_height - this.end_title_height;
        this.end_winners_tab_width = this.end_width;
        this.end_winners_tab_height = this.end_height/5f;;
        this.end_winners_tab_x = this.end_x;
        this.end_winners_tab_y = this.end_title_y - this.end_winners_tab_height;
        this.end_winners_tab_icon_size = this.end_winners_tab_width/4f;
        this.end_text_width = this.end_width;
        this.end_text_height = this.end_height/5f;;
        this.end_text_x = this.end_x;
        this.end_text_y = this.end_winners_tab_y - this.end_text_height;
        this.end_score_x = this.end_x;
        this.end_score_width = this.end_width;
        this.end_score_height = this.end_height/5f;
        this.end_score_y = this.end_text_y - this.end_score_height;
        this.end_score_icon_size = 4*Math.min(this.end_score_height,this.end_score_width/6f)/5f;

        // BUTTON
        float finish_game_ratio = this.game.getMainAssets().button_exit_game.getRegionWidth() / (float) this.game.getMainAssets().button_exit_game.getRegionHeight();
        this.button_finish_game_x = Util.getScreenWidth()/2f - (Util.getRectangleButtonHeight() * finish_game_ratio)/2f;
        this.button_finish_game_y = this.end_y + Util.getCircleButtonSpace() / 4f;
    }
}
