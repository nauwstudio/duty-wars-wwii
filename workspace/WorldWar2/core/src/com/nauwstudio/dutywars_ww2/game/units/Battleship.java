package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Battleship extends Unit {

	public static final String NAME = "battleship";
	public static final String DESC = "battleship_desc";

	public Battleship(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, BATTLESHIP, Kind.NAVAL, BATTLESHIP_PRICE, BATTLESHIP_MOVE_SCOPE, BATTLESHIP_ATTACK_SCOPE, LIFE, BATTLESHIP_AMMO, BATTLESHIP_GAS, BATTLESHIP_DMG, owner.getArmy().getGameAssets().battleship_fire, owner.getArmy().getGameAssets().boat_move);
		}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().battleship_S;
			case WEST:
				return this.owner.getArmy().getAssets().battleship_W;
			case NORTH:
				return this.owner.getArmy().getAssets().battleship_N;
			case EAST:
				return this.owner.getArmy().getAssets().battleship_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().battleship_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().battleship_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().battleship_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().battleship_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().battleship_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_battleship;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_battleship_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_battleship_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
