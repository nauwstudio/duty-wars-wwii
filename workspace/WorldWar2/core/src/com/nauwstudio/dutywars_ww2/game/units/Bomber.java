package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Bomber extends Unit {

	private static final String NAME_USA = "bomber.usa";
	private static final String NAME_GER = "bomber.ger";
	private static final String NAME_USS = "bomber.uss";
	private static final String NAME_JAP = "bomber.jap";
	private static final String NAME_GBR = "bomber.gbr";
	private static final String DESC_USA = "bomber_desc.usa";
	private static final String DESC_GER = "bomber_desc.ger";
	private static final String DESC_USS = "bomber_desc.uss";
	private static final String DESC_JAP = "bomber_desc.jap";
	private static final String DESC_GBR = "bomber_desc.gbr";

	public Bomber(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, BOMBER, Kind.AIR, BOMBER_PRICE, BOMBER_MOVE_SCOPE, BOMBER_ATTACK_SCOPE, LIFE, BOMBER_AMMO, BOMBER_GAS, BOMBER_DMG, owner.getArmy().getGameAssets().bomber_fire, owner.getArmy().getGameAssets().bomber_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().bomber_S;
			case WEST:
				return this.owner.getArmy().getAssets().bomber_W;
			case NORTH:
				return this.owner.getArmy().getAssets().bomber_N;
			case EAST:
				return this.owner.getArmy().getAssets().bomber_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bomber_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().bomber_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().bomber_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().bomber_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().bomber_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_bomber;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_bomber_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_bomber_disabled;
	}

	@Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return NAME_USA;
			case Army.GER:
				return NAME_GER;
			case Army.USS:
				return NAME_USS;
			case Army.JAP:
				return NAME_JAP;
			case Army.GBR:
				return NAME_GBR;
		}
		return "";
	}

	@Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}
}
