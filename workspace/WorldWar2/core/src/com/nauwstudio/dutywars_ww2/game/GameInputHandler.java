package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.GameScreen;
import com.nauwstudio.dutywars_ww2.Util;

public class GameInputHandler implements InputProcessor {

    GameScreen screen;
    Game game;
    GameRenderer renderer;

    public GameInputHandler(GameScreen screen) {
        this.screen = screen;
        this.game = screen.getGame();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.game.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (this.game.isRobotPlaying()) {
            return false;
        } else {
            // Reverse y-axis
            screenY = (int) Util.getScreenHeight() - screenY;
            // Touching button ?
            float button_x = this.renderer.getButtonCameraOriginX() + screenX;
            float button_y = this.renderer.getButtonCameraOriginY() + screenY;
            this.screen.touchedButton = this.game.getTouchedButton(button_x, button_y);
            if (this.screen.touchedButton != null) {
                this.screen.touchedButton.press();
                disableGesture();
                return true;
            } else {
                // Touching check box ?
                this.screen.touchedCheckBox = this.game.getTouchedCheckBox(button_x, button_y);
                if (this.screen.touchedCheckBox != null) {
                    this.screen.touchedCheckBox.press();
                    return true;
                } else {
                    // Touching radio button ?
                    this.screen.touchedRadioButton = this.game.getTouchedRadioButton(button_x, button_y);
                    if (this.screen.touchedRadioButton != null) {
                        this.screen.touchedRadioButton.press();
                        return true;
                    } else {
                        // Touching tile ?
                        float world_x = this.renderer.getCameraOriginX() + screenX * this.renderer.getCameraZoom();
                        float world_y = this.renderer.getCameraOriginY() + screenY * this.renderer.getCameraZoom();
                        this.screen.touchedSquare = this.game.getTouchedSquare(world_x, world_y);
                        if (screen.touchedSquare != null) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Perform button action ?
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.game.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            reset();
            return true;
        } else {
            // Perform check box action ?
            if (this.screen.touchedCheckBox != null) {
                this.game.touchCheckBox(this.screen.touchedCheckBox);
                this.screen.touchedCheckBox.release();
                this.screen.touchedCheckBox = null;
                return true;
            } else {
                // Perform radio button action ?
                if (this.screen.touchedRadioButton != null) {
                    this.game.touchRadioButton(this.screen.touchedRadioButton);
                    this.screen.touchedRadioButton.release();
                    this.screen.touchedRadioButton = null;
                    return true;
                } else {
                    // Perform selection action ?
                    if (this.game.isRunning() && this.screen.touchedSquare != null) {
                        float world_x = this.renderer.getCameraOriginX() + screenX * this.renderer.getCameraZoom();
                        float world_y = this.renderer.getCameraOriginY() + screenY * this.renderer.getCameraZoom();
                        // Perform select TILE action ?
                        if (this.screen.touchedSquare.isTouched(world_x, world_y)) {
                            this.game.touchSquare(screen.touchedSquare);
                        }
                        reset();
                        return true;
                    } else {
                        reset();
                        return false;
                    }
                }
            }
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount > 0) {
            this.renderer.zoomOut();
        } else {
            this.renderer.zoomIn();
        }
        return false;
    }

    public void disableGesture() {
        this.screen.can_pan = false;
        this.screen.can_pinch = false;
    }

    public void enableGesture() {
        this.screen.can_pan = true;
        this.screen.can_pinch = true;
    }

    public void reset() {
        enableGesture();
        this.screen.touchedButton = null;
        this.screen.touchedSquare = null;
    }
}
