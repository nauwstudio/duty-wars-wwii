package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.units.Unit;
import com.nauwstudio.dutywars_ww2.main.SaveDeleteButton;

public class UnitButton extends ButtonCircle {

    private Unit unit;
    private UnitInfoButton infoButton;

    private float info_x, info_y;

    public UnitButton(Vector2 position, float width, String tag, Unit unit, TextureRegion info, TextureRegion info_pressed) {
        super(position, width, tag, unit.getButtonTexture(), unit.getButtonPressedTexture(), unit.getButtonDisabledTexture());
        this.unit = unit;
        if(unit.getOwner().getMoney() < unit.getPrice()) {
            this.disable();
        }
        initHUDVars();
        createInfoButton(info, info_pressed);
    }

    private void initHUDVars() {
        // Delete buttons
        info_x = this.position.x + this.width - 3*Util.getTinyCircleButtonSize()/4f;
        info_y = this.position.y - Util.getTinyCircleButtonSize()/4f;
    }

    public void createInfoButton(TextureRegion info, TextureRegion info_pressed) {
        Vector2 info_pos = new Vector2(this.info_x, this.info_y);
        this.infoButton = new UnitInfoButton(info_pos, Util.getTinyCircleButtonSize(), info, info_pressed, this);
    }

    @Override
    public void render(SpriteBatch batch, BitmapFont font) {
        super.render(batch);
        // Render price
        RenderUtil.drawTextInCellLeft(batch, font, unit.getPrice() + " $", this.position.x, this.position.y + 3*this.height/4f, 5*this.width/4f, this.height/2f);
        // Render info button
        this.infoButton.render(batch);
    }

    @Override
    public boolean isUnitButton() {
        return true;
    }

    public Unit getUnit() {
        return this.unit;
    }

    public int getUnitType() {
        return this.unit.getType();
    }

    public int getUnitPrice() {
        return this.unit.getPrice();
    }

    public UnitInfoButton getInfoButton() {
        return this.infoButton;
    }
}
