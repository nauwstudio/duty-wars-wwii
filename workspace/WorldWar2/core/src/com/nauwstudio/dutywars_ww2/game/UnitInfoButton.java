package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.main.SaveButton;

public class UnitInfoButton extends ButtonCircle {

    private UnitButton unitButton;

    public UnitInfoButton(Vector2 position, float width, TextureRegion texture, TextureRegion texture_pressed, UnitButton unitButton) {
        super(position, width, BUTTON_UNIT_INFO, texture, texture_pressed, null);
        this.unitButton = unitButton;
    }

    public void render(SpriteBatch batch, MainAssets assets) {
        super.render(batch);
    }

    public UnitButton getUnitButton() {
        return this.unitButton;
    }

    @Override
    public boolean isUnitInfoButton() {
        return true;
    }
}
