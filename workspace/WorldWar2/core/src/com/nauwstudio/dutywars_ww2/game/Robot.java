package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by nauwpie on 12/01/2018.
 */

public class Robot extends Player {

    public static final int TRUCK_MAX = 2;
    public static final int TRANSPORTER_MAX = 2;
    public static final int LANDER_MAX = 2;
    public static final int AIRCRAFT_CARRIER_RATIO = 2;
    public static final int ENEMY_AREA_SIZE = 4;
    public static final int ANTI_AIRCRAFT_RATIO = 2;

    private Game game;

    private int playPhaseCounter;
    private Timer.Task playPhaseTask;
    private Timer.Task unitMoveTask;
    private Timer.Task unitActTask;
    private int createPhaseCounter;
    private Timer.Task createPhaseTask;

    private boolean unitPlaying = false;

    public enum Mind {
        DEFENCE, ATTACK
    }

    private Mind mind;
    private boolean buildAntiAirUnit;
    private boolean buildAircraftCarrier;

    public Robot(Army army, int team, int money, int order, int status) {
        super(army, team, money, order, status);
    }

    @Override
    public boolean isHuman() {
        return false;
    }

    @Override
    public int getType() {
        return ROBOT;
    }

    public void play(Game game) {
        System.out.println("=======>>>>> START PLAY");
        if (this.game == null) {
            initGame(game);
        }
        analyzeGame();
        startPlayPhase();
    }

    private void analyzeGame() {
        // Mind
        this.mind = Mind.ATTACK;
        for (Building building : this.buildings) {
            if (building.isCapital() && enemyInArea(building.getRow(), building.getCol())) {
                this.mind = Mind.DEFENCE;
            }
        }
        // Enemy has air unit ?
        this.buildAntiAirUnit = buildAntiAirUnit();
        this.buildAircraftCarrier = buildAircraftCarrier();
        System.out.println("MIND      ==> " + this.mind);
        System.out.println("AIR UNITS ==> " + this.buildAntiAirUnit);
    }

    private void initGame(Game game) {
        this.game = game;
    }

    private GameObject getTarget(Unit unit) {
        // TRANSPORT UNIT
        if (unit.isTransportUnit()) {
            return getTransportUnitTarget(unit);
        }
        // FEET UNIT
        else if (unit.isFeetUnit()) {
            return getFeetUnitTarget(unit);
        }
        // OTHER UNIT
        else {
            return getFightUnitTarget(unit);
        }
    }

    private GameObject getTransportUnitTarget(Unit unit) {
        // No passenger => target the nearest passenger
        if (unit.isEmpty()) {
            return getNearestEmbarkTileFromTarget(unit, getNearestTransportableUnit(unit));
        }
        // Passenger => target the passenger's target
        else {
            GameObject passengerTarget;
            // Passenger is a feet unit (BUILDING => UNIT)
            if (unit.getPassenger().isFeetUnit()) {
                passengerTarget = getNearestEnemyBuilding(unit.getPassenger());
                // Enemy has no more buildings
                if (passengerTarget == null) {
                    passengerTarget = getNearestEnemyUnit(unit.getPassenger());
                }
            }
            // Passenger is a fight unit (UNIT => BUILDING)
            else {
                passengerTarget = getNearestEnemyUnit(unit.getPassenger());
                // Enemy has nos more unit
                if (passengerTarget == null) {
                    passengerTarget = getNearestEnemyBuilding(unit.getPassenger());
                }
            }
            // Return the nearest tile from the passenger target, except the target, to drop passenger on it.
            return getNearestDropTileFromTarget(unit, passengerTarget);
        }
    }

    private GameObject getFeetUnitTarget(Unit unit) {
        // Get nearest building to capture
        GameObject target = getNearestEnemyBuilding(unit);
        // Enemy has no more buildings
        if (target == null) {
            target = getNearestEnemyUnit(unit);
        }
        // If target is far, try to embark in a transport unit
        int distToTarget = Math.abs(target.getRow() - unit.getRow()) + Math.abs(target.getCol() - unit.getCol());
        // Toot far ?
        if (distToTarget > unit.getMoveScope() * 2) {
            // Transport unit on move scope ?
            Unit transportUnit = getNearestTransportUnitInScope(unit, target);
            if (transportUnit != null) {
                target = transportUnit;
            }
        }
        return target;
    }

    private GameObject getFightUnitTarget(Unit unit) {
        // Get nearest enemy unit
        GameObject target = getNearestEnemyUnit(unit);
        // Enemy has no more unit
        if (target == null) {
            target = getNearestEnemyBuilding(unit);
        }
        // If target is far, try to embark in a transport unit
        int distToTarget = Math.abs(target.getRow() - unit.getRow()) + Math.abs(target.getCol() - unit.getCol());
        // Toot far ?
        if (distToTarget > unit.getMoveScope() * 2) {
            // Transport unit on move scope ?
            Unit transportUnit = getNearestTransportUnitInScope(unit, target);
            if (transportUnit != null) {
                target = transportUnit;
            }
        }
        return target;
    }

    private void startPlayPhase() {
        System.out.println("ROBOT : Start play phase");
        this.playPhaseCounter = 0;
        this.playPhaseTask = new Timer.Task() {
            @Override
            public void run() {
                if (!unitPlaying) {
                    // finish play phase
                    if (units.size() == 0 || playPhaseCounter >= units.size()) {
                        finishPlayPhase();
                    }
                    // continue play phase
                    else {
                        Unit unit = units.get(playPhaseCounter);
                        // While unit can do something, do something.
                        if (unit.isIdle()) {
                            playUnit(unit);
                        } else {
                            endUnit(unit);
                        }
                    }
                }
            }
        };
        Timer.schedule(playPhaseTask, 0.0f, 0.5f);
    }

    private void finishPlayPhase() {
        this.playPhaseTask.cancel();
        System.out.println("=======>>>>> START PLAY => ENDING");
        startCreatePhase();
    }

    private void playUnit(final Unit unit) {
        System.out.println("====> Unit plays : " + unit + " (" + unit.getRow() + "," + unit.getCol() + ")");
        System.out.println("COUNTER = " + this.playPhaseCounter);
        unitPlaying = true;
        // Point camera on unit
        this.game.pointCameraOn(unit.getPosition());
        // FIXED UNIT => ONLY ACTING
        if (unit.isFixedUnit()) {
            ArrayList<int[]> redTargets = getRedTargets(unit);
            // ATTACK POSSIBLE => START ACT
            if (!redTargets.isEmpty()) {
                int[] bestRedTarget = getBestRedTarget(unit, redTargets);
                startAttackUnit(unit, bestRedTarget[0], bestRedTarget[1]);
            }
            // NO ATTACK POSSIBLE => FINISH ACT
            else {
                endUnit(unit);
            }
        }
        // OTHER UNIT => MOVE AND ACTING
        else {
            ArrayList<int[]> blueTargets = getBlueTargets(unit);
            // MOVE POSSIBLE => START MOVE
            if (!blueTargets.isEmpty()) {
                int[] bestBlueTarget = getBestBlueTarget(unit, blueTargets);
                if (bestBlueTarget[0] != unit.getRow() && bestBlueTarget[1] != unit.getCol() && this.game.getUnit(bestBlueTarget[0], bestBlueTarget[1]) != null) {
                    System.out.println("==> BUG DETECTED !!");
                    System.out.println("Unit : " + unit + " , " + unit.getRow() + "-" + unit.getCol());
                    System.out.println("Destination : " + bestBlueTarget[0] + "-" + bestBlueTarget[1]);
                    for (int[] test : blueTargets) {
                        System.out.println("Check => " + test[0] + "-" + test[1]);
                    }
                    System.out.println("==> END !!");
                }
                startMoveUnit(unit, bestBlueTarget[0], bestBlueTarget[1]);
            }
            // NO MOVE POSSIBLE => START ACT
            else {
                actUnit(unit);
            }
        }
    }

    private void endUnit(final Unit unit) {
        System.out.println("END UNIT : " + unit);
        unitPlaying = false;
        unit.stop();
        playPhaseCounter++;
    }

    private void startMoveUnit(final Unit unit, int row, int col) {
        System.out.println("START MOVE");
        this.game.moveUnit(unit, row, col);
        this.unitMoveTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not moving anymore
                if (!unit.isMoving()) {
                    finishMoveUnit(unit);
                }
            }
        };
        Timer.schedule(unitMoveTask, 0.0f, 0.5f);
    }

    private void finishMoveUnit(final Unit unit) {
        this.unitMoveTask.cancel();
        unit.disposeWays();
        if (unit.canDoSomethingAfterMoving(this.game)) {
            actUnit(unit);
        } else {
            endUnit(unit);
        }

    }

    // ACT BY PRIORITY => CAPTURE > ATTACK > DROP > EMBARK > RELOAD
    private void actUnit(final Unit unit) {
        System.out.println("ROBOT : UNIT ACTING");
        this.game.pointCameraOn(unit.getPosition());
        ArrayList<int[]> redTargets = getRedTargets(unit);
        ArrayList<int[]> greenTargets = getGreenTargets(unit);
        ArrayList<int[]> dropTargets = new ArrayList<int[]>();
        if (unit.isTransportUnit() && !unit.isEmpty()) {
            dropTargets = getDropTargets(unit);
        }
        // UNDIRECT UNIT
        if (unit.isUndirectUnit()) {
            System.out.println("ROBOT : testRed = " + redTargets.size());
            System.out.println("ROBOT : testGreen = " + greenTargets.size());
            boolean canEmbark = unit.getMoveAmount() > 0;
            boolean canAttack = unit.getMoveAmount() == unit.getMoveScope();
            // 1. CAPTURE
            if (unit.canCapture(this.game)) {
                System.out.println("CAPTURE");
                startCaptureUnit(unit);
            }
            // 2. ATTACK
            else if (!redTargets.isEmpty() && canAttack) {
                int[] bestRedTarget = getBestRedTarget(unit, redTargets);
                System.out.println("ATTACK : " + bestRedTarget[0] + "," + bestRedTarget[1]);
                startAttackUnit(unit, bestRedTarget[0], bestRedTarget[1]);

            }
            // 3. EMBARK
            else if (!greenTargets.isEmpty() && canEmbark) {
                System.out.println("EMBARK");
                int[] bestGreenTarget = getBestEmbarkTarget(unit, greenTargets);
                startEmbarkUnit(unit, bestGreenTarget[0], bestGreenTarget[1]);
                // COUNTER BUG
                playPhaseCounter--;
            }
            // 4. NOTHING
            else {
                System.out.println("NOTHING");
                endUnit(unit);
            }
        }
        // DIRECT UNIT
        else {
            System.out.println("ROBOT : testRed = " + redTargets.size());
            System.out.println("ROBOT : testGreen = " + greenTargets.size());
            boolean canEmbark = unit.getMoveAmount() > 0;
            // 1. CAPTURE
            if (unit.canCapture(this.game)) {
                System.out.println("CAPTURE");
                startCaptureUnit(unit);
            }
            // 2. ATTACK
            else if (!redTargets.isEmpty()) {
                int[] bestRedTarget = getBestRedTarget(unit, redTargets);
                System.out.println("ATTACK : " + bestRedTarget[0] + "," + bestRedTarget[1]);
                startAttackUnit(unit, bestRedTarget[0], bestRedTarget[1]);
                // COUNTER BUG
                if (!unit.isAlive()) {
                    playPhaseCounter--;
                }
            }
            // 3. DROP
            else if (!dropTargets.isEmpty()) {
                System.out.println("DROP");
                int[] bestGreenTarget = getBestDropTarget(unit, dropTargets);
                startDropUnit(unit, bestGreenTarget[0], bestGreenTarget[1]);
            }
            // 4. EMBARK
            else if (!greenTargets.isEmpty() && canEmbark) {
                System.out.println("EMBARK");
                int[] bestGreenTarget = getBestEmbarkTarget(unit, greenTargets);
                startEmbarkUnit(unit, bestGreenTarget[0], bestGreenTarget[1]);
                // COUNTER BUG
                playPhaseCounter--;
            }
            // 5. RELOAD (Reload unit ONLY)
            else if (unit.isReloadUnit()) {
                System.out.println("RELOAD");
                startReloadUnit(unit);
            }
            // 6. NOTHING
            else {
                System.out.println("NOTHING");
                endUnit(unit);
            }
        }
    }

    private void startAttackUnit(final Unit unit, int row, int col) {
        this.game.attackUnit(unit, row, col);
        this.unitActTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not firing anymore
                if (unit.isStop()) {
                    finishActUnit(unit);
                }
            }
        };
        Timer.schedule(unitActTask, 0.0f, 0.5f);
    }

    private void startCaptureUnit(final Unit unit) {
        System.out.println("START CAPTURE");
        this.game.attackBuilding(unit);
        this.unitActTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not capturing anymore
                if (unit.isStop()) {
                    finishActUnit(unit);
                }
            }
        };
        Timer.schedule(unitActTask, 0.0f, 0.5f);
    }

    private void startReloadUnit(final Unit unit) {
        System.out.println("START RELOAD");
        this.game.reloadUnits(unit);
        this.unitActTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not firing anymore
                if (unit.isStop()) {
                    finishActUnit(unit);
                }
            }
        };
        Timer.schedule(unitActTask, 0.0f, 0.5f);
    }

    private void startEmbarkUnit(final Unit unit, int row, int col) {
        System.out.println("START EMBARK");
        this.game.embarkUnit(unit, row, col);
        this.unitActTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not firing anymore
                if (unit.isStop()) {
                    finishActUnit(unit);
                }
            }
        };
        Timer.schedule(unitActTask, 0.0f, 0.5f);
    }

    private void startDropUnit(final Unit unit, int row, int col) {
        System.out.println("START DROP");
        this.game.dropUnit(unit, row, col);
        this.unitActTask = new Timer.Task() {
            @Override
            public void run() {
                // Unit is not firing anymore
                if (unit.isStop()) {
                    finishActUnit(unit);
                }
            }
        };
        Timer.schedule(unitActTask, 0.0f, 0.5f);
    }

    private void finishActUnit(final Unit unit) {
        this.unitActTask.cancel();
        endUnit(unit);
    }

    private ArrayList<int[]> getBlueTargets(Unit unit) {
        unit.updateWays(this.game);
        ArrayList<int[]> blueTargets = new ArrayList<int[]>();
        // Add NO_MOVE to targets
        blueTargets.add(new int[]{unit.getRow(), unit.getCol()});
        for (int row = 0; row < this.game.getMapSize(); row++) {
            for (int col = 0; col < this.game.getMapSize(); col++) {
                if (this.game.isBlueTarget(unit, row, col)) {
                    blueTargets.add(new int[]{row, col});
                }
            }
        }
        return blueTargets;
    }

    private ArrayList<int[]> getRedTargets(Unit unit) {
        ArrayList<int[]> redTargets = new ArrayList<int[]>();
        for (int row = 0; row < this.game.getMapSize(); row++) {
            for (int col = 0; col < this.game.getMapSize(); col++) {
                if (this.game.isRedTarget(unit, row, col)) {
                    redTargets.add(new int[]{row, col});
                }
            }
        }
        return redTargets;
    }

    private ArrayList<int[]> getGreenTargets(Unit unit) {
        ArrayList<int[]> greenTargets = new ArrayList<int[]>();
        for (int row = 0; row < this.game.getMapSize(); row++) {
            for (int col = 0; col < this.game.getMapSize(); col++) {
                if (this.game.isGreenTarget(unit, row, col)) {
                    greenTargets.add(new int[]{row, col});
                }
            }
        }
        return greenTargets;
    }

    private ArrayList<int[]> getDropTargets(Unit unit) {
        ArrayList<int[]> dropTargets = new ArrayList<int[]>();
        for (int row = 0; row < this.game.getMapSize(); row++) {
            for (int col = 0; col < this.game.getMapSize(); col++) {
                if (this.game.isDropTarget(unit, row, col)) {
                    dropTargets.add(new int[]{row, col});
                }
            }
        }
        return dropTargets;
    }

    private int[] getBestBlueTarget(Unit unit, ArrayList<int[]> blueTargets) {
        GameObject target = getTarget(unit);
        if (target != null) {
            System.out.println("TARGET ==> " + unit + " " + unit.getRow() + "-" + unit.getCol() + "  => " + target + " " + target.getRow() + "-" + target.getCol());
        } else {
            System.out.println("TARGET ==> " + unit + " " + unit.getRow() + "-" + unit.getCol() + "  => " + target);
        }
        int[] bestBlueTarget = new int[]{};
        int bestBlueScore = -999;
        for (int[] blueTarget : blueTargets) {
            int score = evaluateBlueTarget(unit, blueTarget, target);
            if (score > bestBlueScore) {
                bestBlueScore = score;
                bestBlueTarget = blueTarget;
            }
        }
        return bestBlueTarget;
    }

    /*
     * Evaluate blue target (on which tile to move ?)
     *
     */
    private int evaluateBlueTarget(Unit unit, int[] blueTarget, GameObject target) {
        int blueTargetScore = 0;
        // TARGET PROXIMITY
        blueTargetScore = getTargetProximityEvaluation(unit, target, blueTarget[0], blueTarget[1]);
        // RETREAT INTO BUILDING ?
        blueTargetScore += getBuildingEvalutation(unit, blueTarget[0], blueTarget[1]);
        // TILE DEFENCE ?
        blueTargetScore += getTileDefenceEvaluation(unit, blueTarget[0], blueTarget[1]);
        return blueTargetScore;
    }

    private int getCaptureEvaluation(Unit unit, int row, int col) {
        int score = 0;
        Building building = this.game.getBuilding(row, col);
        if (building != null && !this.isAllied(building.getOwner())) {
            if (building.isCapital()) {
                score += 1000;
            } else if (building.isCity()) {
                score += 200;
            } else {
                score += 300;
            }
        }
        return score;
    }

    private int getBuildingEvalutation(Unit unit, int row, int col) {
        int score = 0;
        Building building = this.game.getBuilding(row, col);
        if (building != null) {
            // ROBOT BUILDING
            if (this.equals(building.getOwner())) {
                // CAPITAL
                if (building.isCapital()) {
                    if (this.mind == Mind.ATTACK) {
                        score -= 10;
                    } else {
                        score += 10;
                    }
                    if (unit.isGroundUnit() && unit.isHurt()) {
                        score += Unit.LIFE - unit.getLife();
                    }
                }
                // FACTORY
                else if (building.isFactory()) {
                    score -= 100;
                }
                // AIRPORT
                else if (building.isAirport()) {
                    if (unit.isAirUnit()) {
                        if (unit.isHurt()) {
                            score += Unit.LIFE - unit.getLife();
                        }
                        if (unit.isLowAmmo()) {
                            score += unit.getMaxAmmo();
                        }
                        if (unit.getGas() < 10) {
                            score += unit.getMaxGas();
                        }
                    } else if (this.mind == Mind.ATTACK) {
                        score -= 100;
                    }
                }
                // SHIPYARD
                else if (building.isShipyard()) {
                    if (unit.isNavalUnit()) {
                        if (unit.isHurt()) {
                            score += Unit.LIFE - unit.getLife();
                        }
                        if (unit.isLowAmmo()) {
                            score += unit.getMaxAmmo();
                        }
                        if (unit.getGas() < 10) {
                            score += unit.getMaxGas();
                        }
                    } else if (this.mind == Mind.ATTACK) {
                        score -= 100;
                    }
                }
                // CITY
                else if (building.isCity()) {
                    if (this.mind == Mind.DEFENCE) {
                        score += 3;
                    }
                    if (unit.isGroundUnit() && unit.isHurt()) {
                        score += Unit.LIFE - unit.getLife();
                    }
                }
            }
            // ENEMY BUILDING
            else if (!building.getOwner().isAllied(this)) {
                // Capture building
                if (unit.isFeetUnit()) {
                    score += getCaptureEvaluation(unit, building.getRow(), building.getCol());
                }
                // Avoid transporter or truck to block buildings
                if (unit.isTransportUnit()) {
                    score -= 100;
                }
            }
        }

        return score;
    }

    private int getTargetProximityEvaluation(Unit unit, GameObject target, int row, int col) {
        int score = 0;
        if (target != null) {
            // Unit target
            int newDist = Math.abs(row - target.getRow()) + Math.abs(col - target.getCol());
            int oldDist = Math.abs(unit.getRow() - target.getRow()) + Math.abs(unit.getCol() - target.getCol());
            if (target.isUnit() && unit.isUndirectUnit() && newDist <= unit.getAttackScope()[1] && unit.getAmmo() > 0) {
                // Is target in attack scope ?
                score += 10;
                // If already on attack scope, don't move but fire
                if (oldDist <= unit.getAttackScope()[1]) {
                    score += 5;
                }
            } else {
                // Go near from target
                score += (oldDist - newDist) * 10;
            }
        }
        return score;
    }

    private int getTileDefenceEvaluation(Unit unit, int row, int col) {
        int score = 0;
        Tile tile = this.game.getTile(row, col);
        if (tile != null) {
            score += tile.getDefence();
        }
        return score;
    }

    private int[] getBestRedTarget(Unit unit, ArrayList<int[]> redTargets) {
        int[] bestRedTarget = new int[]{};
        int bestRedScore = -999;
        for (int[] redTarget : redTargets) {
            int score = evaluateRedTarget(unit, redTarget);
            if (score > bestRedScore) {
                bestRedScore = score;
                bestRedTarget = redTarget;
            }
        }
        return bestRedTarget;
    }

    /*
     * Evaluate red target (which unit to attack ?)
     *
     * Score is the difference between damage given and damage received.
     * Furthemore, priority is to attack tranport unit which contains passenger.
     *
     */
    private int evaluateRedTarget(Unit unit, int[] redTarget) {
        int redTargetScore = 0;
        Unit target = this.game.getUnit(redTarget[0], redTarget[1]);
        if (target != null) {
            int battleResult[] = this.game.calculateBattleResults(unit, target);
            redTargetScore = battleResult[1];
            if (target.isTransportUnit() && !target.isEmpty()) {
                redTargetScore += 100;
            }
        }
        return redTargetScore;
    }

    private int[] getBestEmbarkTarget(Unit unit, ArrayList<int[]> greenTargets) {
        int[] bestGreenTarget = new int[]{};
        int bestGreenScore = -999;
        for (int[] greenTarget : greenTargets) {
            int score = evaluateEmbarkTarget(unit, greenTarget);
            if (score > bestGreenScore) {
                bestGreenScore = score;
                bestGreenTarget = greenTarget;
            }
        }
        return bestGreenTarget;
    }

    private int evaluateEmbarkTarget(Unit unit, int[] greenTarget) {
        int greenTargetScore = -1;
        // EMBARK MODE : score depends on the transport unit (transporter > lander > truck).
        Unit transport = this.game.getUnit(greenTarget[0], greenTarget[1]);
        if (transport != null && transport.isTransportUnit()) {
            switch (transport.getType()) {
                case Unit.TRANSPORTER:
                    greenTargetScore = 3;
                    break;
                case Unit.LANDER:
                    greenTargetScore = 2;
                    break;
                case Unit.TRUCK:
                    greenTargetScore = 1;
                    break;
            }
        }
        return greenTargetScore;
    }

    private int[] getBestDropTarget(Unit unit, ArrayList<int[]> dropTargets) {
        int[] bestDropTarget = new int[]{};
        int bestDropScore = -999;
        for (int[] dropTarget : dropTargets) {
            int score = evaluateDropTarget(unit, dropTarget);
            if (score > bestDropScore) {
                bestDropScore = score;
                bestDropTarget = dropTarget;
            }
        }
        return bestDropTarget;
    }

    private int evaluateDropTarget(Unit unit, int[] greenTarget) {
        int greenTargetScore = -1;
        // DROP MODE : score depends on defence of the tile/building and if building is an enemy building.
        if (unit.isTransportUnit()) {
            Building building = this.game.getBuilding(greenTarget[0], greenTarget[1]);
            if (building != null) {
                greenTargetScore = building.getDefence();
                if (!this.isAllied(building.getOwner())) {
                    greenTargetScore += 4;
                }
            } else {
                greenTargetScore = this.game.getTile(greenTarget[0], greenTarget[1]).getDefence();
            }
        }
        return greenTargetScore;
    }

    private void startCreatePhase() {
        System.out.println("ROBOT : Start create phase");
        this.createPhaseCounter = 0;
        // Shuffle buildings list
        Collections.shuffle(buildings);
        this.createPhaseTask = new Timer.Task() {
            @Override
            public void run() {
                if (buildings.size() == 0 || createPhaseCounter >= buildings.size()) {
                    finishCreatePhase();
                } else {
                    Building building = buildings.get(createPhaseCounter);
                    createUnit(building);
                    createPhaseCounter++;
                }
            }
        };
        Timer.schedule(createPhaseTask, 0.0f, 0.5f);
    }

    private void finishCreatePhase() {
        this.createPhaseTask.cancel();
        System.out.println("ROBOT : Finish create phase");
        this.game.finishTurn();
    }

    /*
    Returns units that can be created into Factory building (units.price <= MONEY)
     */
    private ArrayList<Integer> getFactoryUnits() {
        ArrayList<Integer> units = new ArrayList<Integer>();
        if (this.money >= Unit.INFANTRY_PRICE) {
            units.add(Unit.INFANTRY);
            if (this.game.getTurn() < this.game.getPlayers().size() * 3) {
                units.add(Unit.INFANTRY);
            }
        }
        if (this.money >= Unit.BAZOOKA_PRICE) {
            units.add(Unit.BAZOOKA);
            if (this.game.getTurn() < this.game.getPlayers().size() * 3) {
                units.add(Unit.BAZOOKA);
            }
        }
        if (this.money >= Unit.SNIPER_PRICE) {
            units.add(Unit.SNIPER);
            if (this.game.getTurn() < this.game.getPlayers().size() * 3) {
                units.add(Unit.SNIPER);
            }
        }
        if (this.money >= Unit.JEEP_PRICE) {
            units.add(Unit.JEEP);
        }
        if (this.money >= Unit.LIGHT_TANK_PRICE) {
            units.add(Unit.LIGHT_TANK);
        }
        if (this.money >= Unit.HEAVY_TANK_PRICE) {
            units.add(Unit.HEAVY_TANK);
        }
        if (this.money >= Unit.TRUCK_PRICE && this.getTruckNumber() < TRUCK_MAX && this.mind == Mind.ATTACK) {
            units.add(Unit.TRUCK);
        }
        if (this.money >= Unit.ARTILLERY_PRICE) {
            units.add(Unit.ARTILLERY);
        }
        if (this.money >= Unit.HALFTRACK_PRICE && this.buildAntiAirUnit) {
            units.add(Unit.HALFTRACK);
        }
        return units;
    }

    /*
   Returns units that can be created into Airport building (units.price <= MONEY)
    */
    private ArrayList<Integer> getAirportUnits() {
        ArrayList<Integer> units = new ArrayList<Integer>();
        if (this.money >= Unit.FIGHTER_PRICE) {
            units.add(Unit.FIGHTER);
        }
        if (this.money >= Unit.BOMBER_PRICE) {
            units.add(Unit.BOMBER);
        }
        if (this.money >= Unit.TRANSPORTER_PRICE && this.getTransporterNumber() < TRANSPORTER_MAX && this.mind == Mind.ATTACK) {
            units.add(Unit.TRANSPORTER);
        }
        return units;
    }

    /*
   Returns units that can be created into Shipyard building (units.price <= MONEY)
    */
    private ArrayList<Integer> getShipyardUnits() {
        ArrayList<Integer> units = new ArrayList<Integer>();
        if (this.money >= Unit.LANDER_PRICE && this.getLanderNumber() < LANDER_MAX && this.mind == Mind.ATTACK) {
            units.add(Unit.LANDER);
        }
        if (this.money >= Unit.DESTROYER_PRICE && this.buildAntiAirUnit) {
            units.add(Unit.DESTROYER);
        }
        if (this.money >= Unit.BATTLESHIP_PRICE) {
            units.add(Unit.BATTLESHIP);
        }
        if (this.money >= Unit.AIRCRAFT_CARRIER_PRICE && this.buildAircraftCarrier && this.mind == Mind.ATTACK) {
            units.add(Unit.AIRCRAFT_CARRIER);
        }
        return units;
    }

    private void createUnit(Building building) {
        // Checking that building is free to create unit (no unit alrady on)
        if (this.game.getUnitLayer()[building.getRow()][building.getCol()] == null) {
            ArrayList<Integer> possibilities = new ArrayList<Integer>();
            if (building.isFactory()) {
                possibilities = getFactoryUnits();
            } else if (building.isAirport()) {
                possibilities = getAirportUnits();
            } else if (building.isShipyard()) {
                possibilities = getShipyardUnits();
            }
            Random ran = new Random();
            if (possibilities.size() > 0) {
                // Point camera on building
                this.game.pointCameraOn(building.getPosition());
                int unitType = possibilities.get(ran.nextInt(possibilities.size()));
                if (unitType != 0) {
                    Unit unit = this.game.createUnit(building.getRow(), building.getCol(), this, unitType, Unit.Orientation.SOUTH);
                    System.out.println("ROBOT : create unit => " + unit);
                    this.removeMoney(unit.getPrice());
                }
            }
        }
    }

    private boolean enemyInArea(int centerRow, int centerCol) {
        for (int row = 0; row < this.game.getMapSize(); row++) {
            for (int col = 0; col < this.game.getMapSize(); col++) {
                int manhattanDistance = Math.abs(centerRow - row) + Math.abs(centerCol - col);
                if (manhattanDistance < ENEMY_AREA_SIZE) {
                    Unit unit = this.game.getUnit(row, col);
                    if (unit != null && !this.isAllied(unit.getOwner()) && manhattanDistance < ENEMY_AREA_SIZE) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean buildAntiAirUnit() {
        int antiAirUnitAmount = this.getAntiAirUnitAmount();
        int enemyAirUnitAmount = 0;
        for (Player player : this.game.getPlayers()) {
            if (!this.isAllied(player)) {
                enemyAirUnitAmount += player.getAirUnitAmount();
            }
        }
        return antiAirUnitAmount < (ANTI_AIRCRAFT_RATIO * enemyAirUnitAmount);
    }

    private boolean buildAircraftCarrier() {
        int airUnitAmount = this.getAirUnitAmount();
        int aircraftCarrierAmount = this.getUnitAmount(Unit.AIRCRAFT_CARRIER);
        return aircraftCarrierAmount < (AIRCRAFT_CARRIER_RATIO * airUnitAmount);
    }

    private Building getNearestEnemyBuilding(Unit unit) {
        Building nearest = null;
        int distance = 99;
        // ENEMY BUILDINGS
        for (Player player : this.game.getPlayers()) {
            if (!this.isAllied(player)) {
                for (Building building : player.getBuildings()) {
                    int dist = Math.abs(unit.getRow() - building.getRow()) + Math.abs(unit.getCol() - building.getCol());
                    if (dist < distance) {
                        nearest = building;
                        distance = dist;
                    }
                }
            }
        }
        // GAIA BUILDINGS
        for (Building building : this.game.getGaiaPlayer().getBuildings()) {
            int dist = Math.abs(unit.getRow() - building.getRow()) + Math.abs(unit.getCol() - building.getCol());
            if (dist < distance) {
                nearest = building;
                distance = dist;
            }
        }
        return nearest;
    }

    private Unit getNearestTransportUnitInScope(Unit unit, GameObject target) {
        Unit nearest = null;
        int distance = 99;
        // TRANSPORT UNITS
        for (Unit transportUnit : this.getUnits()) {
            if (unit.canEmbarkIn(transportUnit)) {
                int dist = Math.abs(unit.getRow() - transportUnit.getRow()) + Math.abs(unit.getCol() - transportUnit.getCol());
                // Not too far ?
                if (dist < distance && dist <= unit.getMoveScope()) {
                    // Not in wrong direction ?
                    int unitTargetDist = Math.abs(unit.getRow() - target.getRow()) + Math.abs(unit.getCol() - target.getCol());
                    int transportTargetDist = Math.abs(transportUnit.getRow() - target.getRow()) + Math.abs(transportUnit.getCol() - target.getCol());
                    if(unitTargetDist > transportTargetDist) {
                        System.out.println("UNIT=>TARGET("+unitTargetDist+") - TRANSPORT=>TARGET("+transportTargetDist+")");
                        nearest = transportUnit;
                        distance = dist;
                    }
                }
            }
        }
        return nearest;
    }

    private Tile getNearestEmbarkTileFromTarget(Unit unit, GameObject target) {
        Tile nearest = null;
        if (target != null) {
            int distance = 99;
            for (int row = 0; row < this.game.getMapSize(); row++) {
                for (int col = 0; col < this.game.getMapSize(); col++) {
                    int dist = Math.abs(target.getRow() - row) + Math.abs(target.getCol() - col);
                    //  "&& distance > 0" => Avoid transporter to go on target
                    if (dist < distance && distance > 0 && unit.canBeOnTile(this.game.getTile(row, col))) {
                        nearest = this.game.getTile(row, col);
                        distance = dist;
                    }
                }
            }
        }
        return nearest;
    }

    private Tile getNearestDropTileFromTarget(Unit unit, GameObject target) {
        System.out.println ("PASSENGER TARGET : " + target + "("+ target.getRow() + "," + target.getCol() + ")");
        Tile nearest = null;
        if (target != null) {
            int distance = 99;
            for (int row = 0; row < this.game.getMapSize(); row++) {
                for (int col = 0; col < this.game.getMapSize(); col++) {
                    int dist = Math.abs(target.getRow() - row) + Math.abs(target.getCol() - col);
                    //  "&& distance > 0" => Avoid transporter to go on target
                    if (dist < distance && dist > 0 && unit.canBeOnTile(this.game.getTile(row, col)) && canDropUnitFromTile(unit, row, col)) {
                        nearest = this.game.getTile(row, col);
                        distance = dist;
                    }
                }
            }
        }
        return nearest;
    }

    /*
    Check if a transport unit can drop its passenger from tile at row,col
     */
    private boolean canDropUnitFromTile(Unit unit, int row, int col) {
        System.out.println("CHECK TILE : " + row + "," + col);
        // Check adjacent tiles
        for (int adjacentRow = 0; adjacentRow < this.game.getMapSize(); adjacentRow++) {
            for (int adjacentCol = 0; adjacentCol < this.game.getMapSize(); adjacentCol++) {
                int dist = Math.abs(row - adjacentRow) + Math.abs(col - adjacentCol);
                if (dist == 1) {
                    System.out.println("=> " + adjacentRow + "," + adjacentCol + unit.getPassenger().canGoOnTarget(this.game, adjacentRow, adjacentCol));
                    // Check if passenger can go on adjacent tile
                    if (unit.getPassenger().canGoOnTarget(this.game, adjacentRow, adjacentCol)) {
                        return true;
                    }
                }
            }
        }
        // Passenger can't go on adjacent tiles
        return false;
    }

    private Unit getNearestEnemyUnit(Unit unit) {
        Unit nearest = null;
        int distance = 99;
        // ENEMY UNITS
        for (Player player : this.game.getPlayers()) {
            if (!this.isAllied(player)) {
                for (Unit enemy : player.getUnits()) {
                    int dist = Math.abs(unit.getRow() - enemy.getRow()) + Math.abs(unit.getCol() - enemy.getCol());
                    if (dist < distance && enemy.canBeAttackedBy(unit)) {
                        nearest = enemy;
                        distance = dist;
                    }
                }
            }
        }
        return nearest;
    }

    private Unit getNearestTransportableUnit(Unit unit) {
        Unit nearest = null;
        int distance = 99;
        // ROBOT UNITS
        for (Unit allied : this.getUnits()) {
            if (allied.canEmbarkIn(unit)) {
                int dist = Math.abs(unit.getRow() - allied.getRow()) + Math.abs(unit.getCol() - allied.getCol());
                if (dist < distance) {
                    nearest = allied;
                    distance = dist;
                }
            }
        }
        return nearest;
    }

    private ArrayList<int[]> getNextMove(Game game, ArrayList<ArrayList<int[]>> allPath, ArrayList<int[]> move, Unit unit, int row, int col) {
        ArrayList<int[]> nextMove = (ArrayList<int[]>) move.clone();
        System.out.print("current path : ");
        for (int[] lol : nextMove) {
            System.out.print("{" + lol[0] + "," + lol[1] + "} => ");
        }
        System.out.println();
        System.out.println("testing to add : {" + row + "," + col + "}");
        // 1. Check that row and col are in map bounds
        if (row >= 0 && row < game.getMapSize() && col >= 0 && col < game.getMapSize()) {
            // 2. Check that tile is empty
            if (game.getUnit(row, col) == null) {
                // 3. Check if unit can be on tile
                if (unit.canBeOnTile(game.getMap().getTile(row, col)) || (game.getMap().getTile(row, col).isSea() && game.getBuildingLayer()[row][col] != null && game.getBuildingLayer()[row][col].isShipyard())) {
                    int[] dest = new int[]{row, col};
                    // No return
                    if (!tileAlreadyInPath(allPath, dest)) {
                        nextMove.add(new int[]{row, col});
                        System.out.println("adding : {" + row + "," + col + "}");
                        return nextMove;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private boolean isTarget(ArrayList<int[]> move, GameObject target) {
        if (move.get(move.size() - 1)[0] == target.getRow() && move.get(move.size() - 1)[1] == target.getCol()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean tileAlreadyInPath(ArrayList<ArrayList<int[]>> allPath, int[] tile) {
        for (ArrayList<int[]> path : allPath) {
            for (int[] point : path) {
                if (point[0] == tile[0] && point[1] == tile[1]) {
                    return true;
                }
            }
        }
        return false;
    }

}


