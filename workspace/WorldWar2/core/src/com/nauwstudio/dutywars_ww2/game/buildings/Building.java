package com.nauwstudio.dutywars_ww2.game.buildings;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.GameAssets;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Game;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.GameObject;
import com.nauwstudio.dutywars_ww2.game.Season;

public abstract class Building extends GameObject {

    // EXTRAS
    public static final String EXTRA_PLAYER = "player";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_SUBTYPE = "subtype";
    // TYPE
    public static final int CAPITAL = 1;
    public static final int CITY = 2;
    public static final int FACTORY = 3;
    public static final int AIRPORT = 4;
    public static final int SHIPYARD = 5;

    public static final int CAPITAL_MONEY = 3000;
    public static final int CITY_MONEY = 1000;
    public static final int FACTORY_MONEY = 1000;
    public static final int AIRPORT_MONEY = 1000;
    public static final int SHIPYARD_MONEY = 1000;

    public static final int CAPITAL_DEFENCE = 4;
    public static final int CITY_DEFENCE = 3;
    public static final int FACTORY_DEFENCE = 3;
    public static final int AIRPORT_DEFENCE = 3;
    public static final int SHIPYARD_DEFENCE = 3;

    public static final int CAPTURE_STEP = 10;

    protected int type;
    protected int subtype;
    protected Player owner;
    protected Army army;
    protected int money;
    protected int defence;

    // Capture values
    private int capture_step = CAPTURE_STEP;
    private TextureRegion capture_texture;

    public Building(int row, int col, Player owner, int type, int money, int defence) {
        super(row, col);
        this.type = type;
        this.owner = owner;
        this.army = owner.getArmy();
        this.money = money;
        this.defence = defence;
        this.subtype = 0;
    }

    @Override
    public void render(SpriteBatch batch, float delta, GameAssets assets) {
        // No capture in progress
        if (capture_step == CAPTURE_STEP) {
            batch.draw(getTexture(), position.x, position.y, width, width * textureRatio);
        }
        // Capture in progress
        else {
            // Drawing old
            batch.draw(capture_texture, position.x, position.y, width, width * textureRatio);
            // Drawing new gradually
            TextureRegion region = new TextureRegion(getTexture(), 0,(int) (getTexture().getRegionHeight() * (1 - (capture_step / (float)CAPTURE_STEP))), getTexture().getRegionWidth(), (int) (getTexture().getRegionHeight() * (capture_step / (float)CAPTURE_STEP)));
            batch.draw(region, position.x, position.y, width, width * textureRatio * (capture_step / (float)CAPTURE_STEP));
        }
    }

    public void renderSmall(SpriteBatch batch, float x, float y, float width, int size) {
        float small_tile_width = width/size;
        float ratio = small_tile_width/this.width;
        float pos_x = x + position.x * ratio + width/2 - small_tile_width/2;
        float pos_y = y + position.y * ratio;
        batch.draw(this.getTexture(), pos_x, pos_y, small_tile_width, small_tile_width * this.textureRatio);
    }

    public int getType() {
        return this.type;
    }

    public int getSubtype() {
        return this.subtype;
    }

    public Player getOwner() {
        return this.owner;
    }

    public int getMoney() {
        return this.money;
    }

    public int getDefence() {
        return this.defence;
    }

    public boolean isCity() {
        return false;
    }

    public boolean isCapital() {
        return false;
    }

    public boolean isFactory() {
        return false;
    }

    public boolean isShipyard() {
        return false;
    }

    public boolean isAirport() {
        return false;
    }

    public boolean canCreateUnit() {
        return isFactory() || isAirport() || isShipyard();
    }

    // Capturing building
    public void capture(final Player player) {
        this.capture_step = 0;
        // Keeping texture before change
        this.capture_texture = getTexture();
        this.owner.removeBuilding(this);
        this.owner = player;
        this.owner.addBuilding(this);
        Timer.Task captureTask = new Timer.Task() {
            @Override
            public void run() {
                capture_step++;
                if (capture_step == CAPTURE_STEP) {
                    this.cancel();
                }
            }
        };
        Timer.schedule(captureTask, 0.0f, 0.025f, CAPTURE_STEP);
    }

    public String save() {
        return this.owner.getOrder() + "#" + this.type;
    }

    public static Building createBuilding(int row, int col, Player player, int type, int subType) {
        switch (type) {
            case 0:
                return null;
            case Building.CAPITAL:
                return new Capital(row, col, player, subType);
            case Building.CITY:
                return new City(row, col, player);
            case Building.FACTORY:
                return new Factory(row, col, player);
            case Building.AIRPORT:
                return new Airport(row, col, player);
            case Building.SHIPYARD:
                return new Shipyard(row, col, player);
            default:
                return null;
        }
    }

    @Override
    public TextureRegion getToggleTexture() {
        return getTexture();
    }
}
