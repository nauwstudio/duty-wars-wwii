package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Transporter extends Unit {

    private static final String NAME_USA = "transporter.usa";
    private static final String NAME_GER = "transporter.ger";
    private static final String NAME_USS = "transporter.uss";
    private static final String NAME_JAP = "transporter.jap";
    private static final String NAME_GBR = "transporter.gbr";
    private static final String DESC_USA = "transporter_desc.usa";
    private static final String DESC_GER = "transporter_desc.ger";
    private static final String DESC_USS = "transporter_desc.uss";
    private static final String DESC_JAP = "transporter_desc.jap";
    private static final String DESC_GBR = "transporter_desc.gbr";

    public Transporter(int row, int col, Player owner, Orientation orientation) {
        super(row, col, orientation, owner, TRANSPORTER, Kind.AIR, TRANSPORTER_PRICE, TRANSPORTER_MOVE_SCOPE, TRANSPORTER_ATTACK_SCOPE, LIFE, TRANSPORTER_AMMO, TRANSPORTER_GAS, TRANSPORTER_DMG, owner.getArmy().getGameAssets().infantry_fire, owner.getArmy().getGameAssets().bomber_move);
    }

    @Override
    public TextureRegion getTextureIdle() {
        switch (orientation) {
            case SOUTH:
                return this.owner.getArmy().getAssets().transporter_S;
            case WEST:
                return this.owner.getArmy().getAssets().transporter_W;
            case NORTH:
                return this.owner.getArmy().getAssets().transporter_N;
            case EAST:
                return this.owner.getArmy().getAssets().transporter_E;
            default:
                return getTexture();
        }
    }

    @Override
    public TextureRegion getTextureFire(float delta) {
        return getTextureIdle();
    }

    @Override
    public TextureRegion getTexture() {
        return this.owner.getArmy().getAssets().transporter_HD;
    }

    @Override
    public TextureRegion getButtonTexture() {
        return this.owner.getArmy().getAssets().button_transporter;
    }

    @Override
    public TextureRegion getButtonPressedTexture() {
        return this.owner.getArmy().getAssets().button_transporter_pressed;
    }

    @Override
    public TextureRegion getButtonDisabledTexture() {
        return this.owner.getArmy().getAssets().button_transporter_disabled;
    }

    @Override
    public String getName() {
        switch (this.owner.getArmy().getType()) {
            case Army.USA:
                return NAME_USA;
            case Army.GER:
                return NAME_GER;
            case Army.USS:
                return NAME_USS;
            case Army.JAP:
                return NAME_JAP;
            case Army.GBR:
                return NAME_GBR;
        }
        return "";
    }

    @Override
    public String getDescription() {
        switch (this.owner.getArmy().getType()) {
            case Army.USA:
                return DESC_USA;
            case Army.GER:
                return DESC_GER;
            case Army.USS:
                return DESC_USS;
            case Army.JAP:
                return DESC_JAP;
            case Army.GBR:
                return DESC_GBR;
        }
        return "";
    }


}
