package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.*;

public abstract class GameObject {

    protected int row, col;
    protected Vector2 position;
    protected float width, height;
    protected Polygon bounds;
    protected float textureRatio;

    public GameObject(int row, int col) {
        this.row = row;
        this.col = col;
        this.position = Util.mapToWorldCoord(new Vector2(row, col));
        this.width = Util.getTileWidth();
        this.height = width * Util.TILE_RATIO;
        float[] left = new float[]{position.x, position.y + this.height / 2};
        float[] top = new float[]{position.x + this.width / 2, position.y + this.height};
        float[] right = new float[]{position.x + this.width, position.y + this.height / 2};
        float[] bottom = new float[]{position.x + this.width / 2, position.y};
        this.bounds = new Polygon(new float[]{left[0], left[1], top[0], top[1], right[0], right[1], bottom[0], bottom[1]});
    }

    public boolean isTouched(float x, float y) {
        return this.bounds.contains(new Vector2(x, y));
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public float getTextureRatio() {
        return this.textureRatio;
    }

    public abstract void render(SpriteBatch batch, float delta, com.nauwstudio.dutywars_ww2.GameAssets assets);

    public abstract TextureRegion getTexture();

    public abstract TextureRegion getToggleTexture();

    public abstract String getName();

    public int getRow() {
        return this.row;
    }

    public int getCol() {
        return this.col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public boolean isUnit() {
        return false;
    }

}
