package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Fighter extends Unit{

	private static final String NAME_USA = "fighter.usa";
	private static final String NAME_GER = "fighter.ger";
	private static final String NAME_USS = "fighter.uss";
	private static final String NAME_JAP = "fighter.jap";
	private static final String NAME_GBR = "fighter.gbr";
	private static final String DESC_USA = "fighter_desc.usa";
	private static final String DESC_GER = "fighter_desc.ger";
	private static final String DESC_USS = "fighter_desc.uss";
	private static final String DESC_JAP = "fighter_desc.jap";
	private static final String DESC_GBR = "fighter_desc.gbr";

	public Fighter(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, FIGHTER, Kind.AIR, FIGHTER_PRICE, FIGHTER_MOVE_SCOPE, FIGHTER_ATTACK_SCOPE, LIFE, FIGHTER_AMMO, FIGHTER_GAS, FIGHTER_DMG, owner.getArmy().getGameAssets().fighter_fire, owner.getArmy().getGameAssets().fighter_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().fighter_S;
			case WEST:
				return this.owner.getArmy().getAssets().fighter_W;
			case NORTH:
				return this.owner.getArmy().getAssets().fighter_N;
			case EAST:
				return this.owner.getArmy().getAssets().fighter_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().fighter_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().fighter_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().fighter_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().fighter_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().fighter_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_fighter;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_fighter_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_fighter_disabled;
	}

    @Override
	public String getName() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return NAME_USA;
			case Army.GER:
				return NAME_GER;
			case Army.USS:
				return NAME_USS;
			case Army.JAP:
				return NAME_JAP;
			case Army.GBR:
				return NAME_GBR;
		}
		return "";
	}

    @Override
	public String getDescription() {
		switch (this.owner.getArmy().getType()) {
			case Army.USA:
				return DESC_USA;
			case Army.GER:
				return DESC_GER;
			case Army.USS:
				return DESC_USS;
			case Army.JAP:
				return DESC_JAP;
			case Army.GBR:
				return DESC_GBR;
		}
		return "";
	}
}
