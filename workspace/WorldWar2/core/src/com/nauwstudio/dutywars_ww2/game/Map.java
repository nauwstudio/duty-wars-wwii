package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.XmlReader;
import com.nauwstudio.dutywars_ww2.*;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;

import java.util.ArrayList;

public class Map {

    public static final int CAMPAIGN = 1;
    public static final int CLASSIC_2P = 2;
    public static final int CLASSIC_3P = 3;
    public static final int CLASSIC_4P = 4;
    public static final int CLASSIC_5P = 5;
    public static final int CUSTOM = 6;
    public static final int[] TYPE = {CLASSIC_2P, CLASSIC_3P, CLASSIC_4P, CLASSIC_5P, CAMPAIGN, CUSTOM};
    public static final String[] TYPE_NAME = {"versus.2p", "versus.3p", "versus.4p", "versus.5p", "versus.campaign", "versus.custom"};
    public static final int SIZE_MAX = 30;
    public static final int SIZE_MIN = 6;
    public static final int PLAYER_MIN = 2;
    public static final int PLAYER_MAX = 5;

    // CLASSIC 2P
    public static final int DUAL_ID = 26;
    public static final String DUAL = "map.dual";
    public static final String DUAL_FILE = "dual.xml";
    public static final int INFINITE_ID = 27;
    public static final String INFINITE = "map.infinite";
    public static final String INFINITE_FILE = "infinite.xml";
    public static final int PLUS_ID = 28;
    public static final String PLUS = "map.plus";
    public static final String PLUS_FILE = "plus.xml";
    public static final int AUSTRALIA_ID = 29;
    public static final String AUSTRALIA = "map.australia";
    public static final String AUSTRALIA_FILE = "australia.xml";
    public static final int UNKNOWN_ID = 30;
    public static final String UNKNOWN = "map.unknown";
    public static final String UNKNOWN_FILE = "unknown.xml";
    // CLASSIC 3P
    public static final int TRIANGLE_ID = 31;
    public static final String TRIANGLE = "map.triangle";
    public static final String TRIANGLE_FILE = "triangle.xml";
    public static final int POSEIDON_ID = 32;
    public static final String POSEIDON = "map.poseidon";
    public static final String POSEIDON_FILE = "poseidon.xml";
    public static final int AFRICA_ID = 33;
    public static final String AFRICA = "map.africa";
    public static final String AFRICA_FILE = "africa.xml";
    public static final int MAZE_ID = 34;
    public static final String MAZE = "map.maze";
    public static final String MAZE_FILE = "maze.xml";
    public static final int CARIBBEAN_ID = 35;
    public static final String CARIBBEAN = "map.caribbean";
    public static final String CARIBBEAN_FILE = "caribbean.xml";
    // CLASSIC 4P
    public static final int SQUARE_ID = 36;
    public static final String SQUARE = "map.square";
    public static final String SQUARE_FILE = "square.xml";
    public static final int SPIRAL_ID = 37;
    public static final String SPIRAL = "map.spiral";
    public static final String SPIRAL_FILE = "spiral.xml";
    public static final int RIVERS_ID = 38;
    public static final String RIVERS = "map.rivers";
    public static final String RIVERS_FILE = "rivers.xml";
    public static final int EIGHT_ID = 39;
    public static final String EIGHT = "map.eight";
    public static final String EIGHT_FILE = "eight.xml";
    public static final int AMERICA_ID = 40;
    public static final String AMERICA = "map.america";
    public static final String AMERICA_FILE = "america.xml";
    // CLASSIC 5P
    public static final int PENTAGON_ID = 41;
    public static final String PENTAGON = "map.pentagon";
    public static final String PENTAGON_FILE = "pentagon.xml";
    public static final int EUROPE_ID = 42;
    public static final String EUROPE = "map.europe";
    public static final String EUROPE_FILE = "europe.xml";
    public static final int STAR_ID = 43;
    public static final String STAR = "map.star";
    public static final String STAR_FILE = "star.xml";
    public static final int ISLANDS_ID = 44;
    public static final String ISLANDS = "map.islands";
    public static final String ISLANDS_FILE = "islands.xml";
    public static final int WORLD_ID = 45;
    public static final String WORLD = "map.world";
    public static final String WORLD_FILE = "world.xml";
    // CAMPAIGN
    public static final int BRITAIN_ID = 1;
    public static final String BRITAIN = "map.britain";
    public static final String BRITAIN_FILE = "britain.xml";
    public static final int TOBRUK_ID = 2;
    public static final String TOBRUK = "map.tobruk";
    public static final String TOBRUK_FILE = "tobruk.xml";
    public static final int PEARL_HARBOR_ID = 3;
    public static final String PEARL_HARBOR = "map.pearl_harbor";
    public static final String PEARL_HARBOR_FILE = "pearl_harbor.xml";
    public static final int HONG_KONG_ID = 4;
    public static final String HONG_KONG = "map.hong_kong";
    public static final String HONG_KONG_FILE = "hong_kong.xml";
    public static final int MIDWAY_ID = 5;
    public static final String MIDWAY = "map.midway";
    public static final String MIDWAY_FILE = "midway.xml";
    public static final int STALINGRAD_ID = 6;
    public static final String STALINGRAD = "map.stalingrad";
    public static final String STALINGRAD_FILE = "stalingrad.xml";
    public static final int GUADALCANAL_ID = 7;
    public static final String GUADALCANAL = "map.guadalcanal";
    public static final String GUADALCANAL_FILE = "guadalcanal.xml";
    public static final int EL_ALAMEIN_ID = 8;
    public static final String EL_ALAMEIN = "map.el_alamein";
    public static final String EL_ALAMEIN_FILE = "el_alamein.xml";
    public static final int CAPRI_ID = 9;
    public static final String CAPRI = "map.capri";
    public static final String CAPRI_FILE = "capri.xml";
    public static final int KURSK_ID = 10;
    public static final String KURSK = "map.kursk";
    public static final String KURSK_FILE = "kursk.xml";
    public static final int HUSKY_ID = 11;
    public static final String HUSKY = "map.husky";
    public static final String HUSKY_FILE = "husky.xml";
    public static final int KHARKOV_ID = 12;
    public static final String KHARKOV = "map.kharkov";
    public static final String KHARKOV_FILE = "kharkov.xml";
    public static final int LENINGRAD_ID = 13;
    public static final String LENINGRAD = "maps.leningrad";
    public static final String LENINGRAD_FILE = "leningrad.xml";
    public static final int CRIMEA_ID = 14;
    public static final String CRIMEA = "map.crimea";
    public static final String CRIMEA_FILE = "crimea.xml";
    public static final int ROMA_ID = 15;
    public static final String ROMA = "map.roma";
    public static final String ROMA_FILE = "roma.xml";
    public static final int OMAHA_BEACH_ID = 16;
    public static final String OMAHA_BEACH = "map.omaha_beach";
    public static final String OMAHA_BEACH_FILE = "omaha_beach.xml";
    public static final int PARIS_ID = 17;
    public static final String PARIS = "map.paris";
    public static final String PARIS_FILE = "paris.xml";
    public static final int MARKET_GARDEN_ID = 18;
    public static final String MARKET_GARDEN = "map.market_garden";
    public static final String MARKET_GARDEN_FILE = "market_garden.xml";
    public static final int BULGE_ID = 19;
    public static final String BULGE = "map.bulge";
    public static final String BULGE_FILE = "bulge.xml";
    public static final int LUZON_ID = 20;
    public static final String LUZON = "map.luzon";
    public static final String LUZON_FILE = "luzon.xml";
    public static final int IWO_JIMA_ID = 21;
    public static final String IWO_JIMA = "map.iwo_jima";
    public static final String IWO_JIMA_FILE = "iwo_jima.xml";
    public static final int VIENNA_ID = 22;
    public static final String VIENNA = "map.vienna";
    public static final String VIENNA_FILE = "vienna.xml";
    public static final int OKINAWA_ID = 23;
    public static final String OKINAWA = "map.okinawa";
    public static final String OKINAWA_FILE = "okinawa.xml";
    public static final int BERLIN_ID = 24;
    public static final String BERLIN = "map.berlin";
    public static final String BERLIN_FILE = "berlin.xml";
    public static final int SAKHALIN_ID = 25;
    public static final String SAKHALIN = "map.sakhalin";
    public static final String SAKHALIN_FILE = "sakhalin.xml";

    private int id;
    private String name;
    private String file;
    private int type;
    private boolean locked;
    private int linkedMissionId;

    private Tile[][] tiles;
    private Building[][] buildings;
    private int size;

    private int playersAmount;
    private int capitalAmount;
    private int cityAmount;
    private int factoryAmount;
    private int airportAmount;
    private int shipyardAmount;

    public Map(int id, String name, String file, int type, boolean locked, int linkedMissionId) {
        this.id = id;
        this.name = name;
        this.file = file;
        this.type = type;
        this.locked = locked;
        this.linkedMissionId = linkedMissionId;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getFile() {
        return this.file;
    }

    public int getType() {
        return this.type;
    }

    public boolean isLocked() {
        return this.locked;
    }

    public int getLinkedMissionId() {
        return this.linkedMissionId;
    }

    public String toString() {
        return this.id + ". " + this.name + " - " + this.type + " : " + this.locked + " => " + this.linkedMissionId;
    }

    public void initMap(ArrayList<Season> seasons, ArrayList<Player> players) {
        try {
            MapFile file = new MapFile(this.getType(), MapFile.FOLDER + this.file);
            this.size = file.getSize();
            this.tiles = new Tile[this.size][this.size];
            this.buildings = new Building[this.size][this.size];
            for (XmlReader.Element e : file.getTileLayer()) {
                int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
                int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
                int season = Integer.parseInt("" + e.getAttribute(MapFile.XML_SEASON));
                int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
                int subtype = Integer.parseInt("" + e.getAttribute(MapFile.XML_SUBTYPE));
                this.tiles[row][col] = new Tile(row, col, seasons.get(season - 1), type, subtype);
            }
            for (XmlReader.Element e : file.getBuildingLayer()) {
                int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
                int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
                int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
                int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
                int subtype = Integer.parseInt("" + e.getAttribute(MapFile.XML_SUBTYPE));
                this.buildings[row][col] = Building.createBuilding(row, col, players.get(player), type, subtype);
            }
            this.setPlayersAmount(file.getPlayers());
            this.setCapitalAmount(file.getCapitals());
            this.setCityAmount(file.getCities());
            this.setFactoryAmount(file.getFactories());
            this.setAirportAmount(file.getAirports());
            this.setShipyardAmount(file.getShipyards());
            file.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMap(int size, ArrayList<Season> seasons, ArrayList<XmlReader.Element> tileLayer, ArrayList<Building> buildings) {
        this.size = size;
        this.tiles = new Tile[this.size][this.size];
        this.buildings = new Building[this.size][this.size];
        for (XmlReader.Element e : tileLayer) {
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int season = Integer.parseInt("" + e.getAttribute(MapFile.XML_SEASON));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            int subtype = Integer.parseInt("" + e.getAttribute(MapFile.XML_SUBTYPE));
            this.tiles[row][col] = new Tile(row, col, seasons.get(season - 1), type, subtype);
        }
        for (Building building : buildings) {
            this.buildings[building.getRow()][building.getCol()] = building;
        }
    }

    public void updateMap(int size, Tile[][] tiles, Building[][] buildings, int playersAmount) {
        this.size = size;
        this.tiles = tiles;
        this.buildings = buildings;
        this.playersAmount = playersAmount;
    }

    public Tile[][] getTiles() {
        return this.tiles;
    }

    public Tile getTile(int row, int col) {
        return this.tiles[row][col];
    }

    public Building[][] getBuildings() {
        return this.buildings;
    }

    public Building getBuilding(int row, int col) {
        return this.buildings[row][col];
    }

    public int getSize() {
        return this.size;
    }

    public void renderTile(SpriteBatch batch, float delta, int row, int col) {
        tiles[row][col].render(batch, delta, null);
    }

    public void renderBuilding(SpriteBatch batch, float delta, int row, int col) {
        if (buildings[row][col] != null) {
            buildings[row][col].render(batch, delta, null);
        }
    }

    public void renderSmall(SpriteBatch batch, float x, float y, float width) {
        for (int row = this.size - 1; row >= 0; row--) {
            for (int col = this.size - 1; col >= 0; col--) {
                tiles[row][col].renderSmall(batch, x, y, width, this.size);
                if (buildings[row][col] != null) {
                    buildings[row][col].renderSmall(batch, x, y, width, this.size);
                }
            }
        }
    }

    public int getPlayersAmount() {
        return playersAmount;
    }

    public void setPlayersAmount(int playersAmount) {
        this.playersAmount = playersAmount;
    }

    public int getCapitalAmount() {
        return capitalAmount;
    }

    public void setCapitalAmount(int capitalAmount) {
        this.capitalAmount = capitalAmount;
    }

    public int getCityAmount() {
        return cityAmount;
    }

    public void setCityAmount(int cityAmount) {
        this.cityAmount = cityAmount;
    }

    public int getFactoryAmount() {
        return factoryAmount;
    }

    public void setFactoryAmount(int factoryAmount) {
        this.factoryAmount = factoryAmount;
    }

    public int getAirportAmount() {
        return airportAmount;
    }

    public void setAirportAmount(int airportAmount) {
        this.airportAmount = airportAmount;
    }

    public int getShipyardAmount() {
        return shipyardAmount;
    }

    public void setShipyardAmount(int shipyardAmount) {
        this.shipyardAmount = shipyardAmount;
    }

    public void setBuilding(int row, int col, Building building) {
        this.buildings[row][col] = building;
    }

    public void updateName(String name) {
        this.name = name;
        this.file = name + ".xml";
    }
}


