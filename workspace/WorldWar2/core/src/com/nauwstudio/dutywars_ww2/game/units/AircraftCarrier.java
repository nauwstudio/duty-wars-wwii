package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class AircraftCarrier extends Unit {

	public static final String NAME = "aircraft_carrier";
	public static final String DESC = "aircraft_carrier_desc";

	public AircraftCarrier(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, AIRCRAFT_CARRIER, Kind.NAVAL, AIRCRAFT_CARRIER_PRICE, AIRCRAFT_CARRIER_MOVE_SCOPE, AIRCRAFT_CARRIER_ATTACK_SCOPE, LIFE, AIRCRAFT_CARRIER_AMMO, AIRCRAFT_CARRIER_GAS, AIRCRAFT_CARRIER_DMG, owner.getArmy().getGameAssets().battleship_fire, owner.getArmy().getGameAssets().boat_move);
		}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().aircraft_carrier_S;
			case WEST:
				return this.owner.getArmy().getAssets().aircraft_carrier_W;
			case NORTH:
				return this.owner.getArmy().getAssets().aircraft_carrier_N;
			case EAST:
				return this.owner.getArmy().getAssets().aircraft_carrier_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		return getTextureIdle();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().aircraft_carrier_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_aircraft_carrier;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_aircraft_carrier_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_aircraft_carrier_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
