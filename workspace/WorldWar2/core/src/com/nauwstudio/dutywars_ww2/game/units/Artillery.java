package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Player;

public class Artillery extends Unit {

	public static final String NAME = "artillery";
	public static final String DESC = "artillery_desc";

	public Artillery(int row, int col, Player owner, Orientation orientation) {
		super(row, col, orientation, owner, ARTILLERY, Kind.GROUND, ARTILLERY_PRICE, ARTILLERY_MOVE_SCOPE, ARTILLERY_ATTACK_SCOPE, LIFE, ARTILLERY_AMMO, ARTILLERY_GAS, ARTILLERY_DMG, owner.getArmy().getGameAssets().artillery_fire, owner.getArmy().getGameAssets().artillery_move);
	}

	@Override
	public TextureRegion getTextureIdle() {
		switch (orientation) {
			case SOUTH:
				return this.owner.getArmy().getAssets().artillery_S;
			case WEST:
				return this.owner.getArmy().getAssets().artillery_W;
			case NORTH:
				return this.owner.getArmy().getAssets().artillery_N;
			case EAST:
				return this.owner.getArmy().getAssets().artillery_E;
			default:
				return getTexture();
		}
	}

	@Override
	public TextureRegion getTextureFire(float delta) {
		switch (orientation) {
			case SOUTH:
				return (TextureRegion) this.owner.getArmy().getAssets().artillery_S_fire.getKeyFrame(delta);
			case WEST:
				return (TextureRegion) this.owner.getArmy().getAssets().artillery_W_fire.getKeyFrame(delta);
			case NORTH:
				return (TextureRegion) this.owner.getArmy().getAssets().artillery_N_fire.getKeyFrame(delta);
			case EAST:
				return (TextureRegion) this.owner.getArmy().getAssets().artillery_E_fire.getKeyFrame(delta);

		}
		return getTexture();
	}

	@Override
	public TextureRegion getTexture() {
		return this.owner.getArmy().getAssets().artillery_HD;
	}

	@Override
	public TextureRegion getButtonTexture() {
		return this.owner.getArmy().getAssets().button_artillery;
	}

	@Override
	public TextureRegion getButtonPressedTexture() {
		return this.owner.getArmy().getAssets().button_artillery_pressed;
	}

	@Override
	public TextureRegion getButtonDisabledTexture() {
		return this.owner.getArmy().getAssets().button_artillery_disabled;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESC;
	}
}
