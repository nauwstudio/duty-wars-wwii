package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.XmlReader;
import com.nauwstudio.dutywars_ww2.*;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;
import com.nauwstudio.dutywars_ww2.settings.Settings;

import java.util.ArrayList;

public class Game {

    public static final int MODE_VERSUS = 1;
    public static final int MODE_CAMPAIGN = 2;

    public enum GameState {
        SWITCH, RUNNING, END, MENU, STAT, INFO, IA_PLAYING
    }

    ;

    private GameScreen screen;

    private GameState state;

    private int mode;
    private Map map;

    private Tile selectedTile;
    private com.nauwstudio.dutywars_ww2.game.buildings.Building[][] buildingLayer;
    private com.nauwstudio.dutywars_ww2.game.buildings.Building selectedBuilding;
    private Square[][] squareLayer;
    private com.nauwstudio.dutywars_ww2.game.units.Unit[][] unitLayer;
    private com.nauwstudio.dutywars_ww2.game.units.Unit selectedUnit;
    private com.nauwstudio.dutywars_ww2.game.units.Unit infoUnit;

    private ArrayList<ButtonCircle> bottomButtons;
    private ArrayList<Button> fixedButtons;
    private ArrayList<CheckBox> checkBoxes;
    private ArrayList<RadioGroup> radioGroups;

    private com.nauwstudio.dutywars_ww2.game.Player playerGaia;
    private ArrayList<com.nauwstudio.dutywars_ww2.game.Player> players;
    private com.nauwstudio.dutywars_ww2.game.Player currentPlayer;

    private int turn;
    private boolean saveGame;

    private GameAssets assets;
    private GameHUDVars hudVars;

    private int endGameStarShown = 0;

    public Game(GameScreen screen, GameAssets assets, Map map, ArrayList<Player> players, int mode) {
        this.screen = screen;
        this.assets = assets;
        this.mode = mode;
        this.playerGaia = players.get(0);
        players.remove(0);
        this.players = players;
        this.map = map;
        try {
            MapFile mapFile = new MapFile(this.map.getType(), MapFile.FOLDER + this.map.getFile());
            // INIT WORLD LAYERS
            System.out.println("INIT BUILDING");
            initBuildingLayer(mapFile.getBuildingLayer());
            System.out.println("INIT SQUARE");
            initSquareLayer();
            System.out.println("INIT UNIT");
            initUnitLayer(mapFile.getUnitLayer(), mapFile.getPassengerLayer());
            // KILLING PLAYER WITHOUT UNITS AND BUILDINGS
            for(Player player : this.players) {
                if (player.buildings.isEmpty() && player.units.isEmpty()) {
                    killPlayer(player);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // INIT HUD
        this.hudVars = new GameHUDVars(this);
        // INIT BUTTONS
        createMainButtons();

        start(0);
    }

    public Game(GameScreen screen, GameAssets assets, Map map, Save save) {
        this.screen = screen;
        this.assets = assets;
        this.mode = save.getMode();
        this.playerGaia = save.getPlayers().get(0);
        save.getPlayers().remove(0);
        this.players = save.getPlayers();
        this.map = map;
        // INIT WORLD LAYERS
        initBuildingLayerFromSave(save);
        initSquareLayer();
        initUnitLayerFromSave(save);
        // INIT HUD
        this.hudVars = new GameHUDVars(this);
        // INIT BUTTONS
        createMainButtons();

        start(save.getTurn());
    }

    public void initBuildingLayer(ArrayList<XmlReader.Element> layer) {
        this.buildingLayer = new Building[this.map.getSize()][this.map.getSize()];
        for (XmlReader.Element e : layer) {
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            int subtype = Integer.parseInt("" + e.getAttribute(MapFile.XML_SUBTYPE));
            createBuilding(row, col, getPlayerFromOrder(player), type, subtype);
        }
        // Check for "Only army players"
        for (Player player : this.players) {
            player.setOnlyArmyPlayer(!player.haveCapital());
        }

    }

    public void initBuildingLayerFromSave(Save save) {
        this.buildingLayer = new Building[this.map.getSize()][this.map.getSize()];
        for (Building building : save.getBuildings()) {
            addBuilding(building, building.getOwner());
        }
        for (Player player : this.players) {
            player.setOnlyArmyPlayer(!player.haveCapital());
        }
    }

    public Building createBuilding(int row, int col, Player player, int type, int subType) {
        Building building = Building.createBuilding(row, col, player, type, subType);
        player.addBuilding(building);
        this.getBuildingLayer()[row][col] = building;
        return building;
    }

    public void addBuilding(Building building, Player player) {
        this.getBuildingLayer()[building.getRow()][building.getCol()] = building;
        player.addBuilding(building);
    }

    public void initSquareLayer() {
        this.squareLayer = new Square[this.map.getSize()][this.map.getSize()];
        for (int row = 0; row < this.map.getSize(); row++) {
            for (int col = 0; col < this.map.getSize(); col++) {
                this.squareLayer[row][col] = new Square(row, col);
            }
        }
    }

    public void initUnitLayer(ArrayList<XmlReader.Element> unit_layer, ArrayList<XmlReader.Element> passenger_layer) {
        this.unitLayer = new Unit[this.map.getSize()][this.map.getSize()];
        for (XmlReader.Element e : unit_layer) {
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            Unit.Orientation orientation = Unit.valueToOrientation(Integer.parseInt("" + e.getAttribute(MapFile.XML_ORIENTATION)));
            createUnit(row, col, getPlayerFromOrder(player), type, orientation);
        }
        for (XmlReader.Element e : passenger_layer) {
            int row = Integer.parseInt("" + e.getAttribute(MapFile.XML_ROW));
            int col = Integer.parseInt("" + e.getAttribute(MapFile.XML_COL));
            int player = Integer.parseInt("" + e.getAttribute(MapFile.XML_PLAYER));
            int type = Integer.parseInt("" + e.getAttribute(MapFile.XML_TYPE));
            Unit.Orientation orientation = Unit.valueToOrientation(Integer.parseInt("" + e.getAttribute(MapFile.XML_ORIENTATION)));
            createUnit(row, col, getPlayerFromOrder(player), type, orientation);
        }
    }

    public void initUnitLayerFromSave(Save save) {
        this.unitLayer = new Unit[this.map.getSize()][this.map.getSize()];
        for (Unit unit : save.getUnits()) {
            addUnit(unit, unit.getOwner());
        }
    }

    public Player getPlayerFromOrder(int order) {
        if (order == 0) {
            return this.playerGaia;
        } else {
            for (Player player : this.players) {
                if (player.getOrder() == order) {
                    return player;
                }
            }
            // NO ORDER
            return this.playerGaia;
        }
    }

    public Unit createUnit(int row, int col, Player player, int type, Unit.Orientation orientation) {
        Unit unit = Unit.createUnit(row, col, player, type, orientation);
        if (this.getUnitLayer()[row][col] != null && unit.canEmbarkIn(this.getUnitLayer()[row][col])) {
            this.getUnitLayer()[row][col].addPassenger(unit);
        } else {
            this.getUnitLayer()[row][col] = unit;
            player.addUnit(unit);
        }
        // Update unit defence
        Building b = getBuildingLayer()[row][col];
        if (b == null) {
            unit.updateDefence(this.map.getTile(row, col).getDefence());
        } else {
            unit.updateDefence(b.getDefence());
        }
        return unit;
    }

    public void addUnit(Unit unit, Player player) {
        if (this.getUnitLayer()[unit.getRow()][unit.getCol()] != null && unit.canEmbarkIn(this.getUnitLayer()[unit.getRow()][unit.getCol()])) {
            this.getUnitLayer()[unit.getRow()][unit.getCol()].addPassenger(unit);
        } else {
            this.getUnitLayer()[unit.getRow()][unit.getCol()] = unit;
            player.addUnit(unit);
        }
        // Update unit defence
        Building b = getBuildingLayer()[unit.getRow()][unit.getCol()];
        if (b == null) {
            unit.updateDefence(this.map.getTile(unit.getRow(), unit.getCol()).getDefence());
        } else {
            unit.updateDefence(b.getDefence());
        }
    }

    public void start(int turn) {
        this.state = GameState.RUNNING;
        this.turn = turn - 1;
        this.saveGame = false;
        finishTurn();
    }

    public void resume() {
        // Game is ended
        if (this.isEndOfGame()) {
            createEndGameButtons();
            this.state = GameState.END;
        }
        // Robot is playing
        else if (!this.currentPlayer.isHuman()) {
            clearButtons();
            this.state = GameState.IA_PLAYING;
        }
        // Human is playing
        else {
            if (this.selectedBuilding != null) {
                this.createBuildingButtons(this.selectedBuilding);
            } else if (this.selectedUnit != null) {
                if (this.selectedUnit.isFeetUnit()) {
                    createFeetUnitButtons(this.selectedUnit);
                } else if (this.selectedUnit.isTransportUnit()) {
                    if (this.selectedUnit.isReloadUnit()) {
                        createReloadUnitButtons(this.selectedUnit);
                    } else {
                        createTransportUnitButtons(this.selectedUnit);
                    }
                } else {
                    createUnitButtons(this.selectedUnit);
                }
            } else if (this.selectedTile != null) {
                this.createTileButtons();
            } else {
                this.createMainButtons();
            }
            this.state = GameState.RUNNING;
        }
    }

    public void robotPlay() {
        this.clearButtons();
        this.state = GameState.IA_PLAYING;
        this.currentPlayer.play(this);
    }

    public void switchMode() {
        this.clearButtons();
        this.state = GameState.SWITCH;
    }

    public GameState getState() {
        return this.state;
    }

    public boolean isRunning() {
        return this.state == GameState.RUNNING;
    }

    public boolean isRobotPlaying() {
        return this.state == GameState.IA_PLAYING;
    }

    public boolean isMenu() {
        return this.state == GameState.MENU;
    }

    public boolean isStat() {
        return this.state == GameState.STAT;
    }

    public boolean isInfo() {
        return this.state == GameState.INFO;
    }

    public boolean isSwitch() {
        return this.state == GameState.SWITCH;
    }

    public boolean isEnded() {
        return this.state == GameState.END;
    }

    public void update(float delta) {
        updateRunning(delta);
    }

    private void updateRunning(float delta) {
        for (Player p : this.players) {
            for (Unit u : p.getUnits()) {
                u.update(delta);
            }
        }
    }

    public Player getGaiaPlayer() {
        return this.playerGaia;
    }

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    public Map getMap() {
        return this.map;
    }

    public void createMainButtons() {
        clearButtons();
        // MENU BUTTON
        Vector2 menu_pos = new Vector2(hudVars.main3_col1, hudVars.main_line);
        this.bottomButtons.add(new ButtonCircle(menu_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MENU, getMainAssets().button_menu, getMainAssets().button_menu_pressed, getMainAssets().button_menu));
        // STAT BUTTON
        Vector2 stat_pos = new Vector2(hudVars.main3_col2, hudVars.main_line);
        this.bottomButtons.add(new ButtonCircle(stat_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_STAT, getMainAssets().button_stat, getMainAssets().button_stat_pressed, getMainAssets().button_stat));
        // NEXT BUTTON
        Vector2 next_pos = new Vector2(hudVars.main3_col3, hudVars.main_line);
        this.bottomButtons.add(new ButtonCircle(next_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_NEXT, getMainAssets().button_end, getMainAssets().button_end_pressed, getMainAssets().button_end));
    }

    public void createEndGameButtons() {
        clearButtons();
        // EXIT BUTTON
        Vector2 exit_pos = new Vector2(hudVars.button_finish_game_x, hudVars.button_finish_game_y);
        this.fixedButtons.add(new ButtonRectangle(exit_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_EXIT, getMainAssets().button_exit_game, getMainAssets().button_exit_game_pressed, getMainAssets().button_exit_game));
    }

    public void createStatButtons() {
        clearButtons();
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_menu_x, hudVars.close_menu_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
    }

    public void createMenuButtons() {
        clearButtons();
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_menu_x, hudVars.close_menu_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        FileHandle baseFileHandle = Gdx.files.internal("bundles/string");
        I18NBundle stringBundle = I18NBundle.createBundle(baseFileHandle);
        // MUSIC CHECKBOX
        Vector2 music_pos = new Vector2(hudVars.music_checkbox_x, hudVars.music_checkbox_y);
        this.checkBoxes.add(new CheckBox(music_pos, hudVars.music_checkbox_width, hudVars.music_checkbox_height, CheckBox.CHECKBOX_MUSIC, stringBundle.get("settings.music"), screen.getController().getMainAssets().checkBox, screen.getController().getMainAssets().checkBox_pressed, screen.getController().getMainAssets().checkBox_checked, screen.getController().getMainAssets().checkBox_checked_pressed));
        if (this.screen.getController().isMusic()) {
            this.checkBoxes.get(0).check();
        }
        // SOUNDS CHECKBOX
        Vector2 sound_pos = new Vector2(hudVars.effects_checkbox_x, hudVars.effects_checkbox_y);
        this.checkBoxes.add(new CheckBox(sound_pos, hudVars.effects_checkbox_width, hudVars.effects_checkbox_height, CheckBox.CHECKBOX_SOUND, stringBundle.get("settings.effects"), screen.getController().getMainAssets().checkBox, screen.getController().getMainAssets().checkBox_pressed, screen.getController().getMainAssets().checkBox_checked, screen.getController().getMainAssets().checkBox_checked_pressed));
        if (this.screen.getController().isSound()) {
            this.checkBoxes.get(1).check();
        }
        // USER SPEED RADIO GROUP
        String[] speedTexts = new String[]{getMainAssets().stringBundle.get("settings.slow"), getMainAssets().stringBundle.get("settings.normal"), getMainAssets().stringBundle.get("settings.fast")};
        Vector2 user_speed_pos = new Vector2(hudVars.speed_user_x, hudVars.speed_user_y);
        this.radioGroups.add(new RadioGroup(user_speed_pos, hudVars.speed_user_width, hudVars.speed_user_height, RadioGroup.RADIO_GROUP_USER, Settings.SPEED_TAGS, speedTexts, Unit.getHumanSpeedPosition(), this.getMainAssets()));
        // ROBOT SPEED RADIO GROUP
        Vector2 robot_speed_pos = new Vector2(hudVars.speed_robot_x, hudVars.speed_robot_y);
        this.radioGroups.add(new RadioGroup(robot_speed_pos, hudVars.speed_robot_width, hudVars.speed_robot_height, RadioGroup.RADIO_GROUP_ROBOT, Settings.SPEED_TAGS, speedTexts, Unit.getRobotSpeedPosition(), this.getMainAssets()));

        // HELP BUTTON
        Vector2 help_pos = new Vector2(hudVars.menu_help_x, hudVars.menu_help_y);
        this.fixedButtons.add(new ButtonRectangle(help_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_HELP, getMainAssets().button_help, getMainAssets().button_help_pressed, getMainAssets().button_help));
        // EXIT BUTTON
        Vector2 exit_pos = new Vector2(hudVars.menu_exit_x, hudVars.menu_exit_y);
        this.fixedButtons.add(new ButtonRectangle(exit_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_EXIT, getMainAssets().button_exit_game, getMainAssets().button_exit_game_pressed, getMainAssets().button_exit_game));
    }

    public void createInfoButtons() {
        clearButtons();
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_menu_x, hudVars.close_menu_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
    }

    public void createTileButtons() {
        clearButtons();
        Vector2 close_pos = new Vector2(hudVars.close_tile_x, hudVars.close_tile_y);
        this.fixedButtons.add(new ButtonCircle(close_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
    }

    public void createFactoryButtons(com.nauwstudio.dutywars_ww2.game.Player player) {
        clearButtons();
        this.screen.getRenderer().resetButtonCamera();
        if (player.equals(this.currentPlayer)) {
            // FEET UNIT BUTTONS
            // Infantry
            Vector2 infantry_pos = new Vector2(hudVars.create_col1, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(infantry_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.INFANTRY, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Bazooka
            Vector2 bazooka_pos = new Vector2(hudVars.create_col2, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(bazooka_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.BAZOOKA, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Sniper
            Vector2 sniper_pos = new Vector2(hudVars.create_col3, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(sniper_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.SNIPER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // VEHICLE UNIT BUTTONS
            // Jeep
            Vector2 jeep_pos = new Vector2(hudVars.create_col4, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(jeep_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.JEEP, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Light Tank
            Vector2 light_pos = new Vector2(hudVars.create_col5, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(light_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.LIGHT_TANK, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Heavy Tank
            Vector2 heavy_pos = new Vector2(hudVars.create_col6, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(heavy_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.HEAVY_TANK, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Truck
            Vector2 truck_pos = new Vector2(hudVars.create_col7, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(truck_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, com.nauwstudio.dutywars_ww2.game.units.Unit.createUnit(0, 0, player, com.nauwstudio.dutywars_ww2.game.units.Unit.TRUCK, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Artillery
            Vector2 artillery_pos = new Vector2(hudVars.create_col8, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(artillery_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, com.nauwstudio.dutywars_ww2.game.units.Unit.createUnit(0, 0, player, com.nauwstudio.dutywars_ww2.game.units.Unit.ARTILLERY, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Halftrack
            Vector2 halftrack_pos = new Vector2(hudVars.create_col9, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(halftrack_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, com.nauwstudio.dutywars_ww2.game.units.Unit.createUnit(0, 0, player, com.nauwstudio.dutywars_ww2.game.units.Unit.HALFTRACK, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
            this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        } else {
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_tile_x, hudVars.close_tile_y);
            this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        }
    }

    public void createAirportButtons(com.nauwstudio.dutywars_ww2.game.Player player) {
        clearButtons();
        if (player.equals(this.currentPlayer)) {
            // Fighter
            Vector2 fighter_pos = new Vector2(hudVars.create_col1, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(fighter_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.FIGHTER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Bomber
            Vector2 bomber_pos = new Vector2(hudVars.create_col2, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(bomber_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.BOMBER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Transporter
            Vector2 transporter_pos = new Vector2(hudVars.create_col3, hudVars.create_line);
            this.bottomButtons.add(new com.nauwstudio.dutywars_ww2.game.UnitButton(transporter_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.TRANSPORTER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
            this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        } else {
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_tile_x, hudVars.close_tile_y);
            this.bottomButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        }
    }

    public void createShipyardButtons(com.nauwstudio.dutywars_ww2.game.Player player) {
        clearButtons();
        if (player.equals(this.currentPlayer)) {
            // Lander
            Vector2 lander_pos = new Vector2(hudVars.create_col1, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(lander_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.LANDER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Destroyer
            Vector2 destroyer_pos = new Vector2(hudVars.create_col2, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(destroyer_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.DESTROYER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Battleship
            Vector2 battleship_pos = new Vector2(hudVars.create_col3, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(battleship_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.BATTLESHIP, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // Aicraft carrier
            Vector2 aircraft_carrier_pos = new Vector2(hudVars.create_col4, hudVars.create_line);
            this.bottomButtons.add(new UnitButton(aircraft_carrier_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_UNIT, Unit.createUnit(0, 0, player, Unit.AIRCRAFT_CARRIER, Unit.Orientation.SOUTH), screen.getController().getMainAssets().button_info, screen.getController().getMainAssets().button_info_pressed));
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
            this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        } else {
            // CLOSE BUTTON
            Vector2 stat_pos = new Vector2(hudVars.close_tile_x, hudVars.close_tile_y);
            this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
        }
    }

    public void createUnitButtons(com.nauwstudio.dutywars_ww2.game.units.Unit unit) {
        clearButtons();
        if (unit.getOwner().equals(this.currentPlayer)) {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main2_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Attack scope
            Vector2 attack_pos = new Vector2(hudVars.main2_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(attack_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ATTACK, getMainAssets().button_next_mission, getMainAssets().button_next_mission_pressed, getMainAssets().button_next_mission));
        } else {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main3_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Move scope
            Vector2 move_pos = new Vector2(hudVars.main3_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(move_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MOVE, getMainAssets().button_move_scope, getMainAssets().button_move_scope_pressed, getMainAssets().button_move_scope));
            // Attack scope
            Vector2 attack_pos = new Vector2(hudVars.main3_col3, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(attack_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ATTACK, getMainAssets().button_next_mission, getMainAssets().button_next_mission_pressed, getMainAssets().button_next_mission));
        }
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
    }

    public void createFeetUnitButtons(com.nauwstudio.dutywars_ww2.game.units.Unit unit) {
        clearButtons();
        if (unit.getOwner().equals(this.currentPlayer)) {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main3_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Capture
            Vector2 capture_pos = new Vector2(hudVars.main3_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(capture_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_CAPTURE, getMainAssets().button_capture, getMainAssets().button_capture_pressed, getMainAssets().button_capture_disabled));
            // Disable capture button
            if (!unit.canCapture(this)) {
                this.bottomButtons.get(1).disable();
            }
            // Attack scope
            Vector2 attack_pos = new Vector2(hudVars.main3_col3, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(attack_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ATTACK, getMainAssets().button_next_mission, getMainAssets().button_next_mission_pressed, getMainAssets().button_next_mission));
        } else {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main3_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Move scope
            Vector2 move_pos = new Vector2(hudVars.main3_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(move_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MOVE, getMainAssets().button_move_scope, getMainAssets().button_move_scope_pressed, getMainAssets().button_move_scope));
            // Attack scope
            Vector2 attack_pos = new Vector2(hudVars.main3_col3, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(attack_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ATTACK, getMainAssets().button_next_mission, getMainAssets().button_next_mission_pressed, getMainAssets().button_next_mission));
        }
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));
    }

    public void createTransportUnitButtons(com.nauwstudio.dutywars_ww2.game.units.Unit unit) {
        clearButtons();
        if (unit.getOwner().equals(this.currentPlayer)) {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main2_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Drop
            Vector2 drop_pos = new Vector2(hudVars.main2_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(drop_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_DROP, getMainAssets().button_drop, getMainAssets().button_drop_pressed, getMainAssets().button_drop_disabled));
            // Disable drop button
            if (unit.isStop() || !unit.canDrop(this)) {
                this.bottomButtons.get(1).disable();
            }
        } else {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main2_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Move scope
            Vector2 move_pos = new Vector2(hudVars.main2_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(move_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MOVE, getMainAssets().button_move_scope, getMainAssets().button_move_scope_pressed, getMainAssets().button_move_scope));
        }
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));

    }

    public void createReloadUnitButtons(com.nauwstudio.dutywars_ww2.game.units.Unit unit) {
        clearButtons();
        if (unit.getOwner().equals(this.currentPlayer)) {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main4_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Drop
            Vector2 drop_pos = new Vector2(hudVars.main4_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(drop_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_DROP, getMainAssets().button_drop, getMainAssets().button_drop_pressed, getMainAssets().button_drop_disabled));
            // Reload
            Vector2 reload_pos = new Vector2(hudVars.main4_col3, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(reload_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_RELOAD, getMainAssets().button_reload, getMainAssets().button_reload_pressed, getMainAssets().button_reload_disabled));
            // Disable drop and reload button
            if (unit.isStop() || !unit.canDrop(this)) {
                this.bottomButtons.get(1).disable();
            }
            if (unit.isStop() || !unit.canReload(this)) {
                this.bottomButtons.get(2).disable();
            }
            // Move scope
            Vector2 move_pos = new Vector2(hudVars.main4_col4, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(move_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MOVE, getMainAssets().button_move_scope, getMainAssets().button_move_scope_pressed, getMainAssets().button_move_scope));
        } else {
            // Info
            Vector2 info_pos = new Vector2(hudVars.main2_col1, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(info_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_INFO, getMainAssets().button_info, getMainAssets().button_info_pressed, getMainAssets().button_info));
            // Move scope
            Vector2 move_pos = new Vector2(hudVars.main2_col2, hudVars.main_line);
            this.bottomButtons.add(new ButtonCircle(move_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_MOVE, getMainAssets().button_move_scope, getMainAssets().button_move_scope_pressed, getMainAssets().button_move_scope));
        }
        // CLOSE BUTTON
        Vector2 stat_pos = new Vector2(hudVars.close_unit_x, hudVars.close_unit_y);
        this.fixedButtons.add(new ButtonCircle(stat_pos, 0.6f * Util.getCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, getMainAssets().button_close, getMainAssets().button_close_pressed, getMainAssets().button_close));

    }

    public int getMapSize() {
        return this.map.getSize();
    }

    public float getMapWidth() {
        return getMapSize() * Util.getTileWidth();
    }

    public float getMapHeight() {
        return getMapSize() * Util.getTileHeight();
    }

    public Square getTouchedSquare(float world_x, float world_y) {
        for (int row = this.map.getSize() - 1; row >= 0; row--) {
            for (int col = this.map.getSize() - 1; col >= 0; col--) {
                if (squareLayer[row][col].isTouched(world_x, world_y)) {
                    return squareLayer[row][col];
                }
            }
        }
        return null;
    }

    public void touchSquare(Square square) {
        // TOUCH UNITS
        if (unitLayer[square.getRow()][square.getCol()] != null) {
            touchUnit(square);
        }
        // TOUCH BUILDING ?
        else if (buildingLayer[square.getRow()][square.getCol()] != null) {
            touchBuilding(square);
        }
        // TOUCH TILE ?
        else {
            touchTile(square);
        }
    }

    public void touchUnit(Square square) {
        // Attack ?
        if (this.selectedUnit != null && square.isRedTarget()) {
            attackUnit(this.selectedUnit, square.getRow(), square.getCol());
        }
        // Embark ?
        else if (this.selectedUnit != null && square.isGreenTarget()) {
            embarkUnit(this.selectedUnit, square.getRow(), square.getCol());
        } else {
            selectUnit(square);
        }
    }

    public void touchBuilding(Square square) {
        if (this.selectedUnit != null && square.isBlueTarget()) {
            moveUnit(this.selectedUnit, square.getRow(), square.getCol());
        } else if (this.selectedUnit != null && square.isGreenTarget()) {
            dropUnit(this.selectedUnit, square.getRow(), square.getCol());
        } else {
            selectBuilding(square);
        }
    }

    public void touchTile(Square square) {
        // Tile is in move scope of selected unit
        if (this.selectedUnit != null && square.isBlueTarget()) {
            moveUnit(this.selectedUnit, square.getRow(), square.getCol());
        } else if (this.selectedUnit != null && square.isGreenTarget() && this.selectedUnit.getPassenger() != null) {
            dropUnit(this.selectedUnit, square.getRow(), square.getCol());
        } else {
            selectTile(square);
        }
    }

    public void clearSquareLayer() {
        for (int row = this.squareLayer.length - 1; row >= 0; row--) {
            for (int col = this.squareLayer.length - 1; col >= 0; col--) {
                this.squareLayer[row][col].release();
            }
        }
    }

    public Square[][] getSquareLayer() {
        return this.squareLayer;
    }

    public void unselectAll() {
        this.selectedUnit = null;
        this.selectedTile = null;
        if (this.infoUnit != null && this.selectedBuilding != null) {
            this.infoUnit = null;
        } else {
            this.selectedBuilding = null;
            this.infoUnit = null;
        }
        clearSquareLayer();
        createMainButtons();
    }

    public void selectTile(Square square) {
        unselectAll();
        square.select();
        Tile tile = this.map.getTile(square.getRow(), square.getCol());
        this.selectedTile = tile;
        // Buttons
        createTileButtons();
    }

    public void unselectTile() {
        this.selectedTile = null;
        clearSquareLayer();
        createMainButtons();
    }

    public Tile getSelectedTile() {
        return this.selectedTile;
    }

    public Tile getTile(int row, int col) {
        return this.map.getTile(row, col);
    }

    public void selectBuilding(Square square) {
        unselectAll();
        square.select();
        Building building = this.getBuildingLayer()[square.getRow()][square.getCol()];
        this.selectedBuilding = building;
        // Buttons
        createBuildingButtons(this.selectedBuilding);
    }

    public void createBuildingButtons(Building building) {
        if (building.isFactory()) {
            createFactoryButtons(building.getOwner());
        } else if (building.isAirport()) {
            createAirportButtons(building.getOwner());
        } else if (building.isShipyard()) {
            createShipyardButtons(building.getOwner());
        } else {
            createTileButtons();
        }
    }

    public void unselectBuilding() {
        this.selectedBuilding = null;
        clearSquareLayer();
        createMainButtons();
    }

    public Building getSelectedBuilding() {
        return this.selectedBuilding;
    }

    public Building[][] getBuildingLayer() {
        return this.buildingLayer;
    }

    public Building getBuilding(int row, int col) {
        return this.buildingLayer[row][col];
    }

    public void selectUnit(Square square) {
        unselectAll();
        square.select();
        Unit unit = this.getUnitLayer()[square.getRow()][square.getCol()];
        this.selectedUnit = unit;
        this.infoUnit = unit;
        // Buttons
        if (this.selectedUnit.isFeetUnit()) {
            createFeetUnitButtons(this.selectedUnit);
        } else if (this.selectedUnit.isTransportUnit()) {
            if (this.selectedUnit.isReloadUnit()) {
                createReloadUnitButtons(this.selectedUnit);
            } else {
                createTransportUnitButtons(this.selectedUnit);
            }
        } else {
            createUnitButtons(this.selectedUnit);
        }
        // Actions
        this.selectedUnit.updateWays(this);
        if (this.selectedUnit.getOwner().equals(this.currentPlayer) && this.selectedUnit.isIdle()) {
            showTargets(this.selectedUnit);
        }
    }

    public void unselectUnit() {
        this.selectedUnit = null;
        this.infoUnit = null;
        clearSquareLayer();
        createMainButtons();
    }

    public Unit getSelectedUnit() {
        return this.selectedUnit;
    }

    public Unit getInfoUnit() {
        return this.infoUnit;
    }

    public Unit[][] getUnitLayer() {
        return this.unitLayer;
    }

    public Unit getUnit(int row, int col) {
        return this.unitLayer[row][col];
    }

    public void showTargets(Unit unit) {
        if (unit != null) {
            for (int row = 0; row < getMapSize(); row++) {
                for (int col = 0; col < getMapSize(); col++) {
                    if (isBlueTarget(unit, row, col)) {
                        this.getSquareLayer()[row][col].targetBlue();
                    } else if (isRedTarget(unit, row, col)) {
                        this.getSquareLayer()[row][col].targetRed();
                    } else if (isGreenTarget(unit, row, col)) {
                        this.getSquareLayer()[row][col].targetGreen();
                    }
                }
            }
        }
    }

    public void showDropTargets(Unit unit) {
        if (unit != null) {
            for (int row = 0; row < getMapSize(); row++) {
                for (int col = 0; col < getMapSize(); col++) {
                    if (isDropTarget(unit, row, col)) {
                        this.getSquareLayer()[row][col].targetGreen();
                    }
                }
            }
        }
    }

    public void showMoveScope(Unit unit) {
        if (unit != null) {
            for (int row = 0; row < getMapSize(); row++) {
                for (int col = 0; col < getMapSize(); col++) {
                    if (isBlueTarget(unit, row, col)) {
                        this.getSquareLayer()[row][col].scopeBlue();
                    }
                }
            }
        }
    }

    public void showAttackScope(Unit unit) {
        if (unit != null) {
            for (int row = 0; row < getMapSize(); row++) {
                for (int col = 0; col < getMapSize(); col++) {
                    int dist = Math.abs(unit.getRow() - row) + Math.abs(unit.getCol() - col);
                    if (unit.getAttackScope()[0] <= dist && dist <= unit.getAttackScope()[1]) {
                        // 4. FIXED UNIT ONLY : Check orientation
                        if (unit.isFixedUnit()) {
                            if (unit.getOrientation() == Unit.Orientation.SOUTH && unit.getCol() > col) {
                                this.getSquareLayer()[row][col].scopeRed();
                            } else if (unit.getOrientation() == Unit.Orientation.NORTH && unit.getCol() < col) {
                                this.getSquareLayer()[row][col].scopeRed();
                            } else if (unit.getOrientation() == Unit.Orientation.WEST && unit.getRow() > row) {
                                this.getSquareLayer()[row][col].scopeRed();
                            } else if (unit.getOrientation() == Unit.Orientation.EAST && unit.getRow() < row) {
                                this.getSquareLayer()[row][col].scopeRed();
                            }
                        } else {
                            this.getSquareLayer()[row][col].scopeRed();
                        }
                    }
                }
            }
        }
    }

    public void reloadUnits(Unit unit) {
            for (int row = 0; row < getMapSize(); row++) {
                for (int col = 0; col < getMapSize(); col++) {
                    int manhattanDistance = Math.abs(unit.getRow() - row) + Math.abs(unit.getCol() - col);
                    // 1. Check if square is in reload scope
                    if (manhattanDistance == 1) {
                        // 2. Check if there is an allied unit => then reload it
                        Unit neighbor = this.getUnitLayer()[row][col];
                        if (neighbor != null && neighbor.getOwner().equals(unit.getOwner())
                                && ((unit.getType() == Unit.TRUCK && (neighbor.isGroundUnit() || neighbor.isFixedUnit()))
                                        || (unit.getType()==Unit.AIRCRAFT_CARRIER && neighbor.isAirUnit()))) {
                            neighbor.reload();
                        }
                    }
                    unit.stop();
                }
            }
        unit.stop();
        this.unselectAll();
    }

    public boolean isBlueTarget(Unit unit, int row, int col) {
        for (ArrayList<int[]> move : unit.getWays()) {
            if (move.get(move.size() - 1)[0] == row && move.get(move.size() - 1)[1] == col) {
                return true;
            }
        }
        return false;
    }

    public boolean isRedTarget(Unit unit, int row, int col) {
        int manhattanDistance = Math.abs(unit.getRow() - row) + Math.abs(unit.getCol() - col);
        // RED TARGET ? (ATTACK)
        // 0. Check undirect unit bug
        if (unit.isDirectUnit() || (unit.isUndirectUnit() && unit.getMoveAmount() == unit.getMoveScope())) {
            // 1. Check ammo
            if (unit.getAmmo() > 0) {
                // 2. Check if square is in move scope
                if (manhattanDistance != 0 && manhattanDistance <= unit.getAttackScope()[1] && manhattanDistance >= unit.getAttackScope()[0]) {
                    // 3. Check if unit can be attacked
                    if (this.getUnitLayer()[row][col] != null && this.getUnitLayer()[row][col].canBeAttackedBy(unit)) {
                        // 4. FIXED UNIT ONLY : Check orientation
                        if (unit.isFixedUnit()) {
                            if (unit.getOrientation() == Unit.Orientation.SOUTH && unit.getCol() > col) {
                                return true;
                            } else if (unit.getOrientation() == Unit.Orientation.NORTH && unit.getCol() < col) {
                                return true;
                            } else if (unit.getOrientation() == Unit.Orientation.WEST && unit.getRow() > row) {
                                return true;
                            } else if (unit.getOrientation() == Unit.Orientation.EAST && unit.getRow() < row) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isGreenTarget(Unit unit, int row, int col) {
        int manhattanDistance = Math.abs(unit.getRow() - row) + Math.abs(unit.getCol() - col);
        // GREEN TARGET ? (EMBARK)
        // 1. Check if square is in embark scope
        if (manhattanDistance == 1) {
            // 2. Check if unit is empty transporter
            if (this.getUnitLayer()[row][col] != null && unit.canEmbarkIn(this.getUnitLayer()[row][col])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isDropTarget(Unit unit, int row, int col) {
        int manhattanDistance = Math.abs(unit.getRow() - row) + Math.abs(unit.getCol() - col);
        // GREEN TARGET ? (DROP)
        // 1. Check if square is in drop scope
        if (manhattanDistance == 1) {
            // 2. Check if passenger can be dropped on target
            if (unit.getPassenger().canGoOnTarget(this, row, col)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public ArrayList<ButtonCircle> getBottomButtons() {
        return this.bottomButtons;
    }

    public ArrayList<Button> getFixedButtons() {
        return this.fixedButtons;
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return this.checkBoxes;
    }

    public ArrayList<RadioGroup> getRadioGroups() {
        return this.radioGroups;
    }

    public void clearButtons() {
        this.bottomButtons = new ArrayList<ButtonCircle>();
        this.fixedButtons = new ArrayList<Button>();
        this.checkBoxes = new ArrayList<CheckBox>();
        this.radioGroups = new ArrayList<RadioGroup>();
        // Waiting camera initialisation
        if (this.screen.getRenderer() != null) {
            this.screen.getRenderer().resetButtonCamera();
        }
    }

    public Button getTouchedButton(float menu_x, float menu_y) {
        // Check bottom buttons
        if (bottomButtons != null) {
            for (ButtonCircle button : bottomButtons) {
                if (button.isUnitButton() && ((UnitButton) button).getInfoButton().isTouched(menu_x, menu_y)) {
                    return ((UnitButton) button).getInfoButton();
                } else if (button.isTouched(menu_x, menu_y)) {
                    return button;
                }
            }
        }
        // Check fixed buttons
        if (fixedButtons != null) {
            for (Button button : fixedButtons) {
                if (button.isTouched(menu_x, menu_y)) {
                    return button;
                }
            }
        }
        return null;
    }

    public void touchButton(Button button) {
        if (button.isEnabled()) {
            // MAIN BUTTONS
            if (button.getTag().equals(ButtonCircle.BUTTON_MENU)) {
                showMenu();
            } else if (button.getTag().equals(ButtonCircle.BUTTON_STAT)) {
                showStats();
            } else if (button.getTag().equals(ButtonCircle.BUTTON_NEXT)) {
                finishTurn();
            }
            // BUILDING BUTTONS
            else if (button.getTag().equals(ButtonCircle.BUTTON_UNIT)) {
                buildUnit((UnitButton) button);
            } else if (button.getTag().equals(ButtonCircle.BUTTON_UNIT_INFO)) {
                showInfo(((UnitInfoButton) button).getUnitButton().getUnit());
            }
            // UNIT BUTTONS
            else if (button.getTag().equals(ButtonCircle.BUTTON_INFO)) {
                if (this.selectedUnit != null) {
                    showInfo(this.selectedUnit);
                }
            } else if (button.getTag().equals(ButtonCircle.BUTTON_CAPTURE)) {
                if (this.selectedUnit != null) {
                    attackBuilding(this.selectedUnit);
                }
            } else if (button.getTag().equals(ButtonCircle.BUTTON_DROP)) {
                if (this.selectedUnit != null) {
                    clearSquareLayer();
                    showDropTargets(this.selectedUnit);
                }
            } else if (button.getTag().equals(ButtonCircle.BUTTON_RELOAD)) {
                if (this.selectedUnit != null && this.selectedUnit.isReloadUnit()) {
                    reloadUnits(this.selectedUnit);
                }
            } else if (button.getTag().equals(ButtonCircle.BUTTON_MOVE)) {
                if (this.selectedUnit != null) {
                    clearSquareLayer();
                    showMoveScope(this.selectedUnit);
                }
            } else if (button.getTag().equals(ButtonCircle.BUTTON_ATTACK)) {
                if (this.selectedUnit != null) {
                    clearSquareLayer();
                    showAttackScope(this.selectedUnit);
                }
            }
            // BACK BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_CLOSE)) {
                back();
            }
            // EXIT BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_EXIT)) {
                exit();
            }
            // HELP BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_HELP)) {
                this.screen.getController().help();
            }
        }
    }

    public CheckBox getTouchedCheckBox(float menu_x, float menu_y) {
        if (checkBoxes != null) {
            for (CheckBox checkBox : checkBoxes) {
                if (checkBox.isTouched(menu_x, menu_y)) {
                    return checkBox;
                }
            }
        }
        return null;
    }

    public void touchCheckBox(CheckBox checkBox) {
        // MUSIC CHECKBOX
        if (checkBox.getTag().equals(CheckBox.CHECKBOX_MUSIC)) {
            if (!checkBox.isChecked()) {
                checkBox.check();
                this.screen.getController().enableMusic();
                this.screen.startBackgroundMusic();
            } else {
                checkBox.uncheck();
                this.screen.getController().disableMusic();
                this.screen.stopBackgroundMusic();
            }
        }
        // SOUNDS CHECKBOX
        else if (checkBox.getTag().equals(CheckBox.CHECKBOX_SOUND)) {
            if (!checkBox.isChecked()) {
                checkBox.check();
                this.screen.getController().enableSound();
            } else {
                checkBox.uncheck();
                this.screen.getController().disableSound();
            }
        }
    }

    public RadioButton getTouchedRadioButton(float x, float y) {
        if (isMenu()) {
            if (radioGroups != null) {
                for (RadioGroup group : radioGroups) {
                    for (RadioButton button : group.getButtons()) {
                        if (button.isTouched(x, y)) {
                            return button;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void touchRadioButton(RadioButton button) {
        if (isMenu()) {
            // USER SPEED
            if (button.getGroup().getTag().equals(RadioGroup.RADIO_GROUP_USER)) {
                button.check();
                switch (button.getGroup().getCheckedPosition()) {
                    case 0:
                        Unit.HUMAN_SPEED = Unit.SPEED_SLOW;
                        this.screen.getController().updateHumanSpeed(0);
                        break;
                    case 1:
                        Unit.HUMAN_SPEED = Unit.SPEED_NORMAL;
                        this.screen.getController().updateHumanSpeed(1);
                        break;
                    case 2:
                        Unit.HUMAN_SPEED = Unit.SPEED_FAST;
                        this.screen.getController().updateHumanSpeed(2);
                        break;
                }
            }
            // ROBOT SPEED
            else if (button.getGroup().getTag().equals(RadioGroup.RADIO_GROUP_ROBOT)) {
                button.check();
                System.out.println("Robot speed : " + button.getGroup().getCheckedPosition());
                switch (button.getGroup().getCheckedPosition()) {
                    case 0:
                        Unit.ROBOT_SPEED = Unit.SPEED_SLOW;
                        this.screen.getController().updateRobotSpeed(0);
                        break;
                    case 1:
                        Unit.ROBOT_SPEED = Unit.SPEED_NORMAL;
                        this.screen.getController().updateRobotSpeed(1);
                        break;
                    case 2:
                        Unit.ROBOT_SPEED = Unit.SPEED_FAST;
                        this.screen.getController().updateRobotSpeed(2);
                        break;
                }
            }
        }
    }

    public void finishTurn() {
        if(isEndOfGame()) {
            endGame();
        } else {
            // Entering in switch state
            this.turn++;
            this.currentPlayer = this.players.get((this.turn) % this.players.size());
            if (this.currentPlayer.isAlive()) {
                Timer.Task switchTask = new Timer.Task() {
                    @Override
                    public void run() {
                        // Point camera to capital
                        pointCameraOn(currentPlayer.getCapitalPosition());
                        if (currentPlayer.isHuman()) {
                            resume();
                        } else {
                            robotPlay();
                        }
                        this.cancel();
                    }
                };
                Timer.Task pointCamTask = new Timer.Task() {
                    @Override
                    public void run() {
                        // Point camera to capital
                        pointCameraOn(currentPlayer.getCapitalPosition());
                    }
                };
                switchMode();
                // Save game
                if (this.saveGame) {
                    this.screen.saveGame();
                } else {
                    this.saveGame = true;
                }
                // Add money
                this.currentPlayer.addMoney(this.currentPlayer.getMoneyByTurn());
                // Wake up units
                wakeUpUnits(this.currentPlayer);
                Timer.schedule(switchTask, 2.0f);
                Timer.schedule(pointCamTask, 1.0f);
            } else {
                finishTurn();
            }
        }
    }

    public void wakeUpUnits(Player player) {
        // Idle all units
        for (Player p : this.players) {
            for (Unit unit : p.getUnits()) {
                // Wake up units
                unit.idle();
                // Heal and reload units that are on buildings at the start of player's turn
                if (p.equals(player)) {
                    Building building = this.getBuildingLayer()[unit.getRow()][unit.getCol()];
                    // AIR UNIT
                    if (unit.isAirUnit() && building != null && building.getOwner().equals(player) && building.isAirport()) {
                        unit.heal(20);
                        unit.reload();
                    }
                    // NAVAL UNIT
                    else if (unit.isNavalUnit() && building != null && building.getOwner().equals(player) && building.isShipyard()) {
                        unit.heal(20);
                        unit.reload();
                    }
                    // GROUND UNIT
                    else if (unit.isGroundUnit() && building != null && building.getOwner().equals(player)) {
                        unit.heal(20);
                        unit.reload();
                    }
                }
            }
        }
    }

    public void attackBuilding(Unit unit) {
        Building building = this.getBuildingLayer()[unit.getRow()][unit.getCol()];
        if (building != null && building.getOwner().getTeam() != unit.getOwner().getTeam()) {
            Player defender = building.getOwner();
            building.capture(unit.getOwner());
            DWController.playSound(getAssets().capture);
            unit.stop();
            // If captured building is a capital and that defender have no other capital and that player is not a "only army player"
            // BUG CAPITAL : && !defender.isOnlyArmyPlayer()
            if (building.isCapital() && !defender.haveCapital()) {
                killPlayer(defender);
            }
        }
        unselectAll();
    }

    public void killPlayer(Player player) {
        for (int row = 0; row < getMapSize(); row++) {
            for (int col = 0; col < getMapSize(); col++) {
                // Building given to GAIA
                Building building = getBuildingLayer()[row][col];
                if (building != null && building.getOwner().equals(player)) {
                    building.capture(this.playerGaia);
                }
                // Unit removed
                Unit unit = getUnitLayer()[row][col];
                if (unit != null && unit.getOwner().equals(player)) {
                    getUnitLayer()[row][col] = null;
                }
            }
        }
        // Killing player
        player.die();
        // Check if game is ended
        if (isEndOfGame()) {
            endGame();
        }
    }

    public boolean isEndOfGame() {
        ArrayList<Integer> teams_alive = new ArrayList<Integer>();
        for (com.nauwstudio.dutywars_ww2.game.Player player : this.players) {
            int team = player.getTeam();
            if (player.isAlive() && !teams_alive.contains(team)) {
                teams_alive.add(team);
            }
        }
        return teams_alive.size() <= 1;
    }

    public void endGame() {
        final Timer.Task showingStarTask = new Timer.Task() {
            @Override
            public void run() {
                if (endGameStarShown == Util.getStarsAmount(getScore()))
                    this.cancel();
                else {
                    DWController.playSound(getAssets().star);
                    endGameStarShown++;
                }
            }
        };
        Timer.Task endGameTask = new Timer.Task() {
            @Override
            public void run() {
                createEndGameButtons();
                state = GameState.END;
                this.cancel();
            }
        };
        Timer.schedule(endGameTask, 1.0f);
        if (isCampaignMode() && this.isCampaignDefeat()) {
            DWController.playSound(getAssets().defeat);
        } else {
            DWController.playSound(getAssets().victory);
        }
        deleteSave();
        if (isCampaignMode() && isCampaignVictory()) {
            // Unlock map
            unlock();
            // Show stars
            endGameStarShown = 0;
            Timer.schedule(showingStarTask, 0.5f, 0.5f, Util.getStarsAmount(getScore()));
            // Submit google play score
            screen.getController().submitScore(map.getLinkedMissionId(), getScore());
        }
    }

    public void buildUnit(com.nauwstudio.dutywars_ww2.game.UnitButton button) {
        if (this.selectedBuilding != null && this.getUnitLayer()[this.selectedBuilding.getRow()][this.selectedBuilding.getCol()] == null) {
            // Check money
            if (this.currentPlayer.getMoney() >= button.getUnitPrice()) {
                this.currentPlayer.removeMoney(button.getUnitPrice());
                createUnit(this.selectedBuilding.getRow(), this.selectedBuilding.getCol(), this.selectedBuilding.getOwner(), button.getUnitType(), Unit.Orientation.SOUTH);
                this.getUnitLayer()[this.selectedBuilding.getRow()][this.selectedBuilding.getCol()].stop();
                unselectBuilding();
            }
        }
    }

    public void moveUnit(Unit unit, int row, int col) {
        // NO MOVE ?
        if (row != unit.getRow() || col != unit.getCol()) {
            // Getting move
            ArrayList<int[]> move = new ArrayList<int[]>();
            for (ArrayList<int[]> way : unit.getWays()) {
                int[] dest = way.get(way.size() - 1);
                if (dest[0] == row && dest[1] == col) {
                    move = way;
                    break;
                }
            }
            // Update unit layer
            this.getUnitLayer()[unit.getRow()][unit.getCol()] = null;
            if (!move.isEmpty()) {
                this.getUnitLayer()[row][col] = unit;
                // Update unit defence
                Building b = getBuildingLayer()[row][col];
                if (b == null) {
                    unit.updateDefence(this.map.getTile(row, col).getDefence());
                } else {
                    unit.updateDefence(b.getDefence());
                }
                // Move unit
                unit.move(move, this);
            }
            unselectUnit();
        }
    }

    public void attackUnit(final Unit unit, int row, int col) {
        final Unit defender = this.getUnitLayer()[row][col];
        if (defender != null && defender.canBeAttackedBy(unit)) {
            // Calculate battle result
            final int[] damages = calculateBattleResults(unit, defender);
            // Attack unit
            unit.attack(defender, damages[1], this);
            Timer.Task stopAttackTask = new Timer.Task() {
                @Override
                public void run() {
                    unit.stop();
                    defendUnit(unit, defender, damages[0]);
                    this.cancel();
                }
            };
            Timer.schedule(stopAttackTask, 0.5f);
        }
        unselectUnit();
    }

    private void defendUnit(final Unit attacker, final Unit defender, final int damage) {
        // Defender is dead ?
        if (defender.getLife() == 0) {
            killUnit(defender);
        } else {
            // Defender can respond to attack ?
            int manhattanDistance = Math.abs(attacker.getRow() - defender.getRow()) + Math.abs(attacker.getCol() - defender.getCol());
            if (manhattanDistance == 1 && attacker.canBeAttackedBy(defender) && defender.getAmmo() > 0 && defender.isDirectUnit()) {
                defender.attack(attacker, damage, this);
                Timer.Task stopDefendTask = new Timer.Task() {
                    @Override
                    public void run() {
                        defender.idle();
                        // Attacker is dead
                        if (attacker.getLife() == 0) {
                            killUnit(attacker);
                        }
                        this.cancel();
                    }
                };
                Timer.schedule(stopDefendTask, 0.5f);
            }
        }
    }

    public void killUnit(final Unit unit) {
        getUnitLayer()[unit.getRow()][unit.getCol()] = null;
        unit.die();
        Timer.Task killTask = new Timer.Task() {
            @Override
            public void run() {
                if (!unit.getOwner().haveCapital() && !unit.getOwner().haveUnit()) {
                    killPlayer(unit.getOwner());
                }
                this.cancel();
            }
        };
        Timer.schedule(killTask, 1.0f);
    }

    public void embarkUnit(Unit unit, int row, int col) {
        Unit transporter = this.getUnitLayer()[row][col];
        if (transporter != null && unit.canEmbarkIn(transporter)) {
            // Update unit layer
            this.getUnitLayer()[unit.getRow()][unit.getCol()] = null;
            // Embark unit
            transporter.addPassenger(unit);
        }
        unselectUnit();
    }

    public void dropUnit(com.nauwstudio.dutywars_ww2.game.units.Unit unit, int row, int col) {
        Unit passenger = unit.getPassenger();
        this.getUnitLayer()[row][col] = passenger;
        unit.dropPassenger(row, col);
        // Update passenger defence
        com.nauwstudio.dutywars_ww2.game.buildings.Building b = getBuildingLayer()[row][col];
        if (b == null) {
            passenger.updateDefence(this.map.getTile(row, col).getDefence());
        } else {
            passenger.updateDefence(b.getDefence());
        }
        unselectUnit();
    }

    protected int[] calculateBattleResults(Unit attacker, Unit defender) {
        int defHurt = attacker.getDamage()[defender.getType() - 1];
        float attNumberRatio = attacker.getLife() / 100f;
        float defDefenceRatio = defender.getDefence() / 10f;
        defHurt = (int) (attNumberRatio * defHurt);
        defHurt = (int) (defHurt - (defHurt * defDefenceRatio));
        int attHurt = defender.getDamage()[attacker.getType() - 1];
        float defNumberRatio = (defender.getLife() - defHurt) / 100f;
        float attDefenceRatio = attacker.getDefence() / 10f;
        attHurt = (int) (defNumberRatio * attHurt);
        attHurt = (int) (attHurt - (attHurt * attDefenceRatio));
        return new int[]{attHurt, defHurt};
    }

    public int getDay() {
        return (this.turn / this.players.size()) + 1;
    }

    public void back() {
        unselectAll();
        if (this.isStat() || this.isMenu() || this.isInfo()) {
            resume();
        }
    }

    public void exit() {
        this.screen.stopBackgroundMusic();
        // EXIT FROM END OF GAME
        if (this.isEnded()) {
            // CAMPAIGN
            if (isCampaignMode()) {
                // MAP UNLOCKED ?
                if (this.map.isLocked() && this.isCampaignVictory()) {
                    this.screen.toCampaignScreen(this.map.getId());
                } else {
                    this.screen.toCampaignScreen(0);
                }
            }
            // VERSUS
            else {
                this.screen.toVersusScreen();
            }
        }
        // EXIT FROM MENU
        else {
            this.screen.toMenuScreen();
        }
    }

    public void unlock() {
        // UPDATE BEST SCORE
        this.screen.getDatabase().updateScore(this.map.getLinkedMissionId(), getScore());
        // UNLOCK MAP FOR VERSUS MODE
        this.screen.getDatabase().unlockMap(this.map.getId());
        // UNLOCK NEXT MISSION
        // LAST MISSION => NOTHING TO UNLOCK
        if (this.map.getLinkedMissionId() < Map.SAKHALIN_ID) {
            this.screen.getDatabase().unlockMission(this.map.getLinkedMissionId() + 1);
        }
    }

    public void deleteSave() {
        // DELETE ONLY ON CAMPAIGN MODE
        if (isCampaignMode()) {
            this.screen.deleteSave();
        }
    }

    public int getScore() {
        if (isCampaignVictory()) {
            return getDay();
        } else {
            return 0;
        }
    }

    public boolean isCampaignVictory() {
        boolean result = true;
        for (Player winner : this.getWinners()) {
            if (winner.getArmy().getType() == Army.GER || winner.getArmy().getType() == Army.JAP) {
                result = false;
            }
        }
        return result;
    }

    public boolean isCampaignDefeat() {
        boolean result = false;
        for (Player winner : this.getWinners()) {
            if (winner.getArmy().getType() == Army.GER || winner.getArmy().getType() == Army.JAP) {
                result = true;
            }
        }
        return result;
    }

    public boolean isCampaignMode() {
        return this.mode == MODE_CAMPAIGN;
    }

    public ArrayList<Player> getWinners() {
        ArrayList<Player> winners = new ArrayList<Player>();
        for (Player player : this.getPlayers()) {
            if (player.isAlive()) {
                winners.add(player);
            }
        }
        return winners;
    }

    public void showMenu() {
        this.createMenuButtons();
        this.state = GameState.MENU;
    }

    public void showStats() {
        this.createStatButtons();
        this.state = GameState.STAT;
    }

    public void showInfo(Unit unit) {
        this.infoUnit = unit;
        this.createInfoButtons();
        this.state = GameState.INFO;
    }

    public GameAssets getAssets() {
        return this.assets;
    }

    public GameHUDVars getHUDVars() {
        return this.hudVars;
    }

    public int getTurn() {
        return turn;
    }

    public void pointCameraOn(Vector2 target) {
        this.screen.getRenderer().pointCamera(target);
    }

    public MainAssets getMainAssets() {
        return this.screen.getController().getMainAssets();
    }

    public int getShowingStar() {
        return this.endGameStarShown;
    }
}