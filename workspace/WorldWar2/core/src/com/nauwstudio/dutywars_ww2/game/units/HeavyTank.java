package com.nauwstudio.dutywars_ww2.game.units;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Player;

public class HeavyTank extends Unit {

    private static final String NAME_USA = "heavytank.usa";
    private static final String NAME_GER = "heavytank.ger";
    private static final String NAME_USS = "heavytank.uss";
    private static final String NAME_JAP = "heavytank.jap";
    private static final String NAME_GBR = "heavytank.gbr";
    private static final String DESC_USA = "heavytank_desc.usa";
    private static final String DESC_GER = "heavytank_desc.ger";
    private static final String DESC_USS = "heavytank_desc.uss";
    private static final String DESC_JAP = "heavytank_desc.jap";
    private static final String DESC_GBR = "heavytank_desc.gbr";

    public HeavyTank(int row, int col, Player owner, Orientation orientation) {
        super(row, col, orientation, owner, HEAVY_TANK, Kind.GROUND, HEAVY_TANK_PRICE, HEAVY_TANK_MOVE_SCOPE, HEAVY_TANK_ATTACK_SCOPE, LIFE, HEAVY_TANK_AMMO, HEAVY_TANK_GAS, HEAVY_TANK_DMG, owner.getArmy().getGameAssets().heavy_tank_fire, owner.getArmy().getGameAssets().heavy_tank_move);
    }

    @Override
    public TextureRegion getTextureIdle() {
        switch (orientation) {
            case SOUTH:
                return this.owner.getArmy().getAssets().heavy_tank_S;
            case WEST:
                return this.owner.getArmy().getAssets().heavy_tank_W;
            case NORTH:
                return this.owner.getArmy().getAssets().heavy_tank_N;
            case EAST:
                return this.owner.getArmy().getAssets().heavy_tank_E;
            default:
                return getTexture();
        }
    }

    @Override
    public TextureRegion getTextureFire(float delta) {
        switch (orientation) {
            case SOUTH:
                return (TextureRegion) this.owner.getArmy().getAssets().heavy_tank_S_fire.getKeyFrame(delta);
            case WEST:
                return (TextureRegion) this.owner.getArmy().getAssets().heavy_tank_W_fire.getKeyFrame(delta);
            case NORTH:
                return (TextureRegion) this.owner.getArmy().getAssets().heavy_tank_N_fire.getKeyFrame(delta);
            case EAST:
                return (TextureRegion) this.owner.getArmy().getAssets().heavy_tank_E_fire.getKeyFrame(delta);

        }
        return getTexture();
    }

    @Override
    public TextureRegion getTexture() {
        return this.owner.getArmy().getAssets().heavy_tank_HD;
    }

    @Override
    public TextureRegion getButtonTexture() {
        return this.owner.getArmy().getAssets().button_heavy_tank;
    }

    @Override
    public TextureRegion getButtonPressedTexture() {
        return this.owner.getArmy().getAssets().button_heavy_tank_pressed;
    }

    @Override
    public TextureRegion getButtonDisabledTexture() {
        return this.owner.getArmy().getAssets().button_heavy_tank_disabled;
    }

    public String getName() {
        switch (this.owner.getArmy().getType()) {
            case com.nauwstudio.dutywars_ww2.game.Army.USA:
                return NAME_USA;
            case com.nauwstudio.dutywars_ww2.game.Army.GER:
                return NAME_GER;
            case com.nauwstudio.dutywars_ww2.game.Army.USS:
                return NAME_USS;
            case com.nauwstudio.dutywars_ww2.game.Army.JAP:
                return NAME_JAP;
            case com.nauwstudio.dutywars_ww2.game.Army.GBR:
                return NAME_GBR;
        }
        return "";
    }

    public String getDescription() {
        switch (this.owner.getArmy().getType()) {
            case Army.USA:
                return DESC_USA;
            case Army.GER:
                return DESC_GER;
            case Army.USS:
                return DESC_USS;
            case Army.JAP:
                return DESC_JAP;
            case Army.GBR:
                return DESC_GBR;
        }
        return "";
    }

}
