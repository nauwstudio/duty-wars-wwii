package com.nauwstudio.dutywars_ww2.game;

import com.badlogic.gdx.assets.AssetManager;
import com.nauwstudio.dutywars_ww2.SeasonAssets;

/**
 * Created by nauwpie on 8/11/2017.
 */

public class Season {
    public static final int CLA = 1;
    public static final int WIN = 2;
    public static final int DES = 3;

    public static final String CLASSIC_ATLAS = "cla";
    public static final String WINTER_ATLAS = "win";
    public static final String DESERT_ATLAS = "des";

    private int type;
    private SeasonAssets assets;

    public Season(AssetManager manager, int type) {
        this.type = type;
        switch (type) {
            case CLA:
                this.assets = new SeasonAssets(manager, CLASSIC_ATLAS);
                break;
            case WIN:
                this.assets = new SeasonAssets(manager, WINTER_ATLAS);
                break;
            case DES:
                this.assets = new SeasonAssets(manager, DESERT_ATLAS);
                break;
        }
    }

    public int getType() {
        return this.type;
    }

    public SeasonAssets getAssets() {
        return this.assets;
    }

    @Override
    public boolean equals(Object object) {
        try {
            if (((Season) object).getType() == this.type) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
