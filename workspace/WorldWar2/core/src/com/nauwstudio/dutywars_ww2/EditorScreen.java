package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.nauwstudio.dutywars_ww2.editor.Editor;
import com.nauwstudio.dutywars_ww2.editor.EditorGestureDetector;
import com.nauwstudio.dutywars_ww2.editor.EditorInputHandler;
import com.nauwstudio.dutywars_ww2.editor.EditorRenderer;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Square;

import java.util.ArrayList;

public class EditorScreen implements Screen {

    private DWController controller;

    private Editor editor;
    private EditorRenderer renderer;
    private GameAssets assets;

    private float runtime = 0;

    // Input handler vars
    private EditorGestureDetector gestureDetector;
    private EditorInputHandler inputHandler;
    public Button touchedButton;
    public Square touchedSquare;
    public ToggleButton touchedToggle;
    public boolean can_pan;
    public boolean can_pinch;

    public EditorScreen(DWController controller, Map map, ArrayList<Player> players) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        this.editor = new Editor(this, this.controller.getMainAssets(), map, players, map.getPlayersAmount());
        this.renderer = new EditorRenderer(this.editor);
        initInputHandler();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new EditorGestureDetector(this);
        this.inputHandler = new EditorInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Editor getEditor() {
        return this.editor;
    }

    public DWController getController() {
        return this.controller;
    }

    public EditorRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
    }

    @Override
    public void render(float delta) {
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
        this.editor.resume();
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        this.assets.dispose();
    }

    public com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface getDatabase() {
        return this.controller.getDatabaseInterface();
    }

    public void back() {
        this.controller.toMainScreen();
    }

    public void openTextInput(String title, String text, String hint) {
        MyTextInputListener listener = new MyTextInputListener();
        Gdx.input.getTextInput(listener, title, text, hint);
    }

    public class MyTextInputListener implements Input.TextInputListener {
        @Override
        public void input (String text) {
            editor.updateMapName(text);
            System.out.println(text);
        }

        @Override
        public void canceled () {
        }
    }

}
