package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.Robot;

import java.util.ArrayList;

public class EditorLoadScreen implements Screen {

    private DWController controller;
    private OrthographicCamera loadCamera;
    private SpriteBatch batch;

    private GameAssets assets;
    private ArrayList<Player> players;
    private Map map;
    private int loadCounter = 0;
    private Timer.Task loadTask;
    private float bullet_size;
    private float bullet_small_icon_size;
    private float bullet_big_icon_size;

    private boolean assetsLoaded = false;

    /*
    NEW GAME
     */
    public EditorLoadScreen(DWController controller, Map map) {
        this.controller = controller;
        this.assets = this.controller.getGameAssets();
        this.players = getPlayers();
        this.map = map;
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.batch = new SpriteBatch();
        this.batch.setProjectionMatrix(this.loadCamera.combined);
        this.loadTask = new Timer.Task() {
            @Override
            public void run() {
                loadCounter = (loadCounter + 1) % 6;
            }
        };
        this.bullet_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 14f;
        this.bullet_small_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 40f;
        this.bullet_big_icon_size = Math.min(Util.getScreenWidth(), Util.getScreenHeight()) / 25f;
    }

    private void loadAssets() {
        System.out.println("Load assets");
        System.out.println("HEAP START " + Gdx.app.getJavaHeap());
        Timer.schedule(loadTask, 0.20f, 0.20f);
        this.assets.load();
        for (Player player : this.players) {
            if (!player.getArmy().isGAI()) {
                player.getArmy().getAssets().loadUnits();
                player.getArmy().getAssets().loadColors();
            }
        }

    }

    private void initAssets() {
        this.assets.init();
        for (Player player : this.players) {
            player.getArmy().getAssets().initUnits();
            player.getArmy().getAssets().initColors();
        }
    }

    /*
    Init players for map editor
     */
    public ArrayList<Player> getPlayers() {
        try {
            ArrayList<Player> players = new ArrayList<Player>();
            players.add(new Player(this.controller.getPlayer(Army.GAI).getArmy(), 0, 0, Player.GAIA_ORDER, Player.ALIVE));
            players.add(new Player(this.controller.getPlayer(Army.USA).getArmy(), 0, 0, 1, Player.ALIVE));
            players.add(new Player(this.controller.getPlayer(Army.GER).getArmy(), 0, 0, 2, Player.ALIVE));
            players.add(new Player(this.controller.getPlayer(Army.USS).getArmy(), 0, 0, 3, Player.ALIVE));
            players.add(new Player(this.controller.getPlayer(Army.JAP).getArmy(), 0, 0, 4, Player.ALIVE));
            players.add(new Player(this.controller.getPlayer(Army.GBR).getArmy(), 0, 0, 5, Player.ALIVE));
            return players;
        } catch (Exception e) {
            return new ArrayList<Player>();
        }
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        if (assetsLoaded) {
            // Render loader
            if (this.assets.update()) {
                initAssets();
                if (this.loadTask != null) {
                    this.loadTask.cancel();
                }
                this.controller.showMapEditorScreen(this.map, this.players);
            } else {
                renderLoading();
            }
            // Render screen
        } else {
            renderLoading();
            loadAssets();
            assetsLoaded = true;
        }

    }

    private void renderLoading() {
        Gdx.gl.glClearColor(5 / 255f, 5 / 255f, 5 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(loadCamera.combined);
        batch.begin();
        RenderUtil.drawTextInCell(batch, this.controller.getMainAssets().font_giant, this.controller.getMainAssets().stringBundle.get("editor.launch"), 0, Util.getScreenHeight() / 2f + this.controller.getMainAssets().font_giant.getXHeight(), Util.getScreenWidth(), this.controller.getMainAssets().font_giant.getXHeight());
        for (int i = 0; i < 6; i++) {
            if (i == loadCounter) {
                RenderUtil.drawTextureInCell(batch, this.controller.getMainAssets().bullet, Util.getScreenWidth() / 2f - 3 * bullet_size + i * bullet_size, Util.getScreenHeight() / 2f - this.controller.getMainAssets().font_giant.getXHeight() * 2, bullet_size, this.controller.getMainAssets().font_giant.getXHeight(), this.bullet_big_icon_size);
            } else {
                RenderUtil.drawTextureInCell(batch, this.controller.getMainAssets().bullet, Util.getScreenWidth() / 2f - 3 * bullet_size + i * bullet_size, Util.getScreenHeight() / 2f - this.controller.getMainAssets().font_giant.getXHeight() * 2, bullet_size, this.controller.getMainAssets().font_giant.getXHeight(), this.bullet_small_icon_size);
            }
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        loadCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        loadCamera.update();
        this.bullet_size = Math.min(width, height) / 14f;
        this.bullet_small_icon_size = Math.min(width, height) / 40f;
        this.bullet_big_icon_size = Math.min(width, height) / 25f;
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {

    }

    public com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface getDatabase() {
        return this.controller.getDatabaseInterface();
    }

    public void back() {
        this.controller.toMainScreen();
    }
}
