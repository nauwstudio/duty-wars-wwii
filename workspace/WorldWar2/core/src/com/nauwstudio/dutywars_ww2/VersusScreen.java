package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.nauwstudio.dutywars_ww2.campaign.Campaign;
import com.nauwstudio.dutywars_ww2.campaign.CampaignGestureDetector;
import com.nauwstudio.dutywars_ww2.campaign.CampaignInputHandler;
import com.nauwstudio.dutywars_ww2.campaign.CampaignRenderer;
import com.nauwstudio.dutywars_ww2.campaign.MissionButton;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.versus.Versus;
import com.nauwstudio.dutywars_ww2.versus.VersusGestureDetector;
import com.nauwstudio.dutywars_ww2.versus.VersusInputHandler;
import com.nauwstudio.dutywars_ww2.versus.VersusRenderer;

public class VersusScreen implements Screen {

    private DWController controller;

    private Versus versus;
    private VersusRenderer renderer;
    private MainAssets assets;

    private float runtime = 0;

    // Input handler vars
    private VersusGestureDetector gestureDetector;
    private VersusInputHandler inputHandler;
    public Button touchedButton;

    public VersusScreen(DWController controller) {
        this.controller = controller;
        this.assets = this.controller.getMainAssets();
        this.versus = new Versus(this, this.assets);
        this.renderer = new VersusRenderer(this.versus);
        initInputHandler();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new VersusGestureDetector(this);
        this.inputHandler = new VersusInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Versus getVersus() {
        return this.versus;
    }

    public VersusRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        this.runtime += delta;
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
    }

   public DWController getController() {
       return this.controller;
   }

    public void startGame(Map map, int[] players_army, int[] players_type, int[] players_team, int[] players_order) {
        this.controller.startVersusGame(map, players_army, players_type, players_team, players_order);
    }

    public void back() {
        this.controller.toMainScreen();
    }
}
