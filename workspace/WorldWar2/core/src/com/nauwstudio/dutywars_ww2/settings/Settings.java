package com.nauwstudio.dutywars_ww2.settings;

import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.CheckBox;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RadioButton;
import com.nauwstudio.dutywars_ww2.RadioGroup;
import com.nauwstudio.dutywars_ww2.SettingsScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.util.ArrayList;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class Settings {

    public static final String[] SPEED_TAGS = {RadioButton.RADIO_SLOW, RadioButton.RADIO_NORMAL, RadioButton.RADIO_FAST};

    private SettingsScreen screen;

    private ArrayList<Button> fixedButtons;
    private ArrayList<Button> buttons;
    private ArrayList<CheckBox> checkBoxes;
    private ArrayList<RadioGroup> radioGroups;
    private ArrayList<Button> aboutButtons;

    private MainAssets assets;
    private SettingsHUDVars hudVars;

    private boolean credits = false;

    public Settings(SettingsScreen screen, MainAssets assets) {
        this.screen = screen;
        this.assets = assets;
        this.hudVars = new SettingsHUDVars(this);
        createButtons();
        if(isAbout()) {
            createAboutButtons();
        }
    }

    public void createButtons() {
        clearButtons();
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.fixedButtons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
        // MUSIC CHECKBOX
        Vector2 music_pos = new Vector2(hudVars.music_checkbox_x, hudVars.music_checkbox_y);
        this.checkBoxes.add(new CheckBox(music_pos,hudVars.music_checkbox_width, hudVars.music_checkbox_height, CheckBox.CHECKBOX_MUSIC, assets.stringBundle.get("settings.music"), assets.checkBox, assets.checkBox_pressed, assets.checkBox_checked, assets.checkBox_checked_pressed));
        if(this.screen.getController().isMusic()) {
            this.checkBoxes.get(0).check();
        }
        // SOUNDS CHECKBOX
        Vector2 sound_pos = new Vector2(hudVars.effects_checkbox_x, hudVars.effects_checkbox_y);
        this.checkBoxes.add(new CheckBox(sound_pos,hudVars.effects_checkbox_width, hudVars.effects_checkbox_height, CheckBox.CHECKBOX_SOUND, assets.stringBundle.get("settings.effects"), assets.checkBox, assets.checkBox_pressed, assets.checkBox_checked, assets.checkBox_checked_pressed));
        if(this.screen.getController().isSound()) {
            this.checkBoxes.get(1).check();
        }
        // USER SPEED RADIO GROUP
        String[] speedTexts = new String[]{assets.stringBundle.get("settings.slow"),assets.stringBundle.get("settings.normal"),assets.stringBundle.get("settings.fast")};
        Vector2 user_speed_pos = new Vector2(hudVars.speed_user_x, hudVars.speed_user_y);
        this.radioGroups.add(new RadioGroup(user_speed_pos,hudVars.speed_user_width, hudVars.speed_user_height, RadioGroup.RADIO_GROUP_USER, SPEED_TAGS, speedTexts, Unit.getHumanSpeedPosition(), this.assets));
        // ROBOT SPEED RADIO GROUP
        Vector2 robot_speed_pos = new Vector2(hudVars.speed_robot_x, hudVars.speed_robot_y);
        this.radioGroups.add(new RadioGroup(robot_speed_pos,hudVars.speed_robot_width, hudVars.speed_robot_height, RadioGroup.RADIO_GROUP_ROBOT, SPEED_TAGS, speedTexts, Unit.getRobotSpeedPosition(), this.assets));
        // HELP BUTTON
        Vector2 help_pos = new Vector2(hudVars.help_x, hudVars.help_y);
        this.buttons.add(new ButtonRectangle(help_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_HELP, assets.button_help, assets.button_help_pressed, assets.button_help));
        // CREDITS BUTTON
        Vector2 credits_pos = new Vector2(hudVars.credits_x, hudVars.credits_y);
        this.buttons.add(new ButtonRectangle(credits_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_ABOUT, assets.button_about, assets.button_about_pressed, assets.button_about));
        // LOG BUTTON
        Vector2 log_pos = new Vector2(hudVars.log_x, hudVars.log_y);
        if(this.screen.getController().isLogged()) {
            this.buttons.add(new ButtonRectangle(log_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_LOG, assets.button_log_out, assets.button_log_out_pressed, assets.button_log_out));
        } else {
            this.buttons.add(new ButtonRectangle(log_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_LOG, assets.button_log_in, assets.button_log_in_pressed, assets.button_log_in));
        }
        // LEADERBOARD BUTTON
        Vector2 leaderboard_pos = new Vector2(hudVars.leaderboard_x, hudVars.leaderboard_y);
        ButtonRectangle br = new ButtonRectangle(leaderboard_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_LEADERBOARD, assets.button_all_leaderboard, assets.button_all_leaderboard_pressed, assets.button_all_leaderboard_disabled);
        if(!this.screen.getController().isLogged()) {
            br.disable();
        }
        this.buttons.add(br);
    }

    public void createAboutButtons() {
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.about_close_x, hudVars.about_close_y);
        this.aboutButtons.add(new ButtonCircle(back_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_close, assets.button_close_pressed, assets.button_close));
        // WEB BUTTON
        Vector2 web_pos = new Vector2(hudVars.web_x, hudVars.web_y);
        this.aboutButtons.add(new ButtonCircle(web_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_WEB, assets.button_web, assets.button_web_pressed, assets.button_web));
        // GOOGLE BUTTON
        Vector2 google_pos = new Vector2(hudVars.google_x, hudVars.google_y);
        this.aboutButtons.add(new ButtonRectangle(google_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_GOOGLE, assets.button_facebook, assets.button_facebook_pressed, assets.button_facebook));
        // FACEBOOK BUTTON
        Vector2 facebook_pos = new Vector2(hudVars.facebook_x, hudVars.facebook_y);
        this.aboutButtons.add(new ButtonRectangle(facebook_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_FACEBOOK, assets.button_google, assets.button_google_pressed, assets.button_google));

    }

    public void clearButtons() {
        this.fixedButtons = new ArrayList<Button>();
        this.buttons = new ArrayList<Button>();
        this.checkBoxes = new ArrayList<CheckBox>();
        this.radioGroups = new ArrayList<RadioGroup>();
        this.aboutButtons = new ArrayList<Button>();
    }

    public Button getTouchedButton(float x, float y) {
        if(isAbout() && this.aboutButtons != null) {
            for (Button button : this.aboutButtons) {
                if (button.isTouched(x, y)) {
                    return button;
                }
            }
        }
        // No dialog opened => can touch each buttons
        else  {
            // Check fixed buttons
            if (fixedButtons != null && !fixedButtons.isEmpty()) {
                for (Button button : fixedButtons) {
                    if (button.isTouched(x, y)) {
                        return button;
                    }
                }
            }
            // Check other buttons
            if (this.buttons != null) {
                for (Button button : this.buttons) {
                    if (button.isTouched(x + this.screen.getRenderer().getSettingsCameraOriginX(), y + this.screen.getRenderer().getSettingsCameraOriginY())) {
                        return button;
                    }
                }
            }
        }
        return null;
    }

    public void touchButton(Button button) {
        if (button.isEnabled()) {
            // BACK BUTTON
            if (button.getTag().equals(ButtonCircle.BUTTON_BACK)) {
                back();
            }
            // HELP BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_HELP)) {
                this.screen.getController().help();
            }
            // LOG BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_LOG)) {
                if(this.screen.getController().isLogged()) {
                    this.screen.getController().logOut();
                } else {
                    this.screen.getController().logIn();
                }
            }
            // LEADERBOARD BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_LEADERBOARD)) {
                this.screen.getController().showAllLeaderboards();
            }
            // ABOUT BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_ABOUT)) {
                showCredits();
            }
            // WEB BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_WEB)) {
                this.screen.getController().web();
            }
            // FACEBOOK BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_FACEBOOK)) {
                this.screen.getController().facebook();
            }
            // GOOGLE PLAY BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_GOOGLE)) {
                this.screen.getController().googlePlay();
            }
        }
    }

    public CheckBox getTouchedCheckBox(float x, float y) {
        if(!isAbout()) {
            if (checkBoxes != null) {
                for (CheckBox checkBox : checkBoxes) {
                    if (checkBox.isTouched(x + this.screen.getRenderer().getSettingsCameraOriginX(), y + this.screen.getRenderer().getSettingsCameraOriginY())) {
                        return checkBox;
                    }
                }
            }
        }
        return null;
    }

    public void touchCheckBox(CheckBox checkBox) {
        if(!isAbout()) {
            // MUSIC CHECKBOX
            if (checkBox.getTag().equals(CheckBox.CHECKBOX_MUSIC)) {
                if (!checkBox.isChecked()) {
                    checkBox.check();
                    this.screen.getController().enableMusic();
                    this.screen.getController().startMenuMusic();
                } else {
                    checkBox.uncheck();
                    this.screen.getController().disableMusic();
                    this.screen.getController().stopMenuMusic();
                }
            }
            // SOUNDS CHECKBOX
            else if (checkBox.getTag().equals(CheckBox.CHECKBOX_SOUND)) {
                if (!checkBox.isChecked()) {
                    checkBox.check();
                    this.screen.getController().enableSound();
                } else {
                    checkBox.uncheck();
                    this.screen.getController().disableSound();
                }
            }
        }
    }

    public RadioButton getTouchedRadioButton(float x, float y) {
        if(!isAbout()) {
            if (radioGroups != null) {
                for (RadioGroup group : radioGroups) {
                    for(RadioButton button : group.getButtons()) {
                        if(button.isTouched(x + this.screen.getRenderer().getSettingsCameraOriginX(), y + this.screen.getRenderer().getSettingsCameraOriginY())) {
                            return button;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void touchRadioButton(RadioButton button) {
        if(!isAbout()) {
            // USER SPEED
            if(button.getGroup().getTag().equals(RadioGroup.RADIO_GROUP_USER)) {
                button.check();
                this.screen.getController().updateHumanSpeed(button.getGroup().getCheckedPosition());
            }
            // ROBOT SPEED
            else if (button.getGroup().getTag().equals(RadioGroup.RADIO_GROUP_ROBOT)) {
                button.check();
                System.out.println("Robot speed : " + button.getGroup().getCheckedPosition());
                this.screen.getController().updateRobotSpeed(button.getGroup().getCheckedPosition());
            }
        }
    }

    public void back() {
        if(credits) {
            this.hideCredits();
        } else {
            this.screen.back();
        }
    }

    private void showCredits() {
        this.credits = true;
        createAboutButtons();
    }

    private void hideCredits() {
        this.credits = false;
        createButtons();
    }

    public void resume() {
        createButtons();
        if(isAbout()) {
            createAboutButtons();
        }
    }

    public ArrayList<Button> getFixedButtons() {
        return this.fixedButtons;
    }

    public ArrayList<Button> getButtons() {
        return this.buttons;
    }

    public ArrayList<Button> getAboutButtons() {
        return this.aboutButtons;
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return this.checkBoxes;
    }

    public ArrayList<RadioGroup> getRadioGroups() {
        return this.radioGroups;
    }

    public MainAssets getAssets() {
        return this.assets;
    }

    public SettingsHUDVars getHUDVars() {
        return this.hudVars;
    }

    public boolean isAbout() {
        return this.credits;
    }
}
