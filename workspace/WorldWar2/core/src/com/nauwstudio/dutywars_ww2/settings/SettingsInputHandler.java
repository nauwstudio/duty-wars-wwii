package com.nauwstudio.dutywars_ww2.settings;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.nauwstudio.dutywars_ww2.SettingsScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.VersusScreen;

public class SettingsInputHandler implements InputProcessor {

    SettingsScreen screen;
    Settings settings;
    SettingsRenderer renderer;

    public SettingsInputHandler(SettingsScreen screen) {
        this.screen = screen;
        this.settings = screen.getSettings();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.settings.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Touching button ?
        float button_x = this.renderer.getMenuCameraOriginX() + screenX;
        float button_y = this.renderer.getMenuCameraOriginY() + screenY;
        this.screen.touchedButton = this.settings.getTouchedButton(button_x, button_y);
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.press();
            return true;
        } else {
            this.screen.touchedCheckBox = this.settings.getTouchedCheckBox(button_x, button_y);
            if (this.screen.touchedCheckBox != null) {
                this.screen.touchedCheckBox.press();
                return true;
            } else {
                this.screen.touchedRadioButton = this.settings.getTouchedRadioButton(button_x, button_y);
                if (this.screen.touchedRadioButton != null) {
                    this.screen.touchedRadioButton.press();
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Perform button action ?
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.settings.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        } else {
            if (this.screen.touchedCheckBox != null) {
                // Perform button action
                this.settings.touchCheckBox(this.screen.touchedCheckBox);
                this.screen.touchedCheckBox.release();
                this.screen.touchedCheckBox = null;
                return true;
            } else {
                if (this.screen.touchedRadioButton != null) {
                    // Perform button action
                    this.settings.touchRadioButton(this.screen.touchedRadioButton);
                    this.screen.touchedRadioButton.release();
                    this.screen.touchedRadioButton = null;
                    return true;
                } else {
                    reset();
                    return false;
                }
            }
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void reset() {
        this.screen.touchedButton = null;
        this.screen.touchedCheckBox = null;
    }
}
