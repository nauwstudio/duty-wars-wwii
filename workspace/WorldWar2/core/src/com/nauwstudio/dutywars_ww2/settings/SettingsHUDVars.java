package com.nauwstudio.dutywars_ww2.settings;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class SettingsHUDVars {

    private Settings settings;

    // BACK BUTTON POSITION
    float back_x;
    float back_y;
    // TITLE
    float title_x, title_y, title_width, title_height;
    // SOUNDS CHECK BOXES
    float sound_title_x, sound_title_y, sound_title_width, sound_title_height;
    float music_checkbox_x, music_checkbox_y, music_checkbox_width, music_checkbox_height;
    float effects_checkbox_x, effects_checkbox_y, effects_checkbox_width, effects_checkbox_height;
    // SPEEDS RADIO BUTTONS
    float speed_title_x, speed_title_y, speed_title_width, speed_title_height;
    float speed_subtitle_x, speed_subtitle_y, speed_subtitle_width, speed_subtitle_height;
    float speed_user_x, speed_user_y, speed_user_width, speed_user_height;
    float speed_robot_x, speed_robot_y, speed_robot_width, speed_robot_height;
    // BUTTONS
    float help_x, help_y;
    float log_x, log_y;
    float leaderboard_x, leaderboard_y;
    float credits_x, credits_y;
    // CREDITS
    float about_x, about_y, about_width, about_height;
    float about_close_x, about_close_y;
    float about_title_x, about_title_y, about_title_width, about_title_height;
    float web_x, web_y;
    float google_x, google_y;
    float facebook_x, facebook_y;
    float about_info_x, about_info_y, about_info_width, about_info_height;

    public SettingsHUDVars(Settings settings) {
        this.settings = settings;
        initHUDVariables();
    }

    public void initHUDVariables() {
        float button_ratio = this.settings.getAssets().button_versus.getRegionWidth() / (float) this.settings.getAssets().button_versus.getRegionHeight();
        float divider = 5.0f;
        // BACK BUTTON POSITION
        back_x = Util.getCircleButtonSpace() / 4f;
        back_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
        // TITLE
        this.title_x = 0;
        this.title_height = Util.getActionBarHeight();
        this.title_width = Util.getScreenWidth();
        this.title_y = Util.getScreenHeight() - Util.getActionBarHeight();
        // VERTICAL
        if (Util.isVertical()) {
            divider = 8.0f;
            // AUDIO
            this.sound_title_x = Util.getCircleButtonSize();
            this.sound_title_height = Util.getContentHeight() / divider;
            this.sound_title_width = Util.getScreenWidth();
            this.sound_title_y = this.title_y - this.sound_title_height;
            // MUSIC CHECKBOX
            this.music_checkbox_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.music_checkbox_height = Util.getContentHeight() / (divider * 2);
            this.music_checkbox_x = Util.getCircleButtonSpace();
            this.music_checkbox_y = this.sound_title_y - this.music_checkbox_height;
            // EFFECTS CHECKBOX
            this.effects_checkbox_width = this.music_checkbox_width;
            this.effects_checkbox_height = this.music_checkbox_height;
            this.effects_checkbox_x = this.music_checkbox_x + this.music_checkbox_width + Util.getCircleButtonSpace();
            this.effects_checkbox_y = this.sound_title_y - this.effects_checkbox_height;
            // SPEED
            this.speed_title_x = Util.getCircleButtonSize();
            this.speed_title_height = Util.getContentHeight()/divider;
            this.speed_title_width = Util.getScreenWidth();
            this.speed_title_y = this.music_checkbox_y - this.speed_title_height;
            // subtitle
            this.speed_subtitle_x = 0;
            this.speed_subtitle_height = Util.getContentHeight() / (divider * 2);
            this.speed_subtitle_width = Util.getScreenWidth();
            this.speed_subtitle_y = this.speed_title_y - this.speed_subtitle_height;
            // USER SPEED RADIOGROUP
            this.speed_user_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_user_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_user_x = Util.getCircleButtonSpace();
            this.speed_user_y = this.speed_subtitle_y - this.speed_user_height;
            // ROBOT SPEED RADIOGROUP
            this.speed_robot_width = (Util.getScreenWidth() - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_robot_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_robot_x = this.speed_user_x + this.speed_user_width + Util.getCircleButtonSpace();
            this.speed_robot_y = this.speed_subtitle_y - this.speed_robot_height;
            // BUTTONS
            this.help_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.help_y = this.speed_user_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            this.credits_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.credits_y = this.help_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            this.log_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.log_y = this.credits_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            this.leaderboard_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.leaderboard_y = this.log_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
        }
        // HORIZONTAL
        else {
            // AUDIO
            this.sound_title_x = Util.getCircleButtonSpace();
            this.sound_title_height = Util.getContentHeight() / (divider*2f);
            this.sound_title_width = 2.5f * Util.getCircleButtonSize();
            this.sound_title_y = this.title_y - this.sound_title_height;
            // MUSIC CHECKBOX
            this.music_checkbox_width = (Util.getScreenWidth() - this.sound_title_x - this.sound_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.music_checkbox_height = Util.getContentHeight() / (divider*2f);
            this.music_checkbox_x = this.sound_title_x + this.sound_title_width + Util.getCircleButtonSpace();
            this.music_checkbox_y = this.sound_title_y;
            // EFFECTS CHECKBOX
            this.effects_checkbox_width = this.music_checkbox_width;
            this.effects_checkbox_height = this.music_checkbox_height;
            this.effects_checkbox_x = this.music_checkbox_x + this.music_checkbox_width + Util.getCircleButtonSpace();
            this.effects_checkbox_y = this.music_checkbox_y ;
            // SPEED
            this.speed_title_x = Util.getCircleButtonSpace();
            this.speed_title_height = Util.getContentHeight() / (divider*2f);
            this.speed_title_width = 2.5f * Util.getCircleButtonSize();
            this.speed_title_y = this.sound_title_y - this.speed_title_height - Util.getCircleButtonSpace()/2f;
            // subtitle
            this.speed_subtitle_x = this.speed_title_x + this.speed_title_width + Util.getCircleButtonSpace();
            this.speed_subtitle_height = Util.getContentHeight() / (divider * 2f);
            this.speed_subtitle_width = (Util.getScreenWidth() -  Util.getCircleButtonSpace() - this.speed_subtitle_x);
            this.speed_subtitle_y = this.speed_title_y;
            // USER SPEED RADIOGROUP
            this.speed_user_width = (Util.getScreenWidth() - this.speed_title_x - this.speed_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_user_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_user_x = this.speed_title_x + this.speed_title_width + Util.getCircleButtonSpace();
            this.speed_user_y = this.speed_title_y - this.speed_user_height;
            // ROBOT SPEED RADIOGROUP
            this.speed_robot_width = (Util.getScreenWidth() - this.speed_title_x - this.speed_title_width - 3 * Util.getCircleButtonSpace()) / 2f;
            this.speed_robot_height = 3 * Util.getContentHeight() / (divider * 2);
            this.speed_robot_x = this.speed_user_x + this.speed_user_width + Util.getCircleButtonSpace();
            this.speed_robot_y = this.speed_subtitle_y - this.speed_robot_height;
            // BUTTONS
            this.help_x = Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.help_y = this.speed_user_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            this.credits_x = Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.credits_y = this.help_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
            this.log_x = 3 * Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.log_y = this.help_y;
            this.leaderboard_x = 3 * Util.getScreenWidth() / 4f - (Util.getRectangleButtonHeight() * button_ratio) / 2f;
            this.leaderboard_y = this.log_y - Util.getRectangleButtonHeight() - Util.getCircleButtonSpace();
        }
        // ABOUT
        this.about_width = Util.getScreenWidth() - Util.getCircleButtonSpace();
        this.about_x = Util.getScreenWidth() / 2f - this.about_width / 2f;
        this.about_height = Util.getScreenHeight() - Util.getCircleButtonSpace();
        this.about_y = Util.getScreenHeight() / 2f - this.about_height / 2f;
        this.about_close_x = this.about_x + this.about_width - 0.7f * Util.getCircleButtonSize();
        this.about_close_y = this.about_y + this.about_height - 0.7f * Util.getCircleButtonSize();
        if (Util.isVertical()) {
            this.about_title_width = this.about_width;
            this.about_title_x = about_x;
            this.about_title_height = this.about_height / 8f;
            this.about_title_y = this.about_y + this.about_height - this.about_title_height;
            this.web_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
            this.web_y = this.about_title_y - this.about_title_height;
            this.google_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f;
            this.google_y = this.web_y;
            this.facebook_x = Util.getScreenWidth() / 2f + Util.getCircleButtonSize() / 2f + Util.getCircleButtonSpace();
            this.facebook_y = this.web_y;
            this.about_info_x = about_x;
            this.about_info_y = about_y;
            this.about_info_width = about_width;
            this.about_info_height = 6 * this.about_height / 8f;
        } else {
            this.about_title_width = this.about_width;
            this.about_title_x = about_x;
            this.about_title_height = this.about_height / 6f;
            this.about_title_y = this.about_y + this.about_height - this.about_title_height;
            this.web_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f - Util.getCircleButtonSpace() - Util.getCircleButtonSize();
            this.web_y = this.about_title_y - this.about_title_height;
            this.google_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f;
            this.google_y = this.web_y;
            this.facebook_x = Util.getScreenWidth() / 2f + Util.getCircleButtonSize() / 2f + Util.getCircleButtonSpace();
            this.facebook_y = this.web_y;
            this.about_info_x = about_x;
            this.about_info_y = about_y;
            this.about_info_width = about_width;
            this.about_info_height = 4 * this.about_height / 6f;
        }
    }
}
