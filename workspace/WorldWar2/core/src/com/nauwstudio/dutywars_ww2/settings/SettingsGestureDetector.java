package com.nauwstudio.dutywars_ww2.settings;

import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.SettingsScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.VersusScreen;

public class SettingsGestureDetector implements GestureListener {

    SettingsScreen screen;
    Settings settings;
    SettingsRenderer renderer;

    public SettingsGestureDetector(SettingsScreen screen) {
        this.screen = screen;
        this.settings = screen.getSettings();
        this.renderer = screen.getRenderer();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return true;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        // Release button
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        }
        // Release checkbox
        else if (this.screen.touchedCheckBox != null) {
            this.screen.touchedCheckBox.release();
            this.screen.touchedCheckBox = null;
            return true;
        }
        // Release radiobutton
        else if (this.screen.touchedRadioButton != null) {
            this.screen.touchedRadioButton.release();
            this.screen.touchedRadioButton = null;
            return true;
        }
        // Move settings camera
        else {
            this.renderer.moveSettingsCamera(deltaY * 1.0f);
            return true;
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2
            pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
