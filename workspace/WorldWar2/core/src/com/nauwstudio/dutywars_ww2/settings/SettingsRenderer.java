package com.nauwstudio.dutywars_ww2.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.CheckBox;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RadioGroup;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class SettingsRenderer {

    private Settings settings;
    SpriteBatch batch;

    private OrthographicCamera mainCamera;
    private OrthographicCamera settingsCamera;


    private MainAssets assets;
    private SettingsHUDVars hudVars;

    public SettingsRenderer(Settings settings) {
        this.settings = settings;
        // CAMERAS
        mainCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        mainCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        mainCamera.update();
        settingsCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        settingsCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        settingsCamera.update();

        this.batch = new SpriteBatch();
        this.assets = this.settings.getAssets();
        this.hudVars = this.settings.getHUDVars();
    }

    public float getMenuCameraPositionX() {
        return this.mainCamera.position.x;
    }

    public float getMenuCameraPositionY() {
        return this.mainCamera.position.y;
    }

    public float getMenuCameraOriginX() {
        return getMenuCameraPositionX() - (getMenuCameraWidth() / 2f);
    }

    public float getMenuCameraOriginY() {
        return getMenuCameraPositionY() - (getMenuCameraHeight() / 2f);
    }

    public float getMenuCameraWidth() {
        return this.mainCamera.viewportWidth;
    }

    public float getMenuCameraHeight() {
        return this.mainCamera.viewportHeight;
    }


    public float getSettingsCameraPositionX() {
        return this.settingsCamera.position.x;
    }

    public float getSettingsCameraPositionY() {
        return this.settingsCamera.position.y;
    }

    public float getSettingsCameraOriginX() {
        return getSettingsCameraPositionX() - (getSettingsCameraWidth() / 2f);
    }

    public float getSettingsCameraOriginY() {
        return getSettingsCameraPositionY() - (getSettingsCameraHeight() / 2f);
    }

    public float getSettingsCameraWidth() {
        return this.settingsCamera.viewportWidth;
    }

    public float getSettingsCameraHeight() {
        return this.settingsCamera.viewportHeight;
    }

    public void moveSettingsCamera(float distY) {
        // Move only if no dialog open
        if (!this.settings.isAbout()) {
            float border_top = this.getSettingsCameraHeight() / 2f;
            float last_save_y = this.settings.getButtons().get(this.settings.getButtons().size() - 1).getPosition().y;
            float border_bottom = Math.min(this.getSettingsCameraHeight() / 2f, this.getSettingsCameraHeight() / 2f + last_save_y - Util.getCircleButtonSpace());
            this.settingsCamera.translate(0, distY);
            this.settingsCamera.update();
            // Out of borders ?
            if (this.settingsCamera.position.y < border_bottom) {
                this.settingsCamera.translate(0, border_bottom - this.settingsCamera.position.y);
            }
            if (this.settingsCamera.position.y > border_top) {
                this.settingsCamera.translate(0, border_top - this.settingsCamera.position.y);
            }
            this.settingsCamera.update();
        }
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(5 / 255f, 5 / 255f, 5 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderFixedElements(delta);
        renderUnfixedElements(delta);
        if (this.settings.isAbout()) {
            renderAboutDialog(delta);
        }
    }

    private void renderFixedElements(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        // Draw background
        RenderUtil.drawBackground(batch, assets);
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        for (Button b : this.settings.getFixedButtons()) {
            b.render(batch);
        }
        RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("main.settings"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        batch.end();
    }

    private void renderUnfixedElements(float delta) {
        /*Cut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) this.hudVars.title_y);
        batch.setProjectionMatrix(settingsCamera.combined);
        batch.begin();
        // Render buttons
        for (Button b : this.settings.getButtons()) {
            b.render(batch);
        }
        // Draw sounds title
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("settings.audio"), this.hudVars.sound_title_x, this.hudVars.sound_title_y, this.hudVars.sound_title_width, this.hudVars.sound_title_height);
        for (CheckBox c : this.settings.getCheckBoxes()) {
            c.render(batch, this.assets.font_big, this.assets.font_big_gray);
        }
        // Draw speeds title
        RenderUtil.drawTextInCellLeft(batch, this.assets.font_bigbig, this.assets.stringBundle.get("settings.speed"), this.hudVars.speed_title_x, this.hudVars.speed_title_y, this.hudVars.speed_title_width, this.hudVars.speed_title_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.speed_user"), this.hudVars.speed_subtitle_x, this.hudVars.speed_subtitle_y, this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_height);
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.speed_robot"), this.hudVars.speed_subtitle_x + this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_y, this.hudVars.speed_subtitle_width / 2f, this.hudVars.speed_subtitle_height);
        for (RadioGroup c : this.settings.getRadioGroups()) {
            c.render(batch, this.assets.font_big, this.assets.font_big_gray);
        }
        batch.end();
        /*Uncut bottom screen*/
        Gdx.gl.glViewport(0, 0, (int) Util.getScreenWidth(), (int) Util.getScreenHeight());
    }

    public void renderAboutDialog(float delta) {
        batch.setProjectionMatrix(mainCamera.combined);
        batch.begin();
        batch.draw(assets.mission_bg, this.hudVars.about_x, this.hudVars.about_y, this.hudVars.about_width, this.hudVars.about_height);
        if (Util.isVertical()) {
            RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("settings.about"), this.hudVars.about_title_x, this.hudVars.about_title_y, this.hudVars.about_title_width, this.hudVars.about_title_height);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.game"), this.hudVars.about_info_x, this.hudVars.about_info_y + 7 * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.game_content"), this.hudVars.about_info_x, this.hudVars.about_info_y + 6f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.archives"), this.hudVars.about_info_x, this.hudVars.about_info_y + 5f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.archives_content"), this.hudVars.about_info_x, this.hudVars.about_info_y + 4f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.sounds"), this.hudVars.about_info_x, this.hudVars.about_info_y + 3f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.sounds_content"), this.hudVars.about_info_x, this.hudVars.about_info_y + 2f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.support"), this.hudVars.about_info_x, this.hudVars.about_info_y + 1f * this.hudVars.about_info_height / 8f, this.hudVars.about_info_width, this.hudVars.about_info_height / 8f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.support_content"), this.hudVars.about_info_x, this.hudVars.about_info_y, this.hudVars.about_info_width, this.hudVars.about_info_height/8f);
        } else {
            RenderUtil.drawTextInCell(batch, this.assets.font_giant, this.assets.stringBundle.get("settings.about"), this.hudVars.about_title_x, this.hudVars.about_title_y, this.hudVars.about_title_width, this.hudVars.about_title_height);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.game"), this.hudVars.about_info_x, this.hudVars.about_info_y + 3 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.game_content"), this.hudVars.about_info_x, this.hudVars.about_info_y + 2 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.archives"), this.hudVars.about_info_x, this.hudVars.about_info_y + 1 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.archives_content"), this.hudVars.about_info_x, this.hudVars.about_info_y, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            //
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.sounds"), this.hudVars.about_info_x + this.hudVars.about_info_width / 2f, this.hudVars.about_info_y + 3 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.sounds_content"), this.hudVars.about_info_x + this.hudVars.about_info_width / 2f, this.hudVars.about_info_y + 2 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("settings.support"), this.hudVars.about_info_x + this.hudVars.about_info_width / 2f, this.hudVars.about_info_y + 1 * this.hudVars.about_info_height / 4f, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
            RenderUtil.drawTextInCellTop(batch, this.assets.font_medium, this.assets.stringBundle.get("settings.support_content"), this.hudVars.about_info_x + this.hudVars.about_info_width / 2f, this.hudVars.about_info_y, this.hudVars.about_info_width / 2f, this.hudVars.about_info_height / 4f);
        }
        for (Button b : this.settings.getAboutButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    public void resize(int width, int height) {
        mainCamera = new OrthographicCamera(width, height);
        mainCamera.setToOrtho(false, width, height);
        mainCamera.update();
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        settingsCamera = new OrthographicCamera(width, (int) this.hudVars.title_y);
        settingsCamera.setToOrtho(false, width, (int) this.hudVars.title_y);
        settingsCamera.update();
        // REPLACE BUTTONS
        this.settings.createButtons();
        if (this.settings.isAbout()) {
            this.settings.createAboutButtons();
        }
    }
}
