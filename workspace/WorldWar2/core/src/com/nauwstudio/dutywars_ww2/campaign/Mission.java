package com.nauwstudio.dutywars_ww2.campaign;

import com.nauwstudio.dutywars_ww2.game.Army;

public class Mission {

    // BRITAIN
    public static final int BRITAIN_ID = 1;
    public static final String BRITAIN_NAME = "britain.name";
    public static final String BRITAIN_LOCATION = "britain.location";
    public static final String BRITAIN_DESCRIPTION = "britain.description";
    public static final int[] BRITAIN_ARMIES = new int[]{Army.GBR,Army.GER};
    public static final int[] BRITAIN_POSITION = new int[]{4600,3640};
	// TOBRUK
	public static final int TOBRUK_ID = 2;
	public static final String TOBRUK_NAME = "tobruk.name";
	public static final String TOBRUK_LOCATION = "tobruk.location";
	public static final String TOBRUK_DESCRIPTION = "tobruk.description";
	public static final int[] TOBRUK_ARMIES = new int[]{Army.GBR,Army.GER};
	public static final int[] TOBRUK_POSITION = new int[]{5260,3030};
	// PEARL HARBOR
	public static final int PEARL_HARBOR_ID = 3;
	public static final String PEARL_HARBOR_NAME = "pearlharbor.name";
	public static final String PEARL_HARBOR_LOCATION = "pearlharbor.location";
	public static final String PEARL_HARBOR_DESCRIPTION = "pearlharbor.description";
	public static final int[] PEARL_HARBOR_ARMIES = new int[]{Army.USA,Army.JAP};
	public static final int[] PEARL_HARBOR_POSITION = new int[]{330,2680};
	// HONG KONG
	public static final int HONG_KONG_ID = 4;
	public static final String HONG_KONG_NAME = "hong_kong.name";
	public static final String HONG_KONG_LOCATION = "hong_kong.location";
	public static final String HONG_KONG_DESCRIPTION = "hong_kong.description";
	public static final int[] HONG_KONG_ARMIES = new int[]{Army.GBR,Army.JAP};
	public static final int[] HONG_KONG_POSITION = new int[]{8050,2740};
	public static final String HONG_KONG_LEADERBOARD = "";
    // MIDWAY
    public static final int MIDWAY_ID = 5;
    public static final String MIDWAY_NAME = "midway.name";
    public static final String MIDWAY_LOCATION = "midway.location";
    public static final String MIDWAY_DESCRIPTION = "midway.description";
    public static final int[] MIDWAY_ARMIES = new int[]{Army.USA,Army.JAP};
    public static final int[] MIDWAY_POSITION = new int[]{9830,2890};
	public static final String MIDWAY_LEADERBOARD = "";
    // STALINGRAD
    public static final int STALINGARD_ID = 6;
    public static final String STALINGRAD_NAME = "stalingrad.name";
    public static final String STALINGRAD_LOCATION = "stalingrad.location";
    public static final String STALINGRAD_DESCRIPTION = "stalingrad.description";
    public static final int[] STALINGRAD_ARMIES = new int[]{Army.USS,Army.GER};
    public static final int[] STALINGRAD_POSITION = new int[]{5700,3560};
	public static final String STALINGRAD_LEADERBOARD = "";
    // GUADALCANAL
    public static final int GUADALCANAL_ID = 7;
    public static final String GUADALCANAL_NAME = "guadalcanal.name";
    public static final String GUADALCANAL_LOCATION = "guadalcanal.location";
    public static final String GUADALCANAL_DESCRIPTION = "guadalcanal.description";
    public static final int[] GUADALCANAL_ARMIES = new int[]{Army.USA,Army.JAP};
    public static final int[] GUADALCANAL_POSITION = new int[]{9560,1740};
	public static final String GUADALCANAL_LEADERBOARD = "";
    // EL ALAMEIN
    public static final int EL_ALAMEIN_ID = 8;
    public static final String EL_ALAMEIN_NAME = "elalamein.name";
    public static final String EL_ALAMEIN_LOCATION = "elalamein.location";
    public static final String EL_ALAMEIN_DESCRIPTION = "elalamein.description";
    public static final int[] EL_ALAMEIN_ARMIES = new int[]{Army.GBR,Army.GER};
    public static final int[] EL_ALAMEIN_POSITION = new int[]{5380,3000};
	public static final String EL_ALAMEIN_LEADERBOARD = "";
    // CAPRI OPERATION
    public static final int CAPRI_ID = 9;
    public static final String CAPRI_NAME = "capri.name";
    public static final String CAPRI_LOCATION = "capri.location";
    public static final String CAPRI_DESCRIPTION = "capri.description";
    public static final int[] CAPRI_ARMIES = new int[]{Army.GBR,Army.GER};
    public static final int[] CAPRI_POSITION = new int[]{4880,3090};
	public static final String CAPRI_LEADERBOARD = "";
    // KURSK
    public static final int KURSK_ID = 10;
    public static final String KURSK_NAME = "kursk.name";
    public static final String KURSK_LOCATION = "kursk.location";
    public static final String KURSK_DESCRIPTION = "kursk.description";
    public static final int[] KURSK_ARMIES = new int[]{Army.USS,Army.GER};
    public static final int[] KURSK_POSITION = new int[]{5470,3690};
	public static final String KURSK_LEADERBOARD = "";
    // HUSKY OPERATION
    public static final int HUSKY_ID = 11;
    public static final String HUSKY_NAME = "husky.name";
    public static final String HUSKY_LOCATION = "husky.location";
    public static final String HUSKY_DESCRIPTION = "husky.description";
    public static final int[] HUSKY_ARMIES = new int[]{Army.USA, Army.GER, Army.GBR};
    public static final int[] HUSKY_POSITION = new int[]{4960,3190};
	public static final String HUSKY_LEADERBOARD = "";
    // BELGOROD-KHARKOV
    public static final int KHARKOV_ID = 12;
    public static final String KHARKOV_NAME = "kharkov.name";
    public static final String KHARKOV_LOCATION = "kharkov.location";
    public static final String KHARKOV_DESCRIPTION = "kharkov.description";
    public static final int[] KHARKOV_ARMIES = new int[]{Army.USS, Army.GER};
    public static final int[] KHARKOV_POSITION = new int[]{5470,3590};
	public static final String KHARKOV_LEADERBOARD = "";
    // LENINGRAD
    public static final int LENINGRAD_ID = 13;
    public static final String LENINGRAD_NAME = "leningrad.name";
    public static final String LENINGRAD_LOCATION = "leningrad.location";
    public static final String LENINGRAD_DESCRIPTION = "leningrad.description";
    public static final int[] LENINGRAD_ARMIES = new int[]{Army.USS,Army.GER};
    public static final int[] LENINGRAD_POSITION = new int[]{5110,3910};
	public static final String LENINGRAD_LEADERBOARD = "";
    // CRIMEA
    public static final int CRIMEA_ID = 14;
    public static final String CRIMEA_NAME = "crimea.name";
    public static final String CRIMEA_LOCATION = "crimea.location";
    public static final String CRIMEA_DESCRIPTION = "crimea.description";
    public static final int[] CRIMEA_ARMIES = new int[]{Army.USS,Army.GER};
    public static final int[] CRIMEA_POSITION = new int[]{5400,3440};
	public static final String CRIMEA_LEADERBOARD = "";
	// ROMA
	public static final int ROMA_ID = 15;
	public static final String ROMA_NAME = "roma.name";
	public static final String ROMA_LOCATION = "roma.location";
	public static final String ROMA_DESCRIPTION = "roma.description";
	public static final int[] ROMA_ARMIES = new int[]{Army.USA,Army.GER, Army.GBR};
	public static final int[] ROMA_POSITION = new int[]{4930,3320};
	public static final String ROMA_LEADERBOARD = "";
    // OMAHA BEACH
    public static final int OMAHA_BEACH_ID = 16;
    public static final String OMAHA_BEACH_NAME = "omahabeach.name";
    public static final String OMAHA_BEACH_LOCATION = "omahabeach.location";
    public static final String OMAHA_BEACH_DESCRIPTION = "omahabeach.description";
    public static final int[] OMAHA_BEACH_ARMIES = new int[]{Army.USA,Army.GER};
    public static final int[] OMAHA_BEACH_POSITION = new int[]{4570,3580};
	public static final String OMAHA_BEACH_LEADERBOARD = "";
    // PARIS
	public static final int PARIS_ID = 17;
	public static final String PARIS_NAME = "paris.name";
	public static final String PARIS_LOCATION = "paris.location";
	public static final String PARIS_DESCRIPTION = "paris.description";
	public static final int[] PARIS_ARMIES = new int[]{Army.USA,Army.GER};
    public static final int[] PARIS_POSITION = new int[]{4650,3560};
	public static final String PARIS_LEADERBOARD = "";
    // MARKET GARDEN
	public static final int MARKET_GARDEN_ID = 18;
	public static final String MARKET_GARDEN_NAME = "marketgarden.name";
	public static final String MARKET_GARDEN_LOCATION = "marketgarden.location";
	public static final String MARKET_GARDEN_DESCRIPTION = "marketgarden.description";
	public static final int[] MARKET_GARDEN_ARMIES = new int[]{Army.USA, Army.GER ,Army.GBR};
    public static final int[] MARKET_GARDEN_POSITION = new int[]{4730,3650};
	public static final String MARKET_GRADEN_LEADERBOARD = "";
    // BATTLE OF THE BULGE
	public static final int BULGE_ID = 19;
	public static final String BULGE_NAME = "bulge.name";
	public static final String BULGE_LOCATION = "bulge.location";
	public static final String BULGE_DESCRIPTION = "bulge.description";
	public static final int[] BULGE_ARMIES = new int[]{Army.USA,Army.GER};
    public static final int[] BULGE_POSITION = new int[]{4730,3580};
	public static final String BULGE_LEADERBOARD = "";
    // BATTLE OF LUZON
	public static final int LUZON_ID = 20;
	public static final String LUZON_NAME = "luzon.name";
	public static final String LUZON_LOCATION = "luzon.location";
	public static final String LUZON_DESCRIPTION = "luzon.description";
	public static final int[] LUZON_ARMIES = new int[]{Army.USA,Army.JAP};
    public static final int[] LUZON_POSITION = new int[]{8310,2570};
	public static final String LUZON_LEADERBOARD = "";
    // IWO JIMA
	public static final int IWO_JIMA_ID = 21;
	public static final String IWO_JIMA_NAME = "iwojima.name";
	public static final String IWO_JIMA_LOCATION = "iwojima.location";
	public static final String IWO_JIMA_DESCRIPTION = "iwojima.description";
	public static final int[] IWO_JIMA_ARMIES = new int[]{Army.USA,Army.JAP};
    public static final int[] IWO_JIMA_POSITION = new int[]{8750,2826};
	public static final String IWO_JIMA_LEADERBOARD = "";
    // VIENNA
    public static final int VIENNA_ID = 22;
    public static final String VIENNA_NAME = "vienna.name";
    public static final String VIENNA_LOCATION = "vienna.location";
    public static final String VIENNA_DESCRIPTION = "vienna.description";
    public static final int[] VIENNA_ARMIES = new int[]{Army.USS,Army.GER};
    public static final int[] VIENNA_POSITION = new int[]{4950,3540};
	public static final String VIENNA_LEADERBOARD = "";
    // OKINAWA
	public static final int OKINAWA_ID = 23;
	public static final String OKINAWA_NAME = "okinawa.name";
	public static final String OKINAWA_LOCATION = "okinawa.location";
	public static final String OKINAWA_DESCRIPTION = "okinawa.description";
	public static final int[] OKINAWA_ARMIES = new int[]{Army.USA,Army.JAP};
    public static final int[] OKINAWA_POSITION = new int[]{8390,2870};
	public static final String OKINAWA_LEADERBOARD = "";
    // BERLIN
	public static final int BERLIN_ID = 24;
	public static final String BERLIN_NAME = "berlin.name";
	public static final String BERLIN_LOCATION = "berlin.location";
	public static final String BERLIN_DESCRIPTION = "berlin.description";
	public static final int[] BERLIN_ARMIES = new int[]{Army.USA, Army.GER ,Army.USS};
    public static final int[] BERLIN_POSITION = new int[]{4860,3660};
	public static final String BERLIN_LEADERBOARD = "";
	// PARIS
	public static final int SAKHALIN_ID = 25;
	public static final String SAKHALIN_NAME = "sakhalin.name";
	public static final String SAKHALIN_LOCATION = "sakhalin.location";
	public static final String SAKHALIN_DESCRIPTION = "sakhalin.description";
	public static final int[] SAKHALIN_ARMIES = new int[]{Army.USS,Army.JAP};
	public static final int[] SAKHALIN_POSITION = new int[]{8600,3610};
	public static final String SAKHALIN_LEADERBOARD = "";

	private int id;
	private String name;
	private int map_id;
	private String desc;
	private String location;
	private int[] armies;
	private int score;
	private int[] position;
	private boolean locked;
	
	public Mission(int id, String name, int map_id, String desc, String location, int[] armies, int score, int[] position, boolean locked) {
		this.id = id;
		this.name = name;
		this.map_id = map_id;
		this.desc = desc;
		this.location = location;
		this.armies = armies;
		this.score = score;
		this.position = position;
		this.locked = locked;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public boolean isLocked() {
		return this.locked;
	}
	
	public int getMapId() {
		return this.map_id;
	}

	public String getLocation() {
		return location;
	}

	public int[] getArmies() {
		return armies;
	}

	public static String armiesToString(int[] armies) {
		String result = "";
		for(int army :armies) {
			if(!result.isEmpty()) {
				result += "#";
			}
			result += "" + army;
		}
		return result;
	}

	public static int[] stringToArmies(String armies) {
		System.out.println("ARMIES : " + armies);
		String[] tab = armies.split("#");
		int[] result = new int[tab.length];
		System.out.println(tab.length);
		for(int i = 0; i < tab.length; i ++) {
			result[i] = Integer.parseInt(tab[i]);
		}
		return result;
	}

	public int[] getPosition() {
		return position;
	}

    public String toString() {
        return this.id + ". " + this.name + " " + this.map_id + " " + this.desc + " " + this.location + " " + this.armies[0]+this.armies[1] + " : " + this.locked;
    }
}
