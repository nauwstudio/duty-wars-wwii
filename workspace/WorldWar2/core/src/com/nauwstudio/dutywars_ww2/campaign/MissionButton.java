package com.nauwstudio.dutywars_ww2.campaign;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;

public class MissionButton extends ButtonCircle {

    private Mission mission;

    private float star_x, star_y1, star_y2, star_width, star_height, star_icon_size;


    public MissionButton(Vector2 position, float width, Mission mission, TextureRegion texture, TextureRegion texture_pressed) {
        super(position, width, BUTTON_MISSION, texture, texture_pressed, null);
        this.mission = mission;
        initHUDVars();
    }

    private void initHUDVars() {
        this.star_x = position.x;
        this.star_width = this.width/3f;
        this.star_height = this.star_width;
        this.star_y1 = this.position.y + 6*this.height/8f;
        this.star_y2 = this.position.y + 7*this.height/8f;
        this.star_icon_size = this.star_width;
    }

    public void render(SpriteBatch batch, MainAssets assets) {
        super.render(batch);
        // RENDER STARS
        for(int i = 0; i < 3; i ++) {
            if(Util.getStarsAmount(this.mission.getScore()) > i) {
                if(i == 1) {
                    RenderUtil.drawTextureInCell(batch, assets.star_full,this.star_x + i*this.star_width, this.star_y2, this.star_width, this.star_height, this.star_icon_size);
                } else {
                    RenderUtil.drawTextureInCell(batch, assets.star_full, this.star_x + i*this.star_width, this.star_y1, this.star_width, this.star_height, this.star_icon_size);
                }
            }
        }
    }

    public Mission getMission() {
        return this.mission;
    }

    @Override
    public boolean isMissionButton() {
        return true;
    }
}
