package com.nauwstudio.dutywars_ww2.campaign;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class CampaignHUDVars {

    private Campaign campaign;

    // NEXT MISSION BUTTON POSITION
    float next_mission_x;
    float next_mission_y;
    // MENU BUTTON POSITION
    float close_menu_x;
    float close_menu_y;
    float start_game_x;
    float start_game_y;
    // BACK BUTTON POSITION
    float back_x;
    float back_y;
    public float score_x,score_y,score_width,score_height;
    public float score_icon_x, score_icon_y, score_icon_size;
    // MISSION HUD
    public float mission_x, mission_y, mission_width, mission_height;
    public float mission_fling_x, mission_fling_y, mission_fling_width, mission_fling_height, mission_fling_small_icon_size, mission_fling_big_icon_size;
    public float mission_title_x, mission_title_y, mission_title_width, mission_title_height;
    public float mission_location_x, mission_location_y, mission_location_width, mission_location_height;
    public float mission_map_x, mission_map_y, mission_map_width, mission_map_height, mission_map_icon_size;
    public float mission_armies_x, mission_armies_y, mission_armies_width, mission_armies_height, mission_armies_icon_size;
    public float mission_description_x, mission_description_y, mission_description_width, mission_description_height;
    public float mission_score_x, mission_score_y, mission_score_width, mission_score_height, mission_score_icon_size;
    public float mission_leaderboard_x, mission_leaderboard_y;
    // UNLOCK HUD
    public float unlock_x, unlock_y, unlock_width, unlock_height;
    public float unlock_close_x, unlock_close_y;
    public float unlock_title_x, unlock_title_y, unlock_title_width, unlock_title_height;
    public float unlock_map_x, unlock_map_y, unlock_map_width, unlock_map_height, unlock_map_icon_size;
    public float unlock_text_x, unlock_text_y, unlock_text_width, unlock_text_height;

    public CampaignHUDVars(Campaign campaign) {
        this.campaign = campaign;
        initHUDVariables();
    }

    public void initHUDVariables() {
        // MAIN BUTTONS POSITIONS
        next_mission_x = Util.getScreenWidth() / 2f - Util.getCircleButtonSize() / 2f;
        next_mission_y = Util.getCircleButtonSpace() / 4f;
        // CLOSE MENU BUTTON POSITIONS
        close_menu_x = Util.getScreenWidth() - 0.7f * Util.getCircleButtonSize();
        close_menu_y = Util.getScreenHeight() - 0.7f * Util.getCircleButtonSize();
        // BACK BUTTON POSITION
        back_x = Util.getCircleButtonSpace() / 4f;
        back_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
        this.score_icon_size = Util.getCircleButtonSize();
        this.score_icon_x = Util.getScreenWidth() - this.score_icon_size - Util.getCircleButtonSpace()/4f;
        this.score_icon_y = Util.getScreenHeight() - this.score_icon_size - Util.getCircleButtonSpace()/4f;
        this.score_height = this.score_icon_size;
        this.score_width = this.score_icon_size*3;
        this.score_y = this.score_icon_y;
        this.score_x = this.score_icon_x - this.score_width;
        initMissionVars();
        initUnlockGameVars();
    }

    private void initMissionVars() {
        this.mission_x = 0;
        this.mission_y = 0;
        this.mission_width = Util.getScreenWidth() - 2 * mission_x;
        this.mission_height = Util.getScreenHeight() - 2 * mission_y;
        if (Util.isVertical()) {
            // Mission fling
            this.mission_fling_x = 0.8f * Util.getCircleButtonSize();
            this.mission_fling_width = this.mission_width - 2 * this.mission_fling_x;
            this.mission_fling_height = this.mission_height / 18f;
            this.mission_fling_y = this.mission_y + this.mission_height - this.mission_fling_height;
            this.mission_fling_small_icon_size = Util.getScreenWidth() / 50f;
            this.mission_fling_big_icon_size = Util.getScreenWidth() / 30f;
            // Mission title
            this.mission_title_x = this.mission_x;
            this.mission_title_width = this.mission_width;
            this.mission_title_height = this.mission_height / 18f;
            this.mission_title_y = this.mission_fling_y - this.mission_title_height;
            // Mission location
            this.mission_location_x = this.mission_x;
            this.mission_location_width = this.mission_width;
            this.mission_location_height = this.mission_height / 18f;
            this.mission_location_y = this.mission_title_y - this.mission_location_height;
            // Mission map
            this.mission_map_x = this.mission_x + Util.getCircleButtonSpace();
            this.mission_map_width = this.mission_width - 2 * this.mission_map_x;
            this.mission_map_height = 6 * this.mission_height / 18f;
            this.mission_map_y = this.mission_location_y - this.mission_map_height;
            this.mission_map_icon_size = this.mission_map_width - 2 * Util.getCircleButtonSpace();
            // Mission armies
            this.mission_armies_x = this.mission_x;
            this.mission_armies_width = this.mission_width;
            this.mission_armies_height = 2 * this.mission_height / 18f;
            this.mission_armies_y = this.mission_map_y - 3*this.mission_armies_height/4f;
            this.mission_armies_icon_size = 4 * this.mission_armies_height / 5f;
            // Mission description
            this.mission_description_x = this.mission_x + Util.getCircleButtonSpace() / 4f;
            this.mission_description_width = this.mission_width - Util.getCircleButtonSpace() / 2f;
            this.mission_description_height = 18*this.mission_height/4f / 18f - Util.getCircleButtonSpace() / 2f;
            this.mission_description_y = this.mission_armies_y - this.mission_description_height - Util.getCircleButtonSpace() / 4f;
            // Mission score
            this.mission_score_x = this.mission_x;
            this.mission_score_width = this.mission_width;
            this.mission_score_height = this.mission_height / 18f;
            this.mission_score_y = this.mission_description_y - this.mission_score_height;
            this.mission_score_icon_size = this.mission_score_height;
            // Mission leaderboard
            this.mission_leaderboard_x = this.mission_score_x + this.mission_score_width - 0.7f * Util.getCircleButtonSize();
            this.mission_leaderboard_y = this.mission_score_y;
        } else {
            // Mission fling
            this.mission_fling_x = 0.8f * Util.getCircleButtonSize();
            this.mission_fling_width = this.mission_width - 2 * this.mission_fling_x;
            this.mission_fling_height = this.mission_height / 12f;
            this.mission_fling_y = this.mission_y + this.mission_height - this.mission_fling_height;
            this.mission_fling_small_icon_size = Util.getScreenHeight() / 50f;
            this.mission_fling_big_icon_size = Util.getScreenHeight() / 30f;
            // Mission title
            this.mission_title_x = this.mission_x;
            this.mission_title_width = this.mission_width;
            this.mission_title_height = this.mission_height / 12f;
            this.mission_title_y = this.mission_fling_y - this.mission_title_height;
            // Mission location
            this.mission_location_x = this.mission_x;
            this.mission_location_width = this.mission_width / 2f;
            this.mission_location_height = 2 * this.mission_height / 12f;
            this.mission_location_y = this.mission_title_y - this.mission_location_height;
            // Mission map
            this.mission_map_x = this.mission_x + Util.getCircleButtonSpace()/4f;
            this.mission_map_width = this.mission_width / 2f - 2 * this.mission_map_x;
            this.mission_map_height = 6 * this.mission_height / 12f;
            this.mission_map_y = this.mission_location_y - this.mission_map_height;
            this.mission_map_icon_size = this.mission_map_width - Util.getCircleButtonSpace();
            // Mission armies
            this.mission_armies_x = this.mission_x + this.mission_width / 2f;
            this.mission_armies_width = this.mission_width / 2f;
            this.mission_armies_height = 2 * this.mission_height / 12f;
            this.mission_armies_y = this.mission_title_y - this.mission_armies_height;
            this.mission_armies_icon_size = 4 * this.mission_armies_height / 5f;
            // Mission description
            this.mission_description_x = this.mission_x + this.mission_width / 2f + Util.getCircleButtonSpace() / 4f;
            this.mission_description_width = this.mission_width / 2f - Util.getCircleButtonSpace() / 2f;
            this.mission_description_height = 5 * this.mission_height / 12f - Util.getCircleButtonSpace() / 2f;
            this.mission_description_y = this.mission_location_y - this.mission_description_height - Util.getCircleButtonSpace() / 4f;
            // Mission score
            this.mission_score_x = this.mission_x + this.mission_width / 2f;
            this.mission_score_width = this.mission_width / 2f;
            this.mission_score_height = 1 * this.mission_height / 12f;
            this.mission_score_y = this.mission_description_y - this.mission_score_height;
            this.mission_score_icon_size = this.mission_score_height;
            // Mission leaderboard
            this.mission_leaderboard_x = this.mission_score_x + this.mission_score_width - 0.7f * Util.getCircleButtonSize();
            this.mission_leaderboard_y = this.mission_score_y;
        }
        // START GAME BUTTON POSITION
        float start_game_ratio = this.campaign.getAssets().button_start_game.getRegionWidth() / (float) this.campaign.getAssets().button_start_game.getRegionHeight();
        start_game_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * start_game_ratio) / 2f;
        start_game_y = Util.getCircleButtonSpace() / 4f;
    }

    private void initUnlockGameVars() {
        this.unlock_width = Math.min(Util.getScreenHeight(), Util.getScreenWidth()) - Util.getCircleButtonSpace() / 2f;
        this.unlock_height = this.unlock_width;
        this.unlock_x = Util.getScreenWidth() / 2f - this.unlock_width / 2f;
        this.unlock_y = Util.getScreenHeight() / 2f - this.unlock_height / 2f;
        // Unlock close
        unlock_close_x = this.unlock_x + this.unlock_width - 0.7f * Util.getCircleButtonSize();
        unlock_close_y = this.unlock_y + this.unlock_height - 0.7f * Util.getCircleButtonSize();
        // Unlock title
        this.unlock_title_width = this.unlock_width;
        this.unlock_title_height = this.unlock_height / 5f;
        this.unlock_title_x = this.unlock_x;
        this.unlock_title_y = this.unlock_y + this.unlock_height - this.unlock_title_height;
        // Unlock map
        this.unlock_map_x = this.unlock_x;
        this.unlock_map_width = this.unlock_width;
        this.unlock_map_height = 3 * this.unlock_height / 5f;
        this.unlock_map_y = this.unlock_title_y - this.unlock_map_height;
        this.unlock_map_icon_size = this.mission_map_width - Util.getCircleButtonSpace();
        // Unlock text
        this.unlock_text_width = this.unlock_width;
        this.unlock_text_height = this.unlock_height / 5f;
        this.unlock_text_x = this.unlock_x;
        this.unlock_text_y = this.unlock_map_y - this.unlock_text_height;
    }

}
