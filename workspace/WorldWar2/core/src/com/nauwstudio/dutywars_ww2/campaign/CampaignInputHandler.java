package com.nauwstudio.dutywars_ww2.campaign;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Square;

public class CampaignInputHandler implements InputProcessor {

    CampaignScreen screen;
    Campaign campaign;
    CampaignRenderer renderer;

    public CampaignInputHandler(CampaignScreen screen) {
        this.screen = screen;
        this.campaign = screen.getCampaign();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.campaign.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Touching button ?
        float button_x = this.renderer.getMenuCameraOriginX() + screenX;
        float button_y = this.renderer.getMenuCameraOriginY() + screenY;
        this.screen.touchedButton = this.campaign.getTouchedButton(button_x, button_y);
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.press();
            disableGesture();
            return true;
        } else {
            // Touching campaign ?
            float campaign_x = this.renderer.getCameraOriginX() + screenX * this.renderer.getCameraZoom();
            float campaign_y = this.renderer.getCameraOriginY() + screenY * this.renderer.getCameraZoom();
            this.screen.touchedMission = this.campaign.getTouchedMission(campaign_x, campaign_y);
            if (screen.touchedMission != null) {
                this.screen.touchedMission.press();
                disableGesture();
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Perform button action ?
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.campaign.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        }
        // Perform mission selection action ?
        else if (this.screen.touchedMission != null) {
            this.campaign.touchMission(this.screen.touchedMission);
            // Release mission
            this.screen.touchedMission.release();
            reset();
            return true;
        } else {
            reset();
            return false;
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if(amount > 0) {
            this.renderer.zoomOut();
        } else {
            this.renderer.zoomIn();
        }
        return false;
    }

    public void disableGesture() {
        this.screen.can_pan = false;
        this.screen.can_pinch = false;
    }

    public void enableGesture() {
        this.screen.can_pan = true;
        this.screen.can_pinch = true;
    }

    public void reset() {
        enableGesture();
        this.screen.touchedButton = null;
        this.screen.touchedMission = null;
    }
}
