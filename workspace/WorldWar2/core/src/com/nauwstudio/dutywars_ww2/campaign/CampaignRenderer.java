package com.nauwstudio.dutywars_ww2.campaign;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.I18NBundle;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;

import java.text.MessageFormat;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class CampaignRenderer {

    public static final float ZOOM_MAX = 4.0f;
    public static final float ZOOM_MIN = 0.5f;

    private Campaign campaign;
    SpriteBatch batch;

    private OrthographicCamera menuCamera;
    private OrthographicCamera campaignCamera;
    private OrthographicCamera missionCamera;

    private MainAssets assets;
    private CampaignHUDVars hudVars;

    public CampaignRenderer(Campaign campaign) {
        this.campaign = campaign;
        // CAMERAS
        menuCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        menuCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        menuCamera.update();
        campaignCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        campaignCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        campaignCamera.update();
        missionCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        missionCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        missionCamera.update();

        this.batch = new SpriteBatch();
        this.assets = this.campaign.getAssets();
        this.hudVars = this.campaign.getHUDVars();

        pointCameraOnMission(this.campaign.getMissions().get(this.campaign.getMissions().size() - 1));
    }

    public float getCameraPositionX() {
        return this.campaignCamera.position.x;
    }

    public float getCameraPositionY() {
        return this.campaignCamera.position.y;
    }

    public float getCameraOriginX() {
        return getCameraPositionX() - (getCameraZoomWidth() / 2f);
    }

    public float getCameraOriginY() {
        return getCameraPositionY() - (getCameraZoomHeight() / 2);
    }

    public float getCameraWidth() {
        return this.campaignCamera.viewportWidth;
    }

    public float getCameraHeight() {
        return this.campaignCamera.viewportHeight;
    }

    public float getCameraZoom() {
        return this.campaignCamera.zoom;
    }

    public float getCameraZoomWidth() {
        return getCameraWidth() * getCameraZoom();
    }

    public float getCameraZoomHeight() {
        return getCameraHeight() * getCameraZoom();
    }

    public void moveCamera(float distX, float distY) {
        distX = distX * getCameraZoom();
        distY = distY * getCameraZoom();
        float border_left = 0 + getCameraZoomWidth() / 2f;
        float border_right = this.campaign.getMapWidth() - getCameraZoomWidth() / 2f;
        float border_bottom = 0 + getCameraZoomHeight() / 2f;
        float border_top = this.campaign.getMapHeight() - getCameraZoomHeight() / 2f;
        this.campaignCamera.translate(distX, distY);
        this.campaignCamera.update();
        // Out of borders ?
        if (this.campaignCamera.position.y < border_bottom) {
            this.campaignCamera.translate(0, border_bottom - this.campaignCamera.position.y);
        }
        if (this.campaignCamera.position.y > border_top) {
            this.campaignCamera.translate(0, border_top - this.campaignCamera.position.y);
        }
        if (this.campaignCamera.position.x < border_left) {
            this.campaignCamera.translate(border_left - this.campaignCamera.position.x, 0);
        }
        if (this.campaignCamera.position.x > border_right) {
            this.campaignCamera.translate(border_right - this.campaignCamera.position.x, 0);
        }
        this.campaignCamera.update();
    }

    public void pointCamera(Vector2 target) {
        int distX = (int) (target.x - this.campaignCamera.position.x);
        int distY = (int) (target.y - this.campaignCamera.position.y);
        this.campaignCamera.translate(distX, distY);
        this.campaignCamera.update();
    }

    public void pointCameraOnMission(Mission mission) {
        pointCamera(new Vector2(mission.getPosition()[0], mission.getPosition()[1]));
    }

    public void zoomIn() {
        this.campaignCamera.zoom = Math.max(this.campaignCamera.zoom - 0.02f, ZOOM_MIN);
        campaignCamera.update();
    }

    public void zoomOut() {
        this.campaignCamera.zoom = Math.min(this.campaignCamera.zoom + 0.02f, ZOOM_MAX);
        campaignCamera.update();
    }

    public float getMenuCameraPositionX() {
        return this.menuCamera.position.x;
    }

    public float getMenuCameraPositionY() {
        return this.menuCamera.position.y;
    }

    public float getMenuCameraOriginX() {
        return getMenuCameraPositionX() - (getMenuCameraWidth() / 2f);
    }

    public float getMenuCameraOriginY() {
        return getMenuCameraPositionY() - (getMenuCameraHeight() / 2f);
    }

    public float getMenuCameraWidth() {
        return this.menuCamera.viewportWidth;
    }

    public float getMenuCameraHeight() {
        return this.menuCamera.viewportHeight;
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(1 / 255f, 1 / 255f, 1 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderMap();
        if (this.campaign.getUnlockedMap() != null) {
            renderMapUnlocked(delta, this.campaign.getUnlockedMap());
        } else if (this.campaign.getSelectedMission() != null) {
            renderMission(delta, this.campaign.getSelectedMission());
        } else {
            renderMissionButton(delta);
            renderScore(delta);
        }
        renderButton(delta);
    }

    private void renderMap() {
        batch.setProjectionMatrix(campaignCamera.combined);
        batch.begin();
        for (int i = 0; i < Campaign.MAP_PART_WIDTH * Campaign.MAP_PART_HEIGHT; i++) {
            batch.draw(assets.world_map[i], (i % Campaign.MAP_PART_WIDTH) * Campaign.MAP_PART_SIZE, (Campaign.MAP_PART_HEIGHT - 1 - (i / Campaign.MAP_PART_WIDTH)) * Campaign.MAP_PART_SIZE, Campaign.MAP_PART_SIZE, Campaign.MAP_PART_SIZE);
        }
        batch.end();
    }

    private void renderMission(float delta, Mission mission) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.mission_bg, this.hudVars.mission_x, this.hudVars.mission_y, this.hudVars.mission_width, this.hudVars.mission_height);
        // Draw fling bullets
        for (int i = 0; i < 25; i++) {
            if (i == mission.getId() - 1) {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.mission_fling_x + i * this.hudVars.mission_fling_width / 25f, this.hudVars.mission_fling_y, this.hudVars.mission_fling_width / 25f, this.hudVars.mission_fling_height, this.hudVars.mission_fling_big_icon_size);
            } else {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.mission_fling_x + i * this.hudVars.mission_fling_width / 25f, this.hudVars.mission_fling_y, this.hudVars.mission_fling_width / 25f, this.hudVars.mission_fling_height, this.hudVars.mission_fling_small_icon_size);
            }
        }
        // Draw map
        this.campaign.getSelectedMissionMap().renderSmall(batch, this.hudVars.mission_map_x, this.hudVars.mission_map_y, this.hudVars.mission_map_width);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_big, mission.getId() + ". " + this.assets.stringBundle.get(mission.getName()), this.hudVars.mission_title_x, this.hudVars.mission_title_y, this.hudVars.mission_title_width, this.hudVars.mission_title_height);
        // Draw location
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get(mission.getLocation()), this.hudVars.mission_location_x, this.hudVars.mission_location_y, this.hudVars.mission_location_width, this.hudVars.mission_location_height);

        // Draw armies
        if (mission.getArmies().length == 3) {
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[mission.getArmies()[0] - 1], this.hudVars.mission_armies_x, this.hudVars.mission_armies_y, this.hudVars.mission_armies_width / 4f, this.hudVars.mission_armies_height, this.hudVars.mission_armies_icon_size);
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[mission.getArmies()[2] - 1], this.hudVars.mission_armies_x + this.hudVars.mission_armies_width / 4f, this.hudVars.mission_armies_y, this.hudVars.mission_armies_width / 4f, this.hudVars.mission_armies_height, this.hudVars.mission_armies_icon_size);
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[mission.getArmies()[1] - 1], this.hudVars.mission_armies_x + this.hudVars.mission_armies_width / 2f, this.hudVars.mission_armies_y, this.hudVars.mission_armies_width / 2f, this.hudVars.mission_armies_height, this.hudVars.mission_armies_icon_size);
        } else {
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[mission.getArmies()[0] - 1], this.hudVars.mission_armies_x, this.hudVars.mission_armies_y, this.hudVars.mission_armies_width / 2f, this.hudVars.mission_armies_height, this.hudVars.mission_armies_icon_size);
            RenderUtil.drawTextureInCell(batch, assets.armies_logo[mission.getArmies()[1] - 1], this.hudVars.mission_armies_x + this.hudVars.mission_armies_width / 2f, this.hudVars.mission_armies_y, this.hudVars.mission_armies_width / 2f, this.hudVars.mission_armies_height, this.hudVars.mission_armies_icon_size);
        }
        // Draw description
        this.assets.font_small.draw(batch, this.assets.stringBundle.get(mission.getDesc()), this.hudVars.mission_description_x, this.hudVars.mission_description_y + this.hudVars.mission_description_height, this.hudVars.mission_description_width, Align.topLeft, true);
        // Draw best score
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("campaign.score") + " " + mission.getScore(), this.hudVars.mission_score_x, this.hudVars.mission_score_y, this.hudVars.mission_score_width / 3f, this.hudVars.mission_score_height);
        // RENDER STARS
        for (int i = 0; i < 3; i++) {
            if (Util.getStarsAmount(mission.getScore()) > i) {
                RenderUtil.drawTextureInCell(batch, assets.star_full, this.hudVars.mission_score_x + this.hudVars.mission_score_width / 3f + i * this.hudVars.mission_score_width / 8f, this.hudVars.mission_score_y, this.hudVars.mission_score_width / 8f, this.hudVars.mission_score_height, this.hudVars.mission_score_icon_size);
            }
        }
        batch.end();
    }

    private void renderMapUnlocked(float delta, Map map) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        batch.draw(assets.mission_bg, this.hudVars.unlock_x, this.hudVars.unlock_y, this.hudVars.unlock_width, this.hudVars.unlock_height);
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get("campaign.map_unlocked"), this.hudVars.unlock_title_x, this.hudVars.unlock_title_y, this.hudVars.unlock_title_width, this.hudVars.unlock_title_height);
        // Draw map
        this.campaign.getUnlockedMap().renderSmall(batch, this.hudVars.unlock_map_x, this.hudVars.unlock_map_y, this.hudVars.unlock_map_width);
        // Draw text
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, MessageFormat.format(this.assets.stringBundle.get("campaign.map_unlocked_congrats"), new String[]{this.assets.stringBundle.get(map.getName())}), this.hudVars.unlock_text_x, this.hudVars.unlock_text_y, this.hudVars.unlock_text_width, this.hudVars.unlock_text_height);
        batch.end();
    }

    private void renderButton(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        for (Button b : this.campaign.getButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    private void renderScore(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        RenderUtil.drawTextureInCell(batch, assets.star_full, this.hudVars.score_icon_x, this.hudVars.score_icon_y, this.hudVars.score_icon_size, this.hudVars.score_icon_size, this.hudVars.score_icon_size*2/3f);
        RenderUtil.drawTextInCellRight(batch, this.assets.font_big, MessageFormat.format(this.assets.stringBundle.get("campaign.stars"), new String[]{this.campaign.stars + ""}), this.hudVars.score_x, this.hudVars.score_y, this.hudVars.score_width, this.hudVars.score_height);
        batch.end();
    }

    private void renderMissionButton(float delta) {
        batch.setProjectionMatrix(campaignCamera.combined);
        batch.begin();
        for (MissionButton b : this.campaign.getMissionButtons()) {
            b.render(batch, assets);
        }
        batch.end();
    }

    public void resize(int width, int height) {
        float oldPosX = campaignCamera.position.x;
        float oldPosY = campaignCamera.position.y;
        campaignCamera = new OrthographicCamera(width, height);
        campaignCamera.setToOrtho(false, width, height);
        campaignCamera.position.x = oldPosX;
        campaignCamera.position.y = oldPosY;
        campaignCamera.update();
        menuCamera = new OrthographicCamera(width, height);
        menuCamera.setToOrtho(false, width, height);
        menuCamera.update();
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        // REPLACE BUTTONS
        if (this.campaign.getSelectedMission() != null) {
            this.campaign.createMissionDetailsButtons();
        } else {
            if (this.campaign.getUnlockedMap() == null) {
                this.campaign.createMainButtons();
                this.campaign.createMissionButtons();
            } else {
                this.campaign.createMapUnlockedButtons();
            }
        }
    }
}
