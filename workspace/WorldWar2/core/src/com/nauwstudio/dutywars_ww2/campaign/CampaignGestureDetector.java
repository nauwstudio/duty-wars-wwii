package com.nauwstudio.dutywars_ww2.campaign;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.Util;

public class CampaignGestureDetector implements GestureListener {

    CampaignScreen screen;
    Campaign campaign;
    CampaignRenderer renderer;

    public CampaignGestureDetector(CampaignScreen screen) {
        this.screen = screen;
        this.campaign = screen.getCampaign();
        this.renderer = screen.getRenderer();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return true;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (this.campaign.getSelectedMission() != null && Math.abs(velocityX) > Math.abs(velocityY)) {
            if (velocityX > 0) {
                this.campaign.selectPreviousMission();
                return true;
            } else if (velocityX < 0) {
                this.campaign.selectNextMission();
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        // Reverse y-axis
        y = Util.getScreenHeight() - y;
        // Release button
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
        }
        if (this.screen.touchedMission != null) {
            this.screen.touchedMission.release();
            this.screen.touchedMission = null;
        }
        // Move campaign camera if can pan
        if (this.screen.can_pan && this.campaign.getSelectedMission() == null) {
            this.renderer.moveCamera(-deltaX, deltaY);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2
            pointer1, Vector2 pointer2) {
        if (this.screen.can_pinch) {
            screen.touchedMission = null;
            if (initialPointer1.dst(initialPointer2) > pointer1.dst(pointer2)) {
                this.renderer.zoomOut();
            } else {
                this.renderer.zoomIn();
            }
            return true;
        }
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
