package com.nauwstudio.dutywars_ww2.campaign;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.MapFile;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.game.Map;

import java.util.ArrayList;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class Campaign {

    public static final int MAP_PART_WIDTH = 8;
    public static final int MAP_PART_HEIGHT = 4;
    public static final int MAP_PART_SIZE = 1250;
    public static final int MAP_TARGET_SIZE = 48;

    public static final int STAR_SCORE = 20;

    private CampaignScreen screen;

    private ArrayList<Mission> missions;
    private Mission selectedMission;
    private Map selectedMissionMap;

    private ArrayList<Button> buttons;
    private ArrayList<MissionButton> missionButtons;

    private MainAssets assets;
    private CampaignHUDVars hudVars;

    public int stars;

    private Map mapUnlocked;

    public Campaign(CampaignScreen screen, MainAssets assets, int mapUnlockedID) {
        this.screen = screen;
        this.assets = assets;
        this.hudVars = new CampaignHUDVars(this);
        createMainButtons();
        // LOAD MISSIONS
        this.missions = this.screen.getController().getMissions();
        createMissionButtons();
        this.stars = getStarsAmount();
        // A map is unlocked
        if (mapUnlockedID > 0) {
            this.mapUnlocked = this.screen.getController().getMap(mapUnlockedID);
            this.mapUnlocked.initMap(this.screen.getController().getSeasons(), this.screen.getController().getPlayers());
            createMapUnlockedButtons();
        }
    }

    public float getMapWidth() {
        return MAP_PART_WIDTH * MAP_PART_SIZE;
    }

    public float getMapHeight() {
        return MAP_PART_HEIGHT * MAP_PART_SIZE;
    }

    public Mission getSelectedMission() {
        return this.selectedMission;
    }

    public Map getSelectedMissionMap() {
        return this.selectedMissionMap;
    }

    public ArrayList<Mission> getMissions() {
        return this.missions;
    }

    public void createMainButtons() {
        clearButtons();
        // NEXT MISSION BUTTON
        Vector2 pacific_pos = new Vector2(hudVars.next_mission_x, hudVars.next_mission_y);
        this.buttons.add(new ButtonCircle(pacific_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_NEXT, assets.button_next_mission, assets.button_next_mission_pressed, assets.button_next_mission));
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
    }

    public void createMissionButtons() {
        clearMissionButtons();
        for (Mission mission : this.missions) {
            Vector2 mission_pos = new Vector2(mission.getPosition()[0], mission.getPosition()[1]);
            this.missionButtons.add(new MissionButton(mission_pos, MAP_TARGET_SIZE, mission, getMissionButtonTexture(mission), getMissionButtonPressedTexture(mission)));
        }
    }

    private TextureRegion getMissionButtonTexture(Mission mission) {
        if (mission.getScore() > 0) {
            return assets.button_mission_completed;
        } else {
            return assets.button_mission;
        }
    }

    private TextureRegion getMissionButtonPressedTexture(Mission mission) {
        if (mission.getScore() > 0) {
            return assets.button_mission_completed_presssed;
        } else {
            return assets.button_mission_pressed;
        }
    }

    public void createMissionDetailsButtons() {
        clearButtons();
        clearMissionButtons();
        // CLOSE BUTTON
        Vector2 close_pos = new Vector2(hudVars.close_menu_x, hudVars.close_menu_y);
        this.buttons.add(new ButtonCircle(close_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, assets.button_close, assets.button_close_pressed, assets.button_close));
        // START BUTTON
        Vector2 start_pos = new Vector2(hudVars.start_game_x, hudVars.start_game_y);
        this.buttons.add(new ButtonRectangle(start_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_START, assets.button_start_game, assets.button_start_game_pressed, assets.button_start_game));
        // LEADERBOARD BUTTON
        Vector2 leaderboard_pos = new Vector2(hudVars.mission_leaderboard_x, hudVars.mission_leaderboard_y);
        ButtonCircle bc = new ButtonCircle(leaderboard_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_LEADERBOARD, assets.button_leaderboard, assets.button_leaderboard_pressed, assets.button_leaderboard_disabled);
        this.buttons.add(bc);
        if(!this.screen.getController().isLogged()) {
            bc.disable();
        }
    }

    public void createMapUnlockedButtons() {
        clearButtons();
        clearMissionButtons();
        // CLOSE BUTTON
        Vector2 close_pos = new Vector2(hudVars.unlock_close_x, hudVars.unlock_close_y);
        this.buttons.add(new ButtonCircle(close_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_CLOSE, assets.button_close, assets.button_close_pressed, assets.button_close));
    }

    public void clearButtons() {
        this.buttons = new ArrayList<Button>();
    }

    public void clearMissionButtons() {
        this.missionButtons = new ArrayList<MissionButton>();
    }

    public Button getTouchedButton(float menu_x, float menu_y) {
        if (buttons != null) {
            for (Button button : buttons) {
                if (button.isTouched(menu_x, menu_y)) {
                    return button;
                }
            }
        }
        return null;
    }

    public MissionButton getTouchedMission(float x, float y) {
        if (missionButtons != null) {
            for (MissionButton button : missionButtons) {
                if (button.isTouched(x, y)) {
                    return button;
                }
            }
        }
        return null;
    }

    private void selectMission(Mission mission) {
        this.selectedMission = mission;
        this.selectedMissionMap = this.screen.getController().getMap(mission.getMapId());
        this.selectedMissionMap.initMap(this.screen.getController().getSeasons(), this.screen.getController().getPlayers());
    }

    private void unselectMission() {
        this.selectedMission = null;
    }

    public void selectNextMission() {
        int nextMissionId = this.selectedMission.getId();
        if (nextMissionId == this.missions.size()) {
            nextMissionId = 1;
        } else {
            nextMissionId++;
        }
        this.selectMission(this.missions.get(nextMissionId - 1));
        this.screen.getRenderer().pointCameraOnMission(this.selectedMission);
    }

    public void selectPreviousMission() {
        int prevMissionId = this.selectedMission.getId();
        if (prevMissionId == 1) {
            prevMissionId = this.missions.size();
        } else {
            prevMissionId--;
        }
        this.selectMission(this.missions.get(prevMissionId - 1));
        this.screen.getRenderer().pointCameraOnMission(this.selectedMission);
    }

    public void touchButton(Button button) {
        if (button.isEnabled()) {
            // BACK BUTTONS
            if (button.getTag().equals(ButtonCircle.BUTTON_BACK)) {
                back();
            }
            // START BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_START)) {
                startGame();
            }
            // CLOSE BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_CLOSE)) {
                back();
            }
            // NEXT MISSION BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_NEXT)) {
                this.screen.getRenderer().pointCameraOnMission(this.missions.get(this.missions.size() - 1));
            }
            // LEADERBOARD BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_LEADERBOARD)) {
                this.screen.getController().showLeaderboard(this.selectedMission.getId());
            }
        }
    }

    public void touchMission(MissionButton button) {
        clearButtons();
        createMissionDetailsButtons();
        selectMission(button.getMission());
    }

    public void back() {
        if (this.mapUnlocked != null) {
            this.mapUnlocked = null;
            createMainButtons();
            createMissionButtons();
        } else if (this.selectedMission != null) {
            unselectMission();
            createMainButtons();
            createMissionButtons();
        } else {
            this.screen.back();
        }
    }

    private void startGame() {
        if (this.getSelectedMission() != null) {
            this.screen.startGame(this.selectedMissionMap, this.getSelectedMission().getArmies());
        }
    }

    public Map getUnlockedMap() {
        return this.mapUnlocked;
    }

    public ArrayList<Button> getButtons() {
        return this.buttons;
    }

    public ArrayList<MissionButton> getMissionButtons() {
        return this.missionButtons;
    }

    public MainAssets getAssets() {
        return this.assets;
    }

    public CampaignHUDVars getHUDVars() {
        return this.hudVars;
    }

    public int getStarsAmount() {
        int amount = 0;
        for(Mission mission : this.missions) {
            System.out.println(mission.getName() + " " + mission.getScore());
            amount += Util.getStarsAmount(mission.getScore());
        }
        return amount;
    }
}
