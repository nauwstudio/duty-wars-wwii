package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.game.GameObject;

import java.util.ArrayList;

/**
 * Created by nauwpie on 12/06/2018.
 */

public class ToggleGroup {

    public static final String TOGGLE_GROUP_TILE = "tile";
    public static final String TOGGLE_GROUP_BUILDING = "building";
    public static final String TOGGLE_GROUP_UNIT = "unit";
    public static final String TOGGLE_GROUP_PASSENGER = "passenger";

    private ArrayList<ToggleButton> buttons;

    protected Vector2 position;
    protected float width, height;
    protected String tag;
    private float elemWidth;
    private float elemHeight;

    public ToggleGroup(Vector2 position, float width, float height, String tag, ArrayList<GameObject> objects, int checked, int columns, float space, MainAssets assets) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.tag = tag;
        this.elemWidth = (this.width - (columns-1)*space)/columns;
        this.elemHeight = this.elemWidth*Util.TILE_RATIO;
        this.buttons = new ArrayList<ToggleButton>();
        for(int i = 0; i < objects.size(); i ++) {
            Vector2 toggle_pos = new Vector2(position.x + (this.elemWidth+space)*(i%columns), position.y + this.height - ((i/columns)+1)*(this.elemHeight+space*4));
            this.buttons.add(new ToggleButton(toggle_pos,this.elemWidth, this.elemHeight, this, objects.get(i), assets.toggle_pressed, assets.toggle_checked, assets.toggle_checked_pressed));
        }
        if(checked >= 0) {
            this.buttons.get(checked).check();
        }
    }

    public String getTag() {
        return this.tag;
    }

    public ArrayList<ToggleButton> getButtons(){
        return this.buttons;
    }

    public void unCheckAll() {
        for(ToggleButton button : this.buttons) {
            button.uncheck();
        }
    }

    public ToggleButton getChecked() {
        for(ToggleButton button : this.buttons) {
            if(button.isChecked()) {
                return button;
            }
        }
        return null;
    }

    public void render(SpriteBatch batch) {
        for(ToggleButton toggleButton : this.buttons) {
            toggleButton.render(batch);
        }
    }
}

