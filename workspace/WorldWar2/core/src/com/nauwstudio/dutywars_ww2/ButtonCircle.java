package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class ButtonCircle extends Button {
    public static final String BUTTON_MENU = "menu";
    public static final String BUTTON_STAT = "stat";
    public static final String BUTTON_NEXT = "next";
    public static final String BUTTON_CLOSE = "close";
    public static final String BUTTON_BACK = "exit";

    public static final String BUTTON_INFO = "info";
    public static final String BUTTON_CAPTURE = "capture";
    public static final String BUTTON_DROP = "drop";
    public static final String BUTTON_RELOAD = "reload";
    public static final String BUTTON_MOVE = "move";
    public static final String BUTTON_ATTACK = "attack";

    public static final String BUTTON_UNIT = "unit";
    public static final String BUTTON_UNIT_INFO = "unit_info";

    public static final String BUTTON_MISSION = "mission";
    public static final String BUTTON_LEADERBOARD = "leaderboard";
    public static final String BUTTON_DELETE_SAVE = "save_delete";
    public static final String BUTTON_DELETE_MAP = "map_delete";
    public static final String BUTTON_EXPORT_MAP = "map_export";
    public static final String BUTTON_IMPORT_MAP = "map_import";

    public static final String BUTTON_MINUS = "part:";
    public static final String BUTTON_ARMY = "army:";
    public static final String BUTTON_TYPE = "type:";
    public static final String BUTTON_TEAM = "team:";
    public static final String BUTTON_SEASON = "season:";

    public static final String BUTTON_WEB = "web";
    public static final String BUTTON_GOOGLE = "google";
    public static final String BUTTON_FACEBOOK = "facebook";

    public static final String BUTTON_SIZE_MINUS = "minus";
    public static final String BUTTON_SIZE_PLUS = "plus";
    public static final String BUTTON_SAVE = "save";
    public static final String BUTTON_LAYER = "layer";
    public static final String BUTTON_DRAW = "draw";
    public static final String BUTTON_CHOOSE = "choose";
    public static final String BUTTON_ERASE = "remove";
    public static final String BUTTON_PARAMS = "params";
    public static final String BUTTON_VISIBILITY = "visibility";
    public static final String BUTTON_OFFSETX_MINUS = "offsetx_minus";
    public static final String BUTTON_OFFSETX_PLUS = "offsetx_plus";
    public static final String BUTTON_OFFSETY_MINUS = "offsety_minus";
    public static final String BUTTON_OFFSETY_PLUS = "offsety_plus";
    public static final String BUTTON_ORIENTATION = "orientation";
    public static final String BUTTON_PLAYER_MINUS = "player_minus";
    public static final String BUTTON_PLAYER_PLUS = "player_plus";

    protected Circle bounds;

    public ButtonCircle(Vector2 position, float size, String tag, TextureRegion texture, TextureRegion texture_pressed, TextureRegion texture_disabled) {
        super(position, size, size, tag, texture, texture_pressed, texture_disabled);
        Vector2 circle_center = new Vector2(this.position.x + width / 2, this.position.y + width / 2);
        this.bounds = new Circle(circle_center, width / 2);
    }

    @Override
    public boolean isTouched(float x, float y) {
        return this.bounds.contains(new Vector2(x, y));
    }

    public void render(SpriteBatch batch, BitmapFont font) {
        super.render(batch);
    }

    public boolean isUnitButton() {
        return false;
    }

    public boolean isMissionButton() {
        return false;
    }

    public boolean isDeleteSaveButton() {
        return false;
    }

    public boolean isUnitInfoButton() {
        return false;
    }

    public boolean isLayerButton() {
        return false;
    }
}
