package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RadioGroup;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.game.GameObject;

public class ToggleButton {

    protected Vector2 position;
    protected float width, height;

    protected ToggleGroup group;
    protected GameObject object;
    protected TextureRegion texture_pressed;
    protected TextureRegion texture_checked;
    protected TextureRegion texture_checked_pressed;
    protected boolean checked = false;
    protected boolean pressed = false;
    protected boolean enabled = true;
    protected String text;

    protected Rectangle bounds;

    public ToggleButton(Vector2 position, float width, float height, ToggleGroup group, GameObject object, TextureRegion texture_pressed, TextureRegion texture_checked, TextureRegion texture_checked_pressed) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.group = group;
        this.object = object;
        this.texture_pressed = texture_pressed;
        this.texture_checked = texture_checked;
        this.texture_checked_pressed = texture_checked_pressed;
        this.bounds = new Rectangle(position.x, position.y, this.width, this.height);
    }

    public boolean isTouched(float x, float y){
        return this.bounds.contains(new Vector2(x, y));
    }

    public void press() {
        this.pressed = true;
        // PLAY SOUND
        DWController.playSound(MainAssets.switchSound);
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public void release() {
        this.pressed = false;
    }

    public void check() {
        this.group.unCheckAll();
        this.checked = true;
    }

    public void uncheck() {
        this.checked = false;
    }

    public boolean isChecked() {
        return this.checked;
    }

    public ToggleGroup getGroup() {
        return this.group;
    }

    public GameObject getObject() {
        return this.object;
    }

    public void render(SpriteBatch batch) {
        if (enabled) {
            if (pressed) {
                if(checked) {
                    batch.draw(texture_checked_pressed, position.x, position.y, width, height);
                } else {
                    batch.draw(texture_pressed, position.x, position.y, width, height);
                }
            } else {
                if(checked) {
                    batch.draw(texture_checked, position.x, position.y, width, height);
                }
            }
        }
        RenderUtil.drawTextureInCellBottom(batch, this.object.getToggleTexture(), position.x, position.y, width, height, width);
    }
}
