package com.nauwstudio.dutywars_ww2;

import java.util.ArrayList;

import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

public class Save {

	private int id;
	private int mode;
	private Map map;
	private ArrayList<Player> players;
	private ArrayList<Building> buildings;
	private ArrayList<Unit> units;
	private int turn;
	
	public Save(int id, int mode, Map map, ArrayList<Player> players, ArrayList<Building> buildings, ArrayList<Unit> units, int turn) {
		this.id = id;
        this.mode = mode;
		this.map = map;
		this.players= players;
		this.buildings = buildings;
		this.units = units;
		this.turn = turn;
	}
	/*
	Save summary
	 */
	public Save(int id, int mode, Map map, ArrayList<Player> players, int turn) {
		this.id = id;
		this.mode = mode;
		this.map = map;
		this.players= players;
        this.buildings = new ArrayList<Building>();
        this.units = new ArrayList<Unit>();
		this.turn = turn;
	}

    public int getId() {
        return this.id;
    }

	public int getMode() {
		return mode;
	}

	public Map getMap() {
        return map;
    }

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}

	public ArrayList<Building> getBuildings() {
		return buildings;
	}

	public void setBuildings(ArrayList<Building> buildings) {
		this.buildings = buildings;
	}

	public ArrayList<Unit> getUnits() {
		return units;
	}

	public void setUnits(ArrayList<Unit> units) {
		this.units = units;
	}

	public int getTurn() {
		return turn;
	}

    public int getDay() {
        return (this.turn / (this.players.size()-1)) + 1;

    }

    public void addBuilding(Building building) {
        this.buildings.add(building);
    }

    public void addUnit(Unit unit) {
        this.units.add(unit);
    }

	public Player getPlayerFromOrder(int order) {
		for (Player player : this.players) {
			if(player.getOrder() == order) {
				return  player;
			}
		}
		return null;
	}
}
