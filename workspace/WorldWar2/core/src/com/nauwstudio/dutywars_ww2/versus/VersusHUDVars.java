package com.nauwstudio.dutywars_ww2.versus;

import com.nauwstudio.dutywars_ww2.Util;

/**
 * Created by nauwpie on 22/12/2017.
 */

public class VersusHUDVars {

    private Versus versus;

    // START GAME BUTTON POSITION
    float start_game_x;
    float start_game_y;
    // BACK BUTTON POSITION
    float back_x;
    float back_y;
    // NEXT BUTTON POSITION
    float next_x;
    float next_y;
    // TITLE
    float title_x, title_y, title_width, title_height;
    // CHOOSE MAP
    public float map_type_fling_x, map_type_fling_y, map_type_fling_width, map_type_fling_height, map_type_fling_small_icon_size, map_type_fling_big_icon_size;
    public float map_type_x, map_type_y, map_type_width, map_type_height;
    public float map_fling_x, map_fling_y, map_fling_width, map_fling_height, map_fling_small_icon_size, map_fling_big_icon_size;
    public float map_name_x, map_name_y, map_name_width, map_name_height;
    public float map_map_x, map_map_y, map_map_width, map_map_height, map_map_icon_size;
    public float map_players_x, map_players_y, map_players_width, map_players_height, map_players_icon_size;
    public float map_buildings_x, map_buildings_y, map_buildings_width, map_buildings_height, map_buildings_icon_size;
    // CHOOSE PLAYERS
    public float players_tab_x, players_tab_y, players_tab_width, players_tab_height;
    public float players_header_x, players_header_y, players_header_width, players_header_height;
    public float players_row_height, players_row_width;

    public VersusHUDVars(Versus versus) {
        this.versus = versus;
        initHUDVariables();
    }

    public void initHUDVariables() {
        // BACK BUTTON POSITION
        back_x = Util.getCircleButtonSpace() / 4f;
        back_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
        // NEXT BUTTON POSITION
        next_x = Util.getScreenWidth() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
        next_y = Util.getScreenHeight() - Util.getCircleButtonSize() - Util.getCircleButtonSpace() / 4f;
        // TITLE
        this.title_x = 0;
        this.title_height = Util.getScreenHeight() / 9f;
        this.title_width = Util.getScreenWidth();
        this.title_y = Util.getScreenHeight() - this.title_height;
        initChooseMapVars();
        initChoosePlayersVars();
    }

    private void initChooseMapVars() {
        if (Util.isVertical()) {
            // Map type fling
            this.map_type_fling_width = Util.getScreenWidth() - 2 * Util.getCircleButtonSize();
            this.map_type_fling_height = Util.getScreenHeight() / 18f;
            this.map_type_fling_x = Util.getScreenWidth() / 2f - this.map_type_fling_width / 2f;
            this.map_type_fling_y = this.title_y - this.map_type_fling_height;
            this.map_type_fling_small_icon_size = Util.getScreenWidth() / 50f;
            this.map_type_fling_big_icon_size = Util.getScreenWidth() / 30f;
            // Map type
            this.map_type_x = 0;
            this.map_type_width = Util.getScreenWidth();
            this.map_type_height = Util.getScreenHeight() / 9f;
            this.map_type_y = this.map_type_fling_y - this.map_type_height;
            // Map fling
            this.map_fling_x = 0;
            this.map_fling_width = Util.getScreenWidth() / 10f;
            this.map_fling_height = this.map_type_fling_width;
            this.map_fling_y = Util.getScreenHeight() / 2f - this.map_fling_height / 2f;
            this.map_fling_small_icon_size = Util.getScreenWidth() / 50f;
            this.map_fling_big_icon_size = Util.getScreenWidth() / 30f;
            // Map name
            this.map_name_x = this.map_fling_width;
            this.map_name_width = Util.getScreenWidth() - 2 * this.map_name_x;
            this.map_name_height = Util.getScreenHeight() / 9f;
            this.map_name_y = this.map_type_y - this.map_name_height;
            // Map map
            this.map_map_x = this.map_fling_width;
            ;
            this.map_map_width = Util.getScreenWidth() - 2 * this.map_map_x;
            this.map_map_height = 3 * Util.getScreenHeight() / 9f;
            this.map_map_y = this.map_name_y - this.map_map_height;
            this.map_map_icon_size = this.map_map_width;
            // Map players
            this.map_players_x = this.map_fling_width;
            this.map_players_width = Util.getScreenWidth() - 2 * this.map_players_x;
            this.map_players_height = Util.getScreenHeight() / 9f;
            this.map_players_y = this.map_map_y - this.map_players_height;
            this.map_players_icon_size = 3 * this.map_players_height / 5f;
            // Map buildings
            this.map_buildings_x = this.map_fling_width;
            this.map_buildings_width = Util.getScreenWidth() - 2 * this.map_buildings_x;
            this.map_buildings_height = 1.5f * Util.getScreenHeight() / 9f;
            this.map_buildings_y = this.map_players_y - this.map_buildings_height;
            this.map_buildings_icon_size = 4 * this.map_buildings_height / 2f / 5f;
        } else {
            // Map type fling
            this.map_type_fling_width = Util.getScreenHeight() - 4 * Util.getCircleButtonSize();
            this.map_type_fling_height = Util.getScreenHeight() / 12f;
            this.map_type_fling_x = Util.getScreenWidth() / 2f - this.map_type_fling_width / 2f;
            this.map_type_fling_y = this.title_y - this.map_type_fling_height;
            this.map_type_fling_small_icon_size = Util.getScreenHeight() / 50f;
            this.map_type_fling_big_icon_size = Util.getScreenHeight() / 30f;
            // Map type
            this.map_type_x = 0;
            this.map_type_width = Util.getScreenWidth();
            this.map_type_height = Util.getScreenHeight() / 12f;
            this.map_type_y = this.map_type_fling_y - this.map_type_height;
            // Map fling
            this.map_fling_x = 0;
            this.map_fling_y = Util.getCircleButtonSpace()/4f;
            this.map_fling_width = Util.getScreenHeight() / 10f;
            this.map_fling_height = Util.getScreenHeight() - Util.getCircleButtonSize() - 3*Util.getCircleButtonSpace()/4f;
            this.map_fling_small_icon_size = Util.getScreenHeight() / 50f;
            this.map_fling_big_icon_size = Util.getScreenHeight() / 30f;
            // Map name
            this.map_name_x = this.map_fling_width;
            ;
            this.map_name_width = Util.getScreenWidth() / 2f - 2 * this.map_name_x;
            this.map_name_height = Util.getScreenHeight() / 6f;
            this.map_name_y = this.map_type_y - this.map_name_height;
            // Map map
            this.map_map_x = this.map_fling_width;
            ;
            this.map_map_width = Util.getScreenWidth() / 2f - 2 * this.map_map_x;
            this.map_map_height = 3 * Util.getScreenHeight() / 6f;
            this.map_map_y = this.map_name_y - this.map_map_height;
            this.map_map_icon_size = this.map_map_width;
            // Map players
            this.map_players_x = Util.getScreenWidth() / 2f;
            this.map_players_width = Util.getScreenWidth() / 2 - Util.getCircleButtonSpace() / 2f;
            this.map_players_height = Util.getScreenHeight() / 6f;
            this.map_players_y = this.map_type_y - this.map_players_height - Util.getScreenHeight() / 12f;
            this.map_players_icon_size = 3 * this.map_players_height / 5f;
            // Map buildings
            this.map_buildings_x = Util.getScreenWidth() / 2f;
            this.map_buildings_width = Util.getScreenWidth() / 2 - Util.getCircleButtonSpace() / 2f;
            this.map_buildings_height = 1.5f * Util.getScreenHeight() / 6f;
            this.map_buildings_y = this.map_players_y - this.map_buildings_height - Util.getScreenHeight() / 12f;
            this.map_buildings_icon_size = 4 * this.map_buildings_height / 2f / 5f;
        }
    }

    private void initChoosePlayersVars() {
        // Column header
        if (Util.isVertical()) {
            this.players_tab_height = 6 * Util.getScreenHeight() / 9f;
            this.players_tab_width = Util.getScreenWidth() - 2 * Util.getCircleButtonSize();
            this.players_tab_x = Util.getScreenWidth() / 2f - this.players_tab_width / 2f;
            this.players_tab_y = Util.getScreenHeight() / 2f - this.players_tab_height / 2f;
            this.players_header_height = 1 * this.players_tab_height / 6;
            this.players_header_width = this.players_tab_width;
            this.players_header_x = this.players_tab_x;
            this.players_header_y = this.players_tab_y + 5 * this.players_tab_height / 6f;
            this.players_row_height = this.players_tab_height - this.players_header_height;
        } else {
            this.players_tab_height = 4 * Util.getScreenHeight() / 6f;
            this.players_tab_width = Util.getScreenWidth() - 2 * Util.getCircleButtonSize();
            this.players_tab_x = Util.getScreenWidth() / 2f - this.players_tab_width / 2f;
            this.players_tab_y = Util.getScreenHeight() / 2f - this.players_tab_height / 2f;
            this.players_header_height = 3 * this.players_tab_height / 4f;
            this.players_header_width = this.players_tab_width / 6f;
            this.players_header_x = this.players_tab_x;
            this.players_header_y = this.players_tab_y;
            this.players_row_height = this.players_tab_height / 4;
            this.players_row_width = this.players_tab_width - this.players_header_width;
        }
        // START GAME BUTTON POSITION
        float start_game_ratio = this.versus.getAssets().button_start_game.getRegionWidth() / (float) this.versus.getAssets().button_start_game.getRegionHeight();
        start_game_x = Util.getScreenWidth() / 2f - (Util.getRectangleButtonHeight() * start_game_ratio) / 2f;
        start_game_y = Util.getCircleButtonSpace() / 4f;

    }

}
