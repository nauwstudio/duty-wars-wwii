package com.nauwstudio.dutywars_ww2.versus;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.VersusScreen;

public class VersusInputHandler implements InputProcessor {

    VersusScreen screen;
    Versus versus;
    VersusRenderer renderer;

    public VersusInputHandler(VersusScreen screen) {
        this.screen = screen;
        this.versus = screen.getVersus();
        this.renderer = screen.getRenderer();
        reset();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            this.versus.back();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Touching button ?
        float button_x = this.renderer.getMenuCameraOriginX() + screenX;
        float button_y = this.renderer.getMenuCameraOriginY() + screenY;
        this.screen.touchedButton = this.versus.getTouchedButton(button_x, button_y);
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.press();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Reverse y-axis
        screenY = (int) Util.getScreenHeight() - screenY;
        // Perform button action ?
        if (this.screen.touchedButton != null) {
            // Perform button action
            this.versus.touchButton(this.screen.touchedButton);
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        } else {
            reset();
            return false;
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void reset() {
        this.screen.touchedButton = null;
    }
}
