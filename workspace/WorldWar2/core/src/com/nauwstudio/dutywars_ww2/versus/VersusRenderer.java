package com.nauwstudio.dutywars_ww2.versus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.I18NBundle;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.RenderUtil;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.campaign.MissionButton;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;

import java.text.MessageFormat;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class VersusRenderer {

    private Versus versus;
    SpriteBatch batch;

    private OrthographicCamera menuCamera;

    private MainAssets assets;
    private VersusHUDVars hudVars;

    public VersusRenderer(Versus versus) {
        this.versus = versus;
        // CAMERAS
        menuCamera = new OrthographicCamera(Util.getScreenWidth(), Util.getScreenHeight());
        menuCamera.setToOrtho(false, Util.getScreenWidth(), Util.getScreenHeight());
        menuCamera.update();

        this.batch = new SpriteBatch();
        this.assets = this.versus.getAssets();
        this.hudVars = this.versus.getHUDVars();
    }

    public float getMenuCameraPositionX() {
        return this.menuCamera.position.x;
    }

    public float getMenuCameraPositionY() {
        return this.menuCamera.position.y;
    }

    public float getMenuCameraOriginX() {
        return getMenuCameraPositionX() - (getMenuCameraWidth() / 2f);
    }

    public float getMenuCameraOriginY() {
        return getMenuCameraPositionY() - (getMenuCameraHeight() / 2f);
    }

    public float getMenuCameraWidth() {
        return this.menuCamera.viewportWidth;
    }

    public float getMenuCameraHeight() {
        return this.menuCamera.viewportHeight;
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(5 / 255f, 5 / 255f, 5 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.versus.getChoosedMap() != null) {
            renderChoosePlayers(delta);
        } else {
            renderChooseMap(delta);
        }
        renderButton(delta);
    }

    private void renderChooseMap(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        RenderUtil.drawBackground(batch, assets);
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_bigbig, this.assets.stringBundle.get("versus.choose_map"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        // Draw map_type fling bullets
        for (int i = 0; i < Map.TYPE.length; i++) {
            if (i == this.versus.getMapTypeCursor()) {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.map_type_fling_x + i * this.hudVars.map_type_fling_width / (float) Map.TYPE.length, this.hudVars.map_type_fling_y, this.hudVars.map_type_fling_width / (float) Map.TYPE.length, this.hudVars.map_type_fling_height, this.hudVars.map_type_fling_big_icon_size);
            } else {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.map_type_fling_x + i * this.hudVars.map_type_fling_width / (float) Map.TYPE.length, this.hudVars.map_type_fling_y, this.hudVars.map_type_fling_width / (float) Map.TYPE.length, this.hudVars.map_type_fling_height, this.hudVars.map_type_fling_small_icon_size);
            }
        }
        // Draw map type
        RenderUtil.drawTextInCell(batch, this.assets.font_big, this.assets.stringBundle.get(Map.TYPE_NAME[this.versus.getMapTypeCursor()]), this.hudVars.map_type_x, this.hudVars.map_type_y, this.hudVars.map_type_width, this.hudVars.map_type_height);
        // Draw map fling bullets
        for (int i = 0; i < this.versus.getMaps().size(); i++) {
            if (this.versus.getMaps().size() - 1 - i == this.versus.getMapCursor()) {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.map_fling_x, this.hudVars.map_fling_y + i * (this.hudVars.map_fling_height / (float) this.versus.getMaps().size()), this.hudVars.map_fling_width, this.hudVars.map_fling_height / (float) this.versus.getMaps().size(), this.hudVars.map_fling_big_icon_size);
            } else {
                RenderUtil.drawTextureInCell(batch, assets.bullet, this.hudVars.map_fling_x, this.hudVars.map_fling_y + i * (this.hudVars.map_fling_height / (float) this.versus.getMaps().size()), this.hudVars.map_fling_width, this.hudVars.map_fling_height / (float) this.versus.getMaps().size(), this.hudVars.map_fling_small_icon_size);
            }
        }
        if (this.versus.getSelectedMap() != null) {
            if (this.versus.getSelectedMap().isLocked()) {
                renderLockedMap(this.versus.getSelectedMap());
            } else {
                renderUnlockedMap(this.versus.getSelectedMap());
            }
        }
        batch.end();
    }

    private void renderUnlockedMap(Map map) {
        // Draw name
        if (map.getType() == Map.CUSTOM) {
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, map.getName(), this.hudVars.map_name_x, this.hudVars.map_name_y, this.hudVars.map_name_width, this.hudVars.map_name_height);
        } else {
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get(map.getName()), this.hudVars.map_name_x, this.hudVars.map_name_y, this.hudVars.map_name_width, this.hudVars.map_name_height);
        }
        // Draw map
        map.renderSmall(batch, this.hudVars.map_map_x, this.hudVars.map_map_y, this.hudVars.map_map_width);
        // Draw players
        for (int i = 0; i < this.versus.getMapPlayers(); i++) {
            RenderUtil.drawTextureInCell(batch, assets.human_alive, this.hudVars.map_players_x + i * (this.hudVars.map_players_width / (float) this.versus.getMapPlayers()), this.hudVars.map_players_y, this.hudVars.map_players_width / (float) this.versus.getMapPlayers(), this.hudVars.map_players_height, this.hudVars.map_players_icon_size);
        }
        // Draw buildings
        RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().capital, this.hudVars.map_buildings_x + 0 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "" + this.versus.getSelectedMap().getCapitalAmount(), this.hudVars.map_buildings_x + 0 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().city, this.hudVars.map_buildings_x + 1 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "" + this.versus.getSelectedMap().getCityAmount(), this.hudVars.map_buildings_x + 1 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().factory, this.hudVars.map_buildings_x + 2 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "" + this.versus.getSelectedMap().getFactoryAmount(), this.hudVars.map_buildings_x + 2 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().airport, this.hudVars.map_buildings_x + 3 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "" + this.versus.getSelectedMap().getAirportAmount(), this.hudVars.map_buildings_x + 3 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
        RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().shipyard, this.hudVars.map_buildings_x + 4 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "" + this.versus.getSelectedMap().getShipyardAmount(), this.hudVars.map_buildings_x + 4 * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
    }

    private void renderLockedMap(Map map) {
        // Draw name
        RenderUtil.drawTextInCell(batch, this.assets.font_medium, "????????", this.hudVars.map_name_x, this.hudVars.map_name_y, this.hudVars.map_name_width, this.hudVars.map_name_height);
        // Draw map
        RenderUtil.drawTextureInCell(batch, assets.locked, this.hudVars.map_map_x, this.hudVars.map_map_y, this.hudVars.map_map_width, this.hudVars.map_map_height, this.hudVars.map_map_icon_size / 4f);
        // Draw players
        for (int i = 0; i < this.versus.getMapPlayers(); i++) {
            RenderUtil.drawTextureInCell(batch, assets.human_alive, this.hudVars.map_players_x + i * (this.hudVars.map_players_width / (float) this.versus.getMapPlayers()), this.hudVars.map_players_y, this.hudVars.map_players_width / (float) this.versus.getMapPlayers(), this.hudVars.map_players_height, this.hudVars.map_players_icon_size);
        }
        // Draw buildings
        for (int i = 0; i < 5; i++) {
            switch (i) {
                case 0:
                    RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().capital, this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
                    break;
                case 1:
                    RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().city, this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
                    break;
                case 2:
                    RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().factory, this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
                    break;
                case 3:
                    RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().airport, this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
                    break;
                case 4:
                    RenderUtil.drawTextureInCell(batch, this.versus.getScreen().getController().getPlayer(Army.GAI).getArmy().getAssets().shipyard, this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y + this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f, this.hudVars.map_buildings_icon_size);
                    break;
            }
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, "?", this.hudVars.map_buildings_x + i * 2 * (this.hudVars.map_buildings_width / 9f), this.hudVars.map_buildings_y, this.hudVars.map_buildings_width / 9f, this.hudVars.map_buildings_height / 2f);
        }
    }

    private void renderChoosePlayers(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        // Draw background
        RenderUtil.drawBackground(batch, assets);
        batch.draw(assets.mission_bg, 0, 0, Util.getScreenWidth(), Util.getScreenHeight());
        // Draw title
        RenderUtil.drawTextInCell(batch, this.assets.font_bigbig, this.assets.stringBundle.get("versus.choose_players"), this.hudVars.title_x, this.hudVars.title_y, this.hudVars.title_width, this.hudVars.title_height);
        // Draw tab
        // Headers
        if (Util.isVertical()) {
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.army"), this.hudVars.players_header_x, this.hudVars.players_header_y, this.hudVars.players_header_width / 3f, this.hudVars.players_header_height);
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.type"), this.hudVars.players_header_x + this.hudVars.players_header_width / 3f, this.hudVars.players_header_y, this.hudVars.players_header_width / 3f, this.hudVars.players_header_height);
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.team"), this.hudVars.players_header_x + 2 * this.hudVars.players_header_width / 3f, this.hudVars.players_header_y, this.hudVars.players_header_width / 3f, this.hudVars.players_header_height);
        } else {
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.army"), this.hudVars.players_header_x, this.hudVars.players_header_y + 2 * this.hudVars.players_header_height / 3f, this.hudVars.players_header_width, this.hudVars.players_header_height / 3f);
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.type"), this.hudVars.players_header_x, this.hudVars.players_header_y + 1 * this.hudVars.players_header_height / 3f, this.hudVars.players_header_width, this.hudVars.players_header_height / 3f);
            RenderUtil.drawTextInCell(batch, this.assets.font_medium, this.assets.stringBundle.get("versus.team"), this.hudVars.players_header_x, this.hudVars.players_header_y, this.hudVars.players_header_width, this.hudVars.players_header_height / 3f);
        }
        batch.end();
    }

    private void renderButton(float delta) {
        batch.setProjectionMatrix(menuCamera.combined);
        batch.begin();
        for (Button b : this.versus.getButtons()) {
            b.render(batch);
        }
        batch.end();
    }

    public void resize(int width, int height) {
        menuCamera = new OrthographicCamera(width, height);
        menuCamera.setToOrtho(false, width, height);
        menuCamera.update();
        // UPDATE HUD VARIABLES
        this.hudVars.initHUDVariables();
        // REPLACE BUTTONS
        if (this.versus.getChoosedMap() != null) {
            this.versus.createChoosePlayersButtons();
        } else {
            this.versus.createChooseMapButtons();
        }
    }
}
