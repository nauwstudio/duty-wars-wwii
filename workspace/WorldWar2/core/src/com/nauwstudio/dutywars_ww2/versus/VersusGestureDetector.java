package com.nauwstudio.dutywars_ww2.versus;

import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.CampaignScreen;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.VersusScreen;

public class VersusGestureDetector implements GestureListener {

    VersusScreen screen;
    Versus versus;
    VersusRenderer renderer;

    public VersusGestureDetector(VersusScreen screen) {
        this.screen = screen;
        this.versus = screen.getVersus();
        this.renderer = screen.getRenderer();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return true;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (this.versus.getChoosedMap() == null) {
            // SWITCH MAP TYPE
            if (Math.abs(velocityX) > Math.abs(velocityY)) {
                if (velocityX > 0) {
                    this.versus.selectPreviousMapType();
                    return true;
                } else if (velocityX < 0) {
                    this.versus.selectNextMapType();
                    return true;
                } else {
                    return false;
                }
            }
            // SWITCH MAP
            else {
                if (velocityY > 0) {
                    this.versus.selectPreviousMap();
                    return true;
                } else if (velocityY < 0) {
                    this.versus.selectNextMap();
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        // Release button
        if (this.screen.touchedButton != null) {
            this.screen.touchedButton.release();
            this.screen.touchedButton = null;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2
            pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
