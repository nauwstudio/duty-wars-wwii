package com.nauwstudio.dutywars_ww2.versus;

import com.badlogic.gdx.math.Vector2;
import com.nauwstudio.dutywars_ww2.Button;
import com.nauwstudio.dutywars_ww2.ButtonCircle;
import com.nauwstudio.dutywars_ww2.ButtonRectangle;
import com.nauwstudio.dutywars_ww2.MainAssets;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.VersusScreen;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by nauwpie on 14/12/2017.
 */

public class Versus {

    private VersusScreen screen;

    // CHOOSE MAP
    private int mapType;
    private ArrayList<Map> maps;
    private Map selectedMap;
    private int mapCursor = 0;
    private int mapTypeCursor = 0;

    // CHOOSE PLAYERS
    private Map choosedMap;
    private int[] playersParticipating;
    private int[] playersArmy;
    private int[] playersType;
    private int[] playersTeam;

    private ArrayList<Button> buttons;

    private MainAssets assets;
    private VersusHUDVars hudVars;

    public Versus(VersusScreen screen, MainAssets assets) {
        this.screen = screen;
        this.assets = assets;
        this.hudVars = new VersusHUDVars(this);
        createChooseMapButtons();
        // LOAD MAPS
        mapTypeCursor = 0;
        selectMapType();
        mapCursor = 0;
        selectMap();
    }

    public Map getSelectedMap() {
        return this.selectedMap;
    }

    public Map getChoosedMap() {
        return this.choosedMap;
    }

    public ArrayList<Map> getMaps() {
        return this.maps;
    }

    public int getMapCursor() {
        return this.mapCursor;
    }

    public int getMapTypeCursor() {
        return this.mapTypeCursor;
    }

    public int getMapPlayers() {
        return this.selectedMap.getPlayersAmount();
    }

    public void createChooseMapButtons() {
        clearButtons();
        // NEXT BUTTON
        Vector2 pacific_pos = new Vector2(hudVars.next_x, hudVars.next_y);
        this.buttons.add(new ButtonCircle(pacific_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_NEXT, assets.button_next, assets.button_next_pressed, assets.button_next_mission));
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
    }

    public void createChoosePlayersButtons() {
        clearButtons();
        // BACK BUTTON
        Vector2 back_pos = new Vector2(hudVars.back_x, hudVars.back_y);
        this.buttons.add(new ButtonCircle(back_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_BACK, assets.button_back, assets.button_back_pressed, assets.button_back));
        // START GAME BUTTON
        Vector2 start_pos = new Vector2(hudVars.start_game_x, hudVars.start_game_y);
        this.buttons.add(new ButtonRectangle(start_pos, Util.getRectangleButtonHeight(), ButtonRectangle.BUTTON_START, assets.button_start_game, assets.button_start_game_pressed, assets.button_start_game));
        if (Util.isVertical()) {
            createChoosePlayersButtonsVertical();
        } else {
            createChoosePlayerButtonsHorizontal();
        }
    }

    private void createChoosePlayersButtonsVertical() {
        // PLUS-MINUS BUTTONS
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_x = Util.getCircleButtonSpace() / 2f;
            float height = hudVars.players_row_height / (float) this.getMapPlayers();
            float pos_y = hudVars.players_tab_y + (this.getMapPlayers() - 1 - i) * height + height / 2f - Util.getSmallCircleButtonSize() / 2f;
            Vector2 plus_minus_pos = new Vector2(pos_x, pos_y);
            if (this.playersParticipating[i] == 1) {
                ButtonCircle button = new ButtonCircle(plus_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_MINUS + i, assets.button_minus, assets.button_minus_pressed, assets.button_minus_disabled);
                if (!this.enoughParticipant()) {
                    button.disable();
                }
                this.buttons.add(button);
            } else {
                this.buttons.add(new ButtonCircle(plus_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_MINUS + i, assets.button_plus, assets.button_plus_pressed, assets.button_plus));
            }
        }
        // ARMY BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_x = hudVars.players_tab_x + hudVars.players_tab_width / 3f / 2f - Util.getCircleButtonSize() / 2f;
            float height = hudVars.players_row_height / (float) this.getMapPlayers();
            float pos_y = hudVars.players_tab_y + (this.getMapPlayers() - 1 - i) * height + height / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 army_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(army_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ARMY + i, assets.button_army[this.playersArmy[i]], assets.button_army_pressed[this.playersArmy[i]], assets.button_army[this.playersArmy[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }
        }
        // TYPE BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_x = hudVars.players_tab_x + hudVars.players_tab_width / 3f / 2f - Util.getCircleButtonSize() / 2f + 1 * hudVars.players_tab_width / 3f;
            float height = hudVars.players_row_height / (float) this.getMapPlayers();
            float pos_y = hudVars.players_tab_y + (this.getMapPlayers() - 1 - i) * height + height / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 type_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(type_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_TYPE + i, assets.button_type[this.playersType[i]], assets.button_type_pressed[this.playersType[i]], assets.button_type[this.playersType[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }

        }
        // TEAM BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_x = hudVars.players_tab_x + hudVars.players_tab_width / 3f / 2f - Util.getCircleButtonSize() / 2f + 2 * hudVars.players_tab_width / 3f;
            float height = hudVars.players_row_height / (float) this.getMapPlayers();
            float pos_y = hudVars.players_tab_y + (this.getMapPlayers() - 1 - i) * height + height / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 team_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(team_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_TEAM + i, assets.button_team[this.playersTeam[i]], assets.button_team_pressed[this.playersTeam[i]], assets.button_team[this.playersTeam[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }
        }
    }

    private void createChoosePlayerButtonsHorizontal() {
        // PLUS-MINUS BUTTONS
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_y = hudVars.players_tab_y + 3 * hudVars.players_row_height + hudVars.players_row_height / 2f - Util.getSmallCircleButtonSize() / 2f;
            float width = hudVars.players_row_width / (float) this.getMapPlayers();
            float pos_x = hudVars.players_tab_x + hudVars.players_header_width + i * width + width / 2f - Util.getSmallCircleButtonSize() / 2f;
            Vector2 plus_minus_pos = new Vector2(pos_x, pos_y);
            if (this.playersParticipating[i] == 1) {
                ButtonCircle button = new ButtonCircle(plus_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_MINUS + i, assets.button_minus, assets.button_minus_pressed, assets.button_minus_disabled);
                if (!this.enoughParticipant()) {
                    button.disable();
                }
                this.buttons.add(button);
            } else {
                this.buttons.add(new ButtonCircle(plus_minus_pos, Util.getSmallCircleButtonSize(), ButtonCircle.BUTTON_MINUS + i, assets.button_plus, assets.button_plus_pressed, assets.button_plus));
            }
        }
        // ARMY BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_y = hudVars.players_tab_y + 2 * hudVars.players_row_height + hudVars.players_row_height / 2f - Util.getCircleButtonSize() / 2f;
            float width = hudVars.players_row_width / (float) this.getMapPlayers();
            float pos_x = hudVars.players_tab_x + hudVars.players_header_width + i * width + width / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 army_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(army_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_ARMY + i, assets.button_army[this.playersArmy[i]], assets.button_army_pressed[this.playersArmy[i]], assets.button_army[this.playersArmy[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }
        }
        // TYPE BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_y = hudVars.players_tab_y + hudVars.players_row_height + hudVars.players_row_height / 2f - Util.getCircleButtonSize() / 2f;
            float width = hudVars.players_row_width / (float) this.getMapPlayers();
            float pos_x = hudVars.players_tab_x + hudVars.players_header_width + i * width + width / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 type_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(type_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_TYPE + i, assets.button_type[this.playersType[i]], assets.button_type_pressed[this.playersType[i]], assets.button_type[this.playersType[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }

        }
        // TEAM BUTTON
        for (int i = 0; i < this.getMapPlayers(); i++) {
            float pos_y = hudVars.players_tab_y + hudVars.players_row_height / 2f - Util.getCircleButtonSize() / 2f;
            float width = hudVars.players_row_width / (float) this.getMapPlayers();
            float pos_x = hudVars.players_tab_x + hudVars.players_header_width + i * width + width / 2f - Util.getCircleButtonSize() / 2f;
            Vector2 team_pos = new Vector2(pos_x, pos_y);
            ButtonCircle button = new ButtonCircle(team_pos, Util.getCircleButtonSize(), ButtonCircle.BUTTON_TEAM + i, assets.button_team[this.playersTeam[i]], assets.button_team_pressed[this.playersTeam[i]], assets.button_team[this.playersTeam[i]]);
            if (this.playersParticipating[i] == 1) {
                this.buttons.add(button);
            }
        }
    }

    public void clearButtons() {
        this.buttons = new ArrayList<Button>();
    }

    public Button getTouchedButton(float menu_x, float menu_y) {
        if (buttons != null) {
            for (Button button : buttons) {
                if (button.isTouched(menu_x, menu_y)) {
                    return button;
                }
            }
        }
        return null;
    }

    private void selectMap() {
        if(this.maps != null && !this.maps.isEmpty()) {
            this.selectedMap = this.maps.get(this.mapCursor);
            // Load map
            this.selectedMap.initMap(this.screen.getController().getSeasons(), this.screen.getController().getPlayers());
        } else {
            this.selectedMap = null;
        }
    }

    private void selectMapType() {
        this.mapType = Map.TYPE[this.mapTypeCursor];
        this.maps = this.screen.getController().getMapsByType(this.mapType);
        selectMap();
    }

    private void chooseMap(Map map) {
        this.choosedMap = map;
        this.playersParticipating = new int[this.getMapPlayers()];
        for (int i = 0; i < this.playersParticipating.length; i++) {
            this.playersParticipating[i] = 1;
        }
        this.playersArmy = new int[this.getMapPlayers()];
        this.playersType = new int[this.getMapPlayers()];
        this.playersTeam = new int[this.getMapPlayers()];
        createChoosePlayersButtons();
    }

    public void selectNextMap() {
        if(this.maps.size() > 0) {
            this.mapCursor = (this.mapCursor + 1) % this.maps.size();
            this.selectMap();
        }
    }

    public void selectPreviousMap() {
        if(this.maps.size() > 0) {
            this.mapCursor = ((this.mapCursor - 1) + this.maps.size()) % this.maps.size();
            this.selectMap();
        }
    }

    public void selectNextMapType() {
        this.mapTypeCursor = (this.mapTypeCursor + 1) % Map.TYPE.length;
        this.mapCursor = 0;
        this.selectMapType();
    }

    public void selectPreviousMapType() {
        this.mapTypeCursor = (this.mapTypeCursor - 1 + Map.TYPE.length) % Map.TYPE.length;
        this.mapCursor = 0;
        this.selectMapType();
    }

    public void touchButton(Button button) {
        System.out.println("OK :" + button.getTag());
        if (button.isEnabled()) {
            // BACK BUTTONS
            if (button.getTag().equals(ButtonCircle.BUTTON_BACK)) {
                back();
            }
            // START BUTTON
            else if (button.getTag().equals(ButtonRectangle.BUTTON_START)) {
                startGame();
            }
            // NEXT BUTTON
            else if (button.getTag().equals(ButtonCircle.BUTTON_NEXT)) {
                if (this.selectedMap != null && !this.selectedMap.isLocked()) {
                    chooseMap(this.selectedMap);
                }
            }
            // PARTICIPATION BUTTON
            else if (button.getTag().substring(0, 5).equals(ButtonCircle.BUTTON_MINUS)) {
                int position = Integer.parseInt(button.getTag().substring(5, 6));
                changeParticipation(position);
            }
            // ARMY BUTTON
            else if (button.getTag().substring(0, 5).equals(ButtonCircle.BUTTON_ARMY)) {
                int position = Integer.parseInt(button.getTag().substring(5, 6));
                changeArmy(position);
            }
            // TYPE BUTTON
            else if (button.getTag().substring(0, 5).equals(ButtonCircle.BUTTON_TYPE)) {
                int position = Integer.parseInt(button.getTag().substring(5, 6));
                changeType(position);
            }
            // TEAM BUTTON
            else if (button.getTag().substring(0, 5).equals(ButtonCircle.BUTTON_TEAM)) {
                int position = Integer.parseInt(button.getTag().substring(5, 6));
                changeTeam(position);
            }
        }
    }

    private void changeParticipation(int position) {
        int participation = this.playersParticipating[position];
        if (participation == 0) {
            this.playersParticipating[position] = 1;
        } else {
            if (enoughParticipant()) {
                this.playersParticipating[position] = 0;
                this.playersArmy[position] = 0;
                this.playersType[position] = 0;
                this.playersTeam[position] = 0;
                // Checking teams validity
                if (allTeamsAreEquals()) {
                    this.playersTeam = new int[this.playersArmy.length];
                }
            }
        }
        // Update buttons
        createChoosePlayersButtons();
    }

    private boolean enoughParticipant() {
        int participant = 0;
        for (int i : this.playersParticipating) {
            participant += i;
        }
        if (participant > 2) {
            return true;
        } else {
            return false;
        }
    }

    private void changeArmy(int position) {
        int army = this.playersArmy[position];
        this.playersArmy[position] = getNextFreeArmy(army);
        // update buttons
        createChoosePlayersButtons();
    }

    private void changeType(int position) {
        if (this.playersType[position] == Player.HUMAN - 1) {
            this.playersType[position] = Player.ROBOT - 1;
            this.playersType[position] = Player.ROBOT - 1;
        } else {
            this.playersType[position] = Player.HUMAN - 1;
        }
        // update buttons
        createChoosePlayersButtons();
    }

    private void changeTeam(int position) {
        this.playersTeam[position] = getNextFreeTeam(position);
        // update buttons
        createChoosePlayersButtons();
    }

    public void back() {
        if (this.choosedMap != null) {
            this.choosedMap = null;
            selectMap();
            createChooseMapButtons();
        } else {
            this.screen.back();
        }
    }

    private void startGame() {
        if (this.getChoosedMap() != null && this.getChoosedMap().getPlayersAmount() > 1) {
            this.screen.startGame(this.getChoosedMap(), getArmies(), getTypes(), getTeams(), getOrders());
        } else {
            this.screen.getController().showMessage(this.assets.stringBundle.get("versus.not_enough_players"));
        }
    }

    private int[] getArmies() {
        // Getting participant armies
        ArrayList<Integer> participantArmies = new ArrayList<Integer>();
        for (int i = 0; i < this.playersParticipating.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participantArmies.add(this.playersArmy[i]);
            }
        }
        // Calculate armies
        int[] armies = new int[participantArmies.size()];
        Random random = new Random();
        for (int i = 0; i < armies.length; i++) {
            if (participantArmies.get(i) != Army.GAI) {
                armies[i] = participantArmies.get(i);
            } else {
                // Getting free armies
                ArrayList<Integer> possibilities = new ArrayList<Integer>();
                for (int army : Army.ARMIES) {
                    if (army != Army.GAI) {
                        boolean alreadyUsed = false;
                        for (int usedArmy : armies) {
                            if (usedArmy == army) {
                                alreadyUsed = true;
                                break;
                            }
                        }
                        if (!alreadyUsed) {
                            possibilities.add(army);
                        }
                    }
                }
                armies[i] = possibilities.get(random.nextInt(possibilities.size()));
            }
        }
        return armies;
    }

    private int getNextFreeArmy(int army) {
        ArrayList<Integer> participatingArmies = new ArrayList<Integer>();
        for (int i = 0; i < this.playersArmy.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participatingArmies.add(this.playersArmy[i]);
            }
        }
        int next_army = (army + 1) % 6;
        while (participatingArmies.contains(next_army) && next_army != Army.GAI) {
            next_army = (next_army + 1) % 6;
        }
        return next_army;
    }

    private int[] getTypes() {
        // Getting participant armies
        ArrayList<Integer> participantType = new ArrayList<Integer>();
        for (int i = 0; i < this.playersParticipating.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participantType.add(this.playersType[i]);
            }
        }
        // Calculate types
        int[] types = new int[participantType.size()];
        for (int i = 0; i < types.length; i++) {
            types[i] = participantType.get(i) + 1;
        }
        return types;
    }

    private int[] getTeams() {
        // Getting participant teams
        ArrayList<Integer> participantTeams = new ArrayList<Integer>();
        for (int i = 0; i < this.playersParticipating.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participantTeams.add(this.playersTeam[i]);
            }
        }
        // Calculate teams
        int[] teams = new int[participantTeams.size()];
        ArrayList<Integer> usedTeams = new ArrayList<Integer>();
        Random random = new Random();
        // 1. Fill teams with already known teams
        for (int i = 0; i < teams.length; i++) {
            if (participantTeams.get(i) != 0) {
                teams[i] = participantTeams.get(i);
            }
        }
        // 2. Getting all usedTeams
        for (int team : teams) {
            if (team != 0 && !usedTeams.contains(team)) {
                usedTeams.add(team);
            }
        }
        // 3. Adding team if less than 2 teams
        if (usedTeams.isEmpty()) {
            addTeam(usedTeams);
            addTeam(usedTeams);
        } else if (usedTeams.size() == 1) {
            addTeam(usedTeams);
        }
        // 4. "?" teams
        for (int i = 0; i < teams.length; i++) {
            if (participantTeams.get(i) == 0) {
                // Getting teams in which player can be
                ArrayList<Integer> possibilities = new ArrayList<Integer>();
                for (int team : usedTeams) {
                    if (playerCountByTeam(teams, team) < participantTeams.size() - (participantTeams.size() / usedTeams.size())) {
                        possibilities.add(team);
                    }
                }
                teams[i] = possibilities.get(random.nextInt(possibilities.size()));
            }
        }
        return teams;
    }

    private int[] getOrders() {
        // Getting participant armies
        ArrayList<Integer> participantOrder = new ArrayList<Integer>();
        for (int i = 0; i < this.playersParticipating.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participantOrder.add(i);
            }
        }
        // Calculate types
        int[] orders = new int[participantOrder.size()];
        for (int i = 0; i < orders.length; i++) {
            orders[i] = participantOrder.get(i) + 1;
        }
        return orders;
    }

    // Add a new team (up to five different teams)
    private int addTeam(ArrayList<Integer> teams) {
        for (int team = 1; team <= 5; team++) {
            if (!teams.contains(team)) {
                teams.add(team);
                return team;
            }
        }
        return 0;
    }

    private int playerCountByTeam(int[] teams, int team) {
        int count = 0;
        for (int playerTeam : teams) {
            if (playerTeam == team) {
                count++;
            }
        }
        return count;
    }

    private int getNextFreeTeam(int position) {
        int team = this.playersTeam[position];
        int next_team = (team + 1) % 6;
        this.playersTeam[position] = next_team;
        while (allTeamsAreEquals()) {
            next_team = (next_team + 1) % 6;
            this.playersTeam[position] = next_team;
            System.out.println("LOL : " + next_team);
        }
        System.out.println("NEXT TEAM : " + next_team);
        return next_team;
    }

    private boolean allTeamsAreEquals() {
        ArrayList<Integer> participatingTeams = new ArrayList<Integer>();
        for (int i = 0; i < this.playersTeam.length; i++) {
            if (this.playersParticipating[i] == 1) {
                participatingTeams.add(this.playersTeam[i]);
            }
        }
        int team = participatingTeams.get(0);
        for (int i = 0; i < participatingTeams.size(); i++) {
            if (team != participatingTeams.get(i)) {
                return false;
            }
        }
        return team != 0 && team != 5;
    }

    public ArrayList<Button> getButtons() {
        return this.buttons;
    }

    public MainAssets getAssets() {
        return this.assets;
    }

    public VersusHUDVars getHUDVars() {
        return this.hudVars;
    }

    public VersusScreen getScreen() {
        return this.screen;
    }
}
