package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.nauwstudio.dutywars_ww2.settings.Settings;
import com.nauwstudio.dutywars_ww2.settings.SettingsGestureDetector;
import com.nauwstudio.dutywars_ww2.settings.SettingsInputHandler;
import com.nauwstudio.dutywars_ww2.settings.SettingsRenderer;
import com.nauwstudio.dutywars_ww2.versus.Versus;
import com.nauwstudio.dutywars_ww2.versus.VersusGestureDetector;
import com.nauwstudio.dutywars_ww2.versus.VersusInputHandler;
import com.nauwstudio.dutywars_ww2.versus.VersusRenderer;

public class SettingsScreen implements Screen {

    private DWController controller;

    private Settings settings;
    private SettingsRenderer renderer;
    private MainAssets assets;

    private float runtime = 0;

    // Input handler vars
    private SettingsGestureDetector gestureDetector;
    private SettingsInputHandler inputHandler;
    public Button touchedButton;
    public CheckBox touchedCheckBox;
    public RadioButton touchedRadioButton;

    public SettingsScreen(DWController controller) {
        this.controller = controller;
        this.assets = this.controller.getMainAssets();
        this.settings = new Settings(this, this.assets);
        this.renderer = new SettingsRenderer(this.settings);
        initInputHandler();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new SettingsGestureDetector(this);
        this.inputHandler = new SettingsInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Settings getSettings() {
        return this.settings;
    }

    public SettingsRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        this.runtime += delta;
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
       this.settings.resume();
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
    }

    public DWController getController() {
        return this.controller;
    }

    public void back() {
        this.controller.toMainScreen();
    }
}
