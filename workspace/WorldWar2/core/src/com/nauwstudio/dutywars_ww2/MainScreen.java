package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.utils.Timer;
import com.nauwstudio.dutywars_ww2.main.Main;
import com.nauwstudio.dutywars_ww2.main.MainGestureDetector;
import com.nauwstudio.dutywars_ww2.main.MainInputHandler;
import com.nauwstudio.dutywars_ww2.main.MainRenderer;

public class MainScreen implements Screen {

    private DWController controller;

    private Main main;
    private MainRenderer renderer;
    private MainAssets assets;

    private float runtime = 0;

    // Input handler vars
    private MainGestureDetector gestureDetector;
    private MainInputHandler inputHandler;
    public Button touchedButton;
    public ToggleButton touchedToggle;
    public boolean can_pan;

    public MainScreen(DWController controller) {
        Timer.instance().start();
        this.controller = controller;
        this.assets = this.controller.getMainAssets();
        this.main = new Main(this, this.assets);
        this.renderer = new MainRenderer(this.main);

       initInputHandler();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new MainGestureDetector(this);
        this.inputHandler = new MainInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Main getMain() {
        return this.main;
    }

    public MainRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        this.runtime += delta;
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
       this.main.resume();

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public DWController getController() {
        return this.controller;
    }

    public void exit() {
        this.controller.getMainAssets().dispose();
        Gdx.app.exit();
    }

    public void openTextInput(String title, String text, String hint) {
        MyTextInputListener listener = new MyTextInputListener();
        Gdx.input.getTextInput(listener, title, text, hint);
    }

    public class MyTextInputListener implements Input.TextInputListener {
        @Override
        public void input (String text) {
            if (text != null && !text.replaceAll(" ", "").isEmpty()) {
                if (getController().getDatabaseInterface().checkMapName(text)) {
                    main.updateCreateMapName(text);
                } else {
                    getController().showMessage("Map already exists");
                    openTextInput(getController().getMainAssets().stringBundle.get("main.name_dialog_title"), text, "");
                }
            } else {
                getController().showMessage("Map name cannot be empty");
                openTextInput(getController().getMainAssets().stringBundle.get("main.name_dialog_title"), text, "");
            }
        }

        @Override
        public void canceled () {
            main.back();
        }
    }
}
