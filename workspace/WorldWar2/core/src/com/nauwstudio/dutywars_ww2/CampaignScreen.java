package com.nauwstudio.dutywars_ww2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.nauwstudio.dutywars_ww2.campaign.Campaign;
import com.nauwstudio.dutywars_ww2.campaign.CampaignGestureDetector;
import com.nauwstudio.dutywars_ww2.campaign.CampaignInputHandler;
import com.nauwstudio.dutywars_ww2.campaign.CampaignRenderer;
import com.nauwstudio.dutywars_ww2.campaign.MissionButton;
import com.nauwstudio.dutywars_ww2.game.GameGestureDetector;
import com.nauwstudio.dutywars_ww2.game.GameInputHandler;
import com.nauwstudio.dutywars_ww2.game.Map;

public class CampaignScreen implements Screen {

    private DWController controller;

    private Campaign campaign;
    private CampaignRenderer renderer;
    private MainAssets assets;

    private float runtime = 0;

    // Input handler vars
    private CampaignGestureDetector gestureDetector;
    private CampaignInputHandler inputHandler;
    public Button touchedButton;
    public MissionButton touchedMission;
    public boolean can_pan;
    public boolean can_pinch;

    public CampaignScreen(DWController controller, int mapUnlocked) {
            this.controller = controller;
            this.assets = this.controller.getMainAssets();
            this.campaign = new Campaign(this, this.assets, mapUnlocked);
            this.renderer = new CampaignRenderer(this.campaign);
            initInputHandler();
    }

    private void initInputHandler() {
        InputMultiplexer im = new InputMultiplexer();
        this.gestureDetector = new CampaignGestureDetector(this);
        this.inputHandler = new CampaignInputHandler(this);
        im.addProcessor(new GestureDetector(this.gestureDetector));
        im.addProcessor(this.inputHandler);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(im);
    }

    public Campaign getCampaign() {
        return this.campaign;
    }

    public CampaignRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        this.runtime += delta;
        this.renderer.render(this.runtime);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.resize(width, height);

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
    }

    public DWController getController() {
        return this.controller;
    }

    public void startGame(Map map, int[] players_army) {
        this.controller.startCampaignGame(map, players_army);
    }

    public void back() {
        this.controller.toMainScreen();
    }
}
