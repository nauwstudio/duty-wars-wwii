package com.nauwstudio.dutywars_ww2.desktop;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nauwpie on 17/10/2018.
 */

public class WebUtils {

    public static final String WEB = "http://nauwstudio.be/dutywars/android/";
    public static final String WEB_SEND_MAIL = "sendMail.php";

    public static final String MAIL_SUBJECT = "subject";
    public static final String MAIL_CONTENT = "content";

    public static void sendMail(String subject, String content) {
        try {
            HashMap<String, String> params = new HashMap<String,String>();
            params.put(MAIL_SUBJECT, subject);
            params.put(MAIL_CONTENT, content);
            postConnection(WEB + WEB_SEND_MAIL, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String postConnection(String path, HashMap<String, String> params) throws Exception {
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, String> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        //connection.setRequestProperty("Authorization", UtilWebCommon.WEB_AUTH);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.getOutputStream().write(postDataBytes);
        connection.connect();
        InputStream is = connection.getInputStream();
        // Converting result to String
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        String result = sb.toString();
        System.out.println(path + " => " + result);
        return result;
    }
}
