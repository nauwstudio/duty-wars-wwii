package com.nauwstudio.dutywars_ww2.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.StringBuilder;
import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.MapFile;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.desktop.database.DatabaseHelper;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.interfaces.AdsInterface;
import com.nauwstudio.dutywars_ww2.interfaces.DatabaseInterface;
import com.nauwstudio.dutywars_ww2.interfaces.FileInterface;
import com.nauwstudio.dutywars_ww2.interfaces.GooglePlayInterface;
import com.nauwstudio.dutywars_ww2.interfaces.WebInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class DesktopLauncher implements DatabaseInterface, GooglePlayInterface, WebInterface, AdsInterface, FileInterface {

    private DatabaseHelper database;

    private DWController controller;

    public static void main(String[] arg) {
       new DesktopLauncher();
    }

    public DesktopLauncher() {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1280;
        config.height = 720;
        config.x=2000;
        config.y=0;
        this.database = new DatabaseHelper();
        this.controller = new DWController(this, this, this, this, this);
        new LwjglApplication(this.controller, config);
    }

    @Override
    public Map getMap(int id) {
        database.open();
        Map result = database.getMap(id);
        database.close();
        return result;
    }

    @Override
    public ArrayList<Map> getMapsByType(int type) {
        database.open();
        ArrayList<Map> maps = database.getMapsByType(type);
        database.close();
        return maps;
    }


    @Override
    public ArrayList<Mission> getMissions() {
        database.open();
        ArrayList<Mission> result = database.getMissions();
        database.close();
        return result;
    }

    @Override
    public void updateScore(int missionID, int score) {
        database.open();
        database.updateMissionScore(missionID, score);
        database.close();
    }

    @Override
    public void unlockMap(int mapID) {
        database.open();
        database.unlockMap(mapID);
        database.close();
    }

    @Override
    public void unlockMission(int missionID) {
        database.open();
        database.unlockMission(missionID);
        database.close();
    }

    @Override
    public int saveGame(Save save) {
        database.open();
        int saveID = database.save(save);
        database.close();
        return saveID;
    }

    @Override
    public ArrayList<Save> getSavesSummary(DWController controller, int mode) {
        database.open();
        ArrayList<Save> saves = database.getSavesSummary(controller, mode);
        database.close();
        return saves;
    }

    @Override
    public void getSaveComplete(Save save) {
        database.open();
        database.getSaveComplete(save);
        database.close();
    }

    @Override
    public void deleteSave(int saveID) {
        database.open();
        database.deleteSave(saveID);
        database.close();
    }

    @Override
    public String getParam(String key) {
        database.open();
        String value = database.getParam(key);
        database.close();
        return value;
    }

    @Override
    public void updateParam(String key, String value) {
        database.open();
        database.updateParam(key, value);
        database.close();
    }

    @Override
    public boolean checkMapName(String name) {
        database.open();
        boolean result = database.checkMapName(name);
        database.close();
        return result;
    }

    @Override
    public Map createMap(String name) {
        database.open();
        int mapID = database.createMap(null, name);
        Map map = database.getMap(mapID);
        database.close();
        return map;
    }

    @Override
    public void deleteMap(int mapID) {
        System.out.println("LOLILOLLLLLLL");
        database.open();
        database.deleteMap(mapID);
        database.close();
    }

    @Override
    public boolean checkMapSaves(int mapId) {
        database.open();
        boolean result = database.checkMapSaves(mapId);
        database.close();
        return result;
    }


    @Override
    public void showLauncherAd() {

    }

    @Override
    public boolean isSignedIn() {
        return false;
    }

    @Override
    public void logIn() {

    }

    @Override
    public void logOut() {

    }

    @Override
    public void submitScore(int missionId, int score) {

    }

    @Override
    public void getAllLeaderboards() {

    }

    @Override
    public void getLeaderboard(int missionId) {

    }

    @Override
    public void facebook() {

    }

    @Override
    public void googlePlay() {

    }

    @Override
    public void web() {
        System.out.println("CALL WEB !");
    }

    @Override
    public void help() {

    }

    @Override
    public boolean checkPermission() {
        return true;
    }

    @Override
    public void getPermission() {

    }

    @Override
    public String getExportFolder() {
        return "D:\\";
    }

    @Override
    public void importFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("*.xml", "xml"));
        int result = fileChooser.showOpenDialog(null);
        if(result == JFileChooser.APPROVE_OPTION) {
            try {
                String fileName = fileChooser.getSelectedFile().getName();
                downloadFile(fileName, fileChooser.getSelectedFile());
                this.controller.importMap(fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void downloadFile(String fileName, File file) throws Exception {
        // Read file
        System.out.println("Downloading file : " + file);
        InputStream is = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine()) != null) {
            sb.append(line);
        }
        showMessage("Downloaded file : " + sb.toString());
        // Create temp file
        FileHandle tempFile = Gdx.files.local(MapFile.TEMP + fileName);
        tempFile.writeString(sb.toString(), false);
        showMessage("TEMP FILE CREATED : " + fileName);
        is.close();
        br.close();
    }


    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }
}
