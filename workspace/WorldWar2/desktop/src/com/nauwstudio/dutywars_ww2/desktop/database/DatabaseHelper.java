package com.nauwstudio.dutywars_ww2.desktop.database;

import com.nauwstudio.dutywars_ww2.DWController;
import com.nauwstudio.dutywars_ww2.Save;
import com.nauwstudio.dutywars_ww2.Util;
import com.nauwstudio.dutywars_ww2.campaign.Mission;
import com.nauwstudio.dutywars_ww2.game.Robot;
import com.nauwstudio.dutywars_ww2.game.Army;
import com.nauwstudio.dutywars_ww2.game.Map;
import com.nauwstudio.dutywars_ww2.game.Player;
import com.nauwstudio.dutywars_ww2.game.buildings.Building;
import com.nauwstudio.dutywars_ww2.game.units.Unit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatabaseHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "database0.db";

    public static final String PARAM_KEY = "key";
    public static final String PARAM_VALUE = "value";
    public static final String PARAM_TABLE = "param";
    public static final String PARAM_TABLE_CREATE =
            "CREATE TABLE " + PARAM_TABLE + " (" +
                    PARAM_KEY + " TEXT, " +
                    PARAM_VALUE + " TEXT); ";
    public static final String PARAM_TABLE_DROP = "DROP TABLE IF EXISTS " + PARAM_TABLE + ";";

    public static final String MAP_ID = "id";
    public static final String MAP_NAME = "name";
    public static final String MAP_FILE = "file";
    public static final String MAP_TYPE = "type";
    public static final String MAP_LOCK = "lock";
    public static final String MAP_MISSION_ID = "mission_id";
    public static final String MAP_TABLE = "map";
    public static final String MAP_TABLE_CREATE =
            "CREATE TABLE " + MAP_TABLE + " (" +
                    MAP_ID + " INTEGER PRIMARY KEY, " +
                    MAP_NAME + " TEXT, " +
                    MAP_FILE + " TEXT, " +
                    MAP_TYPE + " INTEGER, " +
                    MAP_LOCK + " INTEGER NOT NULL CHECK (" + MAP_LOCK + " IN (0,1)), " +
                    MAP_MISSION_ID + " INTEGER); ";
    public static final String MAP_TABLE_DROP = "DROP TABLE IF EXISTS " + MAP_TABLE + ";";

    public static final String MISSION_ID = "id";
    public static final String MISSION_NAME = "name";
    public static final String MISSION_MAP_ID = "map_id";
    public static final String MISSION_DESC = "desc";
    public static final String MISSION_LOCATION = "location";
    public static final String MISSION_ARMIES = "armies";
    public static final String MISSION_SCORE = "score";
    public static final String MISSION_POSX = "x";
    public static final String MISSION_POSY = "y";
    public static final String MISSION_LOCK = "lock";
    public static final String MISSION_TABLE = "mission";
    public static final String MISSION_TABLE_CREATE =
            "CREATE TABLE " + MISSION_TABLE + " (" +
                    MISSION_ID + " INTEGER PRIMARY KEY, " +
                    MISSION_NAME + " TEXT, " +
                    MISSION_MAP_ID + " INTEGER, " +
                    MISSION_DESC + " TEXT, " +
                    MISSION_LOCATION + " TEXT, " +
                    MISSION_ARMIES + " TEXT, " +
                    MISSION_SCORE + " INTEGER, " +
                    MISSION_POSX + " INTEGER, " +
                    MISSION_POSY + " INTEGER, " +
                    MISSION_LOCK + " INTEGER NOT NULL CHECK (" + MISSION_LOCK + " IN (0,1))," +
                    "FOREIGN KEY (" + MISSION_MAP_ID + ") REFERENCES " + MAP_TABLE + "(" + MAP_ID + "));";
    public static final String MISSION_TABLE_DROP = "DROP TABLE IF EXISTS " + MISSION_TABLE + ";";

    public static final String SAVE_ID = "id";
    public static final String SAVE_TYPE = "type";
    public static final String SAVE_MAP_ID = "map_id";
    public static final String SAVE_TURN = "turn";
    public static final String SAVE_TABLE = "save";
    public static final String SAVE_TABLE_CREATE =
            "CREATE TABLE " + SAVE_TABLE + " (" +
                    SAVE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    SAVE_TYPE + " INTEGER, " +
                    SAVE_MAP_ID + " INTEGER, " +
                    SAVE_TURN + " INTEGER);";
    public static final String SAVE_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_TABLE + ";";

    public static final String SAVE_PLAYER_SAVE_ID = "save_id";
    public static final String SAVE_PLAYER_POSITION = "position";
    public static final String SAVE_PLAYER_TYPE = "type";
    public static final String SAVE_PLAYER_ARMY = "army";
    public static final String SAVE_PLAYER_TEAM = "team";
    public static final String SAVE_PLAYER_MONEY = "money";
    public static final String SAVE_PLAYER_ALIVE = "alive";
    public static final String SAVE_PLAYER_TABLE = "save_player";
    public static final String SAVE_PLAYER_TABLE_CREATE =
            "CREATE TABLE " + SAVE_PLAYER_TABLE + " (" +
                    SAVE_PLAYER_SAVE_ID + " INTEGER, " +
                    SAVE_PLAYER_POSITION + " INTEGER, " +
                    SAVE_PLAYER_TYPE + " INTEGER, " +
                    SAVE_PLAYER_ARMY + " INTEGER, " +
                    SAVE_PLAYER_TEAM + " INTEGER, " +
                    SAVE_PLAYER_MONEY + " INTEGER, " +
                    SAVE_PLAYER_ALIVE + " INTEGER);";
    public static final String SAVE_PLAYER_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_PLAYER_TABLE + ";";

    public static final String SAVE_BUILDING_SAVE_ID = "save_id";
    public static final String SAVE_BUILDING_TYPE = "type";
    public static final String SAVE_BUILDING_SUBTYPE = "subtype";
    public static final String SAVE_BUILDING_ROW = "row";
    public static final String SAVE_BUILDING_COL = "col";
    public static final String SAVE_BUILDING_OWNER = "owner";
    public static final String SAVE_BUILDING_TABLE = "save_building";
    public static final String SAVE_BUILDING_TABLE_CREATE =
            "CREATE TABLE " + SAVE_BUILDING_TABLE + " (" +
                    SAVE_BUILDING_SAVE_ID + " INTEGER, " +
                    SAVE_BUILDING_TYPE + " INTEGER, " +
                    SAVE_BUILDING_SUBTYPE + " INTEGER, " +
                    SAVE_BUILDING_ROW + " INTEGER, " +
                    SAVE_BUILDING_COL + " INTEGER, " +
                    SAVE_BUILDING_OWNER + " INTEGER);";
    public static final String SAVE_BUILDING_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_BUILDING_TABLE + ";";

    public static final String SAVE_UNIT_SAVE_ID = "save_id";
    public static final String SAVE_UNIT_TYPE = "type";
    public static final String SAVE_UNIT_ROW = "row";
    public static final String SAVE_UNIT_COL = "col";
    public static final String SAVE_UNIT_OWNER = "owner";
    public static final String SAVE_UNIT_LIFE = "life";
    public static final String SAVE_UNIT_AMMO = "ammo";
    public static final String SAVE_UNIT_GAS = "gas";
    public static final String SAVE_UNIT_ORIENTATION = "orientation";
    public static final String SAVE_UNIT_TABLE = "save_unit";
    public static final String SAVE_UNIT_TABLE_CREATE =
            "CREATE TABLE " + SAVE_UNIT_TABLE + " (" +
                    SAVE_UNIT_SAVE_ID + " INTEGER, " +
                    SAVE_UNIT_TYPE + " INTEGER, " +
                    SAVE_UNIT_ROW + " INTEGER, " +
                    SAVE_UNIT_COL + " INTEGER, " +
                    SAVE_UNIT_OWNER + " INTEGER, " +
                    SAVE_UNIT_LIFE + " INTEGER, " +
                    SAVE_UNIT_AMMO + " INTEGER, " +
                    SAVE_UNIT_GAS + " INTEGER, " +
                    SAVE_UNIT_ORIENTATION + " INTEGER);";
    public static final String SAVE_UNIT_TABLE_DROP = "DROP TABLE IF EXISTS " + SAVE_UNIT_TABLE + ";";

    private Connection database;
    private Statement statement;

    public DatabaseHelper() {
        connect();
        onCreate(database);
    }

    public void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            database = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
            // UNIT DISPARITION BUG
            database.setAutoCommit(false);
            statement = this.database.createStatement();
        } catch (ClassNotFoundException notFoundException) {
            notFoundException.printStackTrace();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }


    public void open() {
        connect();
    }

    public void close() {
        try {
            this.database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addParam(Connection db, String key, String value) {
        try {
            String[] args = new String[]{
                    key,
                    value
            };
            String[] columns = new String[]{
                    PARAM_KEY,
                    PARAM_VALUE,
            };
            insert(PARAM_TABLE, columns, args);
            this.database.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getParam(String key) {
        String result = null;
        try {
            ResultSet rs = this.statement.executeQuery("SELECT " + PARAM_VALUE + " FROM " + PARAM_TABLE + " WHERE " + PARAM_KEY + "='" + key + "';");
            while (rs.next()) {
                result = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void updateParam(String key, String value) {
        try {
            this.statement.execute("UPDATE " + PARAM_TABLE + " SET " + PARAM_VALUE + "='" + value + "'  WHERE " + PARAM_KEY + "='" + key + "';");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addMap(Connection db, Map map) {
        try {
            String locked = "";
            if (map.isLocked()) {
                locked = "1";
            } else {
                locked = "0";
            }
            String[] args = new String[]{
                    "" + map.getId(),
                    map.getName(),
                    map.getFile(),
                    "" + map.getType(),
                    locked,
                    "" + map.getLinkedMissionId()
            };
            String[] columns = new String[]{
                    MAP_ID,
                    MAP_NAME,
                    MAP_FILE,
                    MAP_TYPE,
                    MAP_LOCK,
                    MAP_MISSION_ID
            };
            insert(MAP_TABLE, columns, args);
            this.database.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int createMap(Connection db, String name) {
        try {
            String locked = "0";
            String[] args = new String[]{
                    name,
                    name + ".xml",
                    "" + Map.CUSTOM,
                    locked,
                    "" + 0
            };
            String[] columns = new String[]{
                    MAP_NAME,
                    MAP_FILE,
                    MAP_TYPE,
                    MAP_LOCK,
                    MAP_MISSION_ID
            };
            insert(MAP_TABLE, columns, args);
            // GET MAP ID
            ResultSet rs = this.statement.executeQuery("SELECT MAX(" + MAP_ID + ") FROM " + MAP_TABLE + ";");
            // loop through the result set
            int mapId = 0;
            while (rs.next()) {
                mapId = rs.getInt(1);
            }
            this.database.commit();
            return mapId;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void deleteMap(int mapID) {
        try {
            this.statement.execute("DELETE FROM " + MAP_TABLE + " WHERE " + MAP_ID + "=" + mapID + " AND " + MAP_TYPE + "=" + Map.CUSTOM + ";");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addMission(Connection db, Mission mission) {
        try {
            String locked = "";
            if (mission.isLocked()) {
                locked = "1";
            } else {
                locked = "0";
            }
            String[] columns = new String[]{
                    MISSION_ID,
                    MISSION_NAME,
                    MISSION_MAP_ID,
                    MISSION_DESC,
                    MISSION_LOCATION,
                    MISSION_ARMIES,
                    MISSION_SCORE,
                    MISSION_POSX,
                    MISSION_POSY,
                    MISSION_LOCK
            };
            String[] args = new String[]{
                    "" + mission.getId(),
                    mission.getName(),
                    "" + mission.getMapId(),
                    mission.getDesc(),
                    mission.getLocation(),
                    Mission.armiesToString(mission.getArmies()),
                    "" + mission.getScore(),
                    "" + mission.getPosition()[0],
                    "" + mission.getPosition()[1],
                    locked
            };

            insert(MISSION_TABLE, columns, args);
            this.database.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insert(String table, String columns[], String[] args) {
        String cols = "";
        for (int i = 0; i < columns.length; i++) {
            cols += columns[i];
            if (i < columns.length - 1) {
                cols += ",";
            }
        }
        String values = "";
        for (int i = 0; i < args.length; i++) {
            values += "'" + args[i] + "'";
            if (i < args.length - 1) {
                values += ",";
            }
        }
        try {
            this.statement.execute("INSERT INTO " + table + "(" + cols + ") VALUES (" + values + ");");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Map getMap(int map_id) {
        Map map = null;
        try {
            ResultSet rs4 = this.database.createStatement().executeQuery("SELECT * FROM " + MAP_TABLE + " WHERE " + MAP_ID + "=" + map_id);
            // loop through the result set
            while (rs4.next()) {
                map = new Map(rs4.getInt(1), rs4.getString(2), rs4.getString(3), rs4.getInt(4), rs4.getInt(5) == 1, rs4.getInt(6));
            }
            rs4.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public boolean checkMapName(String map_name) {
        boolean result = true;
        try {
            ResultSet rs4 = this.database.createStatement().executeQuery("SELECT " + MAP_NAME + " FROM " + MAP_TABLE);
            // loop through the result set
            while (rs4.next()) {
                if (map_name.equalsIgnoreCase(rs4.getString(1))) {
                    result = false;
                    break;
                }
            }
            rs4.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Map> getMapsByType(int map_type) {
        ArrayList<Map> mapList = new ArrayList<Map>();
        try {
            ResultSet rs = this.statement.executeQuery("SELECT * FROM " + MAP_TABLE + " WHERE " + MAP_TYPE + "=" + map_type);
            // loop through the result set
            while (rs.next()) {
                mapList.add(new Map(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5) == 1, rs.getInt(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapList;
    }

    public void unlockMap(int map_id) {
        try {
            this.statement.execute("UPDATE " + MAP_TABLE + " SET " + MAP_LOCK + "='0'  WHERE " + MAP_ID + "=" + map_id + ";");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Mission> getMissions() {
        try {
            ResultSet rs = this.statement.executeQuery("SELECT * FROM " + MISSION_TABLE + " WHERE " + MISSION_LOCK + "=" + 0);
            // loop through the result set
            ArrayList<Mission> missionList = new ArrayList<Mission>();
            while (rs.next()) {
                missionList.add(new Mission(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        Mission.stringToArmies(rs.getString(6)),
                        rs.getInt(7),
                        new int[]{rs.getInt(8), rs.getInt(9)},
                        false));
            }
            return missionList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Mission>();
        }
    }

    public void updateMissionScore(int mission_id, int score) {
        try {
            int current_score = 0;
            ResultSet rs = this.statement.executeQuery("SELECT " + MISSION_SCORE + " FROM " + MISSION_TABLE + " WHERE " + MISSION_ID + "=" + mission_id);
            // loop through the result set
            ArrayList<Mission> missionList = new ArrayList<Mission>();
            while (rs.next()) {
                current_score = rs.getInt(1);
            }
            if (current_score == 0 || current_score > score) {
                this.statement.execute("UPDATE " + MISSION_TABLE + " SET " + MISSION_SCORE + "='" + score + "'  WHERE " + MISSION_ID + "=" + mission_id + ";");
            }
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void unlockMission(int mission_id) {
        try {
            this.statement.execute("UPDATE " + MISSION_TABLE + " SET " + MISSION_LOCK + "='0'  WHERE " + MISSION_ID + "=" + mission_id + ";");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int save(Save save) {
        int saveId = save.getId();
        try {
            // CREATION OF NEW SAVE
            if (saveId == 0) {
                String[] columns = new String[]{
                        SAVE_TYPE,
                        SAVE_MAP_ID,
                        SAVE_TURN
                };
                String[] args = new String[]{
                        "" + save.getMode(),
                        "" + save.getMap().getId(),
                        "" + save.getTurn()
                };
                insert(SAVE_TABLE, columns, args);
                // GET SAVE ID
                ResultSet rs = this.statement.executeQuery("SELECT MAX(" + SAVE_ID + ") FROM " + SAVE_TABLE + ";");
                // loop through the result set
                while (rs.next()) {
                    saveId = rs.getInt(1);
                }
            }
            // UPDATE OF EXISTANT SAVE
            else {
                // deleteSave(saveId);
                this.statement.execute("DELETE FROM " + SAVE_TABLE + " WHERE " + SAVE_ID + "=" + saveId + ";");
                System.out.println("DELETE SAVE");
                this.statement.execute("DELETE FROM " + SAVE_PLAYER_TABLE + " WHERE " + SAVE_PLAYER_SAVE_ID + "=" + saveId + ";");
                System.out.println("DELETE PLAYERS");
                this.statement.execute("DELETE FROM " + SAVE_BUILDING_TABLE + " WHERE " + SAVE_BUILDING_SAVE_ID + "=" + saveId + ";");
                System.out.println("DELETE BUILDINGS");
                this.statement.execute("DELETE FROM " + SAVE_UNIT_TABLE + " WHERE " + SAVE_UNIT_SAVE_ID + "=" + saveId + ";");
                System.out.println("DELETE UNITS");
                String[] columns = new String[]{
                        SAVE_ID,
                        SAVE_TYPE,
                        SAVE_MAP_ID,
                        SAVE_TURN
                };
                String[] args = new String[]{
                        "" + saveId,
                        "" + save.getMode(),
                        "" + save.getMap().getId(),
                        "" + save.getTurn()
                };
                insert(SAVE_TABLE, columns, args);
            }
            // PLAYERS
            // Players
            for (Player player : save.getPlayers()) {
                String alive = "";
                if (player.isAlive()) {
                    alive = "1";
                } else {
                    alive = "0";
                }
                String[] columns = new String[]{
                        SAVE_PLAYER_SAVE_ID,
                        SAVE_PLAYER_POSITION,
                        SAVE_PLAYER_TYPE,
                        SAVE_PLAYER_ARMY,
                        SAVE_PLAYER_TEAM,
                        SAVE_PLAYER_MONEY,
                        SAVE_PLAYER_ALIVE
                };
                String[] args = new String[]{
                        "" + saveId,
                        "" + player.getOrder(),
                        "" + player.getType(),
                        "" + player.getArmy().getType(),
                        "" + player.getTeam(),
                        "" + player.getMoney(),
                        "" + alive
                };
                insert(SAVE_PLAYER_TABLE, columns, args);

            }
            // Buildings
            for (Building building : save.getBuildings()) {
                String[] columns = new String[]{
                        SAVE_BUILDING_SAVE_ID,
                        SAVE_BUILDING_OWNER,
                        SAVE_BUILDING_TYPE,
                        SAVE_BUILDING_SUBTYPE,
                        SAVE_BUILDING_ROW,
                        SAVE_BUILDING_COL,
                };
                String[] args = new String[]{
                        "" + saveId,
                        "" + building.getOwner().getOrder(),
                        "" + building.getType(),
                        "" + building.getSubtype(),
                        "" + building.getRow(),
                        "" + building.getCol()
                };
                insert(SAVE_BUILDING_TABLE, columns, args);

            }
            // Units
            for (Unit unit : save.getUnits()) {
                String[] columns = new String[]{
                        SAVE_UNIT_SAVE_ID,
                        SAVE_UNIT_OWNER,
                        SAVE_UNIT_TYPE,
                        SAVE_UNIT_ROW,
                        SAVE_UNIT_COL,
                        SAVE_UNIT_LIFE,
                        SAVE_UNIT_AMMO,
                        SAVE_UNIT_GAS,
                        SAVE_UNIT_ORIENTATION
                };
                String[] args = new String[]{
                        "" + saveId,
                        "" + unit.getOwner().getOrder(),
                        "" + unit.getType(),
                        "" + unit.getRow(),
                        "" + unit.getCol(),
                        "" + unit.getLife(),
                        "" + unit.getAmmo(),
                        "" + unit.getGas(),
                        "" + unit.getOrientationValue()
                };
                insert(SAVE_UNIT_TABLE, columns, args);
            }
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saveId;
    }

    public ArrayList<Save> getSavesSummary(DWController controller, int mode) {
        ArrayList<Save> saves = new ArrayList<Save>();
        try {
            ResultSet rs = this.statement.executeQuery("SELECT * FROM " + SAVE_TABLE + " WHERE " + SAVE_TYPE + "=" + mode + " ORDER BY " + SAVE_ID + " DESC");
            while (rs.next()) {
                int save_id = rs.getInt(1);
                int save_mode = rs.getInt(2);
                int save_map_id = rs.getInt(3);
                int save_turn = rs.getInt(4);
                ArrayList<Player> players = new ArrayList<Player>();
                // ADD GAIA PLAYER
                players.add(0, new Player(controller.getPlayers().get(Army.GAI).getArmy(), 0, 0, Player.GAIA_ORDER, Player.ALIVE));
                ResultSet rs2 = this.database.createStatement().executeQuery("SELECT * FROM " + SAVE_PLAYER_TABLE + " WHERE " + SAVE_PLAYER_SAVE_ID + "=" + save_id);
                while (rs2.next()) {
                    if (rs2.getInt(3) == Player.HUMAN) {
                        players.add(new Player(controller.getPlayers().get(rs2.getInt(4)).getArmy(), rs2.getInt(5), rs2.getInt(6), rs2.getInt(2), rs2.getInt(7)));
                    } else {
                        players.add(new Robot(controller.getPlayers().get(rs2.getInt(4)).getArmy(), rs2.getInt(5), rs2.getInt(6), rs2.getInt(2), rs2.getInt(7)));

                    }
                }
                rs2.close();
                Save save = new Save(save_id, save_mode, getMap(save_map_id), players, save_turn);
                // Get Buildings
                ResultSet rs3 = this.database.createStatement().executeQuery("SELECT * FROM " + SAVE_BUILDING_TABLE + " WHERE " + SAVE_BUILDING_SAVE_ID + "=" + save_id);
                while (rs3.next()) {
                    save.addBuilding(Building.createBuilding(rs3.getInt(4), rs3.getInt(5), save.getPlayerFromOrder(rs3.getInt(6)), rs3.getInt(2), rs3.getInt(3)));
                }
                rs3.close();
                saves.add(save);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saves;
    }

    public void getSaveComplete(Save save) {
        try {
            // Get Units
            ResultSet rs4 = this.statement.executeQuery("SELECT * FROM " + SAVE_UNIT_TABLE + " WHERE " + SAVE_UNIT_SAVE_ID + "=" + save.getId());
            while (rs4.next()) {
                Unit unit = Unit.createUnit(rs4.getInt(3), rs4.getInt(4), save.getPlayerFromOrder(rs4.getInt(5)), rs4.getInt(2), Unit.valueToOrientation(rs4.getInt(9)));
                unit.setLife(rs4.getInt(6));
                unit.setAmmo(rs4.getInt(7));
                unit.setGas(rs4.getInt(8));
                save.addUnit(unit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteSave(int saveID) {
        try {
            this.statement.execute("DELETE FROM " + SAVE_TABLE + " WHERE " + SAVE_ID + "=" + saveID + ";");
            this.statement.execute("DELETE FROM " + SAVE_PLAYER_TABLE + " WHERE " + SAVE_PLAYER_SAVE_ID + "=" + saveID + ";");
            this.statement.execute("DELETE FROM " + SAVE_BUILDING_TABLE + " WHERE " + SAVE_BUILDING_SAVE_ID + "=" + saveID + ";");
            this.statement.execute("DELETE FROM " + SAVE_UNIT_TABLE + " WHERE " + SAVE_UNIT_SAVE_ID + "=" + saveID + ";");
            this.database.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkMapSaves(int mapId) {
        boolean result = true;
        try {
            ResultSet rs = this.statement.executeQuery("SELECT * FROM " + SAVE_TABLE + " WHERE " + SAVE_MAP_ID + "=" + mapId);
            while (rs.next()) {
                result = false;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void onCreate(Connection db) {
        try {
            statement.execute(MAP_TABLE_CREATE);
            initMaps(db);
            statement.execute(PARAM_TABLE_CREATE);
            initParams(db);
            statement.execute(MISSION_TABLE_CREATE);
            initMissions(db);
            statement.execute(SAVE_TABLE_CREATE);
            statement.execute(SAVE_PLAYER_TABLE_CREATE);
            statement.execute(SAVE_BUILDING_TABLE_CREATE);
            statement.execute(SAVE_UNIT_TABLE_CREATE);
            this.database.commit();
        } catch (SQLException e) {

        }
    }

    private void initParams(Connection db) {
        this.addParam(db, Util.PARAM_AUDIO_MUSIC, "1");
        this.addParam(db, Util.PARAM_AUDIO_SOUND, "1");
        this.addParam(db, Util.PARAM_SPEED_HUMAN, "1");
        this.addParam(db, Util.PARAM_SPEED_ROBOT, "1");
    }

    private void initMaps(Connection db) {
        // CAMPAIGN
        System.out.println("initMaps...START");
        // CAMPAIGN MAPS : 1 - 25
        this.addMap(db, new Map(Map.BRITAIN_ID, Map.BRITAIN, Map.BRITAIN_FILE, Map.CAMPAIGN, true, Mission.BRITAIN_ID));
        this.addMap(db, new Map(Map.TOBRUK_ID, Map.TOBRUK, Map.TOBRUK_FILE, Map.CAMPAIGN, true, Mission.TOBRUK_ID));
        this.addMap(db, new Map(Map.PEARL_HARBOR_ID, Map.PEARL_HARBOR, Map.PEARL_HARBOR_FILE, Map.CAMPAIGN, true, Mission.PEARL_HARBOR_ID));
        this.addMap(db, new Map(Map.HONG_KONG_ID, Map.HONG_KONG, Map.HONG_KONG_FILE, Map.CAMPAIGN, true, Mission.HONG_KONG_ID));
        this.addMap(db, new Map(Map.MIDWAY_ID, Map.MIDWAY, Map.MIDWAY_FILE, Map.CAMPAIGN, true, Mission.MIDWAY_ID));
        this.addMap(db, new Map(Map.STALINGRAD_ID, Map.STALINGRAD, Map.STALINGRAD_FILE, Map.CAMPAIGN, true, Mission.STALINGARD_ID));
        this.addMap(db, new Map(Map.GUADALCANAL_ID, Map.GUADALCANAL, Map.GUADALCANAL_FILE, Map.CAMPAIGN, true, Mission.GUADALCANAL_ID));
        this.addMap(db, new Map(Map.EL_ALAMEIN_ID, Map.EL_ALAMEIN, Map.EL_ALAMEIN_FILE, Map.CAMPAIGN, true, Mission.EL_ALAMEIN_ID));
        this.addMap(db, new Map(Map.CAPRI_ID, Map.CAPRI, Map.CAPRI_FILE, Map.CAMPAIGN, true, Mission.CAPRI_ID));
        this.addMap(db, new Map(Map.KURSK_ID, Map.KURSK, Map.KURSK_FILE, Map.CAMPAIGN, true, Mission.KURSK_ID));
        this.addMap(db, new Map(Map.HUSKY_ID, Map.HUSKY, Map.HUSKY_FILE, Map.CAMPAIGN, true, Mission.HUSKY_ID));
        this.addMap(db, new Map(Map.KHARKOV_ID, Map.KHARKOV, Map.KHARKOV_FILE, Map.CAMPAIGN, true, Mission.KHARKOV_ID));
        this.addMap(db, new Map(Map.LENINGRAD_ID, Map.LENINGRAD, Map.LENINGRAD_FILE, Map.CAMPAIGN, true, Mission.LENINGRAD_ID));
        this.addMap(db, new Map(Map.CRIMEA_ID, Map.CRIMEA, Map.CRIMEA_FILE, Map.CAMPAIGN, true, Mission.CRIMEA_ID));
        this.addMap(db, new Map(Map.ROMA_ID, Map.ROMA, Map.ROMA_FILE, Map.CAMPAIGN, true, Mission.ROMA_ID));
        this.addMap(db, new Map(Map.OMAHA_BEACH_ID, Map.OMAHA_BEACH, Map.OMAHA_BEACH_FILE, Map.CAMPAIGN, true, Mission.OMAHA_BEACH_ID));
        this.addMap(db, new Map(Map.PARIS_ID, Map.PARIS, Map.PARIS_FILE, Map.CAMPAIGN, true, Mission.PARIS_ID));
        this.addMap(db, new Map(Map.MARKET_GARDEN_ID, Map.MARKET_GARDEN, Map.MARKET_GARDEN_FILE, Map.CAMPAIGN, true, Mission.MARKET_GARDEN_ID));
        this.addMap(db, new Map(Map.BULGE_ID, Map.BULGE, Map.BULGE_FILE, Map.CAMPAIGN, true, Mission.BULGE_ID));
        this.addMap(db, new Map(Map.LUZON_ID, Map.LUZON, Map.LUZON_FILE, Map.CAMPAIGN, true, Mission.LUZON_ID));
        this.addMap(db, new Map(Map.IWO_JIMA_ID, Map.IWO_JIMA, Map.IWO_JIMA_FILE, Map.CAMPAIGN, true, Mission.IWO_JIMA_ID));
        this.addMap(db, new Map(Map.VIENNA_ID, Map.VIENNA, Map.VIENNA_FILE, Map.CAMPAIGN, true, Mission.VIENNA_ID));
        this.addMap(db, new Map(Map.OKINAWA_ID, Map.OKINAWA, Map.OKINAWA_FILE, Map.CAMPAIGN, true, Mission.OKINAWA_ID));
        this.addMap(db, new Map(Map.BERLIN_ID, Map.BERLIN, Map.BERLIN_FILE, Map.CAMPAIGN, true, Mission.BERLIN_ID));
        this.addMap(db, new Map(Map.SAKHALIN_ID, Map.SAKHALIN, Map.SAKHALIN_FILE, Map.CAMPAIGN, true, Mission.SAKHALIN_ID));
        // 2P MAPS : 26-30
        this.addMap(db, new Map(Map.DUAL_ID, Map.DUAL, Map.DUAL_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.INFINITE_ID, Map.INFINITE, Map.INFINITE_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.PLUS_ID, Map.PLUS, Map.PLUS_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.AUSTRALIA_ID, Map.AUSTRALIA, Map.AUSTRALIA_FILE, Map.CLASSIC_2P, false, 0));
        this.addMap(db, new Map(Map.UNKNOWN_ID, Map.UNKNOWN, Map.UNKNOWN_FILE, Map.CLASSIC_2P, false, 0));
        // 3P MAPS : 31-35
        this.addMap(db, new Map(Map.TRIANGLE_ID, Map.TRIANGLE, Map.TRIANGLE_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.POSEIDON_ID, Map.POSEIDON, Map.POSEIDON_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.AFRICA_ID, Map.AFRICA, Map.AFRICA_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.MAZE_ID, Map.MAZE, Map.MAZE_FILE, Map.CLASSIC_3P, false, 0));
        this.addMap(db, new Map(Map.CARIBBEAN_ID, Map.CARIBBEAN, Map.CARIBBEAN_FILE, Map.CLASSIC_3P, false, 0));
        // 4P MAPS : 36-40
        this.addMap(db, new Map(Map.SQUARE_ID, Map.SQUARE, Map.SQUARE_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.SPIRAL_ID, Map.SPIRAL, Map.SPIRAL_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.RIVERS_ID, Map.RIVERS, Map.RIVERS_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.EIGHT_ID, Map.EIGHT, Map.EIGHT_FILE, Map.CLASSIC_4P, false, 0));
        this.addMap(db, new Map(Map.AMERICA_ID, Map.AMERICA, Map.AMERICA_FILE, Map.CLASSIC_4P, false, 0));
        // 5P MAPS : 41-45
        this.addMap(db, new Map(Map.PENTAGON_ID, Map.PENTAGON, Map.PENTAGON_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.EUROPE_ID, Map.EUROPE, Map.EUROPE_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.STAR_ID, Map.STAR, Map.STAR_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.ISLANDS_ID, Map.ISLANDS, Map.ISLANDS_FILE, Map.CLASSIC_5P, false, 0));
        this.addMap(db, new Map(Map.WORLD_ID, Map.WORLD, Map.WORLD_FILE, Map.CLASSIC_5P, false, 0));
        System.out.println("initMaps...END");
    }

    private void initMissions(Connection db) {
        System.out.println("initMissions...START");
        this.addMission(db, new Mission(Mission.BRITAIN_ID, Mission.BRITAIN_NAME, Map.BRITAIN_ID, Mission.BRITAIN_DESCRIPTION, Mission.BRITAIN_LOCATION, Mission.BRITAIN_ARMIES, 0, Mission.BRITAIN_POSITION, false));
        this.addMission(db, new Mission(Mission.TOBRUK_ID, Mission.TOBRUK_NAME, Map.TOBRUK_ID, Mission.TOBRUK_DESCRIPTION, Mission.TOBRUK_LOCATION, Mission.TOBRUK_ARMIES, 0, Mission.TOBRUK_POSITION, true));
        this.addMission(db, new Mission(Mission.PEARL_HARBOR_ID, Mission.PEARL_HARBOR_NAME, Map.PEARL_HARBOR_ID, Mission.PEARL_HARBOR_DESCRIPTION, Mission.PEARL_HARBOR_LOCATION, Mission.PEARL_HARBOR_ARMIES, 0, Mission.PEARL_HARBOR_POSITION, true));
        this.addMission(db, new Mission(Mission.HONG_KONG_ID, Mission.HONG_KONG_NAME, Map.HONG_KONG_ID, Mission.HONG_KONG_DESCRIPTION, Mission.HONG_KONG_LOCATION, Mission.HONG_KONG_ARMIES, 0, Mission.HONG_KONG_POSITION, true));
        this.addMission(db, new Mission(Mission.MIDWAY_ID, Mission.MIDWAY_NAME, Map.MIDWAY_ID, Mission.MIDWAY_DESCRIPTION, Mission.MIDWAY_LOCATION, Mission.MIDWAY_ARMIES, 0, Mission.MIDWAY_POSITION, true));
        this.addMission(db, new Mission(Mission.STALINGARD_ID, Mission.STALINGRAD_NAME, Map.STALINGRAD_ID, Mission.STALINGRAD_DESCRIPTION, Mission.STALINGRAD_LOCATION, Mission.STALINGRAD_ARMIES, 0, Mission.STALINGRAD_POSITION, true));
        this.addMission(db, new Mission(Mission.GUADALCANAL_ID, Mission.GUADALCANAL_NAME, Map.GUADALCANAL_ID, Mission.GUADALCANAL_DESCRIPTION, Mission.GUADALCANAL_LOCATION, Mission.GUADALCANAL_ARMIES, 0, Mission.GUADALCANAL_POSITION, true));
        this.addMission(db, new Mission(Mission.EL_ALAMEIN_ID, Mission.EL_ALAMEIN_NAME, Map.EL_ALAMEIN_ID, Mission.EL_ALAMEIN_DESCRIPTION, Mission.EL_ALAMEIN_LOCATION, Mission.EL_ALAMEIN_ARMIES, 0, Mission.EL_ALAMEIN_POSITION, true));
        this.addMission(db, new Mission(Mission.CAPRI_ID, Mission.CAPRI_NAME, Map.CAPRI_ID, Mission.CAPRI_DESCRIPTION, Mission.CAPRI_LOCATION, Mission.CAPRI_ARMIES, 0, Mission.CAPRI_POSITION, true));
        this.addMission(db, new Mission(Mission.KURSK_ID, Mission.KURSK_NAME, Map.KURSK_ID, Mission.KURSK_DESCRIPTION, Mission.KURSK_LOCATION, Mission.KURSK_ARMIES, 0, Mission.KURSK_POSITION, true));
        this.addMission(db, new Mission(Mission.HUSKY_ID, Mission.HUSKY_NAME, Map.HUSKY_ID, Mission.HUSKY_DESCRIPTION, Mission.HUSKY_LOCATION, Mission.HUSKY_ARMIES, 0, Mission.HUSKY_POSITION, true));
        this.addMission(db, new Mission(Mission.KHARKOV_ID, Mission.KHARKOV_NAME, Map.KHARKOV_ID, Mission.KHARKOV_DESCRIPTION, Mission.KHARKOV_LOCATION, Mission.KHARKOV_ARMIES, 0, Mission.KHARKOV_POSITION, true));
        this.addMission(db, new Mission(Mission.LENINGRAD_ID, Mission.LENINGRAD_NAME, Map.LENINGRAD_ID, Mission.LENINGRAD_DESCRIPTION, Mission.LENINGRAD_LOCATION, Mission.LENINGRAD_ARMIES, 0, Mission.LENINGRAD_POSITION, true));
        this.addMission(db, new Mission(Mission.CRIMEA_ID, Mission.CRIMEA_NAME, Map.CRIMEA_ID, Mission.CRIMEA_DESCRIPTION, Mission.CRIMEA_LOCATION, Mission.CRIMEA_ARMIES, 0, Mission.CRIMEA_POSITION, true));
        this.addMission(db, new Mission(Mission.ROMA_ID, Mission.ROMA_NAME, Map.ROMA_ID, Mission.ROMA_DESCRIPTION, Mission.ROMA_LOCATION, Mission.ROMA_ARMIES, 0, Mission.ROMA_POSITION, true));
        this.addMission(db, new Mission(Mission.OMAHA_BEACH_ID, Mission.OMAHA_BEACH_NAME, Map.OMAHA_BEACH_ID, Mission.OMAHA_BEACH_DESCRIPTION, Mission.OMAHA_BEACH_LOCATION, Mission.OMAHA_BEACH_ARMIES, 0, Mission.OMAHA_BEACH_POSITION, true));
        this.addMission(db, new Mission(Mission.PARIS_ID, Mission.PARIS_NAME, Map.PARIS_ID, Mission.PARIS_DESCRIPTION, Mission.PARIS_LOCATION, Mission.PARIS_ARMIES, 0, Mission.PARIS_POSITION, true));
        this.addMission(db, new Mission(Mission.MARKET_GARDEN_ID, Mission.MARKET_GARDEN_NAME, Map.MARKET_GARDEN_ID, Mission.MARKET_GARDEN_DESCRIPTION, Mission.MARKET_GARDEN_LOCATION, Mission.MARKET_GARDEN_ARMIES, 0, Mission.MARKET_GARDEN_POSITION, true));
        this.addMission(db, new Mission(Mission.BULGE_ID, Mission.BULGE_NAME, Map.BULGE_ID, Mission.BULGE_DESCRIPTION, Mission.BULGE_LOCATION, Mission.BULGE_ARMIES, 0, Mission.BULGE_POSITION, true));
        this.addMission(db, new Mission(Mission.LUZON_ID, Mission.LUZON_NAME, Map.LUZON_ID, Mission.LUZON_DESCRIPTION, Mission.LUZON_LOCATION, Mission.LUZON_ARMIES, 0, Mission.LUZON_POSITION, true));
        this.addMission(db, new Mission(Mission.IWO_JIMA_ID, Mission.IWO_JIMA_NAME, Map.IWO_JIMA_ID, Mission.IWO_JIMA_DESCRIPTION, Mission.IWO_JIMA_LOCATION, Mission.IWO_JIMA_ARMIES, 0, Mission.IWO_JIMA_POSITION, true));
        this.addMission(db, new Mission(Mission.VIENNA_ID, Mission.VIENNA_NAME, Map.VIENNA_ID, Mission.VIENNA_DESCRIPTION, Mission.VIENNA_LOCATION, Mission.VIENNA_ARMIES, 0, Mission.VIENNA_POSITION, true));
        this.addMission(db, new Mission(Mission.OKINAWA_ID, Mission.OKINAWA_NAME, Map.OKINAWA_ID, Mission.OKINAWA_DESCRIPTION, Mission.OKINAWA_LOCATION, Mission.OKINAWA_ARMIES, 0, Mission.OKINAWA_POSITION, true));
        this.addMission(db, new Mission(Mission.BERLIN_ID, Mission.BERLIN_NAME, Map.BERLIN_ID, Mission.BERLIN_DESCRIPTION, Mission.BERLIN_LOCATION, Mission.BERLIN_ARMIES, 0, Mission.BERLIN_POSITION, true));
        this.addMission(db, new Mission(Mission.SAKHALIN_ID, Mission.SAKHALIN_NAME, Map.SAKHALIN_ID, Mission.SAKHALIN_DESCRIPTION, Mission.SAKHALIN_LOCATION, Mission.SAKHALIN_ARMIES, 0, Mission.SAKHALIN_POSITION, true));
        System.out.println("initMissions...END");
    }
}