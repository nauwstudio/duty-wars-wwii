<?xml version="1.0" encoding="UTF-8"?>
<tileset name="map_desert" tilewidth="1023" tileheight="886" tilecount="20" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="1"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0001.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="2"/>
  </properties>
  <image width="1023" height="792" source="textures/tiles/desert/0002.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="3"/>
  </properties>
  <image width="1023" height="719" source="textures/tiles/desert/0003.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="4"/>
  </properties>
  <image width="1023" height="699" source="textures/tiles/desert/0004.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="5"/>
  </properties>
  <image width="1023" height="886" source="textures/tiles/desert/0005.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="1"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0006.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="2"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0007.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="3"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0008.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="4"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0009.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="5"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0010.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="6"/>
   <property name="type" type="int" value="6"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0011.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="7"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0012.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="8"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0013.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="1"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0014.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="2"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0015.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="3"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0016.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="4"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0017.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="5"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0018.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="6"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0019.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="season" type="int" value="3"/>
   <property name="subtype" type="int" value="9"/>
   <property name="type" type="int" value="7"/>
  </properties>
  <image width="1023" height="738" source="textures/tiles/desert/0020.png"/>
 </tile>
</tileset>
