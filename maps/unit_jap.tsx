<?xml version="1.0" encoding="UTF-8"?>
<tileset name="unit_jap" tilewidth="511" tileheight="357" tilecount="188" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="188">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0001.png"/>
 </tile>
 <tile id="189">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0002.png"/>
 </tile>
 <tile id="190">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0003.png"/>
 </tile>
 <tile id="191">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0004.png"/>
 </tile>
 <tile id="192">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0005.png"/>
 </tile>
 <tile id="193">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0006.png"/>
 </tile>
 <tile id="194">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0007.png"/>
 </tile>
 <tile id="195">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0008.png"/>
 </tile>
 <tile id="196">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0009.png"/>
 </tile>
 <tile id="197">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0010.png"/>
 </tile>
 <tile id="198">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0011.png"/>
 </tile>
 <tile id="199">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0012.png"/>
 </tile>
 <tile id="200">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0013.png"/>
 </tile>
 <tile id="201">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0014.png"/>
 </tile>
 <tile id="202">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0015.png"/>
 </tile>
 <tile id="203">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0016.png"/>
 </tile>
 <tile id="204">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0017.png"/>
 </tile>
 <tile id="205">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0018.png"/>
 </tile>
 <tile id="206">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0019.png"/>
 </tile>
 <tile id="207">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0020.png"/>
 </tile>
 <tile id="208">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0021.png"/>
 </tile>
 <tile id="209">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0022.png"/>
 </tile>
 <tile id="210">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0023.png"/>
 </tile>
 <tile id="211">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="2"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0024.png"/>
 </tile>
 <tile id="212">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0025.png"/>
 </tile>
 <tile id="213">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0026.png"/>
 </tile>
 <tile id="214">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0027.png"/>
 </tile>
 <tile id="215">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0028.png"/>
 </tile>
 <tile id="216">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0029.png"/>
 </tile>
 <tile id="217">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0030.png"/>
 </tile>
 <tile id="218">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0031.png"/>
 </tile>
 <tile id="219">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="3"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0032.png"/>
 </tile>
 <tile id="220">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0033.png"/>
 </tile>
 <tile id="221">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0034.png"/>
 </tile>
 <tile id="222">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0035.png"/>
 </tile>
 <tile id="223">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0036.png"/>
 </tile>
 <tile id="224">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0037.png"/>
 </tile>
 <tile id="225">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0038.png"/>
 </tile>
 <tile id="226">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0039.png"/>
 </tile>
 <tile id="227">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="4"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0040.png"/>
 </tile>
 <tile id="228">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0041.png"/>
 </tile>
 <tile id="229">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0042.png"/>
 </tile>
 <tile id="230">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0043.png"/>
 </tile>
 <tile id="231">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0044.png"/>
 </tile>
 <tile id="232">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0045.png"/>
 </tile>
 <tile id="233">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0046.png"/>
 </tile>
 <tile id="234">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0047.png"/>
 </tile>
 <tile id="235">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0048.png"/>
 </tile>
 <tile id="236">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0049.png"/>
 </tile>
 <tile id="237">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0050.png"/>
 </tile>
 <tile id="238">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0051.png"/>
 </tile>
 <tile id="239">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0052.png"/>
 </tile>
 <tile id="240">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0053.png"/>
 </tile>
 <tile id="241">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0054.png"/>
 </tile>
 <tile id="242">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0055.png"/>
 </tile>
 <tile id="243">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="5"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0056.png"/>
 </tile>
 <tile id="244">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0057.png"/>
 </tile>
 <tile id="245">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0058.png"/>
 </tile>
 <tile id="246">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="302" source="textures/units/jap/0059.png"/>
 </tile>
 <tile id="247">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="302" source="textures/units/jap/0060.png"/>
 </tile>
 <tile id="248">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0061.png"/>
 </tile>
 <tile id="249">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0062.png"/>
 </tile>
 <tile id="250">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="307" source="textures/units/jap/0063.png"/>
 </tile>
 <tile id="251">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="307" source="textures/units/jap/0064.png"/>
 </tile>
 <tile id="252">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0065.png"/>
 </tile>
 <tile id="253">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0066.png"/>
 </tile>
 <tile id="254">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="309" source="textures/units/jap/0067.png"/>
 </tile>
 <tile id="255">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="309" source="textures/units/jap/0068.png"/>
 </tile>
 <tile id="256">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0069.png"/>
 </tile>
 <tile id="257">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0070.png"/>
 </tile>
 <tile id="258">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="316" source="textures/units/jap/0071.png"/>
 </tile>
 <tile id="259">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="6"/>
  </properties>
  <image width="511" height="316" source="textures/units/jap/0072.png"/>
 </tile>
 <tile id="260">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="7"/>
  </properties>
  <image width="511" height="327" source="textures/units/jap/0073.png"/>
 </tile>
 <tile id="261">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="7"/>
  </properties>
  <image width="511" height="327" source="textures/units/jap/0074.png"/>
 </tile>
 <tile id="262">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="7"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0075.png"/>
 </tile>
 <tile id="263">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="7"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0076.png"/>
 </tile>
 <tile id="264">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0077.png"/>
 </tile>
 <tile id="265">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0078.png"/>
 </tile>
 <tile id="266">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0079.png"/>
 </tile>
 <tile id="267">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0080.png"/>
 </tile>
 <tile id="268">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0081.png"/>
 </tile>
 <tile id="269">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0082.png"/>
 </tile>
 <tile id="270">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0083.png"/>
 </tile>
 <tile id="271">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0084.png"/>
 </tile>
 <tile id="272">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0085.png"/>
 </tile>
 <tile id="273">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0086.png"/>
 </tile>
 <tile id="274">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0087.png"/>
 </tile>
 <tile id="275">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0088.png"/>
 </tile>
 <tile id="276">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0089.png"/>
 </tile>
 <tile id="277">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0090.png"/>
 </tile>
 <tile id="278">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="299" source="textures/units/jap/0091.png"/>
 </tile>
 <tile id="279">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="8"/>
  </properties>
  <image width="511" height="299" source="textures/units/jap/0092.png"/>
 </tile>
 <tile id="280">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="297" source="textures/units/jap/0093.png"/>
 </tile>
 <tile id="281">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0094.png"/>
 </tile>
 <tile id="282">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0095.png"/>
 </tile>
 <tile id="283">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0096.png"/>
 </tile>
 <tile id="284">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="305" source="textures/units/jap/0097.png"/>
 </tile>
 <tile id="285">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="305" source="textures/units/jap/0098.png"/>
 </tile>
 <tile id="286">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0099.png"/>
 </tile>
 <tile id="287">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="9"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0100.png"/>
 </tile>
 <tile id="288">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="344" source="textures/units/jap/0101.png"/>
 </tile>
 <tile id="289">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="344" source="textures/units/jap/0102.png"/>
 </tile>
 <tile id="290">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="320" source="textures/units/jap/0103.png"/>
 </tile>
 <tile id="291">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="320" source="textures/units/jap/0104.png"/>
 </tile>
 <tile id="292">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="344" source="textures/units/jap/0105.png"/>
 </tile>
 <tile id="293">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="344" source="textures/units/jap/0106.png"/>
 </tile>
 <tile id="294">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="320" source="textures/units/jap/0107.png"/>
 </tile>
 <tile id="295">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="10"/>
  </properties>
  <image width="511" height="320" source="textures/units/jap/0108.png"/>
 </tile>
 <tile id="296">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0109.png"/>
 </tile>
 <tile id="297">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0110.png"/>
 </tile>
 <tile id="298">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="330" source="textures/units/jap/0111.png"/>
 </tile>
 <tile id="299">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="330" source="textures/units/jap/0112.png"/>
 </tile>
 <tile id="300">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0113.png"/>
 </tile>
 <tile id="301">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0114.png"/>
 </tile>
 <tile id="302">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="330" source="textures/units/jap/0115.png"/>
 </tile>
 <tile id="303">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0116.png"/>
 </tile>
 <tile id="304">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0117.png"/>
 </tile>
 <tile id="305">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0118.png"/>
 </tile>
 <tile id="306">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="330" source="textures/units/jap/0119.png"/>
 </tile>
 <tile id="307">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="342" source="textures/units/jap/0120.png"/>
 </tile>
 <tile id="308">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0121.png"/>
 </tile>
 <tile id="309">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="357" source="textures/units/jap/0122.png"/>
 </tile>
 <tile id="310">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="354" source="textures/units/jap/0123.png"/>
 </tile>
 <tile id="311">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="11"/>
  </properties>
  <image width="511" height="354" source="textures/units/jap/0124.png"/>
 </tile>
 <tile id="312">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0125.png"/>
 </tile>
 <tile id="313">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0126.png"/>
 </tile>
 <tile id="314">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0127.png"/>
 </tile>
 <tile id="315">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0128.png"/>
 </tile>
 <tile id="316">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0129.png"/>
 </tile>
 <tile id="317">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0130.png"/>
 </tile>
 <tile id="318">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0131.png"/>
 </tile>
 <tile id="319">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="12"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0132.png"/>
 </tile>
 <tile id="320">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0133.png"/>
 </tile>
 <tile id="321">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0134.png"/>
 </tile>
 <tile id="322">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0135.png"/>
 </tile>
 <tile id="323">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0136.png"/>
 </tile>
 <tile id="324">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0137.png"/>
 </tile>
 <tile id="325">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0138.png"/>
 </tile>
 <tile id="326">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0139.png"/>
 </tile>
 <tile id="327">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="13"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0140.png"/>
 </tile>
 <tile id="328">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0141.png"/>
 </tile>
 <tile id="329">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0142.png"/>
 </tile>
 <tile id="330">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0143.png"/>
 </tile>
 <tile id="331">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0144.png"/>
 </tile>
 <tile id="332">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0145.png"/>
 </tile>
 <tile id="333">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0146.png"/>
 </tile>
 <tile id="334">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0147.png"/>
 </tile>
 <tile id="335">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0148.png"/>
 </tile>
 <tile id="336">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0149.png"/>
 </tile>
 <tile id="337">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0150.png"/>
 </tile>
 <tile id="338">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0151.png"/>
 </tile>
 <tile id="339">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0152.png"/>
 </tile>
 <tile id="340">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0153.png"/>
 </tile>
 <tile id="341">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0154.png"/>
 </tile>
 <tile id="342">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0155.png"/>
 </tile>
 <tile id="343">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="14"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0156.png"/>
 </tile>
 <tile id="344">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="15"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0157.png"/>
 </tile>
 <tile id="345">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="15"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0158.png"/>
 </tile>
 <tile id="346">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="15"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0159.png"/>
 </tile>
 <tile id="347">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="15"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0160.png"/>
 </tile>
 <tile id="348">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0161.png"/>
 </tile>
 <tile id="349">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0162.png"/>
 </tile>
 <tile id="350">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0163.png"/>
 </tile>
 <tile id="351">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0164.png"/>
 </tile>
 <tile id="352">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0165.png"/>
 </tile>
 <tile id="353">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0166.png"/>
 </tile>
 <tile id="354">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0167.png"/>
 </tile>
 <tile id="355">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0168.png"/>
 </tile>
 <tile id="356">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0169.png"/>
 </tile>
 <tile id="357">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0170.png"/>
 </tile>
 <tile id="358">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0171.png"/>
 </tile>
 <tile id="359">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0172.png"/>
 </tile>
 <tile id="360">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0173.png"/>
 </tile>
 <tile id="361">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="298" source="textures/units/jap/0174.png"/>
 </tile>
 <tile id="362">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0175.png"/>
 </tile>
 <tile id="363">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="16"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0176.png"/>
 </tile>
 <tile id="364">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0177.png"/>
 </tile>
 <tile id="365">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0178.png"/>
 </tile>
 <tile id="366">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0179.png"/>
 </tile>
 <tile id="367">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0180.png"/>
 </tile>
 <tile id="368">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0181.png"/>
 </tile>
 <tile id="369">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0182.png"/>
 </tile>
 <tile id="370">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0183.png"/>
 </tile>
 <tile id="371">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="17"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0184.png"/>
 </tile>
 <tile id="372">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="18"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0185.png"/>
 </tile>
 <tile id="373">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="18"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0186.png"/>
 </tile>
 <tile id="374">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="18"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0187.png"/>
 </tile>
 <tile id="375">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="4"/>
   <property name="type" value="18"/>
  </properties>
  <image width="511" height="295" source="textures/units/jap/0188.png"/>
 </tile>
</tileset>
