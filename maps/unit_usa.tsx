<?xml version="1.0" encoding="UTF-8"?>
<tileset name="unit_usa" tilewidth="256" tileheight="179" tilecount="192" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="188">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0001.png"/>
 </tile>
 <tile id="189">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0002.png"/>
 </tile>
 <tile id="190">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0003.png"/>
 </tile>
 <tile id="191">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0004.png"/>
 </tile>
 <tile id="192">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0005.png"/>
 </tile>
 <tile id="193">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0006.png"/>
 </tile>
 <tile id="194">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0007.png"/>
 </tile>
 <tile id="195">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0008.png"/>
 </tile>
 <tile id="196">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0009.png"/>
 </tile>
 <tile id="197">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0010.png"/>
 </tile>
 <tile id="198">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0011.png"/>
 </tile>
 <tile id="199">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0012.png"/>
 </tile>
 <tile id="200">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0013.png"/>
 </tile>
 <tile id="201">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0014.png"/>
 </tile>
 <tile id="202">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0015.png"/>
 </tile>
 <tile id="203">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0016.png"/>
 </tile>
 <tile id="204">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0017.png"/>
 </tile>
 <tile id="205">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0018.png"/>
 </tile>
 <tile id="206">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0019.png"/>
 </tile>
 <tile id="207">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0020.png"/>
 </tile>
 <tile id="208">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0021.png"/>
 </tile>
 <tile id="209">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0022.png"/>
 </tile>
 <tile id="210">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0023.png"/>
 </tile>
 <tile id="211">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="2"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0024.png"/>
 </tile>
 <tile id="212">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0025.png"/>
 </tile>
 <tile id="213">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0026.png"/>
 </tile>
 <tile id="214">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0027.png"/>
 </tile>
 <tile id="215">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0028.png"/>
 </tile>
 <tile id="216">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0029.png"/>
 </tile>
 <tile id="217">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0030.png"/>
 </tile>
 <tile id="218">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0031.png"/>
 </tile>
 <tile id="219">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="3"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0032.png"/>
 </tile>
 <tile id="220">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0033.png"/>
 </tile>
 <tile id="221">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0034.png"/>
 </tile>
 <tile id="222">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0035.png"/>
 </tile>
 <tile id="223">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0036.png"/>
 </tile>
 <tile id="224">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0037.png"/>
 </tile>
 <tile id="225">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0038.png"/>
 </tile>
 <tile id="226">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0039.png"/>
 </tile>
 <tile id="227">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="4"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0040.png"/>
 </tile>
 <tile id="228">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0041.png"/>
 </tile>
 <tile id="229">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0042.png"/>
 </tile>
 <tile id="230">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0043.png"/>
 </tile>
 <tile id="231">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0044.png"/>
 </tile>
 <tile id="232">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0045.png"/>
 </tile>
 <tile id="233">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0046.png"/>
 </tile>
 <tile id="234">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0047.png"/>
 </tile>
 <tile id="235">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0048.png"/>
 </tile>
 <tile id="236">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0049.png"/>
 </tile>
 <tile id="237">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0050.png"/>
 </tile>
 <tile id="238">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0051.png"/>
 </tile>
 <tile id="239">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0052.png"/>
 </tile>
 <tile id="240">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0053.png"/>
 </tile>
 <tile id="241">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0054.png"/>
 </tile>
 <tile id="242">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0055.png"/>
 </tile>
 <tile id="243">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="5"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0056.png"/>
 </tile>
 <tile id="244">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0057.png"/>
 </tile>
 <tile id="245">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0058.png"/>
 </tile>
 <tile id="246">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="151" source="textures/units/usa/0059.png"/>
 </tile>
 <tile id="247">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="151" source="textures/units/usa/0060.png"/>
 </tile>
 <tile id="248">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0061.png"/>
 </tile>
 <tile id="249">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0062.png"/>
 </tile>
 <tile id="250">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="154" source="textures/units/usa/0063.png"/>
 </tile>
 <tile id="251">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="154" source="textures/units/usa/0064.png"/>
 </tile>
 <tile id="252">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0065.png"/>
 </tile>
 <tile id="253">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0066.png"/>
 </tile>
 <tile id="254">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="155" source="textures/units/usa/0067.png"/>
 </tile>
 <tile id="255">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="155" source="textures/units/usa/0068.png"/>
 </tile>
 <tile id="256">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0069.png"/>
 </tile>
 <tile id="257">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0070.png"/>
 </tile>
 <tile id="258">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="158" source="textures/units/usa/0071.png"/>
 </tile>
 <tile id="259">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="6"/>
  </properties>
  <image width="256" height="158" source="textures/units/usa/0072.png"/>
 </tile>
 <tile id="260">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="7"/>
  </properties>
  <image width="256" height="164" source="textures/units/usa/0073.png"/>
 </tile>
 <tile id="261">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="7"/>
  </properties>
  <image width="256" height="164" source="textures/units/usa/0074.png"/>
 </tile>
 <tile id="262">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="7"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0075.png"/>
 </tile>
 <tile id="263">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="7"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0076.png"/>
 </tile>
 <tile id="264">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0077.png"/>
 </tile>
 <tile id="265">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0078.png"/>
 </tile>
 <tile id="266">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0079.png"/>
 </tile>
 <tile id="267">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0080.png"/>
 </tile>
 <tile id="268">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0081.png"/>
 </tile>
 <tile id="269">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0082.png"/>
 </tile>
 <tile id="270">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0083.png"/>
 </tile>
 <tile id="271">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0084.png"/>
 </tile>
 <tile id="272">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0085.png"/>
 </tile>
 <tile id="273">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0086.png"/>
 </tile>
 <tile id="274">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0087.png"/>
 </tile>
 <tile id="275">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0088.png"/>
 </tile>
 <tile id="276">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0089.png"/>
 </tile>
 <tile id="277">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0090.png"/>
 </tile>
 <tile id="278">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="150" source="textures/units/usa/0091.png"/>
 </tile>
 <tile id="279">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="8"/>
  </properties>
  <image width="256" height="150" source="textures/units/usa/0092.png"/>
 </tile>
 <tile id="280">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0093.png"/>
 </tile>
 <tile id="281">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0094.png"/>
 </tile>
 <tile id="282">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0095.png"/>
 </tile>
 <tile id="283">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0096.png"/>
 </tile>
 <tile id="284">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="153" source="textures/units/usa/0097.png"/>
 </tile>
 <tile id="285">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="153" source="textures/units/usa/0098.png"/>
 </tile>
 <tile id="286">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0099.png"/>
 </tile>
 <tile id="287">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="9"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0100.png"/>
 </tile>
 <tile id="288">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="172" source="textures/units/usa/0101.png"/>
 </tile>
 <tile id="289">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="172" source="textures/units/usa/0102.png"/>
 </tile>
 <tile id="290">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="160" source="textures/units/usa/0103.png"/>
 </tile>
 <tile id="291">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="160" source="textures/units/usa/0104.png"/>
 </tile>
 <tile id="292">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="172" source="textures/units/usa/0105.png"/>
 </tile>
 <tile id="293">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="172" source="textures/units/usa/0106.png"/>
 </tile>
 <tile id="294">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="160" source="textures/units/usa/0107.png"/>
 </tile>
 <tile id="295">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="10"/>
  </properties>
  <image width="256" height="160" source="textures/units/usa/0108.png"/>
 </tile>
 <tile id="296">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0109.png"/>
 </tile>
 <tile id="297">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0110.png"/>
 </tile>
 <tile id="298">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="165" source="textures/units/usa/0111.png"/>
 </tile>
 <tile id="299">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="165" source="textures/units/usa/0112.png"/>
 </tile>
 <tile id="300">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0113.png"/>
 </tile>
 <tile id="301">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0114.png"/>
 </tile>
 <tile id="302">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="169" source="textures/units/usa/0115.png"/>
 </tile>
 <tile id="303">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="169" source="textures/units/usa/0116.png"/>
 </tile>
 <tile id="304">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0117.png"/>
 </tile>
 <tile id="305">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0118.png"/>
 </tile>
 <tile id="306">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="171" source="textures/units/usa/0119.png"/>
 </tile>
 <tile id="307">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="171" source="textures/units/usa/0120.png"/>
 </tile>
 <tile id="308">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0121.png"/>
 </tile>
 <tile id="309">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="179" source="textures/units/usa/0122.png"/>
 </tile>
 <tile id="310">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="177" source="textures/units/usa/0123.png"/>
 </tile>
 <tile id="311">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="11"/>
  </properties>
  <image width="256" height="177" source="textures/units/usa/0124.png"/>
 </tile>
 <tile id="312">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0125.png"/>
 </tile>
 <tile id="313">
  <properties>
   <property name="orientation" value="1"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0126.png"/>
 </tile>
 <tile id="314">
  <properties>
   <property name="orientation" value="2"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0127.png"/>
 </tile>
 <tile id="315">
  <properties>
   <property name="orientation" value="3"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0128.png"/>
 </tile>
 <tile id="316">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0129.png"/>
 </tile>
 <tile id="317">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0130.png"/>
 </tile>
 <tile id="318">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0131.png"/>
 </tile>
 <tile id="319">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="12"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0132.png"/>
 </tile>
 <tile id="320">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0133.png"/>
 </tile>
 <tile id="321">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0134.png"/>
 </tile>
 <tile id="322">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0135.png"/>
 </tile>
 <tile id="323">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0136.png"/>
 </tile>
 <tile id="324">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0137.png"/>
 </tile>
 <tile id="325">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0138.png"/>
 </tile>
 <tile id="326">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0139.png"/>
 </tile>
 <tile id="327">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="13"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0140.png"/>
 </tile>
 <tile id="328">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0141.png"/>
 </tile>
 <tile id="329">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0142.png"/>
 </tile>
 <tile id="330">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0143.png"/>
 </tile>
 <tile id="331">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0144.png"/>
 </tile>
 <tile id="332">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0145.png"/>
 </tile>
 <tile id="333">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0146.png"/>
 </tile>
 <tile id="334">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0147.png"/>
 </tile>
 <tile id="335">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0148.png"/>
 </tile>
 <tile id="336">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0149.png"/>
 </tile>
 <tile id="337">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0150.png"/>
 </tile>
 <tile id="338">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0151.png"/>
 </tile>
 <tile id="339">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0152.png"/>
 </tile>
 <tile id="340">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0153.png"/>
 </tile>
 <tile id="341">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0154.png"/>
 </tile>
 <tile id="342">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0155.png"/>
 </tile>
 <tile id="343">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="14"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0156.png"/>
 </tile>
 <tile id="344">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="15"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0157.png"/>
 </tile>
 <tile id="345">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="15"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0158.png"/>
 </tile>
 <tile id="346">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="15"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0159.png"/>
 </tile>
 <tile id="347">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="15"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0160.png"/>
 </tile>
 <tile id="348">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0161.png"/>
 </tile>
 <tile id="349">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0162.png"/>
 </tile>
 <tile id="350">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0163.png"/>
 </tile>
 <tile id="351">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0164.png"/>
 </tile>
 <tile id="352">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0165.png"/>
 </tile>
 <tile id="353">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0166.png"/>
 </tile>
 <tile id="354">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0167.png"/>
 </tile>
 <tile id="355">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0168.png"/>
 </tile>
 <tile id="356">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0169.png"/>
 </tile>
 <tile id="357">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0170.png"/>
 </tile>
 <tile id="358">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0171.png"/>
 </tile>
 <tile id="359">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0172.png"/>
 </tile>
 <tile id="360">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0173.png"/>
 </tile>
 <tile id="361">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="149" source="textures/units/usa/0174.png"/>
 </tile>
 <tile id="362">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0175.png"/>
 </tile>
 <tile id="363">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="16"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0176.png"/>
 </tile>
 <tile id="364">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0177.png"/>
 </tile>
 <tile id="365">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0178.png"/>
 </tile>
 <tile id="366">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0179.png"/>
 </tile>
 <tile id="367">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0180.png"/>
 </tile>
 <tile id="368">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0181.png"/>
 </tile>
 <tile id="369">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0182.png"/>
 </tile>
 <tile id="370">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0183.png"/>
 </tile>
 <tile id="371">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="17"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0184.png"/>
 </tile>
 <tile id="372">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="18"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0185.png"/>
 </tile>
 <tile id="373">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="18"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0186.png"/>
 </tile>
 <tile id="374">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="18"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0187.png"/>
 </tile>
 <tile id="375">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="18"/>
  </properties>
  <image width="256" height="148" source="textures/units/usa/0188.png"/>
 </tile>
 <tile id="376">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="19"/>
  </properties>
  <image width="256" height="161" source="textures/units/usa/0207.png"/>
 </tile>
 <tile id="377">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="19"/>
  </properties>
  <image width="256" height="161" source="textures/units/usa/0208.png"/>
 </tile>
 <tile id="378">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="19"/>
  </properties>
  <image width="256" height="156" source="textures/units/usa/0209.png"/>
 </tile>
 <tile id="379">
  <properties>
   <property name="orientation" value="0"/>
   <property name="player" value="1"/>
   <property name="type" value="19"/>
  </properties>
  <image width="256" height="156" source="textures/units/usa/0210.png"/>
 </tile>
</tileset>
