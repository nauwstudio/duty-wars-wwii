<?xml version="1.0" encoding="UTF-8"?>
<tileset name="building_gai" tilewidth="511" tileheight="448" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="player" type="int" value="0"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="1"/>
  </properties>
  <image width="511" height="436" source="textures/buildings/gai/0001.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="player" type="int" value="0"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="2"/>
  </properties>
  <image width="511" height="359" source="textures/buildings/gai/0002.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="player" type="int" value="0"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="3"/>
  </properties>
  <image width="511" height="320" source="textures/buildings/gai/0003.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="player" type="int" value="0"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="4"/>
  </properties>
  <image width="511" height="418" source="textures/buildings/gai/0004.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="player" type="int" value="0"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="5"/>
  </properties>
  <image width="511" height="376" source="textures/buildings/gai/0005.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="player" value="0"/>
   <property name="subtype" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="448" source="textures/buildings/gai/0006.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="player" value="0"/>
   <property name="subtype" value="2"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="448" source="textures/buildings/gai/0007.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="player" value="0"/>
   <property name="subtype" value="3"/>
   <property name="type" value="1"/>
  </properties>
  <image width="511" height="448" source="textures/buildings/gai/0008.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="player" value="1"/>
   <property name="subtype" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="269" source="textures/buildings/gai/0009.png"/>
 </tile>
</tileset>
