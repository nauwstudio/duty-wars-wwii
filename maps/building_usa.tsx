<?xml version="1.0" encoding="UTF-8"?>
<tileset name="building_usa" tilewidth="256" tileheight="293" tilecount="9" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="player" type="int" value="1"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="1"/>
  </properties>
  <image width="256" height="218" source="textures/buildings/usa/0001.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="player" type="int" value="1"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="2"/>
  </properties>
  <image width="256" height="180" source="textures/buildings/usa/0002.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="player" type="int" value="1"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="3"/>
  </properties>
  <image width="256" height="160" source="textures/buildings/usa/0003.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="player" type="int" value="1"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="4"/>
  </properties>
  <image width="256" height="209" source="textures/buildings/usa/0004.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="player" type="int" value="1"/>
   <property name="subtype" type="int" value="0"/>
   <property name="type" type="int" value="5"/>
  </properties>
  <image width="256" height="186" source="textures/buildings/usa/0005.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="player" value="1"/>
   <property name="subtype" value="1"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="293" source="textures/buildings/usa/0006.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="player" value="1"/>
   <property name="subtype" value="2"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="224" source="textures/buildings/usa/0007.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="player" value="1"/>
   <property name="subtype" value="3"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="198" source="textures/buildings/usa/0008.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="player" value="1"/>
   <property name="subtype" value="4"/>
   <property name="type" value="1"/>
  </properties>
  <image width="256" height="269" source="textures/buildings/usa/0009.png"/>
 </tile>
</tileset>
