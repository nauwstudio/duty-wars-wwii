==========
===SHORT===
==========

Strategy game inspired by World War 2.

==========
===LONG===
==========

Duty Wars - WWII is a turn by turn strategy game inspired by world war 2. In this indie game, you can create and lead troops on the battlefield. The aim of each army is to defeat all its enemies by capturing all its capitals.

Duty Wars - WWII includes two game modes (Versus and Campaign) and a map editor :

� Campaign mode enables you to replay 25 historic battle of the world war 2 all around the world. In these famous battles, you will lead the armies of USA, Soviet Union or Great Britain against Germany or Japan. Once a mission completed, the linked map is available in versus mode.

� Versus mode enables you to play with your friend or with robot up to 5 players on some maps. There is 45 maps available by default, whose 25 are unlocked by completing missions in campaign mode. Furthermore, you can play on the maps you have created with Map Editor.

� Map Editor enables you to create your own map, to play on it in the versus mode and to share them with other players. 

Features :

- Campaign mode of 25 missions.
- Versus mode of up to 5 players.
- Map editor.
- 45 default maps available.
- 5 available armies : USA, Germany, USSR, Japan and Great Britain.
- 18 differents units by army.
- Auto save of your games.
- Up to 5 players on versus mode

If you like this game, please rate it 5 stars :)

Website:  http://www.nauwstudio.be/dutywars/
Facebook: https://www.facebook.com/dutywarswwii/